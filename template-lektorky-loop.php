<div class="lektorka">
	
	<?php if( $prouzek = get_field('prouzek') ) { ?>
	<div class="lektorka-prouzek <?= get_field('zobrazit_prouzek_nahore') ? '' : 'lektorka-prouzek_bot' ?>">
		<?php echo $prouzek ?>
	</div>
	<?php } ?>
		
	<a href="<?php the_permalink() ?>">
		<h2><?php the_title() ?></h2>
		
		<p><img src="<?php echo get_template_directory_uri(); ?>/img/envelope.png" width="18" height="14" alt="envelope"></p>
		
		<p class="email">
			<?php the_field('email') ?>
		</p>
		<p><img src="<?php echo get_template_directory_uri(); ?>/img/phone.png" width="18" height="14" alt="phone"></p>

		<p class="phone">
			<?php the_field('telefonni_cislo') ?>
		</p>
		
	</a>
</div>