<?php get_header(); ?>

<div class="content lektorka-content content-padding cf" id="content">

<h1 class="main-title"><?php _e('Lektorky','jz') ?></h1>

<div class="lektorky-main cf">



<?php get_template_part('template-lektorky-mapa') ?>



<div class="lektorky-prehled cf">
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	
	<?php get_template_part('template-lektorky-loop') ?>
		
<?php endwhile;?>
<?php endif; ?>
</div>

</div>


</div>

<?php get_footer(); ?>
