<?php

include_once __DIR__.'/iDoklad/src/iDoklad.php';
use malcanek\iDoklad\iDoklad;
use malcanek\iDoklad\request\iDokladRequest;
use malcanek\iDoklad\request\iDokladResponse;
use malcanek\iDoklad\auth\iDokladCredentials;
use malcanek\iDoklad\iDokladException;

if( !session_id() )
	session_start();

require_once dirname(__FILE__).'/functions-framework.php';

require_once dirname(__FILE__).'/functions-woocommerce.php';

require_once dirname(__FILE__).'/functions-fio-kontrola-uctu.php';



//--------------------------------------------------------


define( 'JZ_KONTAKTNI_EMAIL', 'info@jemnezrozeni.cz' );

define('PAGE_ID_HYPNOPOROD', ICL_LANGUAGE_CODE == 'cs' ? 792 : 7599 ); // TODO EN?
define('PAGE_ID_KONTAKT', ICL_LANGUAGE_CODE == 'cs' ? 981 : 9418 ); // TODO EN?
define('PAGE_ID_RELAXACNI_HUDBA', ICL_LANGUAGE_CODE == 'cs' ? 44 : 9421 ); // TODO EN?
define('PAGE_ID_OBCHODNI_PODMINKY', ICL_LANGUAGE_CODE == 'cs' ? 1105 : 8082 ); // TODO EN?


define('GMAPS_API_KEY', 'AIzaSyBHV9E2pUJ6Th6_L6HQDGe25QACSqnZfYQ');



define( 'KURZ_ID_1DENNI', ICL_LANGUAGE_CODE == 'cs' ? 526 : 6575 ); // TODO en?
define( 'KURZ_ID_2DENNI', ICL_LANGUAGE_CODE == 'cs' ? 476 : 6325 );
define( 'KURZ_ID_SOUKROMY', ICL_LANGUAGE_CODE == 'cs' ? 538 : 7145 );


define( 'MAX_POCET_STAZENI_MATERIALU', 3 );



// FIO PLATEBNI BRANA
define( 'FIO_CLIENT_URL', 'https://secureshop.firstdata.lv/ecomm/ClientHandler' );
define( 'FIO_SERVER_URL', 'https://secureshop.firstdata.lv:8443/ecomm/MerchantHandler' );
define( 'FIO_CERT_PATH', __DIR__.'/fio/cert/IO160285keystore.pem' );
define( 'FIO_CERT_PASSWORD', 'Qp6SMvC7+' );


// SUPERFAKTURA
define('SF_USEREMAIL', 'michaela@bartvisions.cz');
define('SF_TOKEN', 'ef61dd8a717fc351f81ab565249992dd');
define('SF_COMPANY_ID', '14677');
define('SF_TAX', 21 );
define('SF_TAX_KOEFICIENT', 0.173553719 ); // 21/121



// info o uctech, ktere mohou byt u terminu, a ktere se propisuji do SuperFaktury
$sf_ucty = array(
	'2800835793/2010' => array(
		'nazev' => 'Fio banka',
		'iban' => 'CZ0620100000002800835793',
		'swift' => 'FIOBCZPPXXX'
	)
);


function jz_after_setup_theme() {

	add_theme_support( 'post-thumbnails', array( 'post', 'lektorka', 'pribeh' ) );

	load_theme_textdomain( 'jz', get_template_directory() . '/languages' );

	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'jz_after_setup_theme' );





function menu_a_sidebary() {
	register_nav_menu( 'primary', 'Hlavní navigace' );
}
add_action('init','menu_a_sidebary');







function jz_wp_title( $title ) {

	if( is_front_page() && get_bloginfo('description') ) {
		return get_bloginfo('description');
	}

	return $title;
}
add_filter( 'pre_get_document_title', 'jz_wp_title' );








function jz_scripts_enqueue(){
	wp_enqueue_script( 'fancybox-script', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.pack.js', array( 'jquery' ), false, true );
	wp_enqueue_style( 'fancybox-style', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.css' );
	
	wp_enqueue_script( 'magnific', 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js', array( 'jquery' ), false, true );
	wp_enqueue_style( 'magnific-style', 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css' );

	wp_enqueue_style( 'fonts', 'https://fonts.googleapis.com/css?family=Hind:300,400,500,600,700|Kalam:300,400,700&subset=latin-ext' );

	$filemtime = filemtime( dirname(__FILE__).'/style-woocommerce.css' );
	wp_enqueue_style( 'woocommerce-custom-style', get_template_directory_uri() . '/style-woocommerce.css', array('woocommerce-general'), $filemtime );

	if( is_page_template( 'page-uvodni-stranka.php' ) || is_singular( array('kurz') )  ) {
		wp_enqueue_script( 'caroufredsel', get_bloginfo('template_directory').'/js/caroufredsel/jquery.caroufredsel.js', array('jquery'), null, true);
	}

	if( is_page_template( 'page-tehotenska-kalkulacka.php' ) ) {
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
	}

	if( is_singular( array('lokalita') ) ) {
		wp_enqueue_script( 'google-maps-api', 'https://maps.googleapis.com/maps/api/js?v=3&key='.GMAPS_API_KEY, null, false, true);
	}

	$filemtime = filemtime( dirname(__FILE__).'/js/scripts.js' );
	wp_enqueue_script( 'custom-scripts', get_bloginfo('template_directory').'/js/scripts.js', array('jquery'), $filemtime, true);
}
add_action('wp_enqueue_scripts', 'jz_scripts_enqueue', 11);






function jz_ajaxurl() {
	?>
	<script type="text/javascript">
		var ajaxurl = '<?php echo admin_url('admin-ajax.php') ?>';
	</script>
	<?php
}
add_action( 'wp_head', 'jz_ajaxurl' );










if( function_exists('acf_add_options_page') ) {
	acf_add_options_page( array(
		'page_title' => 'Nastavení JZ',
		'position' => '5.99'
	) );

}





// Clear items that are older than 30 days (i.e. keep only the most recent 30 days in the log)
add_filter( 'simple_history/db_purge_days_interval', function( $days ) {
	$days = 30;
	return $days;
} );
























function jz_acf_google_map_api( $api ){

	$api['key'] = GMAPS_API_KEY;

	return $api;

}
add_filter('acf/fields/google_map/api', 'jz_acf_google_map_api');







// Enable font size & font family selects in the editor

function jz_fontsize_mce_buttons( $buttons ) {
	array_unshift( $buttons, 'fontsizeselect' ); // Add Font Size Select
	return $buttons;
}
add_filter( 'mce_buttons_2', 'jz_fontsize_mce_buttons' );



function jz_fontsize_text_sizes( $initArray ) {

	$initArray['fontsize_formats'] = "10px 11px 12px 13px 14px 16px 18px 22px 26px 30px 36px";

	return $initArray;
}
add_filter( 'tiny_mce_before_init', 'jz_fontsize_text_sizes' );















/*
88888888ba     ,ad8888ba,    88888888ba   88        88  88888888ba
88      "8b   d8"'    `"8b   88      "8b  88        88  88      "8b
88      ,8P  d8'        `8b  88      ,8P  88        88  88      ,8P
88aaaaaa8P'  88          88  88aaaaaa8P'  88        88  88aaaaaa8P'
88""""""'    88          88  88""""""'    88        88  88""""""'
88           Y8,        ,8P  88           88        88  88
88            Y8a.    .a8P   88           Y8a.    .a8P  88
88             `"Y8888Y"'    88            `"Y8888Y"'   88
*/








/*
function jz_popup_v_paticce() {

	$timeout_pro_zobrazeni = 1000;

	?>

	<script>
	jQuery(document).ready(function() {

		if( document.cookie.indexOf("jzpopup=") >= 0 ) {

			// hello again, handsome!

		} else {

			setTimeout( function(){

				jQuery.fancybox.open([
					{
					href : '<?php echo get_stylesheet_directory_uri() ?>/img/popup.jpg'
					}
				]);


				var date = new Date();
				date.setTime(date.getTime() + ( 5 * 24 * 60 * 60 * 1000)); // platne 7 dni
				expires = "; expires=" + date.toGMTString();
				document.cookie = "jzpopup=ano; expires=" + date.toGMTString() + "; path=/";

			} , <?php echo $timeout_pro_zobrazeni ?> );

		}

	});

	</script><?php


}
add_action('wp_footer', 'jz_popup_v_paticce');
*/


























/*
  ,ad8888ba,   88888888ba   888888888888
 d8"'    `"8b  88      "8b       88
d8'            88      ,8P       88
88             88aaaaaa8P'       88
88             88""""""'         88
Y8,            88                88
 Y8a.    .a8P  88                88
  `"Y8888Y"'   88                88
*/





function jz_post_types() {


	$labels = array(
		'name'                => 'Lektorky',
		'singular_name'       => 'Lektorka',
		'menu_name'           => 'Lektorky',
		'parent_item_colon'   => 'Nadřazený záznam:',
		'all_items'           => 'Všechny záznamy',
		'view_item'           => 'Zobrazit záznam',
		'add_new_item'        => 'Přidat záznam',
		'add_new'             => 'Přidat záznam',
		'edit_item'           => 'Upravit záznam',
		'update_item'         => 'Aktualizovat záznam',
		'search_items'        => 'Vyhledat záznam',
		'not_found'           => 'Žádný záznam',
		'not_found_in_trash'  => 'Žádný záznam',
	);
	$args = array(
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail', 'editor' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 29,
		'can_export'          => true,
		'has_archive'         => 'lektorky',
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite'             => array( 'slug' => 'lektorka' ),
		);
	register_post_type( 'lektorka', $args );


	$labels = array(
		'name'                => 'Termíny',
		'singular_name'       => 'Termín',
		'menu_name'           => 'Kurzy a termíny',
		'parent_item_colon'   => 'Nadřazený termín:',
		'all_items'           => 'Přehled termínů',
		'view_item'           => 'Zobrazit termín',
		'add_new_item'        => 'Přidat termín',
		'add_new'             => 'Přidat termín',
		'edit_item'           => 'Upravit termín',
		'update_item'         => 'Aktualizovat termín',
		'search_items'        => 'Vyhledat termín',
		'not_found'           => 'Žádný termín',
		'not_found_in_trash'  => 'Žádný termín',
	);
	$args = array(
		'labels'              => $labels,
		'supports'            => array( 'editor', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 3,
		'can_export'          => true,
		'has_archive'         => 'terminy',
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite'             => array( 'slug' => 'termin',  ),
		);
	register_post_type( 'termin', $args );


	$labels = array(
		'name'                => 'Kurzy',
		'singular_name'       => 'Kurz',
		'menu_name'           => 'Kurzy',
		'parent_item_colon'   => 'Nadřazený kurz:',
		'all_items'           => 'Kurzy',
		'view_item'           => 'Zobrazit kurz',
		'add_new_item'        => 'Přidat kurz',
		'add_new'             => 'Přidat kurz',
		'edit_item'           => 'Upravit kurz',
		'update_item'         => 'Aktualizovat kurz',
		'search_items'        => 'Vyhledat kurz',
		'not_found'           => 'Žádný kurz',
		'not_found_in_trash'  => 'Žádný kurz',
	);
	$args = array(
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail', 'editor', 'page-attributes' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => 'edit.php?post_type=termin',
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 3,
		'can_export'          => true,
		'has_archive'         => 'kurzy',
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite'             => array( 'slug' => 'kurz' ),
		);
	register_post_type( 'kurz', $args );



	$labels = array(
		'name'                => 'Lokality',
		'singular_name'       => 'Lokalita',
		'menu_name'           => 'Lokality',
		'parent_item_colon'   => 'Nadřazený záznam:',
		'all_items'           => 'Lokality',
		'view_item'           => 'Zobrazit záznam',
		'add_new_item'        => 'Přidat záznam',
		'add_new'             => 'Přidat záznam',
		'edit_item'           => 'Upravit záznam',
		'update_item'         => 'Aktualizovat záznam',
		'search_items'        => 'Vyhledat záznam',
		'not_found'           => 'Žádný záznam',
		'not_found_in_trash'  => 'Žádný záznam',
	);
	$args = array(
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => 'edit.php?post_type=termin',
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 3,
		'can_export'          => true,
		'has_archive'         => 'lokality',
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite'             => array( 'slug' => 'lokalita' ),
		);
	register_post_type( 'lokalita', $args );



	$labels = array(
		'name'                => 'Informace',
		'singular_name'       => 'Informace',
		'menu_name'           => 'Informace',
		'parent_item_colon'   => 'Nadřazený záznam:',
		'all_items'           => 'Informace ke kurzům',
		'view_item'           => 'Zobrazit záznam',
		'add_new_item'        => 'Přidat záznam',
		'add_new'             => 'Přidat záznam',
		'edit_item'           => 'Upravit záznam',
		'update_item'         => 'Aktualizovat záznam',
		'search_items'        => 'Vyhledat záznam',
		'not_found'           => 'Žádný záznam',
		'not_found_in_trash'  => 'Žádný záznam',
	);
	$args = array(
		'labels'              => $labels,
		'supports'            => array( 'title',  ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => 'edit.php?post_type=termin',
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 3,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite'             => array( 'slug' => 'informace' ),
		);
	register_post_type( 'informace', $args );




	$labels = array(
		'name'                => 'Přihlášky',
		'singular_name'       => 'Přihláška',
		'menu_name'           => 'Přihlášky',
		'parent_item_colon'   => 'Nadřazený záznam:',
		'all_items'           => 'Přihlášky',
		'view_item'           => 'Zobrazit záznam',
		'add_new_item'        => 'Přidat záznam',
		'add_new'             => 'Přidat záznam',
		'edit_item'           => 'Upravit záznam',
		'update_item'         => 'Aktualizovat záznam',
		'search_items'        => 'Vyhledat záznam',
		'not_found'           => 'Žádný záznam',
		'not_found_in_trash'  => 'Žádný záznam',
	);
	$args = array(
		'labels'              => $labels,
		'supports'            => array( 'title',  ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => 'edit.php?post_type=termin',
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 3,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		//'rewrite'             => array( 'slug' => 'kurz' ),
		);
	register_post_type( 'prihlaska', $args );




	$labels = array(
		'name'                => 'Slevové poukazy',
		'singular_name'       => 'Slevový poukaz',
		'menu_name'           => 'Slevové poukazy',
		'parent_item_colon'   => 'Nadřazený záznam:',
		'all_items'           => 'Slevové poukazy',
		'view_item'           => 'Zobrazit záznam',
		'add_new_item'        => 'Přidat záznam',
		'add_new'             => 'Přidat záznam',
		'edit_item'           => 'Upravit záznam',
		'update_item'         => 'Aktualizovat záznam',
		'search_items'        => 'Vyhledat záznam',
		'not_found'           => 'Žádný záznam',
		'not_found_in_trash'  => 'Žádný záznam',
	);
	$args = array(
		'labels'              => $labels,
		'supports'            => array( 'title',  ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => 'edit.php?post_type=termin',
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 3,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		//'rewrite'             => array( 'slug' => 'kurz' ),
		);
	register_post_type( 'slevovy-poukaz', $args );







	$labels = array(
		'name'                => 'Příběhy',
		'singular_name'       => 'Příběh',
		'menu_name'           => 'Příběhy',
		'parent_item_colon'   => 'Nadřazený záznam:',
		'all_items'           => 'Přehled záznamů',
		'view_item'           => 'Zobrazit záznam',
		'add_new_item'        => 'Přidat záznam',
		'add_new'             => 'Přidat záznam',
		'edit_item'           => 'Upravit záznam',
		'update_item'         => 'Aktualizovat záznam',
		'search_items'        => 'Vyhledat záznam',
		'not_found'           => 'Žádný záznam',
		'not_found_in_trash'  => 'Žádný záznam',
	);
	$args = array(
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail', 'editor', 'author' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 29,
		'can_export'          => true,
		'has_archive'         => 'pribehy',
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite'             => array( 'slug' => 'pribeh' ),
		);
	register_post_type( 'pribeh', $args );





	$labels = array(
		'name'                => 'Reference',
		'singular_name'       => 'Reference',
		'menu_name'           => 'Reference',
		'parent_item_colon'   => 'Nadřazený záznam:',
		'all_items'           => 'Přehled záznamů',
		'view_item'           => 'Zobrazit záznam',
		'add_new_item'        => 'Přidat záznam',
		'add_new'             => 'Přidat záznam',
		'edit_item'           => 'Upravit záznam',
		'update_item'         => 'Aktualizovat záznam',
		'search_items'        => 'Vyhledat záznam',
		'not_found'           => 'Žádný záznam',
		'not_found_in_trash'  => 'Žádný záznam',
	);
	$args = array(
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 29,
		'can_export'          => true,
		'has_archive'         => 'reference',
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite'             => array( 'slug' => 'reference' ),
		);
	register_post_type( 'reference', $args );





	// TAXONOMIE

	/*
	$labels = array(
		'name'                       => 'Typy oborů',
		'singular_name'              => 'Typ oboru',
		'menu_name'                  => 'Typy oborů',
		'all_items'                  => 'Všechny záznamy',
		'parent_item'                => 'Nadřazený záznam',
		'parent_item_colon'          => 'Nadřazený záznam:',
		'new_item_name'              => 'Přidat nový záznam',
		'add_new_item'               => 'Přidat nový záznam',
		'edit_item'                  => 'Upravit záznam',
		'update_item'                => 'Aktualizovat záznam',
		'separate_items_with_commas' => 'Oddělte záznamy čárkami',
		'search_items'               => 'Vyhledat záznam',
		'add_or_remove_items'        => 'Přidat nebo odstranit záznam',
		'choose_from_most_used'      => 'Vyberte z nejpoužívanějších záznamů',
		'not_found'                  => 'Žádný nenalezen',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => false,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
		'rewrite'					=> false
	);
	register_taxonomy( 'typ-oboru', array('obor') , $args );

	register_taxonomy_for_object_type( 'typ-oboru', 'obor' );
	*/
}
add_action( 'init', 'jz_post_types');












// ZAKAZ DETAILOVYCH STRANEK NEKTERYCH CPT

function jz_template_redirect() {

	global $post;

	if( is_singular( array( 'reference', 'prihlaska', 'informace', 'slevovy-poukaz' ) ) ) {

		wp_redirect( home_url() , 301 );

		exit();

	// presmerovani attachmentu na rodice nebo na HP
	} else if ( is_attachment() && isset($post->post_parent) && is_numeric($post->post_parent) && ($post->post_parent != 0) ) {

		wp_redirect(get_permalink($post->post_parent), 301); // permanent redirect to post/page where image or document was uploaded
		exit;

	} elseif ( is_attachment() && isset($post->post_parent) && is_numeric($post->post_parent) && ($post->post_parent < 1) ) {   // for some reason it doesnt works checking for 0, so checking lower than 1 instead...

		wp_redirect( home_url() , 302); // temp redirect to home for image or document not associated to any post/page
		exit;

	}


}
add_action( 'template_redirect', 'jz_template_redirect' );


















/*
88888888ba   88888888ba   88888888888        ,ad8888ba,   88888888888  888888888888      88888888888  88888888ba     ,ad8888ba,    888b      88  888888888888
88      "8b  88      "8b  88                d8"'    `"8b  88                88           88           88      "8b   d8"'    `"8b   8888b     88       88
88      ,8P  88      ,8P  88               d8'            88                88           88           88      ,8P  d8'        `8b  88 `8b    88       88
88aaaaaa8P'  88aaaaaa8P'  88aaaaa          88             88aaaaa           88           88aaaaa      88aaaaaa8P'  88          88  88  `8b   88       88
88""""""'    88""""88'    88"""""          88      88888  88"""""           88           88"""""      88""""88'    88          88  88   `8b  88       88
88           88    `8b    88               Y8,        88  88                88           88           88    `8b    Y8,        ,8P  88    `8b 88       88
88           88     `8b   88                Y8a.    .a88  88                88           88           88     `8b    Y8a.    .a8P   88     `8888       88
88           88      `8b  88888888888        `"Y88888P"   88888888888       88           88           88      `8b    `"Y8888Y"'    88      `888       88
*/






function jz_pre_get_posts( $query ) {

	$post_type = isset($query->query_vars['post_type']) ? $query->query_vars['post_type'] : '';

	if( $query->is_main_query() && $post_type == 'lektorka' && ! is_admin() ) {

		$query->set('posts_per_page', -1);

		if( !empty($_GET['kraj']) ) {

			$query->set('meta_query', array(
				array(
					'key' => 'kraj',
					'value' => $_GET['kraj'],
					'compare' => 'LIKE'
				)
			));

		}

	} else if( $query->is_main_query() && $post_type == 'kurz' ) {

		$query->set( 'posts_per_page', -1 );
		$query->set( 'orderby', 'menu_order' );
		$query->set( 'order', 'ASC' );

	} else if( $post_type == 'termin' && ! is_admin() ) {

		$nova_meta_query = array();

		if( isset( $query->query_vars['meta_query'] ) ) {
			$nova_meta_query = $query->query_vars['meta_query'];
		}

		if( $query->is_main_query() ) {

			$query->set( 'posts_per_page', -1 );

			// main query automaticky nastavim jen na aktualni a budouci terminy
			$nova_meta_query[] = array(
				'key' => 'datum_konani',
				'value' => current_time('Ymd'),
				'compare' => '>='
			);

			if( is_post_type_archive( 'termin' ) ) { // hlavni prehled terminu -> filtrovani

				$nova_meta_query[] = array(
					'key' => 'kurz',
					'value' => KURZ_ID_SOUKROMY, // proste v tom vypisu za zadnou cenu nesmi byt soukrome terminy ...
					'compare' => '!='
				);

				$filter_meta_query = array();

				if( !empty($_GET['filter-course']) ) {

					if($_GET['filter-course'] == 'online'){
						$filter_meta_query[] = array(
							'key' => 'kurz',
							'value' => array(12956, 537, 13035, 6706),
							'operator' => 'IN'
						);
					} else {
						$filter_meta_query[] = array(
							'key' => 'kurz',
							'value' => intval( $_GET['filter-course'] ),
						);
					}
				}
				if( !empty($_GET['filter-locality']) ) {

					$ids = explode(',', $_GET['filter-locality']);
					$ids = array_map('intval', $ids );

					$filter_meta_query[] = array(
						'key' => 'lokalita',
						'value' => $ids,
						'operator' => 'IN'
					);
				}
				if( !empty($_GET['filter-lecturer']) ) {
					$filter_meta_query[] = array(
						'key' => 'lektorka',
						'value' => intval( $_GET['filter-lecturer'] ),
					);
				}

				if( $filter_meta_query ) {
					$nova_meta_query[] = $filter_meta_query;
				}

			}

		}


		if( count( $nova_meta_query ) ) {
			$query->set('meta_query', $nova_meta_query);
		}


		$query->set('meta_key', 'datum_konani');
		$query->set('orderby', 'meta_value_num');
		$query->set('order', 'ASC');


	}
	return $query;
}
add_filter( 'pre_get_posts', 'jz_pre_get_posts');






















































/*
function zvyrazneni_odkazu_na_vypis($classes, $item) {
	$post_type = get_query_var('post_type');

	if($post_type == 'event' && in_array('menu-sportovni-akce',$classes))	{
		$classes[] = 'current-menu-item';
	}

	if($post_type == 'pr' && in_array('menu-media',$classes))	{
		$classes[] = 'current-menu-item';
	}

	if( (is_singular('post') || is_singular('review') ) && in_array('menu-novinky',$classes) ) {
		$classes[] = 'current-menu-item';
	}

	return $classes;
}
add_filter('nav_menu_css_class', 'zvyrazneni_odkazu_na_vypis', 10, 2);
*/









function jz_zmenit_metabox_s_fotografii() {
	remove_meta_box( 'postimagediv', 'lektorka', 'side' );
	add_meta_box('postimagediv', 'Hlavní fotografie lektorky', 'post_thumbnail_meta_box', 'lektorka', 'side', 'low');
}
add_action('do_meta_boxes', 'jz_zmenit_metabox_s_fotografii', 100);



















/*
 ad88888ba          db         88888888ba   88             ,ad8888ba,    888b      88    ,ad8888ba,    8b           d8  88888888888      88888888888    ,ad8888ba,   88888888888
d8"     "8b        d88b        88      "8b  88            d8"'    `"8b   8888b     88   d8"'    `"8b   `8b         d8'  88               88            d8"'    `"8b  88
Y8,               d8'`8b       88      ,8P  88           d8'        `8b  88 `8b    88  d8'        `8b   `8b       d8'   88               88           d8'            88
`Y8aaaaa,        d8'  `8b      88aaaaaa8P'  88           88          88  88  `8b   88  88          88    `8b     d8'    88aaaaa          88aaaaa      88             88aaaaa
  `"""""8b,     d8YaaaaY8b     88""""""8b,  88           88          88  88   `8b  88  88          88     `8b   d8'     88"""""          88"""""      88             88"""""
        `8b    d8""""""""8b    88      `8b  88           Y8,        ,8P  88    `8b 88  Y8,        ,8P      `8b d8'      88               88           Y8,            88
Y8a     a8P   d8'        `8b   88      a8P  88            Y8a.    .a8P   88     `8888   Y8a.    .a8P        `888'       88               88            Y8a.    .a8P  88
 "Y88888P"   d8'          `8b  88888888P"   88888888888    `"Y8888Y"'    88      `888    `"Y8888Y"'          `8'        88888888888      88             `"Y8888Y"'   88888888888
*/














function prepinac_jazyku() {

    $jazyky = icl_get_languages('skip_missing=0');
	if (isset($jazyky)){
		foreach ($jazyky as $jazyk){
			?>
			<a href="<?= $jazyk['url'] ?>" class="lang__item <?= $jazyk['active']?'aktivni':'' ?>"><?= strtoupper($jazyk['code']) ?></a>
			<?php
		}
	}
}













function autor_clanku() {
	$post = get_post();

	$autor = '';

	if( false ) { // lektorka?
		// ...
	} else {
		$autor = 'Jemné Zrození s.r.o.';
	}

	return $autor;
}












function share_buttons() {
	get_template_part( 'template-share-buttons' );
}















/*
88           88888888888  88      a8P   888888888888    ,ad8888ba,    88888888ba   88      a8P   8b        d8
88           88           88    ,88'         88        d8"'    `"8b   88      "8b  88    ,88'     Y8,    ,8P
88           88           88  ,88"           88       d8'        `8b  88      ,8P  88  ,88"        Y8,  ,8P
88           88aaaaa      88,d88'            88       88          88  88aaaaaa8P'  88,d88'          "8aa8"
88           88"""""      8888"88,           88       88          88  88""""88'    8888"88,          `88'
88           88           88P   Y8b          88       Y8,        ,8P  88    `8b    88P   Y8b          88
88           88           88     "88,        88        Y8a.    .a8P   88     `8b   88     "88,        88
88888888888  88888888888  88       Y8b       88         `"Y8888Y"'    88      `8b  88       Y8b       88
*/





function pridani_uzivatelske_role_lektorka() {
	global $wp_roles;

	if ( ! isset( $wp_roles ) )
		$wp_roles = new WP_Roles();

	if( ! isset($wp_roles->role_names['lektorka']) ) {

		$contributor = $wp_roles->get_role('contributor');

		add_role('lektorka', 'Lektorka', $contributor->capabilities );

	}

}
add_action('init','pridani_uzivatelske_role_lektorka');
































// admin bar jen pro adminy
function jz_remove_admin_bar() {

	$user = wp_get_current_user();

	if( $user && ! array_intersect( (array) $user->roles , array('administrator') ) ) {
		show_admin_bar( false );
	}

}
add_action('after_setup_theme', 'jz_remove_admin_bar');


// presmeruju nejnizsi role z admina pryc, nemaji tam co delat

function jz_redirect_non_admin_users() {

	$user = wp_get_current_user();

	if( $user && ! wp_doing_ajax() && ! array_intersect( (array) $user->roles , array('administrator','editor','shop_manager') ) ) {
		wp_redirect( home_url() );
		exit;
	}

}
add_action( 'admin_init', 'jz_redirect_non_admin_users' );























/*
  ,ad8888ba,   88888888888    ,ad8888ba,    88888888ba   88b           d88   ad88888ba
 d8"'    `"8b  88            d8"'    `"8b   88      "8b  888b         d888  d8"     "8b
d8'            88           d8'        `8b  88      ,8P  88`8b       d8'88  Y8,
88             88aaaaa      88          88  88aaaaaa8P'  88 `8b     d8' 88  `Y8aaaaa,
88      88888  88"""""      88          88  88""""88'    88  `8b   d8'  88    `"""""8b,
Y8,        88  88           Y8,        ,8P  88    `8b    88   `8b d8'   88          `8b
 Y8a.    .a88  88            Y8a.    .a8P   88     `8b   88    `888'    88  Y8a     a8P
  `"Y88888P"   88             `"Y8888Y"'    88      `8b  88     `8'     88   "Y88888P"
*/



/*
function grant_gforms_editor_access() {

  $role = get_role( 'editor' );
  $role->add_cap( 'gform_full_access' );

  $role = get_role( 'shop_manager' );
  $role->add_cap( 'gform_full_access' );


}

if( isset($_GET['gformsroles']) )
	add_action( 'init', 'grant_gforms_editor_access' );
*/












/*
88b           d88  88        88          88      88        88    ,ad8888ba,   88888888888  888888888888
888b         d888  88        88          88      88        88   d8"'    `"8b  88                88
88`8b       d8'88  88        88          88      88        88  d8'            88                88
88 `8b     d8' 88  88        88          88      88        88  88             88aaaaa           88
88  `8b   d8'  88  88        88          88      88        88  88             88"""""           88
88   `8b d8'   88  88        88          88      88        88  Y8,            88                88
88    `888'    88  Y8a.    .a8P  88,   ,d88      Y8a.    .a8P   Y8a.    .a8P  88                88
88     `8'     88   `"Y8888Y"'    "Y8888P"        `"Y8888Y"'     `"Y8888Y"'   88888888888       88
*/



/*

*/
function mujucet_custom_user_redirect( $redirect, $user ) {

	if( preg_match('~/termin/[0-9]+/~', $redirect) && isset( $_POST['justloggingin'] ) ) {

		$redirect .= (parse_url($redirect, PHP_URL_QUERY) ? '&' : '?') . 'justlogged=yes';

	}

	return $redirect;
}
add_filter( 'woocommerce_login_redirect', 'mujucet_custom_user_redirect', 10, 2 );




// prihlasovani po presmerovani z prihlasky?
function jz_woocommerce_login_form_end() {

	if( ! empty( $_GET['termin_redirect'] ) ) {
		?>
		<input type="hidden" name="redirect" value="<?php echo esc_attr( $_GET['termin_redirect'] ) ?>">
		<input type="hidden" name="justloggingin" value="yes">
		<?php
	}

}
add_action( 'woocommerce_login_form_end', 'jz_woocommerce_login_form_end' );



























/*
88      a8P          db         88           88      a8P   88        88  88                  db           ,ad8888ba,   88      a8P          db
88    ,88'          d88b        88           88    ,88'    88        88  88                 d88b         d8"'    `"8b  88    ,88'          d88b
88  ,88"           d8'`8b       88           88  ,88"      88        88  88                d8'`8b       d8'            88  ,88"           d8'`8b
88,d88'           d8'  `8b      88           88,d88'       88        88  88               d8'  `8b      88             88,d88'           d8'  `8b
8888"88,         d8YaaaaY8b     88           8888"88,      88        88  88              d8YaaaaY8b     88             8888"88,         d8YaaaaY8b
88P   Y8b       d8""""""""8b    88           88P   Y8b     88        88  88             d8""""""""8b    Y8,            88P   Y8b       d8""""""""8b
88     "88,    d8'        `8b   88           88     "88,   Y8a.    .a8P  88            d8'        `8b    Y8a.    .a8P  88     "88,    d8'        `8b
88       Y8b  d8'          `8b  88888888888  88       Y8b   `"Y8888Y"'   88888888888  d8'          `8b    `"Y8888Y"'   88       Y8b  d8'          `8b
*/



function jz_tehotenska_kalkulacka() {
	$response = array();

	if( ! empty( $_POST['datum-souloze'] ) ) {

		$d = str_replace(' ','', $_POST['datum-souloze']);

		$datum_porodu_time = strtotime( '+266 days', strtotime( $d ) );

	} else if( ! empty( $_POST['prvni-den'] ) && ! empty( $_POST['prumerna-delka-cyklu'] ) ) {

		$d = str_replace(' ','', $_POST['prvni-den']);
		$prumerna_delka_cyklu = intval( $_POST['prumerna-delka-cyklu'] );

		$pricist = ( 28 * 10 ) - ( 28 - $prumerna_delka_cyklu );

		$datum_porodu_time = strtotime( '+'.$pricist.' days', strtotime( $d ) );

	}

	$success = __('Předpokládané datum porodu je','jz').' <b>'.date('j. n. Y', $datum_porodu_time).'</b>';


	if( $datum_porodu_time <= current_time('timestamp') ) { // porod uz probehl.

		$success .= '.';

	} else {

		$success .= __(', a je proto pravděpodobné, že porodíte', 'jz').' ';

		$den_porodu = date('j', $datum_porodu_time);
		$mesic_porodu = date('n', $datum_porodu_time);

		//$success .= ' ('.$mesic_porodu.') ';

		if( $den_porodu <= 5 ) {

			$success .= __('kolem poloviny','jz').' '.mesic_2pad( $mesic_porodu ).' '.__('nebo v druhé polovině','jz').' '.mesic_2pad( $mesic_porodu ).'. ';

		} else if( $den_porodu >= 6 && $den_porodu <= 15 ) {

			$success .= __('v druhé polovině','jz').' '.mesic_2pad( $mesic_porodu ).'. ';

		} else if( $den_porodu >= 16 && $den_porodu <= 31 ) {

			$pristi_mesic = $mesic_porodu == 12 ? 1 : ($mesic_porodu+1);

			$success .= __('v první polovině','jz').' '.mesic_2pad( $pristi_mesic ).'. ';

		}


		$pocet_dnu_do_porodu = ceil( ( $datum_porodu_time - current_time('timestamp') ) / DAY_IN_SECONDS );

		$pocet_dnu_od_ovulace = ( 280 - $pocet_dnu_do_porodu ) ;

		$pocet_tydnu_do_porodu = floor( $pocet_dnu_od_ovulace / 7 );
		$a_jeste_dni = $pocet_dnu_od_ovulace % 7;

		$success .= __('Jste v','jz').' <b>'.$pocet_tydnu_do_porodu .'. '.__('týdnu','jz');

		if( $a_jeste_dni )
			$success .= ' '. sprintf( __('a %s. dni','jz'), $a_jeste_dni );

		$success .= '</b> '.__('těhotenství','jz').' ';

		$success .= ' '.__('a do porodu Vám zbývá cca','jz').' <b>'.sklonovani( $pocet_dnu_do_porodu, __('den','jz'), __('dny','jz'), __('dní','jz') ).'</b>.';

	}

	$response['success'] = $success;

	wp_send_json( $response);
	exit;
}
add_action( 'wp_ajax_tehotenska_kalkulacka', 'jz_tehotenska_kalkulacka' );
add_action( 'wp_ajax_nopriv_tehotenska_kalkulacka', 'jz_tehotenska_kalkulacka' );



function mesic_2pad( $m ) {

	$mesice = array(
		1 => __('ledna','jz'),
		__('února','jz'),
		__('března','jz'),
		__('dubna','jz'),
		__('května','jz'),
		__('června','jz'),
		__('července','jz'),
		__('srpna','jz'),
		__('září','jz'),
		__('října','jz'),
		__('listopadu','jz'),
		__('prosince','jz'),
	);

	return $mesice[ $m ];

}





















/*
88      a8P     ,ad8888ba,    888b      88  888888888888         db         88      a8P   888888888888
88    ,88'     d8"'    `"8b   8888b     88       88             d88b        88    ,88'         88
88  ,88"      d8'        `8b  88 `8b    88       88            d8'`8b       88  ,88"           88
88,d88'       88          88  88  `8b   88       88           d8'  `8b      88,d88'            88
8888"88,      88          88  88   `8b  88       88          d8YaaaaY8b     8888"88,           88
88P   Y8b     Y8,        ,8P  88    `8b 88       88         d8""""""""8b    88P   Y8b          88
88     "88,    Y8a.    .a8P   88     `8888       88        d8'        `8b   88     "88,        88
88       Y8b    `"Y8888Y"'    88      `888       88       d8'          `8b  88       Y8b       88
*/


function odeslani_kontaktniho_formulare() {
	$return = array();

	$prijemce = 'info@jemnezrozeni.cz';

	$_POST = stripslashes_deep($_POST);

	$headers = 'From: '.$_POST['kf-email'].' <'.$prijemce.'>' . "\r\n";
	$headers .= 'Reply-To: <'.$_POST['kf-email'].'>' . "\r\n";

	$lektorka = @intval( $_POST['kf-lektorka'] );

	if( $lektorka && $lektorka_post = get_post( $lektorka ) ) {

		$lektorka = $lektorka_post->post_title;

		$predmet = '['.mb_strtoupper(get_bloginfo('name'), 'utf-8').'] Poptávka soukromého kurzu';

	} else {

		$predmet = '['.mb_strtoupper(get_bloginfo('name'), 'utf-8').'] Email z webových stránek';

	}


	$text = '';

	if( $lektorka ) {

		$text .= 'Poptávka soukromého kurzu s lektorkou '.$lektorka.PHP_EOL.PHP_EOL;

	} else {

		$text .= 'Email z webových stránek:'. PHP_EOL.PHP_EOL;

	}

	$text .= 'Jméno: '.$_POST['kf-jmeno'] . PHP_EOL;
	$text .= 'E-mail: '.$_POST['kf-email']. PHP_EOL;
	$text .= 'Telefon: '.$_POST['kf-telefon']. PHP_EOL;
	$text .= PHP_EOL;
	$text .= $_POST['kf-zprava'];

	if( wp_mail( $prijemce, $predmet, $text, $headers ) ) {
		$return['success'] = __('Vaše zpráva byla úspěšně odeslána!','jz');
	} else {
		$return['error'] = __('Vaši zprávu nyní nelze odeslat. Zkuste to prosím později.','jz');
	}

	wp_send_json( $return ); exit;
}
add_action( 'wp_ajax_jz_kontakt_send','odeslani_kontaktniho_formulare' );
add_action( 'wp_ajax_nopriv_jz_kontakt_send','odeslani_kontaktniho_formulare' );





































/*
88888888ba   88888888ba   88888888888        ,ad8888ba,   88888888888  888888888888             db         88888888ba,    88b           d88  88  888b      88
88      "8b  88      "8b  88                d8"'    `"8b  88                88                 d88b        88      `"8b   888b         d888  88  8888b     88
88      ,8P  88      ,8P  88               d8'            88                88                d8'`8b       88        `8b  88`8b       d8'88  88  88 `8b    88
88aaaaaa8P'  88aaaaaa8P'  88aaaaa          88             88aaaaa           88               d8'  `8b      88         88  88 `8b     d8' 88  88  88  `8b   88
88""""""'    88""""88'    88"""""          88      88888  88"""""           88              d8YaaaaY8b     88         88  88  `8b   d8'  88  88  88   `8b  88
88           88    `8b    88               Y8,        88  88                88             d8""""""""8b    88         8P  88   `8b d8'   88  88  88    `8b 88
88           88     `8b   88                Y8a.    .a88  88                88            d8'        `8b   88      .a8P   88    `888'    88  88  88     `8888
88           88      `8b  88888888888        `"Y88888P"   88888888888       88           d8'          `8b  88888888Y"'    88     `8'     88  88  88      `888
*/





 function jz_admin_pre_get_posts( $query ) {

 	if( is_admin() && ! defined('DOING_AJAX') ) {

 		$post_type = !empty($query->query['post_type']) ? $query->query['post_type'] : '';

 		if( $post_type == 'prihlaska' ) {

 			$query->set('orderby', 'post_date');
 			$query->set('order', 'DESC');

 			if( !empty($_GET['pouze-termin']) ) {
 				$query->set('meta_key', 'prihlaska-termin');
 				$query->set('meta_value',  intval($_GET['pouze-termin']) );
 			}
 		} else if( $post_type == 'termin' ) {

 			$query->set('meta_key', 'datum_konani');
 			$query->set('meta_value', '0');
 			$query->set('meta_compare', '>=');
 			$query->set('meta_type', 'NUMERIC');

 			$query->set('orderby', 'meta_value_num');
 			$query->set('order', 'ASC');

 			if( $query->is_main_query() ) {

 				if( ! isset( $_GET['vsechny-terminy'] ) ) {
 					$query->set('meta_value', date('Ymd', strtotime('-8 days') ) );
 				}

 			}


 		} else if( $post_type == 'informace' ) {

 			// informace "trochu" seradim ... ale jen je-li to hlavni query

 			if( $query->is_main_query() ) {

 				$query->set('meta_query', array(
 					'order_kurz' => array(
 						'key' => 'souvisejici_kurz',
 						'compare' => 'EXISTS',
 					),
 				));

 				$query->set( 'orderby', array(
 					'order_kurz' => 'ASC',
 				) );

 			}



 		}

 	}

 }
add_action( 'pre_get_posts', 'jz_admin_pre_get_posts');





















/*
88             ,ad8888ba,    88      a8P          db         88           88  888888888888         db
88            d8"'    `"8b   88    ,88'          d88b        88           88       88             d88b
88           d8'        `8b  88  ,88"           d8'`8b       88           88       88            d8'`8b
88           88          88  88,d88'           d8'  `8b      88           88       88           d8'  `8b
88           88          88  8888"88,         d8YaaaaY8b     88           88       88          d8YaaaaY8b
88           Y8,        ,8P  88P   Y8b       d8""""""""8b    88           88       88         d8""""""""8b
88            Y8a.    .a8P   88     "88,    d8'        `8b   88           88       88        d8'        `8b
88888888888    `"Y8888Y"'    88       Y8b  d8'          `8b  88888888888  88       88       d8'          `8b
*/


function lokalita_adresa( $lokalita_id, $breaks = false ) {

	$adresa = array();

	$adresa[] = get_post_meta( $lokalita_id, 'adresa_nazev', true);
	$adresa[] = get_post_meta( $lokalita_id, 'adresa_ulice_a_cp', true);
	$adresa[] = get_post_meta( $lokalita_id, 'adresa_mesto', true);
	$adresa[] = get_post_meta( $lokalita_id, 'adresa_psc', true);

	$adresa = array_filter($adresa);

	$join_by = $breaks ? '<br>' : ', ';

	return implode( $join_by , $adresa);

}




































/*
88      a8P   88        88  88888888ba   888888888888  8b        d8
88    ,88'    88        88  88      "8b           ,88   Y8,    ,8P
88  ,88"      88        88  88      ,8P         ,88"     Y8,  ,8P
88,d88'       88        88  88aaaaaa8P'       ,88"        "8aa8"
8888"88,      88        88  88""""88'       ,88"           `88'
88P   Y8b     88        88  88    `8b     ,88"              88
88     "88,   Y8a.    .a8P  88     `8b   88"                88
88       Y8b   `"Y8888Y"'   88      `8b  888888888888       88
*/



function kurz_rozvrh_sloupce( $den ) {

	$lines = explode("\n", $den );

	$pocet_radku = count( $lines );
	$break = ceil( $pocet_radku / 2 );

	$uls = array();
	$li_counter = 1;

	$a = 0;
	foreach( $lines as $line ) {
		$a++;

		$uls[ $li_counter ][] = '<li>'.$line.'</li>';

		if( $a == $break )
			$li_counter++;
	}

	if( isset( $uls[1] ) ) {
		return '<ul class="sl1">'.implode('', $uls[1] ).'</ul><ul class="sl2">'.implode('', $uls[2] ).'</ul>';
	}

}












/*
88      a8P   88        88  88888888ba   888888888888  8b        d8                           db         88888888ba,    88b           d88  88  888b      88
88    ,88'    88        88  88      "8b           ,88   Y8,    ,8P                           d88b        88      `"8b   888b         d888  88  8888b     88
88  ,88"      88        88  88      ,8P         ,88"     Y8,  ,8P                           d8'`8b       88        `8b  88`8b       d8'88  88  88 `8b    88
88,d88'       88        88  88aaaaaa8P'       ,88"        "8aa8"                           d8'  `8b      88         88  88 `8b     d8' 88  88  88  `8b   88
8888"88,      88        88  88""""88'       ,88"           `88'          aaaaaaaa         d8YaaaaY8b     88         88  88  `8b   d8'  88  88  88   `8b  88
88P   Y8b     88        88  88    `8b     ,88"              88           """"""""        d8""""""""8b    88         8P  88   `8b d8'   88  88  88    `8b 88
88     "88,   Y8a.    .a8P  88     `8b   88"                88                          d8'        `8b   88      .a8P   88    `888'    88  88  88     `8888
88       Y8b   `"Y8888Y"'   88      `8b  888888888888       88                         d8'          `8b  88888888Y"'    88     `8'     88  88  88      `888
*/





add_action( 'manage_kurz_posts_columns', 'kurzAdminSloupce' );
add_action( 'manage_kurz_posts_custom_column', 'kurzAdminSloupceContent', 10, 2);
add_action( 'admin_head', 'kurzAdminHead' );

function kurzAdminSloupce( $columns ) {
	$new_columns = array();
	foreach( $columns as $index=>$title ) {
		$new_columns[ $index ] = $title;
		if( $index == 'cb' ) {
			$new_columns[ 'poradi' ] = 'Pořadí';
		}
	}
	return $new_columns;
}

function kurzAdminSloupceContent( $column_name, $post_id ) {
	if( $column_name == 'poradi' ) {
		$post = get_post($post_id);
		echo $post->menu_order;
	}
}

function kurzAdminHead(  ) {
	?>
	<style type="text/css">
		.post-type-kurz .wp-list-table { table-layout: auto;}
		.type-kurz .column-poradi { text-align: left; width:60px !important; overflow:hidden }
	</style>
	<?php
}
























/*
888888888888  88888888888  88888888ba   88b           d88  88  888b      88  8b        d8
     88       88           88      "8b  888b         d888  88  8888b     88   Y8,    ,8P
     88       88           88      ,8P  88`8b       d8'88  88  88 `8b    88    Y8,  ,8P
     88       88aaaaa      88aaaaaa8P'  88 `8b     d8' 88  88  88  `8b   88     "8aa8"
     88       88"""""      88""""88'    88  `8b   d8'  88  88  88   `8b  88      `88'
     88       88           88    `8b    88   `8b d8'   88  88  88    `8b 88       88
     88       88           88     `8b   88    `888'    88  88  88     `8888       88
     88       88888888888  88      `8b  88     `8'     88  88  88      `888       88
*/














/*
	Nastavim post_name a post_title na nejakou volovinu ...
*/
function jz_wp_insert_post_data( $data, $postarr ) {

	if( $data['post_type'] == 'termin' ) {

		$data['post_title'] = md5( $data['post_date'] );
		$data['post_name'] = $data['post_title'];

	}

	return $data;
}
add_action( 'wp_insert_post_data', 'jz_wp_insert_post_data', 10, 2 );





/*
	POST ID jako soucast url -> http://jz.loc/termin/118/
*/

function jz_termin_post_type_link( $url, $post ) {
	if ( 'termin' == get_post_type( $post ) ) {

		$url = str_replace( $post->post_name, $post->ID, $url );

	}
	return $url;
}
add_filter( 'post_type_link', 'jz_termin_post_type_link', 10, 2 );



// TODO en?
function jz_termin_post_type_link_rewrite(){
	add_rewrite_rule('termin/([0-9]+)/?$', 'index.php?post_type=termin&p=$matches[1]', 'top');
}
add_action('init', 'jz_termin_post_type_link_rewrite');










// Termin (get_)the_title ... i v adminu v prehledu

function jz_the_title( $title, $post_id ) {

	if( 'termin' == get_post_type( $post_id ) ) {

		if( is_admin() ) {

			$kurz_id = get_post_meta( $post_id, 'kurz', true );

			if( $kurz_id ) {
				$kurz_post = get_post( $kurz_id );
				return $kurz_post->post_title;
			}

		} else {

			$kurz_id = get_post_meta( $post_id, 'kurz', true );

			if( $kurz_id ) {
				$kurz_post = get_post( $kurz_id );
				return /*__('Termín kurzu','jz').' '.*/ $kurz_post->post_title;
			}

			return __('Termín','jz');
		}

	}

	return $title;

}
add_filter( 'the_title', 'jz_the_title', 10, 2 );



// Termin HTML title

function jz_single_post_title( $post_title, $post ) {

	if( 'termin' == get_post_type( $post ) ) {
		return __('Termín','jz');
	}

	return $post_title;
}
add_filter( 'single_post_title', 'jz_single_post_title', 10, 2 );

















function typ_kurzu_kod( $kurz ) {

	if( is_numeric( $kurz ) ) {
		$post = get_post( $kurz );

		if( 'termin' == get_post_type( $post ) ) { // jejda

			$kurz_id = get_field('kurz', $post->ID);

		} else {

			$kurz_id = $kurz;

		}

	} else {

		$kurz_id = $kurz;

	}

	return get_field('typ', $kurz_id );
}



function typ_kurzu_nazev( $post ) {

	$kod = typ_kurzu_kod( $post );

	$typy = array(
		'1denni' => __('1 denní','jz'),
		'2denni' => __('2 denní','jz'),
		'bebalanced' => __('Be Balanced','jz'),
		'online' => __('Online','jz'),
		'soukromy' => __('Soukromý','jz'),
	);

	return $typy[ $kod ];
}









function termin_datum_konani( $termin_id ) {

	$html = '';

	$datum_konani = get_post_meta( $termin_id, 'datum_konani', true );
	$html .= date('j. n. Y', strtotime( $datum_konani ) );

	$typ_kurzu_kod = typ_kurzu_kod( $termin_id );

    if ($typ_kurzu_kod == '2denni' && ($druhy_den = get_post_meta($termin_id, 'datum_konani_druhy_den', true)) && !get_field('typ_terminu', $termin_id)) {
        $html .= ', ' . date('j. n. Y', strtotime($druhy_den));
    }

	return $html;
}

function termin_cas_konani( $termin_id ) {

	$html = '';

	$cas_konani = get_field('cas_konani', $termin_id);
	$cas_konani_do = get_field('cas_konani_do', $termin_id);
	$html .= $cas_konani . ' - '.$cas_konani_do;

	$typ_kurzu_kod = typ_kurzu_kod( $termin_id );

    if ($typ_kurzu_kod == '2denni' && ($druhy_den = get_post_meta($termin_id, 'datum_konani_druhy_den', true)) && !get_field('typ_terminu', $termin_id)) {
        // ano, kurz se kona i druhy den ... a ma zadany u druhy cas konani?

        $cas_konani_druhy_den = get_post_meta($termin_id, 'cas_konani_druhy_den', true);

        if ($cas_konani_druhy_den) {
            $html .= ', ' . $cas_konani_druhy_den;
        }
    }

	return $html;
}










function termin_id_kurzu( $termin_id ) {

	return get_post_meta( $termin_id, 'kurz', true);

}




function termin_nazev_kurzu( $termin_id ) {

	$kurz_id = termin_id_kurzu( $termin_id );

	$kurz = get_post( $kurz_id );

	return $kurz->post_title;
}






function termin_lokalita( $termin_id ) {

	$lokalita_id = get_post_meta($termin_id, 'lokalita', true);

	$lokalita = get_post( $lokalita_id );

	return $lokalita->post_title;

}

function termin_lokalita_adresa( $termin_id, $breaks = false ) {

	$lokalita_id = get_post_meta($termin_id, 'lokalita', true);

	return lokalita_adresa( $lokalita_id, $breaks );

}


function termin_lokalita_url( $termin_id ) {

	$lokalita_id = get_post_meta($termin_id, 'lokalita', true);

	return get_permalink( $lokalita_id );
}




function termin_lokalita_informace( $termin_id ) {

	$informace_o_lokalite = '';

	$lokalita_id = get_post_meta($termin_id, 'lokalita', true);

	if( $lokalita_id ) {

		$informace_o_lokalite = get_field( 'informace', $lokalita_id );

	}

	return $informace_o_lokalite;
}






function termin_id_lektorky( $termin_id ) {

	return get_post_meta( $termin_id, 'lektorka', true);

}



function termin_lektorka( $termin_id ) {

	$lektorka_id = termin_id_lektorky( $termin_id );

	$lektorka = get_post( $lektorka_id );

	return $lektorka->post_title;
}






function termin_cislo_uctu( $termin_id ) {
	$user_id = get_field('uzivatelsky_ucet_lektorky', get_field('lektorka', $termin_id))['ID'];
	$cislo_uctu = get_user_meta( $user_id, 'bank_account', true );
	
	if($cislo_uctu){
		return $cislo_uctu;
	} else {
		return get_post_meta( $termin_id, 'moznost_platby_bankovni_ucet', true );
	}
	
}


function termin_online_platba( $termin_id ) {

	$povolena = get_post_meta( $termin_id, 'moznost_platby_platebni_brana', true );

	// TODO
	/*
	if( ! current_user_can( 'manage_woocommerce' ) )
		$povolena = false;
	*/

	return $povolena;
}





function termin_vystavit_superfakturu( $termin_id ) {

	//  $kurz_id = termin_id_kurzu( $termin_id );
	if(termin_cislo_uctu( $termin_id ) == '2800835793/2010') {
	    return true;
    } else {
	    return false;
    }

	//return (bool) get_post_meta( $kurz_id, 'vystavit_superfakturu', true );

}

// pokud lektorka má nastaveno FAPI
function lektorka_fapi($termin_id){
	$lektorka_post_id = get_field('lektorka', $termin_id);

	$lektorka_user = get_field( 'uzivatelsky_ucet_lektorky',  $lektorka_post_id);
	
	if (get_user_meta( $lektorka_user['ID'], 'fapi_apiKey', false ) 
		&& get_user_meta( $lektorka_user['ID'], 'fapi_login', false )
		&& get_user_meta( $lektorka_user['ID'], 'fapi_allow', false ) ) {
		return true;
	} else {
		return false;
	}
}

// pokud lektorka má nastaveno iDoklad
function lektorka_iDoklad($termin_id){
	$lektorka_post_id = get_field('lektorka', $termin_id);

	$lektorka_user = get_field( 'uzivatelsky_ucet_lektorky',  $lektorka_post_id);
	
	if (get_user_meta( $lektorka_user['ID'], 'iDoklad_login', false ) 
		&& get_user_meta( $lektorka_user['ID'], 'iDoklad_apiKey', false )) {
		return true;
	} else {
		return false;
	}
}

// pokud lektorka má nastaveno Superfakturu
function lektorka_sf($termin_id){
	$lektorka_post_id = get_field('lektorka', $termin_id);

	$lektorka_user = get_field( 'uzivatelsky_ucet_lektorky',  $lektorka_post_id);
	
	if (get_user_meta( $lektorka_user['ID'], 'sf_apiKey', false ) 
		&& get_user_meta( $lektorka_user['ID'], 'sf_login', false )
		&& get_user_meta( $lektorka_user['ID'], 'sf_company', false )
		&& get_user_meta( $lektorka_user['ID'], 'sf_allow', false ) ) {
		return true;
	} else {
		return false;
	}
}

function lektorka_fapi_credentials($termin_id){
	$lektorka_post_id = get_field('lektorka', $termin_id);

	$lektorka_user = get_field( 'uzivatelsky_ucet_lektorky',  $lektorka_post_id);
	
	return array(
		'login' => get_user_meta( $lektorka_user['ID'], 'fapi_login', 1 ),
		'heslo' => get_user_meta( $lektorka_user['ID'], 'fapi_apiKey', 1 ),
		'vat' => get_user_meta( $lektorka_user['ID'], 'lektorka_vat', 1 ),
	);
}

function lektorka_iDoklad_credentials($termin_id){
	$lektorka_post_id = get_field('lektorka', $termin_id);

	$lektorka_user = get_field( 'uzivatelsky_ucet_lektorky',  $lektorka_post_id);
	
	return array(
		'login' => get_user_meta( $lektorka_user['ID'], 'iDoklad_login', 1 ),
		'heslo' => get_user_meta( $lektorka_user['ID'], 'iDoklad_apiKey', 1 ),
		'vat' => get_user_meta( $lektorka_user['ID'], 'lektorka_vat', 1 ),
	);
}

function lektorka_sf_credentials($termin_id){
	$lektorka_post_id = get_field('lektorka', $termin_id);

	$lektorka_user = get_field( 'uzivatelsky_ucet_lektorky',  $lektorka_post_id);
	
	return array(
		'login' => get_user_meta( $lektorka_user['ID'], 'sf_login', 1 ),
		'heslo' => get_user_meta( $lektorka_user['ID'], 'sf_apiKey', 1 ),
		'company' => get_user_meta( $lektorka_user['ID'], 'sf_company', 1 ),
		'vat' => get_user_meta( $lektorka_user['ID'], 'lektorka_vat', 1 ),
	);
}




function termin_pocet_volnych_mist( $termin_id ) {

	$kapacita = get_post_meta( $termin_id, 'kapacita', true );

	$pocet_platnych_ucastniku = termin_pocet_platnych_ucastniku( $termin_id );
	$volnych_mist = $kapacita - $pocet_platnych_ucastniku;

	if( $volnych_mist < 0 )
		$volnych_mist = 0;

	return $volnych_mist;

}








function termin_volna_mista_html( $termin_id ) {

	$kapacita = get_post_meta( $termin_id, 'kapacita', true );

	if( $kapacita > 0 ) {

		$volnych_mist = termin_pocet_volnych_mist( $termin_id );

	} else {
		$volnych_mist = '-';
	}

	$html = '<span class="termin-volna-mista">
	'.__('Volných míst','jz').'
	<span class="cislo">'.$volnych_mist.'</span>
	</span>';

	return $html;
}












// platne prihlasky odecitajici kapacitu jsou jen ty ve stavu "uhrazeno" a "neuhrazeno" (-> cekani na platbu)
function termin_pocet_platnych_ucastniku( $termin_id ) {

	$pocet_platnych_ucastniku = get_transient( 'prihlasek_na_termin_'.$termin_id );

	if( $pocet_platnych_ucastniku === FALSE ) { // transient neni nastaveny

		$pocet_platnych_ucastniku = 0;

		$prihlasky = get_posts(array(
			'post_type' => 'prihlaska',
			'posts_per_page' => -1,
			'meta_query' => array(
				array(
					'key' => 'prihlaska-termin',
					'value' => $termin_id
				),
				array(
					'key' => 'prihlaska-stav',
					'value' => array( 'uhrazeno', 'neuhrazeno' ),
					'operator' => 'IN'
				),
			),
		));

		if( $prihlasky ) {
			foreach( $prihlasky as $prihlaska ) {

				$typ_prihlasky = get_post_meta( $prihlaska->ID, 'prihlaska-typ', true );

				if( $typ_prihlasky == 'par' ) {
					$pocet_platnych_ucastniku += 2;
				} else {
					$pocet_platnych_ucastniku += 1;
				}

			}
		}

		set_transient( 'prihlasek_na_termin_'.$termin_id, $pocet_platnych_ucastniku, 3600 );
	}

	return intval( $pocet_platnych_ucastniku ); // sichr je sichr
}










function termin_id_zaplacenych_prihlasek( $termin_id ) {

	$zaplacene_prihlasky = get_posts(array(
		'post_type' => 'prihlaska',
		'posts_per_page' => -1,
		'fields' => 'ids',
		'meta_query' => array(
			array(
				'key' => 'prihlaska-termin',
				'value' => $termin_id
			),
			array(
				'key' => 'prihlaska-stav',
				'value' => 'uhrazeno',
			),
		),
	));

	if( $zaplacene_prihlasky ) {
		return $zaplacene_prihlasky;
	}

	return array();
}


















































/*
888888888888  88888888888  88888888ba   88b           d88  88  888b      88  8b        d8             db         88888888ba,    88b           d88  88  888b      88
     88       88           88      "8b  888b         d888  88  8888b     88   Y8,    ,8P             d88b        88      `"8b   888b         d888  88  8888b     88
     88       88           88      ,8P  88`8b       d8'88  88  88 `8b    88    Y8,  ,8P             d8'`8b       88        `8b  88`8b       d8'88  88  88 `8b    88
     88       88aaaaa      88aaaaaa8P'  88 `8b     d8' 88  88  88  `8b   88     "8aa8"             d8'  `8b      88         88  88 `8b     d8' 88  88  88  `8b   88
     88       88"""""      88""""88'    88  `8b   d8'  88  88  88   `8b  88      `88'             d8YaaaaY8b     88         88  88  `8b   d8'  88  88  88   `8b  88
     88       88           88    `8b    88   `8b d8'   88  88  88    `8b 88       88             d8""""""""8b    88         8P  88   `8b d8'   88  88  88    `8b 88
     88       88           88     `8b   88    `888'    88  88  88     `8888       88            d8'        `8b   88      .a8P   88    `888'    88  88  88     `8888
     88       88888888888  88      `8b  88     `8'     88  88  88      `888       88           d8'          `8b  88888888Y"'    88     `8'     88  88  88      `888
*/






function terminAdminFiltrovani( $views ) {
	$views['termin-vsechny-terminy'] = '<a href="'.add_query_arg( 'vsechny-terminy', '1' ).'" class="'.( isset($_GET['vsechny-terminy']) ? 'current' : '' ).'">Všechny termíny</a>';

	$views['all'] = str_replace('Celkem','Aktuální termíny', $views['all']);

	return $views;
}
add_filter( 'views_edit-termin', 'terminAdminFiltrovani' );



add_action( 'manage_termin_posts_columns', 'terminAdminSloupce' );
add_action( 'manage_termin_posts_custom_column', 'terminAdminSloupceContent', 10, 2);
add_action( 'admin_head', 'terminAdminHead' );

function terminAdminSloupce( $columns ) {
	$new_columns = array();
	foreach( $columns as $index=>$title ) {

		if( $index == 'date' ) continue;

		$new_columns[ $index ] = $title;

		if( $index == 'cb' ) {
			$new_columns[ 'datum_konani' ] = 'Datum konání';
		}

		if( $index == 'title' ) {
			$new_columns[ 'lokalita' ] = 'Lokalita';
			$new_columns[ 'lektorka' ] = 'Lektorka';
			$new_columns[ 'kapacita' ] = 'Kapacita';
			$new_columns[ 'prihlasky' ] = 'Přihlášky';
		}

	}
	return $new_columns;
}

function terminAdminSloupceContent( $column_name, $termin_id ) {

	static $pocty_prihlasek = null;

	if( is_null($pocty_prihlasek) ) {
		$pocty_prihlasek = spocitat_pocty_prihlasek();
	}

	if( $column_name == 'datum_konani' ) {

		$datum_konani = termin_datum_konani( $termin_id );
		$cas_konani = termin_cas_konani( $termin_id );
        $typ = get_field('typ_terminu', $termin_id);

		echo $datum_konani;

		echo '<br><small>('.$cas_konani.')</small>';
        if($typ == 'viceden') {
            echo '<br><small>Vícedenní</small>';
        }

	} else if( $column_name == 'kapacita' ) {

		$platnych_ucastniku = termin_pocet_platnych_ucastniku( $termin_id );

		$kapacita = get_post_meta( $termin_id, 'kapacita', true );
		echo $platnych_ucastniku.'/'.$kapacita;

	} else if( $column_name == 'lokalita' ) {

		echo termin_lokalita( $termin_id );

	} else if( $column_name == 'lektorka' ) {

		$lektorka_id = get_post_meta( $termin_id, 'lektorka', true );
		$lektorka_post = get_post( $lektorka_id );
		echo $lektorka_post->post_title;

	} else if( $column_name == 'prihlasky' ) {

		$pocet_text = '';

		if( isset($pocty_prihlasek[ $termin_id ]) ) {
			$pocet_uhrazenych = $pocty_prihlasek[ $termin_id ][ 'uhrazeno' ];
			$pocet_neuhrazenych = $pocty_prihlasek[ $termin_id ][ 'neuhrazeno' ];
			$pocet_stornovanych = $pocty_prihlasek[ $termin_id ][ 'stornovano' ];
			$pocet_odmitnutych = $pocty_prihlasek[ $termin_id ][ 'odmitnuto' ];
			$pocet_kontrolovanych = $pocty_prihlasek[ $termin_id ][ 'kontrolovano' ];
			$pocet_neznamych = $pocty_prihlasek[ $termin_id ][ 'nevim' ];

			if( $pocet_uhrazenych ) {
				$pocet_text .= '<span class="pocet-uhrazenych" title="Počet uhrazených přihlášek">'.$pocet_uhrazenych.'</span> ';
			}
			if( $pocet_neuhrazenych ) {
				$pocet_text .= '<span class="pocet-neuhrazenych" title="Počet neuhrazených přihlášek">'.$pocet_neuhrazenych.'</span> ';
			}
			if( $pocet_stornovanych ) {
				$pocet_text .= '<span class="pocet-stornovanych" title="Počet stornovaných přihlášek">'.$pocet_stornovanych.'</span> ';
			}
			if( $pocet_odmitnutych ) {
				$pocet_text .= '<span class="pocet-odmitnutych" title="Počet odmítnutých přihlášek (na Fio bráně)">'.$pocet_odmitnutych.'</span> ';
			}
			if( $pocet_kontrolovanych ) {
				$pocet_text .= '<span class="pocet-kontrolovanych" title="Počet přihlášek ke kontrole">'.$pocet_kontrolovanych.'</span> ';
			}
			if( $pocet_neznamych ) {
				$pocet_text .= '<span class="pocet-neznamych" title="Počet přihlášek s neznámým stavem">'.$pocet_neznamych.'</span> ';
			}

		} else {
			$pocet_text = '<span class="pocet-zadne" title="Termín nemá zatím žádnou přihlášku">0</span>';
		}


		echo '<a href="edit.php?post_type=prihlaska&amp;pouze-termin='.$termin_id.'" class="pocet-objednavek">'.$pocet_text.'</a>';


	}

}

function terminAdminHead(  ) {
	?>
	<style type="text/css">
		.post-type-termin .wp-list-table { table-layout: auto;}
		.type-termin .column-poradi { text-align: left; width: 60px !important; overflow:hidden }

		body.post-type-termin a.pocet-objednavek span.pocet-uhrazenych { padding: 3px 8px; background: green; color: #fff; font-weight: bold; }
		body.post-type-termin a.pocet-objednavek span.pocet-neuhrazenych { padding: 3px 8px; background: gray; color: #fff; font-weight: bold; }
		body.post-type-termin a.pocet-objednavek span.pocet-stornovanych { padding: 3px 8px; background: orange; color: #fff; font-weight: bold; }
		body.post-type-termin a.pocet-objednavek span.pocet-odmitnutych { padding: 3px 8px; background: red; color: #fff; font-weight: bold; }
		body.post-type-termin a.pocet-objednavek span.pocet-neznamych { padding: 3px 8px; background: silver; color: #fff; font-weight: bold; }
		body.post-type-termin a.pocet-objednavek span.pocet-kontrolovanych { padding: 3px 8px; background: navy; color: #fff; font-weight: bold; }
		body.post-type-termin a.pocet-objednavek span.pocet-zadne { padding: 3px 8px; background: #fff; color: #000; font-weight: bold; }

		.table-termin td, .table-objednavka td {padding: 3px 20px 3px 5px}

	</style>
	<?php
}









function spocitat_pocty_prihlasek() {
	global $wpdb;

	$prihlasky = array( 0 => 1 );

	$platne_prihlasky = get_posts( array(
		'post_type' => 'prihlaska',
		'meta_key' => 'prihlaska-termin',
		'meta_value_num' => 0,
		'meta_compare' => '>',
		'posts_per_page' => -1,
		'fields' => 'ids'
	) );

	if( ! $platne_prihlasky )
		return $prihlasky;

	$platne_prihlasky = array_flip($platne_prihlasky);

	$prihlasky_k_terminum = array();
	$prihlasky_post_ids = array();
	$prihlasky_stavy = array();

	$post_ids = $wpdb->get_results( "SELECT post_id,meta_value FROM $wpdb->postmeta WHERE meta_key='prihlaska-termin' AND meta_value > 0" );

	foreach( $post_ids as $meta ) {

		if( !isset($platne_prihlasky[ $meta->post_id ]) ) continue;

		$prihlasky_k_terminum[ $meta->post_id ] = $meta->meta_value;
		$prihlasky_post_ids[] = $meta->post_id;
	}

	$metas = $wpdb->get_results( "SELECT post_id,meta_key,meta_value
									FROM $wpdb->postmeta 
									WHERE meta_key='prihlaska-stav'
											AND post_id IN (".implode(',',$prihlasky_post_ids).")" );

	foreach($metas as $meta) {
		$prihlaska_id = $meta->post_id; // post_id id ID prihlasky

		$prihlasky_stavy[ $prihlaska_id ] = $meta->meta_value;
	}


	foreach( $prihlasky_stavy as $prihlaska_id => $stav ) {

		$termin_id = $prihlasky_k_terminum[ $prihlaska_id ];

		if( !isset($prihlasky[ $termin_id ])) {
			$prihlasky[ $termin_id ] = array(
				'uhrazeno' => 0,
				'neuhrazeno' => 0,
				'stornovano' => 0,
				'odmitnuto' => 0,
				'kontrolovano' => 0,
				'nevim' => 0,
			);
		}



		if( $stav == 'stornovano' )
			$prihlasky[ $termin_id ]['stornovano'] += 1;
		else if( $stav == 'odmitnuto' )
			$prihlasky[ $termin_id ]['odmitnuto'] += 1;
		else if( $stav == 'uhrazeno' )
			$prihlasky[ $termin_id ]['uhrazeno'] += 1;
		else if( $stav == 'neuhrazeno' )
			$prihlasky[ $termin_id ]['neuhrazeno'] += 1;
		else if( $stav == 'manual' )
			$prihlasky[ $termin_id ]['kontrolovano'] += 1;
		else
			$prihlasky[ $termin_id ]['nevim'] += 1;
	}

	//print_r($prihlasky);exit;

	unset($platne_prihlasky);
	unset($prihlasky_k_terminum);
	unset($prihlasky_post_ids);
	unset($prihlasky_stavy);

	return $prihlasky;
}









// meta box s prihlaskami v detailu terminu


function termin_registrace_metaboxu() {

	add_meta_box(
		'metabox-termin-prihlasky',
		'Přihlášky na tento termín',
		'termin_prihlasky_metabox',
		'termin',
		'side',
		'default'
	);

	add_meta_box(
		'metabox-termin-rozesilky',
		'Automatické rozesílky informací',
		'termin_rozesilky_metabox',
		'termin',
		'side',
		'default'
	);

}
add_action('add_meta_boxes', 'termin_registrace_metaboxu');










function termin_prihlasky_metabox( $termin ) {

	$termin_id = $termin->ID;

	$prihlasky = get_posts(array(
		'post_type' => 'prihlaska',
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key' => 'prihlaska-termin',
				'value' => $termin_id
			)
		)
	));
	if( $prihlasky ) {

		?>
		<p>
			<a href="<?php echo admin_url('edit.php?post_type=prihlaska&pouze-termin='.$termin_id ) ?>">Všechny přihlášky</a>
			|
			<a href="<?php echo admin_url('?jz-export-prihlasek=' . $termin_id ) ?>" target="_blank">Export do Excelu</a>
		</p>
		<?php

		foreach( $prihlasky as $prihlaska ) {

			$jmeno = get_post_meta( $prihlaska->ID, 'prihlaska-jmeno', true ).' '.get_post_meta( $prihlaska->ID, 'prihlaska-prijmeni', true );
			$stav = prihlaska_stav_vizualne( $prihlaska->ID );
			$typ = get_post_meta( $prihlaska->ID, 'prihlaska-typ', true );
			$s_kym = get_post_meta( $prihlaska->ID, 'prihlaska-par-upresneni', true );
			$vicercata = get_post_meta( $prihlaska->ID, 'prihlaska-vicercata', true );
			$poznamka = get_post_meta( $prihlaska->ID, 'prihlaska-poznamka', true );
			$interni_poznamka = get_post_meta( $prihlaska->ID, 'prihlaska-interni-poznamka', true );


			if( $typ == 'par' )
				$jmeno .= ' <span title="Zúčastní se v páru ('.htmlspecialchars($s_kym).')">(2)</span>';
			else
				$jmeno .= ' <span title="Zúčastní se sama">(1)</span>';

			if( $vicercata == 'ano' )
				$jmeno .= ' <span style="color:red" title="Čeká vícerčata">(V)</span>';

			if( $poznamka ) {
				$poznamka = esc_attr( mb_substr( trim( $poznamka) , 0, 100, 'utf-8' ) );
				$jmeno .= ' <span style="color: blue" title="Poznámka k přihlášce: '.$poznamka.'">(P)</span>';
			}

			if( $interni_poznamka ) {
				$interni_poznamka = esc_attr( mb_substr( trim( $interni_poznamka) , 0, 100, 'utf-8' ) );
				$jmeno .= ' <span style="color:orange" title="Interní poznámka: '.$interni_poznamka.'">(IP)</span>';
			}




			?>
			<p>
				<strong><?php echo $jmeno ?></strong>
				&nbsp; &nbsp;
				<small><?php echo $stav ?></small>
				<br>
				(<?php echo date('j. n. Y H:i', strtotime( $prihlaska->post_date ) )  ?>)

				<small><a href="<?php echo admin_url('post.php?post='.$prihlaska->ID.'&action=edit') ?>">detail</a></small>
			</p>
			<?php

		}
	} else {
		?>
		<p><em>Zatím žádné přihlášky.</em></p>
		<?php
	}

}





function termin_rozesilky_metabox( $termin ) {

	?>
	<p><strong>Automatické informování o zveřejnění informací v zákaznickém účtu.</strong></p>
	<?php

	$termin_id = $termin->ID;

	$rozesilky = get_post_meta( $termin_id, 'probehle_rozesilky', true );

	if( ! $rozesilky ) {

		?>
		<p><em>Zatím žádné neproběhlo.</em></p>
		<?php

		return;
	}

	foreach( $rozesilky as $informace_id => $data ) {

		$datum = date('j. n. Y H:i', strtotime($data['datum']));
		$informace = $data['informace'];

		?>
		<p style="margin-bottom: 25px">
			<strong><?php echo $datum ?></strong><br>
			<em><?php echo $informace ?></em> <br>
			<em><small><a href="<?php echo admin_url('post.php?post='.$informace_id.'&action=edit') ?>">(<?php echo get_post_field( 'post_title', $informace_id ) ?>)</a></small></em>
		</p>
		<?php

	}

}













// Pridam URL do editace terminu, at se z nej da prokliknout primo na detail / prihlasku

function jz_termin_edit_form_link() {
    $screen = get_current_screen();

    if ( $screen->base == 'post' && $screen->id == 'termin' && $screen->action != 'add' ) {

        global $post;
    	?>
    	<div class="inside">
    		<div id="edit-slug-box">
    			<strong>URL adresa přihlášky:</strong>
    			<a href="<?php echo get_permalink( $post->ID ) ?>"><?php echo get_permalink( $post->ID ) ?></a>
    		</div>
    	</div>
    	<?php
    }
}

add_action( 'edit_form_after_title', 'jz_termin_edit_form_link' );






















/*
888888888888  88888888888  88888888ba   88b           d88  88  888b      88  8b        d8      88888888888  8b        d8  88888888ba     ,ad8888ba,    88888888ba   888888888888
     88       88           88      "8b  888b         d888  88  8888b     88   Y8,    ,8P       88            Y8,    ,8P   88      "8b   d8"'    `"8b   88      "8b       88
     88       88           88      ,8P  88`8b       d8'88  88  88 `8b    88    Y8,  ,8P        88             `8b  d8'    88      ,8P  d8'        `8b  88      ,8P       88
     88       88aaaaa      88aaaaaa8P'  88 `8b     d8' 88  88  88  `8b   88     "8aa8"         88aaaaa          Y88P      88aaaaaa8P'  88          88  88aaaaaa8P'       88
     88       88"""""      88""""88'    88  `8b   d8'  88  88  88   `8b  88      `88'          88"""""          d88b      88""""""'    88          88  88""""88'         88
     88       88           88    `8b    88   `8b d8'   88  88  88    `8b 88       88           88             ,8P  Y8,    88           Y8,        ,8P  88    `8b         88
     88       88           88     `8b   88    `888'    88  88  88     `8888       88           88            d8'    `8b   88            Y8a.    .a8P   88     `8b        88
     88       88888888888  88      `8b  88     `8'     88  88  88      `888       88           88888888888  8P        Y8  88             `"Y8888Y"'    88      `8b       88
*/







function jz_export_terminu_do_ics_odkaz() {

	$screen = get_current_screen();

	if( $screen->id == 'edit-termin' ) {

		?>
		<script>
			jQuery( function(){
				jQuery('.page-title-action').after('<a href="<?php echo admin_url('?jz-export-terminu=1') ?>" target="_blank" id="exportdokalendare" class="page-title-action">Export termínů do ICS souboru</a>');
			});
		</script>
		<?php
	}
}
add_action( 'admin_footer', 'jz_export_terminu_do_ics_odkaz' );










function jz_export_terminu_do_ics() {

	$terminy = get_posts( array(
		'post_type' => 'termin',
		'posts_per_page' => -1,
		'meta_key' => 'datum_konani',
		'order' => 'ASC',
		'orderby' => 'meta_value_num',
		'meta_query' => array(
			array(
				'key' => 'datum_konani',
				'value' => current_time( 'Ymd' ),
				'compare' => '>=',
				'type' => 'NUMERIC',
			)
		)
	) );

	if( ! $terminy ) {
		wp_die('Žádné termíny nejsou uloženy!');
	}

	header('Content-type: text/calendar');
	header('Content-Disposition: attachment; filename="export-terminu.ics"');

	echo generator_ics_kodu( $terminy );

	exit;

}

if( isset( $_GET['jz-export-terminu'] ) )
	add_action( 'admin_init', 'jz_export_terminu_do_ics' );








function generator_ics_kodu( $terminy ){ // array of WP_Post

	$code = '';

	$puvodni_timezone = date_default_timezone_get();
	date_default_timezone_set('Europe/Prague');


	$code .= "BEGIN:VCALENDAR".PHP_EOL;
	$code .= "VERSION:2.0".PHP_EOL;
	$code .= "PRODID:-//hacksw/handcal//NONSGML v1.0//EN".PHP_EOL;
	$code .= "CALSCALE:GREGORIAN".PHP_EOL;

	foreach( $terminy as $termin ) {

		$termin_id = $termin->ID;

		$adresa_do_ics = termin_lokalita_adresa( $termin_id );

		$nazev_a_lektorka = termin_nazev_kurzu( $termin_id ) ;
		$nazev_a_lektorka .= ', '. termin_lektorka( $termin_id );

		$typ_kurzu_kod = typ_kurzu_kod( $termin_id );

		$dvoudenni = false;


		$shrnuti = $nazev_a_lektorka;

		if( $typ_kurzu_kod == '2denni' && ( $druhy_den = get_post_meta( $termin_id, 'datum_konani_druhy_den', true ) ) ) {

			$dvoudenni = true;

			$shrnuti .= ' (1. den)';

		}


		$datum_konani = get_post_meta( $termin_id, 'datum_konani', true );
		$cas_konani = get_post_meta( $termin_id, 'cas_konani', true );

		$cas_start = jz_calendar_cas_start( $cas_konani );
		$cas_end = jz_calendar_cas_end( $cas_konani );

		$datum_start = date('Y-m-d',strtotime($datum_konani)) . ( $cas_start ? ' '. $cas_start : '' );
		$datum_end = date('Y-m-d',strtotime($datum_konani)) . ( $cas_end ? ' '. $cas_end : '' );

		$uid = 'TERMIN'.$termin_id.'A';

		$code .= "BEGIN:VEVENT".PHP_EOL;
		$code .= "DTSTART:". jz_calendar_date($datum_start) . PHP_EOL;
		$code .= "DTEND:". jz_calendar_date($datum_end) . PHP_EOL;
		$code .= "UID:". $uid . PHP_EOL;
		$code .= "DTSTAMP:". jz_calendar_date( current_time('mysql') ) . PHP_EOL;
		$code .= "LOCATION:". jz_calendar_escape_string($adresa_do_ics) . PHP_EOL;
		$code .= "URL;VALUE=URI:". jz_calendar_escape_string( get_the_permalink( $termin->ID ) ) . PHP_EOL;
		$code .= "SUMMARY:". jz_calendar_escape_string($shrnuti) . PHP_EOL;
		$code .= "END:VEVENT".PHP_EOL;



		// je, li to dvoudenni, tak vlozim ten samy den

		if( $dvoudenni ) {

			$datum_konani = $druhy_den;
			$cas_konani = get_post_meta( $termin_id, 'cas_konani_druhy_den', true );

			$cas_start = jz_calendar_cas_start( $cas_konani );
			$cas_end = jz_calendar_cas_end( $cas_konani );

			$datum_start = date('Y-m-d',strtotime($datum_konani)) . ( $cas_start ? ' '. $cas_start : '');
			$datum_end = date('Y-m-d',strtotime($datum_konani)) . ( $cas_end ? ' '. $cas_end : '' );

			$shrnuti = $nazev_a_lektorka . ' (2. den)';

			$uid = 'TERMIN'.$termin_id.'B';

			$code .= "BEGIN:VEVENT".PHP_EOL;
			$code .= "DTSTART:". jz_calendar_date($datum_start) . PHP_EOL;
			$code .= "DTEND:". jz_calendar_date($datum_end) . PHP_EOL;
			$code .= "UID:".$uid . PHP_EOL;
			$code .= "DTSTAMP:". jz_calendar_date( current_time('mysql') ) . PHP_EOL;
			$code .= "LOCATION:". jz_calendar_escape_string($adresa_do_ics) . PHP_EOL;
			$code .= "URL;VALUE=URI:". jz_calendar_escape_string( get_the_permalink( $termin->ID ) ) . PHP_EOL;
			$code .= "SUMMARY:". jz_calendar_escape_string($shrnuti) . PHP_EOL;
			$code .= "END:VEVENT".PHP_EOL;

		}

	}

	$code .= "END:VCALENDAR".PHP_EOL;

	date_default_timezone_set( $puvodni_timezone );

	return $code;
}










function jz_calendar_cas_start( $cas ) { // 9.00-17.00 hod

	$cas = str_replace('hod','',$cas);

	$temp = explode( '-', trim($cas) );

	$cas = str_replace('.',':', $temp[0] );

	return trim($cas);
}


function jz_calendar_cas_end( $cas ) { // 9.00-17.00 hod

	$cas = str_replace('hod','',$cas);

	$temp = explode( '-', trim($cas) );

	$cas = str_replace('.',':', $temp[1] );

	return trim($cas);
}




function jz_calendar_date($date) {



	$timestamp = strtotime( $date );

	return gmdate('Ymd\THis\Z', $timestamp);

}
// Escapes a string of characters
function jz_calendar_escape_string($string) {
  return preg_replace('/([\,;])/','\\\$1', $string);
}













/*
88    ,ad8888ba,    ad88888ba           88      888888888888  88888888888  88888888ba   88b           d88  88  888b      88  88        88
88   d8"'    `"8b  d8"     "8b        ,d88           88       88           88      "8b  888b         d888  88  8888b     88  88        88
88  d8'            Y8,              888888           88       88           88      ,8P  88`8b       d8'88  88  88 `8b    88  88        88
88  88             `Y8aaaaa,            88           88       88aaaaa      88aaaaaa8P'  88 `8b     d8' 88  88  88  `8b   88  88        88
88  88               `"""""8b,          88           88       88"""""      88""""88'    88  `8b   d8'  88  88  88   `8b  88  88        88
88  Y8,                    `8b          88           88       88           88    `8b    88   `8b d8'   88  88  88    `8b 88  88        88
88   Y8a.    .a8P  Y8a     a8P          88           88       88           88     `8b   88    `888'    88  88  88     `8888  Y8a.    .a8P
88    `"Y8888Y"'    "Y88888P"           88           88       88888888888  88      `8b  88     `8'     88  88  88      `888   `"Y8888Y"'
*/





function jeden_termin_do_ics() {

	$termin_id = intval( $_GET['termin-do-ics'] );

	$termin = get_post( $termin_id );

	if( ! $termin ) {
		wp_die( __('Je nám líto, ICS soubor nelze vytvořit.','jz') );
	}

	$terminy = array( $termin )	;

	$filename = sanitize_title( termin_nazev_kurzu( $termin_id )  ).'.ics';

	header('Content-type: text/calendar');
	header('Content-Disposition: attachment; filename="'.$filename.'"');

	echo generator_ics_kodu( $terminy );

	exit;

}
if( isset( $_GET['termin-do-ics'] ) )
	add_action( 'init', 'jeden_termin_do_ics' );


































/*
88b           d88  88888888888  888b      88         db
888b         d888  88           8888b     88        d88b
88`8b       d8'88  88           88 `8b    88       d8'`8b
88 `8b     d8' 88  88aaaaa      88  `8b   88      d8'  `8b
88  `8b   d8'  88  88"""""      88   `8b  88     d8YaaaaY8b
88   `8b d8'   88  88           88    `8b 88    d8""""""""8b
88    `888'    88  88           88     `8888   d8'        `8b
88     `8'     88  88888888888  88      `888  d8'          `8b
*/




/*
	Nastavnim menu do COOKIE.
*/
function jz_set_currency() {

	if( $_GET['set-currency'] != 'czk' && $_GET['set-currency'] != 'eur' )
		return; // fuck off

	setcookie( 'jz-currency', $_GET['set-currency'], current_time('timestamp') + YEAR_IN_SECONDS, '/' );

	// url byla /neconeco/?set-currency=czk
	$back_temp = explode('?', $_SERVER['REQUEST_URI']);

	wp_redirect( $back_temp[0] ); exit;
}

if( isset($_GET['set-currency']) )
	add_action( 'init', 'jz_set_currency' );







function formatovana_cena( $cislo, $mena ) {

	if( $cislo == '')
		$cislo = 0;

	return number_format($cislo, 0, '.', ' ') . ' '.jz_mena_znacka( $mena ) ;

}





function jz_aktualni_mena() {

	if( jz_aktualni_mena_kod() == 'czk' )
		return 'Kč';
	else
		return '€';
}

function jz_aktualni_mena_kod() {

	if( isset($_COOKIE['jz-currency']) && ( $_COOKIE['jz-currency'] == 'czk' || $_COOKIE['jz-currency'] == 'eur' ) )
		return $_COOKIE['jz-currency'];

	return 'czk';

}

function jz_mena_znacka( $mena ) {

	$znacky = array(
		'czk' => 'Kč',
		'eur' => '€',
	);

	return $znacky[ $mena ];
}





function termin_cena_zvoleneho_typu( $typ, $termin_id, $kod_meny = '' ) {

	if( ! $kod_meny ) {
		$kod_meny = jz_aktualni_mena_kod();
	}

	return get_post_meta( $termin_id, 'cena_'.$typ.'_'.$kod_meny, true);
}


function termin_akcni_cena_zvoleneho_typu( $typ, $termin_id, $kod_meny = '' ) {

	if( ! $kod_meny ) {
		$kod_meny = jz_aktualni_mena_kod();
	}

	return get_post_meta( $termin_id, 'akcni_cena_'.$typ.'_'.$kod_meny, true);
}

function termin_akcni_cena_zvoleneho_typu_poznamka( $typ, $termin_id ) {

	return get_post_meta( $termin_id, 'akcni_cena_'.$typ.'_poznamka', true);
}


































































/*
88888888ba   88888888ba   88  88        88  88                  db          ad88888ba   88      a8P          db
88      "8b  88      "8b  88  88        88  88                 d88b        d8"     "8b  88    ,88'          d88b
88      ,8P  88      ,8P  88  88        88  88                d8'`8b       Y8,          88  ,88"           d8'`8b
88aaaaaa8P'  88aaaaaa8P'  88  88aaaaaaaa88  88               d8'  `8b      `Y8aaaaa,    88,d88'           d8'  `8b
88""""""'    88""""88'    88  88""""""""88  88              d8YaaaaY8b       `"""""8b,  8888"88,         d8YaaaaY8b
88           88    `8b    88  88        88  88             d8""""""""8b            `8b  88P   Y8b       d8""""""""8b
88           88     `8b   88  88        88  88            d8'        `8b   Y8a     a8P  88     "88,    d8'        `8b
88           88      `8b  88  88        88  88888888888  d8'          `8b   "Y88888P"   88       Y8b  d8'          `8b
*/






// fce bud vraci VS prihlasky, a nebo ji ho take prvotne nastavuje

function prihlaska_vs( $prihlaska_id, $vytvorit = false ) {

	if( $vytvorit ) {

		// TODO? nejaka posloupna rada?

		$vs = $prihlaska_id;

		update_post_meta( $prihlaska_id, 'prihlaska-vs', $vs );

		return $vs;

	} else {

		$vs = get_post_meta( $prihlaska_id, 'prihlaska-vs', true );

		if( $vs ) {
			return $vs;
		}

	}

	return $prihlaska_id; // fallback
}








function prihlaska_id_terminu( $prihlaska_id ) {
	return get_post_meta( $prihlaska_id, 'prihlaska-termin', true );
}













function tabulka_s_podrobnostmi_o_terminu($termin_id)
{

    $termin = get_post($termin_id);

    $kurz_id = get_post_meta($termin_id, 'kurz', true);
    $kurz = get_post($kurz_id);

    $typ = get_post_meta($termin_id, 'typ', true);

    if ($_POST['prihlaska-zmenit-termin'] && $_POST['tid_change']) {
		$novy_termin = intval($_POST['prihlaska-zmenit-termin']);
		$novy_kurz =  get_post_meta( $novy_termin, 'kurz', true );
		$stary_termin = $termin_id;

		update_post_meta( $_POST['tid_change'], 'prihlaska-termin', $novy_termin );
		update_post_meta( $_POST['tid_change'], 'prihlaska-kurz', $novy_kurz );

		delete_transient( 'prihlasek_na_termin_'.$stary_termin );
		delete_transient( 'prihlasek_na_termin_'.$novy_termin ); ?>

		<div class="woocommerce-message" role="alert"><?= __('Termín byl úspěšně změněn.', 'jz'); ?></div>
    <?php }

    ?>
    <div class="table-termin-wrapper">
        <table class="table-termin table-informace">

            <?php if (!is_account_page()) { ?>
                <tr>
                    <td class="td1"><?= __('Kurz', 'jz'); ?></td>
                    <td class="td2"><a
                                href="<?php echo get_permalink($kurz_id) ?>"><?php echo esc_html($kurz->post_title) ?></a>
                    </td>
                </tr>
            <?php } ?>

            <span style="display: none; ">
            <?php print_r(get_field('typ_terminu', $termin_id)) ?>
        </span>
            <?php if (!get_field('typ_terminu', $termin_id) || get_field('typ_terminu', $termin_id) == "jednoden"): ?>
                <tr>
                    <td class="td1"><?= __('Datum a čas konání', 'jz'); ?></td>
                    <td class="td2">
                        <?php

                        echo termin_datum_konani($termin_id);

                        $cas_konani = termin_cas_konani($termin_id);
                        echo ' (' . $cas_konani . ')';

                        ?>
                    </td>
                </tr>
            <?php else: ?>
                <tr>
                    <td class="td1"><?= __('Datum a čas konání', 'jz'); ?></td>
                    <td class="td2">
					<?= __('od ', 'jz'); ?> <?= date('j. n. Y', strtotime(get_field('datum_konani', $termin_id))) . ' ' . get_field('cas_konani', $termin_id) .' - '. get_field('cas_konani_do', $termin_id) ?></td>
                </tr>
                <tr>
                    <td class="td1"><?= __('Další dny', 'jz'); ?></td>
                    <td class="td2">
                        <?php while(have_rows('dalsi_dny', $termin_id)): the_row() ?>
                            <?= date("d. m. Y", strtotime(get_sub_field('datum'))). ' '.get_sub_field('cas'). ' - '.get_sub_field('cas_do') ?> <br>
                        <?php endwhile; ?>
                    </td>
                </tr>
                <tr>
                    <td class="td1"><?= __('Specifikace termínu', 'jz'); ?></td>
                    <td class="td2"><?= the_field('specifikace_terminu', $termin_id) ?></td>
                </tr>
            <?php endif; ?>
            <tr>
                <td class="td1"><?= __('Adresa', 'jz'); ?></td>
                <td class="td2">
                    <?php
                    echo termin_lokalita_adresa($termin_id);

                    $lokalita_url = termin_lokalita_url($termin_id);

                    echo ' &nbsp; <a href="' . $lokalita_url . '">(' . __('podrobné informace', 'jz') . ')</a>';
                    ?>
                </td>
            </tr>
            <tr>
                <td class="td1"><?= __('Lektorka', 'jz'); ?></td>
                <td class="td2">
                    <?php
                    $lektorka_id = get_post_meta($termin_id, 'lektorka', true);
                    $lektorka_post = get_post($lektorka_id);
                    echo '<a href="' . get_permalink($lektorka_id) . '">' . $lektorka_post->post_title . '</a>';
                    ?>

                    <?php if (is_account_page()) {

                        $tel = get_field('telefonni_cislo', $lektorka_id);

                        if ($tel) {
                            echo ', tel.: <strong>' . $tel . '</strong>';
                        }

                    }
                    ?>
                </td>
            </tr>

        </table>
    </div>
    <?php

}

function prihlaska_required_and_maybe_highlited( $index ) {

	echo ' required ';

	if( isset($_SESSION['prihlaska-vyplnit'][$index]) ) {
		echo ' class="vyplnit" placeholder="'.__('Povinné pole!','jz').'" ';
	}

}


function prihlaska_container_class( $index ) {

	if( isset($_SESSION['prihlaska-vyplnit'][$index]) ) {
		echo ' vyplnit-cont ';
	}

}


// vyplnim input z pripadnych dat v pameti
function prihlaska_form_item( $index ) {

	if( isset($_SESSION['prihlaska-data'][$index]) && !current_user_can( 'manage_options' ) ) {
		return esc_attr($_SESSION['prihlaska-data'][$index]);
	}

	return '';
}


function prihlaska_predvyplnit_data_formulare() {

	$user_id = get_current_user_id();

	$_SESSION['prihlaska-data']['prihlaska-psc'] = get_user_meta( $user_id, 'shipping_postcode', true );
	$_SESSION['prihlaska-data']['prihlaska-mesto'] = get_user_meta( $user_id, 'shipping_city', true );
	$_SESSION['prihlaska-data']['prihlaska-ulice-cp'] = get_user_meta( $user_id, 'shipping_address_1', true );
	$_SESSION['prihlaska-data']['prihlaska-prijmeni'] = get_user_meta( $user_id, 'shipping_last_name', true );
	$_SESSION['prihlaska-data']['prihlaska-jmeno'] = get_user_meta( $user_id, 'shipping_first_name', true );
	$_SESSION['prihlaska-data']['prihlaska-telefon'] = get_user_meta( $user_id, 'billing_phone', true );
	$_SESSION['prihlaska-data']['prihlaska-email'] = get_user_meta( $user_id, 'billing_email', true );
	$_SESSION['prihlaska-data']['prihlaska-psc'] = get_user_meta( $user_id, 'billing_postcode', true );
	$_SESSION['prihlaska-data']['prihlaska-mesto'] = get_user_meta( $user_id, 'billing_city', true );
	$_SESSION['prihlaska-data']['prihlaska-ulice-cp'] = get_user_meta( $user_id, 'billing_address_1', true );
	$_SESSION['prihlaska-data']['prihlaska-prijmeni'] = get_user_meta( $user_id, 'billing_last_name', true );
	$_SESSION['prihlaska-data']['prihlaska-jmeno'] = get_user_meta( $user_id, 'billing_first_name', true );
	// billing_country TODO?
}




function prihlaska_maybe_readonly( $index = '' ) {

	if( isset($_SESSION['prihlaska-prvotni-krok-je-v-poradku']) && $_SESSION['prihlaska-prvotni-krok-je-v-poradku'] ) {
		echo ' readonly="readonly" class="readonly" ';
	}

}















function formular_prihlasky( $termin_id ) {

	$termin = get_post( $termin_id );

	$kurz_id = get_field('kurz', $termin_id);
	$kurz = get_post( $kurz_id );

	$typ_kurzu = typ_kurzu_kod( $kurz );


	$zobrazit_formular = true;


	if( $zobrazit_formular ) {

		if(isset($_SESSION['prihlaska-formular-info'])) {
			echo $_SESSION['prihlaska-formular-info'];
			unset($_SESSION['prihlaska-formular-info']);
		}



		// jde o redirect po uspesne prihlasce, takze ukoncim vypisovani zbytku stranky (= formulare)
		if( isset( $_SESSION['prihlaska-uspesne-dokoncena'] ) ) {

			unset( $_SESSION['prihlaska-uspesne-dokoncena'] );

			return;
		}





		$je_to_kontrola = false;

		if( isset($_SESSION['prihlaska-prvotni-krok-je-v-poradku']) && $_SESSION['prihlaska-prvotni-krok-je-v-poradku'] ) {
			$je_to_kontrola = true;
		}


		// pokud prisla z (uspesneho) prihlaseni, tak to znamena, ze prihlaska uz byla zkontrolovana a kompletni
		// takze ji muzu rovnou prepnout na kontrolni zobrazeni

		if( !empty( $_GET['justlogged'] ) && is_user_logged_in() ) {
			$je_to_kontrola = true;
		}


		// prihlaseny uzivatel ... predvyplnim mu formular
		if( is_user_logged_in() && empty( $_SESSION['prihlaska-data'] ) ) {

			prihlaska_predvyplnit_data_formulare();

		}

		?>





		<form method="post" class="prihlaska" action="<?php echo get_permalink( $termin->ID ) ?>">

		<?php if( $je_to_kontrola ) { ?>
			<div class="prihlaska-kontrola-nadpis">

				<?php if( isset( $_GET['justlogged'] ) && is_user_logged_in() ) { ?>
					<p class="uspech"><?php _e('Byla jste úspěšně přihlášena do svého uživatelského účtu. Nyní můžete přihlášku dokončit.','jz') ?></p>
				<?php } ?>

				<h2 class="center"><?php _e('Zkontrolujte prosím zadané informace','jz') ?></h2>

				<p class="center"><?php _e('Poté přihlášku potvrďte tlačítkem na konci.','jz') ?></p>

			</div>
		<?php } ?>


		<?php

		tabulka_s_podrobnostmi_o_terminu( $termin_id );

		?>


		<?php if( ! termin_pocet_volnych_mist( $termin_id ) ) {  ?>

			<p class="chyba"><?php _e('Je nám líto, tento termín je již plně obsazen.','jz') ?></p>

		<?php } else { ?>

			<div class="table-termin-wrapper">
			<table class="table-termin table-prihlaska">

			<tr>
				<td colspan="2" class="tdnadpis tdnadpisdruhprihlasky">
				
					<h2><?php $typ_kurzu == 'online'  ? _e('Cena přihlášky','jz') : _e('Vyberte druh přihlášky','jz') ?> </h2>
				</td>
			</tr>
			<tr>
				<td class="tdcolspan" colspan="2">


				<?php if( $typ_kurzu == 'soukromy' ) { ?>

					<p><em><?php _e('V případě soukromého kurzu není třeba volit druh přihlášky. Pokračujte prosím dále.','jz') ?></em></p>

				<?php } if( $typ_kurzu == 'online' ) {
											
					$mena = jz_aktualni_mena_kod();

					$cena = termin_cena_zvoleneho_typu( 'jednotlivec', $termin_id, $mena );
					$cena_formatovana = formatovana_cena( $cena, $mena );

					$akcni_cena = termin_akcni_cena_zvoleneho_typu( 'jednotlivec', $termin_id, $mena );

					if( $akcni_cena ) {
						$akcni_cena_formatovana = formatovana_cena( $akcni_cena, $mena );
						$akcni_cena_poznamka = termin_akcni_cena_zvoleneho_typu_poznamka( 'jednotlivec', $termin_id );
					}

					?>
					<label>
						<input type="hidden" name="prihlaska-typ-prihlasky" value="jednotlivec">
						<strong>Cena přihlášky</strong>

						<?php if( $akcni_cena ) { ?>

							<span class="cena bezna-behem-akce">(<?php echo $cena_formatovana ?>)</span>

							<span class="cena akcni"><?php echo $akcni_cena_formatovana ?></span>

							<?php if( $akcni_cena_poznamka ) { ?>
								<span class="poznamka poznamka-k-akci"><?php echo $akcni_cena_poznamka ?></span>
							<?php } ?>

						<?php } else { ?>

							<span class="cena bezna-bez-akce">(<?php echo $cena_formatovana ?>)</span>

						<?php } ?>
					</label>

				<?php } else {

					$moznosti = array();

					if( $typ_kurzu == '1denni' ) {

						$moznosti = array(
							'jednotlivec' => __('Zúčastním se sama','jz'),
						);

					} else if( $typ_kurzu == '2denni' || $typ_kurzu == 'bebalanced' ) {

						$moznosti = array(
							'par' => __('Zúčastníme se v páru','jz'),
							'jednotlivec' => __('Zúčastním se sama','jz'),
						);

					}

					foreach( $moznosti as $moznost_kod => $moznost_nazev ) { ?>

						<?php if( $je_to_kontrola && $moznost_kod != $_SESSION['prihlaska-data']['prihlaska-typ-prihlasky'] ) { continue; } ?>

						<?php

							$mena = jz_aktualni_mena_kod();

							$cena = termin_cena_zvoleneho_typu( $moznost_kod, $termin_id, $mena );
							$cena_formatovana = formatovana_cena( $cena, $mena );

							$akcni_cena = termin_akcni_cena_zvoleneho_typu( $moznost_kod, $termin_id, $mena );

							if( $akcni_cena ) {
								$akcni_cena_formatovana = formatovana_cena( $akcni_cena, $mena );
								$akcni_cena_poznamka = termin_akcni_cena_zvoleneho_typu_poznamka( $moznost_kod, $termin_id );
							}

						?>

						<div class="typ-prihlasky <?php if( ! $je_to_kontrola ) { prihlaska_container_class('prihlaska-typ-prihlasky'); } ?>">

							<label>
							<?php if( ! $je_to_kontrola ) { ?>
								<input type="radio" name="prihlaska-typ-prihlasky" value="<?php echo $moznost_kod ?>"

									<?php prihlaska_required_and_maybe_highlited('prihlaska-typ-prihlasky') ?>

									<?php prihlaska_maybe_readonly('prihlaska-typ-prihlasky') ?>

									<?php echo ( prihlaska_form_item('prihlaska-typ-prihlasky') == $moznost_kod ? 'checked="checked"' : '' ) ?>
								>
							<?php } ?>

							<strong><?php echo $moznost_nazev ?></strong>

							<?php if( $akcni_cena ) { ?>

								<span class="cena bezna-behem-akce">(<?php echo $cena_formatovana ?>)</span>

								<span class="cena akcni"><?php echo $akcni_cena_formatovana ?></span>

								<?php if( $akcni_cena_poznamka ) { ?>
									<span class="poznamka poznamka-k-akci"><?php echo $akcni_cena_poznamka ?></span>
								<?php } ?>

							<?php } else { ?>

								<span class="cena bezna-bez-akce">(<?php echo $cena_formatovana ?>)</span>

							<?php } ?>
							</label>

							<?php if( $moznost_kod == 'par' ) { ?>
								<div class="par-upresneni" style="display: <?php echo 'par' == prihlaska_form_item('prihlaska-typ-prihlasky') ? 'block' : 'none'; ?>">
									<label for="prihlaska-par-upresneni"><?php _e('S kým přijdete?','jz') ?> *</label>
									<input type="text" name="prihlaska-par-upresneni" id="prihlaska-par-upresneni"
											placeholder="<?php _e('Např. s kamarádkou / dulou','jz') ?>"
											value="<?php echo prihlaska_form_item( 'prihlaska-par-upresneni' ) ?>"

											<?php echo isset($_SESSION['prihlaska-vyplnit']['prihlaska-par-upresneni']) ? ' class="vyplnit" placeholder="'.__('Povinné pole!','jz').'" ' : '' ?>

											<?php echo 'par' == prihlaska_form_item('prihlaska-typ-prihlasky') ? 'required' : ''; /* tim padem je input zobrazeny */ ?>

											<?php prihlaska_maybe_readonly('prihlaska-par-upresneni') ?>
											/>
								</div>
							<?php } ?>



							<?php
							// Bokem si ulozim "lidsky" popisek one moznosti, at ho muzu zobrazit v souhrnu prihlasky
							?>
							<input type="hidden" name="prihlaska-typ-prihlasky-<?php echo $moznost_kod ?>-popis" value="<?php echo esc_attr($moznost_nazev) ?>" />

						</div>

					<?php } ?>

				<?php } // soukromy ?>

				</td>
			</tr>


			<?php if( $typ_kurzu != 'soukromy' ) { ?>

				<tr class="prihlaska-tr-slevovy-poukaz">
					<td class="td1"><label for="prihlaska-slevovy-poukaz"><?php _e('Mám slevový poukaz','jz') ?>:</label></td>
					<td class="td2">

						<?php if( $je_to_kontrola ) {

							$slevovy_poukaz = prihlaska_form_item( 'prihlaska-slevovy-poukaz' );

							// s nevalidnim poukazem neni mozne prejit na kontrolu (2. krok), takze pokud nyni resime poukaz, pak je validni

							if( $slevovy_poukaz ) {

								echo '<strong>'.htmlspecialchars($slevovy_poukaz).' - ';
								echo slevovy_poukaz_nazev( $slevovy_poukaz );
								echo '</strong>';

							} else {

								echo '<strong>'.__('Ne','jz').'</strong>';

							}

						} else { ?>
							<input type="text" name="prihlaska-slevovy-poukaz" id="prihlaska-slevovy-poukaz" value="<?php echo prihlaska_form_item( 'prihlaska-slevovy-poukaz' ) ?>" <?php prihlaska_maybe_readonly('prihlaska-slevovy-poukaz') ?> />
						<?php } ?>
					</td>
				</tr>

			<?php } // not soukromy ?>



			<tr>
				<td colspan="2" class="tdnadpis">
					<h2><?php _e('Osobní údaje','jz') ?></h2>
				</td>
			</tr>
			<tr>
				<td class="td1"><label for="prihlaska-jmeno"><?php _e('Jméno','jz') ?>: *</label></td>
				<td class="td2"><input type="text" name="prihlaska-jmeno" id="prihlaska-jmeno" value="<?php echo prihlaska_form_item( 'prihlaska-jmeno' ) ?>" <?php prihlaska_required_and_maybe_highlited('prihlaska-jmeno') ?> <?php prihlaska_maybe_readonly('prihlaska-jmeno') ?> /></td>
			</tr>
			<tr>
				<td class="td1"><label for="prihlaska-prijmeni"><?php _e('Příjmení','jz') ?>: *</label></td>
				<td class="td2"><input type="text" name="prihlaska-prijmeni" id="prihlaska-prijmeni" value="<?php echo prihlaska_form_item( 'prihlaska-prijmeni' ) ?>" <?php prihlaska_required_and_maybe_highlited('prihlaska-prijmeni') ?> <?php prihlaska_maybe_readonly('prihlaska-prijmeni') ?> /></td>
			</tr>

			<tr>
				<td class="td1"><label for="prihlaska-ocekavany-termin-porodu"><?php _e('Očekávaný termín porodu','jz') ?>: *</label></td>
				<td class="td2"><input type="text" name="prihlaska-ocekavany-termin-porodu" id="prihlaska-ocekavany-termin-porodu" value="<?php echo prihlaska_form_item( 'prihlaska-ocekavany-termin-porodu' ) ?>" <?php prihlaska_required_and_maybe_highlited('prihlaska-ocekavany-termin-porodu') ?> <?php prihlaska_maybe_readonly('prihlaska-ocekavany-termin-porodu') ?> /></td>
			</tr>

			<tr>
				<td class="td1"><label for="prihlaska-vicercata"><?php _e('Čekám vícerčata','jz') ?>:</label></td>
				<td class="td2">
					<?php if( $je_to_kontrola ) { ?>

						<?php if( prihlaska_form_item( 'prihlaska-vicercata' ) == 'ano' ) { ?>
							<strong>Ano</strong>
						<?php } else { ?>
							<strong>Ne</strong>
						<?php } ?>

					<?php } else { ?>
						<label>
							<input type="checkbox" name="prihlaska-vicercata" id="prihlaska-vicercata" value="ano" <?php checked( 'ano', prihlaska_form_item( 'prihlaska-vicercata' ) ) ?> />
							<?php _e('Ano','jz') ?>
						</label>
					<?php } ?>
				</td>
			</tr>


			<tr>
				<td class="td1"><label for="prihlaska-ulice-cp"><?php _e('Ulice, čp.','jz') ?>: *</label></td>
				<td class="td2"><input type="text" name="prihlaska-ulice-cp" id="prihlaska-ulice-cp" value="<?php echo prihlaska_form_item( 'prihlaska-ulice-cp' ) ?>" <?php prihlaska_required_and_maybe_highlited('prihlaska-ulice-cp') ?> <?php prihlaska_maybe_readonly('prihlaska-ulice-cp') ?> /></td>
			</tr>
			<tr>
				<td class="td1"><label for="prihlaska-mesto"><?php _e('Město','jz') ?>: *</label></td>
				<td class="td2"><input type="text" name="prihlaska-mesto" id="prihlaska-mesto" value="<?php echo prihlaska_form_item( 'prihlaska-mesto' ) ?>" <?php prihlaska_required_and_maybe_highlited('prihlaska-mesto') ?> <?php prihlaska_maybe_readonly('prihlaska-mesto') ?> /></td>
			</tr>
			<tr>
				<td class="td1"><label for="prihlaska-psc"><?php _e('PSČ','jz') ?>: *</label></td>
				<td class="td2"><input type="text" name="prihlaska-psc" id="prihlaska-psc" value="<?php echo prihlaska_form_item( 'prihlaska-psc' ) ?>" <?php prihlaska_required_and_maybe_highlited('prihlaska-psc') ?> <?php prihlaska_maybe_readonly('prihlaska-psc') ?> /></td>
			</tr>

			<tr>
				<td class="td1"><label for="prihlaska-stat"><?php _e('Stát','jz') ?>: *</label></td>
				<td class="td2">
				<?php

					if( $je_to_kontrola ) {

						$staty = include( WC()->plugin_path() . '/i18n/countries.php' );

						$stat = $staty[ prihlaska_form_item( 'prihlaska-stat' ) ];

						// falesny input jen pro vypsani spravne formatovane hodnoty, a realna hodnota ze selectu je ve skrytem poli
						?>
						<input type="text" value="<?php echo $stat ?>" <?php prihlaska_maybe_readonly('prihlaska-stat') ?> />
						<input type="hidden" name="prihlaska-stat" value="<?php echo prihlaska_form_item( 'prihlaska-stat' ) ?>">
						<?php

					} else {

						$args = array(
							'type' => 'country',
							'required' => true,
						);

						if( isset($_SESSION['prihlaska-vyplnit']['prihlaska-stat']) ) {
							$args['input_class'] = array( 'vyplnit' );
						}

						woocommerce_form_field( 'prihlaska-stat', $args, prihlaska_form_item( 'prihlaska-stat' ) );

					}

				?>
				</td>
			</tr>

			<tr>
				<td colspan="2" class="tdcolspan tdnadpis">
					<h2><?= __('Kontaktní informace', 'jz'); ?></h2>
				</td>
			</tr>
			<tr>
				<td class="td1"><label for="prihlaska-email"><?php _e('E-mail','jz') ?>: *</label></td>
				<td class="td2"><input type="email" name="prihlaska-email" id="prihlaska-email" value="<?php echo prihlaska_form_item( 'prihlaska-email' ) ?>" <?php prihlaska_required_and_maybe_highlited('prihlaska-email') ?> <?php prihlaska_maybe_readonly('prihlaska-email') ?> /></td>
			</tr>
			<tr>
				<td class="td1"><label for="prihlaska-telefon"><?php _e('Telefon','jz') ?>: *</label></td>
				<td class="td2"><input type="text" name="prihlaska-telefon" id="prihlaska-telefon" value="<?php echo prihlaska_form_item( 'prihlaska-telefon' ) ?>" <?php prihlaska_required_and_maybe_highlited('prihlaska-telefon') ?> <?php prihlaska_maybe_readonly('prihlaska-telefon') ?> /></td>
			</tr>



			<?php
			// poznamku pri kontrole zobrazim jen tehdy, neni-li prazdna
			if( ! $je_to_kontrola || prihlaska_form_item('prihlaska-poznamka') ) {	?>
			<tr>
				<td colspan="2" class="tdcolspan tdnadpis">
					<h2><?php _e('Poznámka','jz') ?></h2>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="tdpoznamka">

					<p>
						<textarea name="prihlaska-poznamka" id="prihlaska-poznamka" <?php prihlaska_maybe_readonly('prihlaska-poznamka') ?>><?php echo esc_html(prihlaska_form_item('prihlaska-poznamka')) ?></textarea>
					</p>

				</td>
			</tr>
			<?php } ?>


			<?php if( $typ_kurzu != 'soukromy' ) { ?>

				<tr>
					<td colspan="2" class="tdcolspan tdnadpis">
						<h2><?php _e('Výběr způsobu platby','jz') ?></h2>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="tdplatebniinformace">

						<?php

						$moznosti_platby = array();

						if( termin_cislo_uctu( $termin_id ) ) { // termin ma nastavene cislo uctu
							$moznosti_platby[ 'bankovni-prevod' ] = __('Bankovní převod','jz');
						}

						if( termin_online_platba( $termin_id ) ) {
							$moznosti_platby[ 'on-line' ] = __('Platba kartou on-line','jz');
						}

						?>


						<?php foreach( $moznosti_platby as $platba_kod => $platba_nazev ) { ?>

							<?php if( $je_to_kontrola ) { ?>

								<?php if( $platba_kod ==  prihlaska_form_item('prihlaska-platba') ) { ?>

									<p>
										<strong><?php echo $platba_nazev ?></strong>
										<input type="hidden" name="prihlaska-platba" value="<?php echo prihlaska_form_item('prihlaska-platba') ?>">
									</p>

								<?php } ?>

							<?php } else { ?>

								<p>
									<label>
										<input type="radio" name="prihlaska-platba" value="<?php echo $platba_kod ?>" <?php checked( $platba_kod, prihlaska_form_item('prihlaska-platba') ) ?> >
										<?php echo $platba_nazev ?>
									</label>
								</p>

							<?php } ?>

						<?php } ?>


					</td>
				</tr>

			<?php } // not soukromy ?>

			<tr>
				<td colspan="2" class="tdcolspan tdnadpis">
					<h2><?php _e('Souhlas se zpracováním osobních údajů a s obchodními podmínkami','jz') ?></h2>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="tdcolspan tdsouhlassezpracovanim">
					<p><em><?php

						// POZOR !!! Souhlas je i v kontaktnim formulari v soukromem kurzu

						echo sprintf(
							__('Odesláním tohoto formuláře souhlasím se <a href="/wp-content/uploads/2018/06/souhlas.pdf" target="_blank">zpracováním svých osobních údajů</a> a <a href="%s">obchodními podmínkami</a>.','jz'),
							get_permalink(  PAGE_ID_OBCHODNI_PODMINKY  )
						) ?></em></p>

					<?php if( ! $je_to_kontrola ) { ?>
						<p class="<?php prihlaska_container_class('prihlaska-souhlas-se-zpracovanim') ?>">
							<input type="checkbox" name="prihlaska-souhlas-se-zpracovanim" id="prihlaska-souhlas-se-zpracovanim"
									value="ano"
									<?php echo ( prihlaska_form_item('prihlaska-souhlas-se-zpracovanim') == 'ano' ? 'checked="checked"' : '' ) ?>
									<?php prihlaska_required_and_maybe_highlited('prihlaska-souhlas-se-zpracovanim') ?>
									<?php prihlaska_maybe_readonly('prihlaska-souhlas-se-zpracovanim') ?>
							/>
							<label for="prihlaska-souhlas-se-zpracovanim"><?php _e('Souhlasím se zpracováním svých osobních údajů a s obchodními podmínkami','jz') ?></label>
						</p>
					<?php } else { ?>
						<p><strong><?php _e('Ano, souhlasím se zpracováním osobních údajů a s obchodními podmínkami','jz') ?></strong></p>
					<?php } ?>
				</td>
			</tr>







			<tr>
				<td colspan="2" class="tdcolspan tdsubmit">

					<input type="hidden" name="prihlaska-post-id" value="<?php echo $termin_id ?>">

					<?php if( $je_to_kontrola ) { ?>

						<p class="center">
							<a href="<?php echo get_permalink($termin->ID) ?>"><?php _e('Chci se vrátit o krok zpět a opravit informace','jz') ?></a>
						</p>
						<p class="center">
							<input type="hidden" name="prihlaska-potvrzene-odeslani" value="yes" />
							<input type="submit" id="prihlaska-potvrzeni-submit" value="<?php _e('Potvrzuji přihlášku','jz') ?>" />
						</p>

						<script>
							jQuery(function(){

								jQuery('form.prihlaska').on( 'submit', function(){

									jQuery('#prihlaska-potvrzeni-submit').prop('disabled', true).css({ 'opacity' : '0.3' });

								});

							});
						</script>

					<?php } else { ?>

						<p class="center">
							<input type="hidden" name="prihlaska-prvotni-odeslani" value="yes" />
							<input type="submit" value="<?php _e('Pokračovat na kontrolu informací','jz') ?>" />
						</p>
						<p class="center">
							<?php _e('Po odeslání formuláře si budete moci ještě zkontrolovat zadané údaje, a teprve poté přihlášku finálně potvrdit.','jz') ?><br>
							<strong><?php _e('Kurzovné uhraďte do 7 dní. Pokud se na kurz přihlašujete méně než sedm dní před kurzem, uhraďte obratem. Kurzovné je potřeba mít uhrazené před nástupem na kurz.','jz') ?></strong>
						</p>

					<?php } ?>

				</td>
			</tr>

			</table>
			</div>
			</form>

			<?php

				// Je zobrazena objednavka ... pokud uz jde o kontrolu, tak vymazu session, ktera urcuje, ze jsou data OK. Pri dalsim reloadu
				// tak bude mozne objednavku opet editovat (pole nebudou readonly)

				unset($_SESSION['prihlaska-prvotni-krok-je-v-poradku']);

			?>

		<?php } // if volna mista ?>


	<?php } /* if zobrazit_formular */


}
















function prihlaska_prvotni_odeslani() {

	$termin_id = intval( $_POST['prihlaska-post-id'] );

	$return_url = get_permalink( $termin_id );


	$kurz_id = get_field('kurz', $termin_id);
	$kurz = get_post( $kurz_id );

	$typ_kurzu = typ_kurzu_kod( $kurz );


	unset($_SESSION['prihlaska-prvotni-krok-je-v-poradku']);
	unset($_SESSION['prihlaska-vyplnit']);

	$_SESSION['prihlaska-data'] = stripslashes_deep($_POST);

	$vse_je_ok = true;

	$povinne = array( 'prihlaska-jmeno', 'prihlaska-prijmeni', 'prihlaska-ocekavany-termin-porodu', 'prihlaska-email', 'prihlaska-telefon',
						'prihlaska-ulice-cp', 'prihlaska-mesto', 'prihlaska-psc', 'prihlaska-stat', 'prihlaska-souhlas-se-zpracovanim' );


	// pokud nejde o soukromy termin, tak jeste doplnit typ prihlasky (jednotlivec/par) a zpusob platby

	if( $typ_kurzu != 'soukromy') {
		if( $typ_kurzu != 'online') {
			$povinne[] = 'prihlaska-typ-prihlasky';
		}
		$povinne[] = 'prihlaska-platba';

		if( $_POST['prihlaska-typ-prihlasky'] == 'par' ) {
			$povinne[] = 'prihlaska-par-upresneni';
		}

	}


	$nevyplnene_povinne_pole = array();

	foreach( $povinne as $pov ) {
		if( !isset($_POST[$pov]) || empty($_POST[$pov]) || trim( $_POST[$pov] ) == '' ) {
			$nevyplnene_povinne_pole[ $pov ] = true;
		}
	}

	if( count($nevyplnene_povinne_pole) ) {
		$vse_je_ok = false;
		$_SESSION['prihlaska-formular-info'] = '<p class="chyba">'.__('Nebyla vyplněna všechna povinná pole!','jz').'</p>';
		$_SESSION['prihlaska-vyplnit'] = $nevyplnene_povinne_pole;
		wp_redirect( $return_url ); exit;
	}


	// Co slevovy poukaz? nevalidni slevovy poukaz nemohu nechat zadany ... takze ho musim zkontrolovat nyni, pri prvotnim odeslani

	// Identicka kontrola je i v druhem kroku pred ulozenim ... co kdyby mezitim behem par minut prestal byt poukaz platny?

	$slevovy_poukaz = '';

	if( ! empty( $_POST['prihlaska-slevovy-poukaz'] )) {

		$slevovy_poukaz = trim( $_POST['prihlaska-slevovy-poukaz'] );

		$kurz_id = termin_id_kurzu( $termin_id );

		if( ! slevovy_poukaz_je_validni( $slevovy_poukaz, $kurz_id ) ) {

			$vse_je_ok = false;
			$_SESSION['prihlaska-formular-info'] = '<p class="chyba">'.__('Slevový poukaz bohužel není platný! Mohla skončit jeho platnost, nebo byl špatně opsán jeho kód.','jz').'</p>';
			wp_redirect( $return_url ); exit;

		}

	}






	// vse povinne se zda se ok, takze zkontroluji uzivatelsky ucet


	if( ! is_user_logged_in() ) { // budeme uzivatele i registrovat ... neexistuje uz tento email v db?

		$email = strtolower( trim( $_POST['prihlaska-email'] ) );

		$user = get_user_by( 'email', $email ); // email je nastaveny jako login

		if( $user ) { // tak ... prihlasuje se clovek, co uz ma existujici registraci ... nemuzu ho pustit dal :-( (protoze mu nemuzu vytvorit novy ucet, aby mel pristup k materialum atd)

			$hlasky = '<h2 class="chyba">'.sprintf( __('Účet s e-mailovou adresou %s již existuje, nelze vytvořit nový!','jz'), '<strong>'.$email.'</strong>' ).'</h2>';

			$url = wc_get_page_permalink( 'myaccount' ).'?termin_redirect='.urlencode( get_permalink( $termin_id ) );

			$hlasky .= '<p class="chyba">'.sprintf( __('Ve Vašem uživatelském účtu Vám zpřístupníme všechny materiály a relaxační MP3, 
														je tedy nutné se před dokončením přihlášky do svého účtu <a href="%s">přihlásit</a>.','jz'), $url ).'</p>';

			$hlasky .= '<p class="chyba">'.sprintf( __('Pokud jste zapomněla heslo, snadno si můžete nechat <a href="%s">poslat nové</a>!','jz'), wc_lostpassword_url() ).'</p>';


			$_SESSION['prihlaska-formular-info'] = $hlasky;
			wp_redirect( $return_url ); exit;

		}
	}


	// uplne vse se zda ok

	$_SESSION['prihlaska-prvotni-krok-je-v-poradku'] = true;

}


if( isset($_POST['prihlaska-prvotni-odeslani']) )
	add_action('init', 'prihlaska_prvotni_odeslani');




































function prihlaska_potvrzene_odeslani() {

	$termin_id = intval($_POST['prihlaska-post-id']);

	$return_url = get_permalink( $termin_id );

	$kurz_id = get_post_meta( $termin_id, 'kurz', true );
	$kurz = get_post( $kurz_id );

	$typ_kurzu = typ_kurzu_kod( $kurz );



	// vyprsela session ????

	if( empty( $_SESSION ) ) {

		wp_redirect( $return_url ); exit;

	}




	$_SESSION['prihlaska-formular-info'] = '';

	$_SESSION['prihlaska-uspesne-dokoncena'] = false;




	// data si beru z $_SESSION['prihlaska-data'] ...




	// nejdrive zalozim uzivatelsky ucet, pokud uz neni prihlaseny ...
	// kontrola, jestli se nahodou neregistroval nekdy v minulosti, probehla uz v kontrolnim kroku... tady to tedy nemusim resit.

	$aktualizovat_user_metas = false;

	if( ! is_user_logged_in() ) {

		$user_login = strtolower( $_SESSION['prihlaska-data']['prihlaska-email'] );
		$user_pass = wp_generate_password();

		$user_data = array(
			'user_login' => $user_login,
			'user_email' => $user_login,
			'user_pass' => $user_pass,
			'first_name' => $_SESSION['prihlaska-data']['prihlaska-jmeno'],
			'last_name' => $_SESSION['prihlaska-data']['prihlaska-prijmeni'],
			'display_name' => $_SESSION['prihlaska-data']['prihlaska-jmeno'].' '.$_SESSION['prihlaska-data']['prihlaska-prijmeni'],
			'role' => 'customer',
		);

		$user_id = wp_insert_user($user_data);

		if( is_wp_error( $user_id ) ) {
            $error_string = $user_id->get_error_message();
            error_log($error_string , 0);
			$_SESSION['prihlaska-formular-info'] = '<p class="chyba">'.__('Bohužel, došlo k problému při vytváření uživatelského účtu. Zkuste to prosím později znovu. Za komplikace se omlouváme.','jz').'</p>';
			wp_redirect( $return_url ); exit;

		} else {

			$aktualizovat_user_metas = true;

			// no ... tak si uzivatele rovnou prihlasim??? :-)

			wp_clear_auth_cookie();
			wp_set_current_user ( $user_id );
			wp_set_auth_cookie  ( $user_id );

		}

	} else {

		$aktualizovat_user_metas = true;

		// login udaje jdou to do notifikacniho emailu ... (heslo jen pri nove registraci)
		$current_user = wp_get_current_user();
		$user_id = $current_user->ID;
		$user_login = $current_user->user_login;

		$user_pass = '';


		// a co aktualizace zakladnich informaci? Co kdyz se zmenilo jmeno, nebo spis prijmeni ...
		wp_update_user( array(
			'ID' => $user_id,
			'first_name' => $_SESSION['prihlaska-data']['prihlaska-jmeno'],
			'last_name' => $_SESSION['prihlaska-data']['prihlaska-prijmeni'],
		) );

	}


	// Mozna aktualizovat woocommerce meta data dle soucasne prihlasky???
	// No asi jo ... to jsou vlastne nejaktualnejsi informace o zakaznici, ktere mame ... ty ulozene v eshopu jsou "starsi" nez ty zadane nyni do formulare

	if( $aktualizovat_user_metas ) {

		$user_metas = array(
			'shipping_country' => 'CZ',
			'shipping_postcode' => $_SESSION['prihlaska-data']['prihlaska-psc'],
			'shipping_city' => $_SESSION['prihlaska-data']['prihlaska-mesto'],
			'shipping_address_1' => $_SESSION['prihlaska-data']['prihlaska-ulice-cp'],
			'shipping_last_name' => $_SESSION['prihlaska-data']['prihlaska-prijmeni'],
			'shipping_first_name' => $_SESSION['prihlaska-data']['prihlaska-jmeno'],
			'billing_phone' => $_SESSION['prihlaska-data']['prihlaska-telefon'],
			'billing_email' => $_SESSION['prihlaska-data']['prihlaska-email'],
			'billing_country' => $_SESSION['prihlaska-data']['prihlaska-stat'],
			'billing_postcode' => $_SESSION['prihlaska-data']['prihlaska-psc'],
			'billing_city' => $_SESSION['prihlaska-data']['prihlaska-mesto'],
			'billing_address_1' => $_SESSION['prihlaska-data']['prihlaska-ulice-cp'],
			'billing_last_name' => $_SESSION['prihlaska-data']['prihlaska-prijmeni'],
			'billing_first_name' => $_SESSION['prihlaska-data']['prihlaska-jmeno'],
		);

		foreach( $user_metas as $meta_key => $meta_value ) {

			update_user_meta( $user_id, $meta_key, $meta_value );

		}

	}












	// mam uzivatelsky ucet, tralala.



	// vytvorim prihlasku

	// asi je vse ok :)
	$nazev_prihlasky = termin_nazev_kurzu( $termin_id );
	$nazev_prihlasky .= ' - '. $_SESSION['prihlaska-data']['prihlaska-jmeno'].' '.$_SESSION['prihlaska-data']['prihlaska-prijmeni'];

	$arg = array(
		'post_type' => 'prihlaska',
		'post_title' => $nazev_prihlasky,
		'post_status' => 'publish',
		'post_author' => $user_id, // treba to k necemu bude
	);


	$prihlaska_id = wp_insert_post( $arg );

	if( ! $prihlaska_id) {
		$_SESSION['prihlaska-formular-info'] = '<p class="chyba">'.__('Přihlášku se nepodařilo uložit! Zkuste to prosím později znovu. Za komplikace se omlouváme.','jz').'</p>';


		// zrusim registraci uziv. uctu
		if( ! function_exists('wp_delete_user') ) {
			require_once( ABSPATH . 'wp-admin/includes/user.php' );
		}

		wp_delete_user( $user_id );

		wp_redirect( $return_url ); exit;
	}







	// opet kontrola slevoveho poukazu ... ?

	$slevovy_poukaz = '';

	if( ! empty( $_SESSION['prihlaska-data']['prihlaska-slevovy-poukaz'] )) {

		$slevovy_poukaz = trim( $_SESSION['prihlaska-data']['prihlaska-slevovy-poukaz'] );

		if( ! slevovy_poukaz_je_validni( $slevovy_poukaz, $kurz_id ) ) {

			$_SESSION['prihlaska-formular-info'] = '<p class="chyba">'.__('Slevový poukaz bohužel není platný! Mohla skončit jeho platnost, nebo byl špatně opsán jeho kód.','jz').'</p>';
			wp_redirect( $return_url ); exit;

		}

	}







	/*
	$variabilni_symbol = variabilni_symbol( $prihlaska_id ); // VS je rovnou i nastaven jako meta!

	$nazev_prihlasky .= ' (#'.$variabilni_symbol.')';

	if( ! wp_update_post( array('ID' => $prihlaska_id, 'post_title' => $nazev_prihlasky) ) ) {
		// TODO?
	}
	*/


	$save_item = array();

	$save_item['prihlaska-datum'] = current_time('Y-m-d H:i:s');

	$save_item['prihlaska-termin'] = $termin_id;
	$save_item['prihlaska-kurz'] = $kurz_id;
	$save_item['prihlaska-uzivatel'] = $user_id;


	if( $typ_kurzu == 'soukromy' ) {

		$save_item['prihlaska-soukromy-kurz'] = 'yes';

	} else {

		// typ, castka, zpusob platby ... to se resi jen pokud nejde o soukromy kurz

		$save_item['prihlaska-typ'] = $_SESSION['prihlaska-data']['prihlaska-typ-prihlasky'];
		$save_item['prihlaska-typ-popis'] = $_SESSION['prihlaska-data']['prihlaska-typ-prihlasky-'.$save_item['prihlaska-typ'].'-popis' ];

		if( $save_item['prihlaska-typ'] == 'par' ) {
			$save_item['prihlaska-par-upresneni'] = $_SESSION['prihlaska-data']['prihlaska-par-upresneni'];
		}


		$castka = termin_cena_zvoleneho_typu( $save_item['prihlaska-typ'], $termin_id );



		$akcni_cena = termin_akcni_cena_zvoleneho_typu( $save_item['prihlaska-typ'], $termin_id );

		if( $akcni_cena ) {

			$save_item['prihlaska-puvodni-castka' ] = $castka;
			$save_item['prihlaska-akcni-cena' ] = $akcni_cena; // to si ulozim jen tak, abych mel jistotu, ze prihlaska byla za akcni cenu

			$castka = $akcni_cena;

		}


		// co slevovy poukaz? ---------------------------------------

		if( $slevovy_poukaz ) {

			// po slevovem poukazu bude "castka" nejak ponizena, takze si puvodni poznamenam
			// ale pozor! mozna uz se puvodni castka nastavovala vyse! tak abych ji neprepsal uz ponizenou akcni cenou

			if( ! isset( $save_item['prihlaska-puvodni-castka' ] ) ) {
				$save_item['prihlaska-puvodni-castka' ] = $castka;
			}

			$save_item['prihlaska-slevovy-poukaz'] = $slevovy_poukaz;

			$slevovy_poukaz_post_id = slevovy_poukaz_post_id( $slevovy_poukaz );

			$slevovy_poukaz_typ = get_field( 'typ_slevy', $slevovy_poukaz_post_id);

			$save_item['prihlaska-slevovy-poukaz-typ'] = $slevovy_poukaz_typ;

			if( $slevovy_poukaz_typ == 'procentualni' ) {

				$slevovy_poukaz_procentualni_sleva = get_field( 'procentualni_sleva', $slevovy_poukaz_post_id );

				$save_item['prihlaska-slevovy-poukaz-procentualni-sleva'] = $slevovy_poukaz_procentualni_sleva;

				$castka = $castka - round( $castka * ( $slevovy_poukaz_procentualni_sleva / 100 )  );

				if( $castka < 0 )  // co kdyby ...
					$castka = 0;

			} else if( $slevovy_poukaz_typ == 'odecitaci' ) {

				$slevovy_poukaz_odecet = get_field( 'odecist_castku_'.jz_aktualni_mena_kod(), $slevovy_poukaz_post_id );

				$save_item['prihlaska-slevovy-poukaz-odecet'] = $slevovy_poukaz_odecet;

				$castka = $castka - $slevovy_poukaz_odecet;

				if( $castka < 0 )
					$castka = 0;

			} else if( $slevovy_poukaz_typ == 'zdarma' ) {

				$save_item['prihlaska-slevovy-poukaz-zdarma'] = 'ano'; // nebo nic neukladat? je to zbyto

				$castka = 0;

			}

		}

		$save_item[ 'prihlaska-castka' ] = $castka;
		$save_item[ 'prihlaska-mena' ] = jz_aktualni_mena_kod();


		// zpusob platby
		$save_item['prihlaska-platba'] = $_SESSION['prihlaska-data']['prihlaska-platba'];

		if( $save_item['prihlaska-platba'] == 'bankovni-prevod' ) {

			$save_item['prihlaska-platba-cislo-uctu'] = termin_cislo_uctu( $termin_id );

		} else if( $save_item['prihlaska-platba'] == 'on-line' ) {

			// ... no tady asi nyní nic neresim ... az pri vyvolani transakci

		}

	}


	// radeji at vidim, co vsechno se uklada
	$save_item['prihlaska-jmeno'] = $_SESSION['prihlaska-data']['prihlaska-jmeno'];
	$save_item['prihlaska-prijmeni'] = $_SESSION['prihlaska-data']['prihlaska-prijmeni'];
	$save_item['prihlaska-ocekavany-termin-porodu'] = $_SESSION['prihlaska-data']['prihlaska-ocekavany-termin-porodu'];
	$save_item['prihlaska-vicercata'] = !empty($_SESSION['prihlaska-data']['prihlaska-vicercata']) ? 'ano' : 'ne';
	$save_item['prihlaska-ulice-cp'] = $_SESSION['prihlaska-data']['prihlaska-ulice-cp'];
	$save_item['prihlaska-mesto'] = $_SESSION['prihlaska-data']['prihlaska-mesto'];
	$save_item['prihlaska-psc'] = $_SESSION['prihlaska-data']['prihlaska-psc'];
	$save_item['prihlaska-stat'] = $_SESSION['prihlaska-data']['prihlaska-stat'];
	$save_item['prihlaska-email'] = $_SESSION['prihlaska-data']['prihlaska-email'];
	$save_item['prihlaska-telefon'] = $_SESSION['prihlaska-data']['prihlaska-telefon'];
	$save_item['prihlaska-poznamka'] = $_SESSION['prihlaska-data']['prihlaska-poznamka'];





	// souhlas neukladam, bez nej to nejde ulozit tak jako tak


	foreach( $save_item as $index=>$item ) {
		if( ! add_post_meta( $prihlaska_id, $index, $item, true ) ) {
			// co ja vim?
		}
	}

	// var. symbol

	$vs = prihlaska_vs( $prihlaska_id, true );




	$_SESSION['prihlaska-uspesne-dokoncena'] = true;





	// tak ... nastavujeme novy stav

	zmena_stavu_prihlasky( $prihlaska_id, 'neuhrazeno' );



	echo 'neuhrazeno';





	// zalohova faktura? (ale jen je-li co platit a je-li to bankovni prevod, a neni to soukromy kurz)

	$prilohy = array();

	if (lektorka_fapi($termin_id) && $save_item['prihlaska-platba'] == 'bankovni-prevod' && $save_item[ 'prihlaska-castka' ] > 0) {
		
		fapi_zalohova_faktura( $prihlaska_id );


	} else if( lektorka_sf($termin_id) && $save_item['prihlaska-platba'] == 'bankovni-prevod' && $save_item[ 'prihlaska-castka' ] > 0 ) {

		$response = prihlaska_zalohova_faktura( $prihlaska_id );

		if( is_wp_error( $response )) {

			// a co ted?

			if( $response->get_error_code() == 'sf_response_error' ) {

				// faktura vubec nezalozena ...

			} else if( $response->get_error_code() == 'sf_no_pdf' ) {

				// nemam pdf ...

			}

		} else if( is_string( $response ) ) {

			$prilohy[] = $response; // cesta k pdf fakture

		}

	}







	// takze vse je ok, mame ulozenou prihlasku, jeji metadata.
	// cas na email

	$email_data = array(
		'user_login' => $user_login,
		'user_pass' => $user_pass ,
	);

	if( count( $prilohy ) ) {
		$email_data['prilohy'] = $prilohy;
	}



	if( ! prihlaska_email_se_shrnutim( $prihlaska_id, $email_data ) ) {

		$_SESSION['prihlaska-formular-info'] .= '<p class="chyba">'. sprintf( __('Bohužel došlo k chybě při odesílání e-mailu s podrobnými informacemi a platebními pokyny.
													Kontaktujte nás prosím na e-mailu %s. Za komplikace se omlouváme.','jz'), '<a href="mailto:'.JZ_KONTAKTNI_EMAIL.'">'.JZ_KONTAKTNI_EMAIL.'</a>' ) .'</p>';


	} else {

		ob_start(); ?>

		<p class="uspech"><?=__('Děkujeme za Vaši přihlášku. Obratem Vám přijde potvrzovací email spolu s platebními údaji. Někdy se stane, že email spadne do spamu. Prosím mrkněte se i tam. Budeme se moc těšit na viděnou!','jz') ?></p>

		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '457911591627581');
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=457911591627581&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->

		<?php
		$_SESSION['prihlaska-formular-info'] .= ob_get_clean();

	}


	// musim uklidit pripadne prilohy
	if( $prilohy ) {
		foreach( $prilohy as $priloha ) {
			@unlink( $priloha );
		}
	}



	// zaznamenam email do MC

	jz_mailchimp(
		$_SESSION['prihlaska-data']['prihlaska-email'],
		array( 'FNAME' => $_SESSION['prihlaska-data']['prihlaska-jmeno'], 'LNAME' => $_SESSION['prihlaska-data']['prihlaska-prijmeni'] )
	);




	// prihlaska "uhrazena" slevovym poukazem?
	// ! ale nesmi jit o soukromy kurz ! Ten ma taky ulovou castku, ale uhrazeny neni!!!!

	if( $save_item[ 'prihlaska-castka' ] == 0 && $typ_kurzu != 'soukromy' ) {
		zmena_stavu_prihlasky( $prihlaska_id, 'uhrazeno' );

	}


	// mozna si poznamenam nejake info ... ale hlavne musim strhnout jedno pouziti, pokud je poukaz omezen poctem zadani!

	if( $slevovy_poukaz ) {

		slevovy_poukaz_zapocitat_pouziti( $slevovy_poukaz );

	}





	// cas na FIO PLATEBNI BRAAAAAANUUUUUU!

	// ale pouze v pripade nenulove castky!!!!!
	// pokud byl zadan kupon a zakaznik nema nic platit, tak ho na branu posilat nebudu ...

	if( $save_item[ 'prihlaska-castka' ] > 0 && $save_item['prihlaska-platba'] == 'on-line' ) {

		$merchant = fio_merchant();

		$amount = $save_item[ 'prihlaska-castka' ] * 100;

		$currency = $save_item[ 'prihlaska-mena' ] == 'czk' ? 203 : 978; // 203 czk / 978 eur

		$ip = $_SERVER['REMOTE_ADDR'];

		$description = urlencode( __('Přihláška na kurz','jz' ) );

		$language = ICL_LANGUAGE_CODE;

		$account = prihlaska_vs( $prihlaska_id );

		$response = $merchant->startSMSTrans( $amount, $currency, $ip, $description, $language, $account);

		if ( substr( $response, 0, 14) == 'TRANSACTION_ID' ) {

			$trans_id = substr($response,16,28);

			update_post_meta( $prihlaska_id, 'prihlaska-fio-trans-id', $trans_id ); // !!!!

			$url = FIO_CLIENT_URL ."?trans_id=". urlencode($trans_id);

			// odchazi na platebni branu ... unsetnu session hlasku, aby se mu nezobrazila az pote, co se zase vrati.
			unset( $_SESSION['prihlaska-formular-info'] );

			header("Location: $url");
			exit;

		} else {

			$_SESSION['prihlaska-formular-info'] .= '<p class="chyba">'. sprintf( __('Bohužel došlo k chybě při při komunikaci s platební bránou.
														Kontaktujte nás prosím na e-mailu %s. Za komplikace se omlouváme.','jz'), '<a href="mailto:'.JZ_KONTAKTNI_EMAIL.'">'.JZ_KONTAKTNI_EMAIL.'</a>' ) .'</p>';

			fio_log_chyby( 'ON-LINE PLATBA', array( array('error' => 'Chyba při předávání přihlášky <a href="post.php?post='.$prihlaska_id.'&action=edit"><strong>'.$prihlaska_id.'</strong></a> na platební bránu! <em>Response: ' .$response.'</em>' ) ) );
		}


	}

	wp_redirect( $return_url ); exit;

}


if( isset($_POST['prihlaska-potvrzene-odeslani']) )
	add_action('init', 'prihlaska_potvrzene_odeslani');






















































function jz_zacatek_html_emailu() {
	$html = '<html xmlns="http://www.w3.org/1999/xhtml">
		 <head>
		  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		  <title>Přihláška</title>
		  <style type="text/css">
		  table h4 { margin: 1em 0; }
		  
		  h1, h2, h3, h4, h5, strong, b { color: #CA4C6E; }
		  a { color: #CA4C6E;}
		  td { padding: 5px 20px 5px 10px;}
		  hr { border: 0; border-bottom: 1px solid #CA4C6E; }
		  </style>
		</head>
		<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td style="background-color:#FBE5DD;padding: 20px;">
		
		
		
		<table style="width:800px;border: 1px solid #CA4C6E;background-color:#FFFFFF;" align="center">
		<tr>
		<td style="text-align:right; padding: 10px;">
			
			<a href="'.home_url().'"><img src="'.get_template_directory_uri().'/img/logo-125.png" width="125" height="87" alt="Jemné zrození" style="border: 0;"></a>
		
		</td>
		</tr>
		<tr>
		<td style="padding: 20px;color: #6a6a6a;font-family:sans-serif;background-image:url('.get_template_directory_uri().'/img/firemni-papir.jpg);background-repeat:no-repeat;background-position:left bottom;background-size:contain">
		';

	return $html;
}

function jz_konec_html_emailu() {
	$html = '
	
	</td>
	</tr>
	</table>
	
	</td>
	</tr>
	</table>
	
	</body>
	</html>';

	return $html;
}







/*
	$data = array()
		user_login ... vzdy
		user_pass ... jen pri vytvoreni uctu
		prilohy = array() ... mozna
*/
function prihlaska_email_se_shrnutim( $prihlaska_id, $data = array() ) {

	$user_login = !empty( $data['user_login'] ) ? $data['user_login'] : '';
	$user_pass = !empty( $data['user_pass'] ) ? $data['user_pass'] : '';
	$prilohy = !empty( $data['prilohy'] ) ? $data['prilohy'] : array();

	$prihlaska_jmeno = get_post_meta($prihlaska_id,'prihlaska-jmeno', true).' '.get_post_meta($prihlaska_id,'prihlaska-prijmeni', true);
	$prihlaska_email = get_post_meta($prihlaska_id,'prihlaska-email', true);


	$registracni_email = get_field('email_pro_prijem_prihlasek', 'options');

	if( !$registracni_email )
		$registracni_email = JZ_KONTAKTNI_EMAIL;

	$predmet = get_field('souhrn_prihlasky_predmet', 'options');
	$sablona = get_field('souhrn_prihlasky_sablona', 'options');




	// ---------- souhrn

	$souhrn_prihlasky = jz_informace_o_prihlasce( $prihlaska_id, true );

	$sablona = str_replace( '<p>SOUHRN_PRIHLASKY</p>', 'SOUHRN_PRIHLASKY', $sablona);
	$sablona = str_replace( 'SOUHRN_PRIHLASKY', $souhrn_prihlasky, $sablona);


	// ---------- platebni pokyny

	$platebni_pokyny = '';

	$soukromy_kurz = get_post_meta( $prihlaska_id, 'prihlaska-soukromy-kurz', true );

	$zpusob_platby = get_post_meta( $prihlaska_id, 'prihlaska-platba', true );

	$prihl_castka = get_post_meta( $prihlaska_id, 'prihlaska-castka', true );
	$prihl_mena = get_post_meta( $prihlaska_id, 'prihlaska-mena', true );

	if( $soukromy_kurz == 'yes' ) {

		$platebni_pokyny .= '<p>'.__('Přihláška bude uhrazena dle domluvy s lektorkou.','jz').'</p>';

	} else if( $prihl_castka == 0 ) {

		$platebni_pokyny .= '<p>'.__('Přihláška je uhrazená.','jz').'</p>';

	} else if( $zpusob_platby == 'on-line' ) {

		$platebni_pokyny .= '<p>'.__('Přihláška bude uhrazena on-line na platební bráně.','jz').'</p>';

	} else if( $zpusob_platby == 'bankovni-prevod' ) {

		$cislo_uctu = get_post_meta( $prihlaska_id, 'prihlaska-platba-cislo-uctu', true );
		$variabilni_symbol = prihlaska_vs( $prihlaska_id );

		$castka = $prihl_castka.' '.jz_mena_znacka( $prihl_mena );

		$platebni_pokyny .= '<p>'.__('Informace pro bankovní převod').':</p>';
		$platebni_pokyny .= '<p>';;
		$platebni_pokyny .= '<strong>'.__('Číslo účtu','jz').':</strong> ' .$cislo_uctu .'<br>';
		$platebni_pokyny .= '<strong>'.__('Variabilní symbol','jz').':</strong> ' .$variabilni_symbol .'<br>';
		$platebni_pokyny .= '<strong>'.__('Částka','jz').':</strong> ' .$castka .'<br>';
		$platebni_pokyny .= '<p>';

	}

	$prihl_slevovy_poukaz = get_post_meta( $prihlaska_id, 'prihlaska-slevovy-poukaz', true );

	if( $prihl_slevovy_poukaz ) {
		$platebni_pokyny .= '<p>'.__('Slevový poukaz:','jz').' '.$prihl_slevovy_poukaz.'</p>';
	}



	$sablona = str_replace( '<p>PLATEBNI_POKYNY</p>', 'PLATEBNI_POKYNY', $sablona);
	$sablona = str_replace( 'PLATEBNI_POKYNY', $platebni_pokyny, $sablona);


	// ---------- uzivatelsky ucet

	$uzivatelsky_ucet = '<p>';
	$uzivatelsky_ucet .= '<strong>'.__('Přihlašovací e-mail','jz').':</strong> '.$user_login.'<br>';

	if( $user_pass ) {
		$uzivatelsky_ucet .= '<strong>'.__('Přihlašovací heslo','jz').':</strong> '.$user_pass.'<br>';
	} else {
		$uzivatelsky_ucet .= '<strong>'.__('Přihlašovací heslo','jz').':</strong> <em>'.__('nastaveno někdy dříve (můžete si nechat zaslat nové na odkazu níže)','jz').'</em><br>';
	}

	$uzivatelsky_ucet .= '<strong>'.__('Adresa pro přihlášení','jz').':</strong> <a href="'.wc_get_page_permalink( 'myaccount' ).'">'.wc_get_page_permalink( 'myaccount' ).'</a>';

	$uzivatelsky_ucet .= '</p>';


	$sablona = str_replace( '<p>UZIVATELSKY_UCET</p>', 'UZIVATELSKY_UCET', $sablona);
	$sablona = str_replace( 'UZIVATELSKY_UCET', $uzivatelsky_ucet, $sablona);


	$html_email_zakaznik =  jz_zacatek_html_emailu() . $sablona . jz_konec_html_emailu();





	// admin email
	$html_email_admin = '<p><strong>Nová přihláška.</strong></p>
		
		<p>Odeslána: '.current_time('H:i j. n. Y').'</p>
		
		<p><a href="'.home_url( '/wp-admin/post.php?post='.$prihlaska_id.'&action=edit' ).'">Přihláška v administraci</a></p>
		<hr />';

	$html_email_admin .= $sablona;



	// =============== Prilohy?

	$attachments = $prilohy;


	// =============== EMAIL DO FIRMY
	$to = $registracni_email;

	$subject =  'Nová přihláška - '.$prihlaska_jmeno;
	$lektorka_email = get_field('email', get_field('lektorka', get_field('prihlaska-termin', $prihlaska_id)));

	$headers = array();
	$headers[] = 'Content-Type: text/html; charset=UTF-8';
	$headers[] = 'From: '.$prihlaska_jmeno.' <'.$registracni_email.'>';
	$headers[] = 'Cc: '.$lektorka_email;
	$headers[] = 'Reply-To: <'.$prihlaska_email.'>';

	$html_email_admin = jz_zacatek_html_emailu() . $html_email_admin . jz_konec_html_emailu();

	if( ! wp_mail( $to, $subject, $html_email_admin, $headers, $attachments ) ) {
		// no, nenadelam nic
	}



	// =============== EMAIL ZAKAZNIKOVI
	$to = $prihlaska_email;

	$headers = array();
	$headers[] = 'Content-Type: text/html; charset=UTF-8';
	$headers[] = 'From: '.get_bloginfo('name').' <'.$registracni_email.'>';
	$headers[] = 'Reply-To: <'.$lektorka_email.'>';

	if( ! wp_mail( $to, $predmet, $html_email_zakaznik, $headers, $attachments ) ) {

		return false;

	}

	return true;

}





















/*
88888888888  88    ,ad8888ba,
88           88   d8"'    `"8b
88           88  d8'        `8b
88aaaaa      88  88          88
88"""""      88  88          88
88           88  Y8,        ,8P
88           88   Y8a.    .a8P
88           88    `"Y8888Y"'
*/


function fio_merchant() {

	require __DIR__.'/fio/Merchant.php';

	return new Merchant( FIO_SERVER_URL, FIO_CERT_PATH, FIO_CERT_PASSWORD, 1);

}





function fio_id_prihlasky_pres_trans_id( $trans_id ) {

	global $wpdb;

	return $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key='prihlaska-fio-trans-id' AND meta_value=%s", $trans_id ) );

}








function fio_navrat_z_platebni_brany() {


    if(strpos($_SERVER["REQUEST_URI"],'fio-ok') !== false){
        error_log('PLATBA ' . $_SERVER["REQUEST_URI"] , 0);
        error_log(json_encode($_SERVER['REQUEST_METHOD']), 0);
        error_log(json_encode($_REQUEST), 0);
        error_log(json_encode($_POST), 0);
        error_log(json_encode($_GET), 0);
    }




	if( !$_POST['trans_id'])	return;

	$_SESSION['prihlaska-formular-info'] = '';

	$trans_id = $_POST['trans_id'];

	$prihlaska_id = fio_id_prihlasky_pres_trans_id( $trans_id );

	$termin_id = prihlaska_id_terminu( $prihlaska_id ) ;

	$return_url  = get_permalink( $termin_id );

	$ip = $_SERVER['REMOTE_ADDR'];
	//$ip = '192.168.1.2';

	$merchant = fio_merchant();

	$response = $merchant->getTransResult( urlencode($trans_id), $ip );

	if( isset( $_POST['error'] ) ) { // je to ERROR stav!

		$message = 'Error: '.$_POST['error'].' (response: '.$response.')';

		update_post_meta( $prihlaska_id, 'prihlaska-fio-result', 'ERROR' );

		update_post_meta( $prihlaska_id, 'prihlaska-fio-error', $message );

		$_SESSION['prihlaska-formular-info'] .= '<p class="chyba">'. sprintf( __('Bohužel došlo k chybě při při komunikaci s platební bránou.
													Kontaktujte nás prosím na e-mailu %s. Za komplikace se omlouváme.','jz'), '<a href="mailto:'.JZ_KONTAKTNI_EMAIL.'">'.JZ_KONTAKTNI_EMAIL.'</a>' ) .'</p>';

		zmena_stavu_prihlasky( $prihlaska_id, 'odmitnuto' );

		fio_log_chyby( 'ON-LINE PLATBA', array( array('error' => 'Úhrada přihlášky <a href="post.php?post='.$prihlaska_id.'&action=edit"><strong>'.$prihlaska_id.'</strong></a> byla platební bránou zamítnuta! <em>Response: ' .$response.'</em>' ) ) );

	} else { // OK stav --------------------------------

		$result = $result_code = '';

		if (strstr($response, 'RESULT:')) {
			$result = explode('RESULT: ', $response);
			$result = preg_split( '/\r\n|\r|\n/', $result[1] );
			$result = $result[0];
		}

		if (strstr($response, 'RESULT_CODE:')) {
			$result_code = explode('RESULT_CODE: ', $response);
			$result_code = preg_split( '/\r\n|\r|\n/', $result_code[1] );
			$result_code = $result_code[0];
		}

		update_post_meta( $prihlaska_id, 'prihlaska-fio-result', $result );
		update_post_meta( $prihlaska_id, 'prihlaska-fio-result-code', $result_code );
		update_post_meta( $prihlaska_id, 'prihlaska-fio-response', $response );

		$stav = $result == 'OK' ? 'uhrazeno' : 'odmitnuto';

		zmena_stavu_prihlasky( $prihlaska_id, $stav );


		if( $result == 'OK' ) {

			$_SESSION['prihlaska-formular-info'] .= '<h2 class="uspech">'. __('Přihláška byla úspěšně uhrazena!','jz').'</h2>';

			fio_log_chyby( 'ON-LINE PLATBA', array( array('success' => '<strong>Přihláška <a href="post.php?post='.$prihlaska_id.'&action=edit">'.$prihlaska_id.'</a> 
							úspěšně uhrazena na platební bráně.</strong> <em>Response: ' .$response.'</em>' ) ) );

		} else {

			$_SESSION['prihlaska-formular-info'] .= '<p class="chyba">'. sprintf( __('Bohužel, přihláška nebyla na platební bráně uhrazena.
														Kontaktujte nás prosím na e-mailu %s. Za komplikace se omlouváme.','jz'), '<a href="mailto:'.JZ_KONTAKTNI_EMAIL.'">'.JZ_KONTAKTNI_EMAIL.'</a>' ) .'</p>';

			fio_log_chyby( 'ON-LINE PLATBA', array( array('error' => 'Přihláška <a href="post.php?post='.$prihlaska_id.'&action=edit">'.$prihlaska_id.'</strong></a> 
							nebyla na platební bráně uhrazena! <em>Response: ' .$response.'</em>' ) ) );

		}


	}

	// presmeruju na stranku prihlasky tak jako tak

	wp_redirect( $return_url ); exit;

}

add_action( 'init', 'fio_navrat_z_platebni_brany' );























/*


function fio_cron_init ( ) {

	if ( !wp_next_scheduled( 'fio_close_business_day' ) ) {
		wp_schedule_event( strtotime('23:58:00'), 'daily', 'fio_close_business_day' );
	}
}
add_action( 'init', 'fio_cron_init' );






function fio_close_business_day_job() {

	$merchant = fio_merchant();

	$response = $merchant->closeDay();

	$message = 'CLOSE BUSINESS DAY: '.$response;

	fio_log( $message, true );

}
add_action( 'fio_close_business_day', 'fio_close_business_day_job' );

*/





















/*
88888888ba   88888888ba   88  88        88  88                    88888888888         db         88      a8P   888888888888  88        88  88888888ba          db
88      "8b  88      "8b  88  88        88  88                    88                 d88b        88    ,88'         88       88        88  88      "8b        d88b
88      ,8P  88      ,8P  88  88        88  88                    88                d8'`8b       88  ,88"           88       88        88  88      ,8P       d8'`8b
88aaaaaa8P'  88aaaaaa8P'  88  88aaaaaaaa88  88                    88aaaaa          d8'  `8b      88,d88'            88       88        88  88aaaaaa8P'      d8'  `8b
88""""""'    88""""88'    88  88""""""""88  88                    88"""""         d8YaaaaY8b     8888"88,           88       88        88  88""""88'       d8YaaaaY8b
88           88    `8b    88  88        88  88                    88             d8""""""""8b    88P   Y8b          88       88        88  88    `8b      d8""""""""8b
88           88     `8b   88  88        88  88           888      88            d8'        `8b   88     "88,        88       Y8a.    .a8P  88     `8b    d8'        `8b
88           88      `8b  88  88        88  88888888888  888      88           d8'          `8b  88       Y8b       88        `"Y8888Y"'   88      `8b  d8'          `8b
*/



function prihlaska_zalohova_faktura( $prihlaska_id ) {

	return prihlaska_vystaveni_faktury( $prihlaska_id, 'proforma' );

}

function fapi_uhrazena_faktura( $prihlaska_id ) {

	$invoice_id = get_post_meta( $prihlaska_id, 'fapi_invoice_id', 1);
	$termin_id = get_post_meta( $prihlaska_id, 'prihlaska-termin', true );

	$data =  array(
	    'paid' => true,
	);
	$credentials = lektorka_fapi_credentials($termin_id);
	$login = $credentials['login'];
	$heslo = $credentials['heslo'];



	$json = file_get_contents('http://api.fapi.cz/invoices/'.$invoice_id, false, stream_context_create(array(
		'http' => array(
			'method' => 'PUT',
			'header' => array(
				'Authorization: Basic ' . base64_encode("$login:$heslo"),
				'Accept: application/json',
				'Content-Type: application/json'
			),
			'content' => json_encode($data)
		)
	)));
}

function iDoklad_uhrazena_faktura( $prihlaska_id ) {

	$termin_id = get_post_meta( $prihlaska_id, 'prihlaska-termin', true );
	$prihlaska_email = get_post_meta( $prihlaska_id, 'prihlaska-email', true );
	$kurz_id = get_post_meta( $prihlaska_id, 'prihlaska-kurz', true );
	$credentials = lektorka_iDoklad_credentials($termin_id);

	$iDoklad = new iDoklad($credentials['login'],  $credentials['heslo'], site_url());
	$iDoklad->authCCF();

	$contacts = new iDokladRequest('Contacts');
	$response = $iDoklad->sendRequest($contacts);

	if($response->getCode() === 200){
		$purchaserId = iDokladSearchForEmail($prihlaska_email, $response->getData());
	}

	if(!isset($purchaserId)){

		$country_ids = array( 'CZ' => 2, 'SK' => 1 );
		$prihlaska_stat = get_post_meta( $prihlaska_id, 'prihlaska-stat', true );
		$contact = array(
			"CompanyName" => get_post_meta( $prihlaska_id, 'prihlaska-jmeno', true ) . ' ' .get_post_meta( $prihlaska_id, 'prihlaska-prijmeni', true ),
			"CountryId" => $country_ids[$prihlaska_stat],
			"City"=> get_post_meta( $prihlaska_id, 'prihlaska-mesto', true ),
			"Email"=> $prihlaska_email,
			"Firstname"=> get_post_meta( $prihlaska_id, 'prihlaska-jmeno', true ),
			"Phone"=> get_post_meta( $prihlaska_id, 'prihlaska-telefon', true ),
			"PostalCode"=> get_post_meta( $prihlaska_id, 'prihlaska-psc', true ),
			"Street"=> get_post_meta( $prihlaska_id, 'prihlaska-ulice-cp', true ),
			"Surname"=> get_post_meta( $prihlaska_id, 'prihlaska-prijmeni', true )
		);
		$contacts->addMethodType('POST');
		$contacts->addPostParameters($contact);
		$response = $iDoklad->sendRequest($contacts);
		$purchaserId = $response->getData()['Id'];
	}

	$faktura = new iDokladRequest('IssuedInvoices');
	if ($purchaserId) {
		$vs = get_post_meta( $prihlaska_id, 'prihlaska-vs', true );

		// $castka je castka, ktere bylo nakonec po zakaznikovi pozadovano ...
		$castka = get_post_meta( $prihlaska_id, 'prihlaska-castka', true );

		if( $castka == 0 ) { // na "bezplatnou" prihlasku fakturu nevystavuji
			return false;
		}

		$jednotkova_castka_kurzu = $castka;

		$radek_se_slevou = null;

		// pokud je nejaka "puvodni castka", tak to znamena, ze "castka" je snizena cena (akcni, nebo slevovym poukazem atd.)
		$puvodni_castka = get_post_meta( $prihlaska_id, 'prihlaska-puvodni-castka', true );

		if( $puvodni_castka && $puvodni_castka != $castka ) {

			$jednotkova_castka_kurzu = $puvodni_castka;

			// v $castka je nyni cena po nejake sleve

			$sleva_castka = $jednotkova_castka_kurzu - $castka; // rozdil normalni ceny kurzu a castky, co se platila

			$radek_se_slevou = array(
				"Amount" => 1.0,
				"Name" => __('Sleva','jz'),
				"PriceType" => 0,
				"VatRateType" => 1,
				"UnitPrice" => $sleva_castka * -1 ,
			);	

		}

		if(get_post_meta( $prihlaska_id, 'prihlaska-typ', true) == 'jednotlivec'){
			$note = get_field('superfaktura_poznamka_jednotlivec', $kurz_id);
		} else {
			$note = get_field('superfaktura_poznamka_par', $kurz_id);
		}

		$polozky[] = array(
			"Amount" => 1.0,
			"Name" => get_field('nazev_kurzu_ve_fakture', $kurz_id) ?? 'Kurz',
			"PriceType" => 0,
			"VatRateType" => 1,
			"UnitPrice" => $jednotkova_castka_kurzu,
		);

		if( $radek_se_slevou ) {
			$polozky[] = $radek_se_slevou;
		}
		
		try {
			$invoice_data['payment_type'] = 'transfer';

			$cislo_uctu = get_post_meta( $prihlaska_id, 'prihlaska-platba-cislo-uctu', true );

			$ucet_exploded = explode('/', $cislo_uctu );


			$bankName = '';
			$BankId = 0;

			$banks = new iDokladRequest('Banks');
			$response = $iDoklad->sendRequest($banks);
		
			foreach($response->getData() as $account) {
				
				if($account["NumberCode"] == $ucet_exploded[1]) {
					$bankName = $account["Name"];
					$BankId = $account["Id"];
				}
			}

			$data = array(
				"IssuedInvoiceItems" => $polozky,
				"ConstantSymbolId" => 7,
				"ItemsTextPrefix" => $note,
				"ItemsTextSuffix" => 'Prosím neplaťte, faktura je již uhrazená!!!',
				"Note" => 'Jemné zrození VS '. $vs,
				"OrderNumber" => $vs,
				"PaymentOptionId" => 1,
				"PurchaserId" => $purchaserId,
				"VariableSymbol" => $vs,
				"AccountNumber" => $ucet_exploded[0],
				"BankId" => $BankId,
				"BankName" => $bankName,
				"BankNumberCode" => $ucet_exploded[1]
			);

			$faktura->addMethodType('POST');
			$faktura->addPostParameters($data);

			$response_proforma = $iDoklad->sendRequest($faktura);

			$invoice = $response_proforma->getData();
			update_post_meta( $prihlaska_id, 'iDoklad_invoice_id', $invoice['Id'] );

			$pay_regular = new iDokladRequest('IssuedInvoices/'.$invoice['Id'].'/FullyPay');
			$pay_regular->addMethodType('PUT');
			$response_regular = $iDoklad->sendRequest($pay_regular);

			$email_proforma= new iDokladRequest('IssuedInvoices/'.$invoice['Id'].'/SendMailToPurchaser');
			$email_proforma->addMethodType('PUT');
			$response_email_proforma = $iDoklad->sendRequest($email_proforma);

			
		} catch (Exception $e) {
			update_post_meta( $prihlaska_id, 'iDoklad_error',$e->getMessage() );
		}
	}
}


function prihlaska_faktura( $prihlaska_id ) {

	return prihlaska_vystaveni_faktury( $prihlaska_id, 'regular' );

}



function fapi_zalohova_faktura ( $prihlaska_id ) {
	if( ! class_exists('FAPIClient') ) {
		require_once __DIR__.'/FAPIClient/FAPIClient.php';
	}
	$termin_id = get_post_meta( $prihlaska_id, 'prihlaska-termin', true );
	$prihlaska_email = get_post_meta( $prihlaska_id, 'prihlaska-email', true );
	$kurz_id = get_post_meta( $prihlaska_id, 'prihlaska-kurz', true );
	$credentials = lektorka_fapi_credentials($termin_id);
	$fapi = new FAPIClient($credentials['login'], $credentials['heslo']);

	$client = $fapi->client->search(array( 'email' => $prihlaska_email));

	if (!count($client['clients'])) {

		$country_ids = array( 'CZ' => 57, 'SK' => 191 );

		$prihlaska_stat = get_post_meta( $prihlaska_id, 'prihlaska-stat', true );

		$client = $fapi->client->create([
		    'first_name' => get_post_meta( $prihlaska_id, 'prihlaska-jmeno', true ),
		    'last_name' => get_post_meta( $prihlaska_id, 'prihlaska-prijmeni', true ),
		    'email' => get_post_meta( $prihlaska_id, 'prihlaska-email', true ),
		    'phone' => get_post_meta( $prihlaska_id, 'prihlaska-telefon', true ),
		    'address' => [
		        'street' => get_post_meta( $prihlaska_id, 'prihlaska-ulice-cp', true ),
		        'city' => get_post_meta( $prihlaska_id, 'prihlaska-mesto', true ),
		        'zip' => get_post_meta( $prihlaska_id, 'prihlaska-psc', true ),
		        'country' => $prihlaska_stat,
		    ]
		]);

		$clientID = $client['id'];
	} else {
		$clientID = $client['clients'][0]['id'];
	}

	if ($clientID) {
		$vs = get_post_meta( $prihlaska_id, 'prihlaska-vs', true );

		// $castka je castka, ktere bylo nakonec po zakaznikovi pozadovano ...
		$castka = get_post_meta( $prihlaska_id, 'prihlaska-castka', true );

		if( $castka == 0 ) { // na "bezplatnou" prihlasku fakturu nevystavuji
			return false;
		}

		$jednotkova_castka_kurzu = $castka;

		$radek_se_slevou = null;

		// pokud je nejaka "puvodni castka", tak to znamena, ze "castka" je snizena cena (akcni, nebo slevovym poukazem atd.)
		$puvodni_castka = get_post_meta( $prihlaska_id, 'prihlaska-puvodni-castka', true );

		if( $puvodni_castka && $puvodni_castka != $castka ) {

			$jednotkova_castka_kurzu = $puvodni_castka;

			// v $castka je nyni cena po nejake sleve

			$sleva_castka = $jednotkova_castka_kurzu - $castka; // rozdil normalni ceny kurzu a castky, co se platila


			// Takze ... logika je, ze na fakture nesmi byt uvedeno neco jako Darkovy poukaz a spol.
			// Bude tam maximalne 2. polozka oznacena jako Sleva (a zaporna castka), coz je rozdil puvodni ceny kurzu a uctovane ceny
			// Coz je fajn, tim padem nemusim nic komplikovane vyhodnocovat, staci mi tam uvest rozdil puvodni a soucasne castky

			$sleva_castka_bez_dph =  round( ($sleva_castka - ( $sleva_castka * SF_TAX_KOEFICIENT ) ), 4 );

			$radek_se_slevou = array(
				'name' => __('Sleva','jz'),      // název
	            'description' => '', // popis (nepovinné pole)
	            'price' => ( $credentials['vat']? $sleva_castka_bez_dph * -1 : $sleva_castka * -1 ),     // cena
	            'vat' => 21,              // sazba DPH (pouze pro plátce DPH)
	            'count' => 1,             // počet
			);

		}

		$jednotkova_castka_kurzu_bez_dph = round( ($jednotkova_castka_kurzu - ( $jednotkova_castka_kurzu * SF_TAX_KOEFICIENT ) ), 4 );

		if(get_post_meta( $prihlaska_id, 'prihlaska-typ', true) == 'jednotlivec'){
			$note = get_field('superfaktura_poznamka_jednotlivec', $kurz_id);
		} else {
			$note = get_field('superfaktura_poznamka_par', $kurz_id);
		}

		$polozky[] = array(
			'name' => ' - ' . get_field('nazev_kurzu_ve_fakture', $kurz_id) ,      // název
            'description' => $note, // popis (nepovinné pole)
            'price' => $credentials['vat']? $jednotkova_castka_kurzu_bez_dph : $jednotkova_castka_kurzu,        // cena
            'vat' => 21,              // sazba DPH (pouze pro plátce DPH)
            'count' => 1,             // počet
		);


		if( $radek_se_slevou ) {
			$polozky[] = $radek_se_slevou;
		}

		try {

			$request = [
			    'client' => $clientID,        // ID klienta   
			    'proforma' => true,               // jedná se o zálohovou fakturu?
			    'reverse_charge' => false,         // jedná se o fakturu v režimu přenesené daňové povinnosti?
			    'notes' => 'Jemné zrození VS '. $vs,            // interní poznámka k faktuře (nepovinné pole)
			    'currency' => 'CZK',              // kód měny dle normy ISO 4217
			    'items' => $polozky,
			    'payment_type'=> 'wire',
			    'notify' => true,
				'paid' => false,
			    'variable_symbol' => $vs
			];

			if ($credentials['vat']) {
				$request['vat_date'] = current_time('Y-m-d');
			}

			$invoice = $fapi->invoice->create($request);
			update_post_meta( $prihlaska_id, 'fapi_invoice_id',$invoice['id'] );

		} catch (Exception $e) {
			update_post_meta( $prihlaska_id, 'fapi_error',$e->getMessage() );
		}
	}
}

function iDokladSearchForEmail($email, $array) {
	foreach ($array as $val) {
		if ($val['Email'] === $email) {
			return $val['Id'];
		}
	}
	return null;
}

/*
	Vraci:
	- false ... prihlaska na nulovou castku
	- wp_error ... nejaka chyba
	- string ... cesta k pdf fakture (jejiz soubor musim unlinknout!!!)
*/


function prihlaska_vystaveni_faktury( $prihlaska_id, $typ_faktury ) {
	
	if( ! class_exists('SFAPIclient') ) {
		require_once __DIR__.'/superfaktura/SFAPIclient.php';
	}
	$termin_id = get_post_meta( $prihlaska_id, 'prihlaska-termin', true );
	$kurz_id = get_post_meta( $prihlaska_id, 'prihlaska-kurz', true );
	$credentials = lektorka_sf_credentials($termin_id);
	$lektorka_post_id = get_field('lektorka', $termin_id);

	$lektorka_user = get_field( 'uzivatelsky_ucet_lektorky',  $lektorka_post_id);
	
	$sf_api = new SFAPIclientCZ( $credentials['login'], $credentials['heslo'], 'JZKURZY', 'API',  $credentials['company'] );

	$country_ids = array( 'CZ' => 57, 'SK' => 191 );

	$prihlaska_stat = get_post_meta( $prihlaska_id, 'prihlaska-stat', true );

	$country_id = 57; // CZ default

	if( isset( $country_ids[ $prihlaska_stat ] ) ) {
		$country_id = $country_ids[ $prihlaska_stat ];
	}


	// set client for new invoice
	$sf_api->setClient(array(
		'name' => get_post_meta( $prihlaska_id, 'prihlaska-jmeno', true ) . ' '.  get_post_meta( $prihlaska_id, 'prihlaska-prijmeni', true ),
		'address' => get_post_meta( $prihlaska_id, 'prihlaska-ulice-cp', true ),
		'zip' => get_post_meta( $prihlaska_id, 'prihlaska-psc', true ),
		'city' => get_post_meta( $prihlaska_id, 'prihlaska-mesto', true ),
		'country_id' => $country_id,
		'email' => get_post_meta( $prihlaska_id, 'prihlaska-email', true ),
		'phone' => get_post_meta( $prihlaska_id, 'prihlaska-telefon', true ),
	));




	$typ_prihlasky = get_post_meta( $prihlaska_id, 'prihlaska-typ', true );

	$poznamka = get_post_meta( $kurz_id, 'superfaktura_poznamka_'.$typ_prihlasky, true );;

	$datum_splatnosti = $typ_faktury == 'regular' ? current_time('Y-m-d') : date('Y-m-d', strtotime('+7 days') );
	
	$invoice_data = array(
		'name' => $typ_faktury == 'proforma' ? 'Zálohová faktura' : 'Faktura',
		'variable' => get_post_meta( $prihlaska_id, 'prihlaska-vs', true ),
		'invoice_currency' => mb_strtoupper( get_post_meta( $prihlaska_id, 'prihlaska-mena', true ), 'utf-8' ),
		'payment_type' => 'transfer',
		'type' => $typ_faktury,
		'due' =>  $datum_splatnosti,
		'comment' => "\n". $poznamka,
		'issued_by' => $lektorka_user['display_name'],
		'issued_by_phone' => get_field( 'telefonni_cislo',  $lektorka_post_id),
		'issued_by_email' => $lektorka_user['user_email'],
		'issued_by_web' =>'www.jemnezrozeni.cz',
	);


	// jaky bankovni ucet?

	$typ_platby = get_post_meta( $prihlaska_id, 'prihlaska-platba', true );

	if( $typ_platby == 'bankovni-prevod' ) {

		$invoice_data['payment_type'] = 'transfer';

		$cislo_uctu = get_post_meta( $prihlaska_id, 'prihlaska-platba-cislo-uctu', true );

		$ucet_exploded = explode('/', $cislo_uctu );

		$invoice_data['bank_accounts'] = array(
			'account' => $ucet_exploded[0],
			'bank_code' => $ucet_exploded[1],
		);

	

	} else if( $typ_platby == 'on-line' ) {

		$invoice_data['payment_type'] = 'credit';

	}

	// TODO VAT, ceny a č- u lektorky

	if( $typ_faktury == 'regular' ) {

		$invoice_data['already_paid'] = 1;

		// jestli je to regulerni faktura, tak doplnim info o proforma fakture ... ktera by tedy mela existovat z drivejska
		$proforma_id = get_post_meta( $prihlaska_id, 'prihlaska-superfaktura-proforma-id', true );

		if( $proforma_id ) {

			$invoice_data['proforma_id'] = $proforma_id;

		}

	}

	$sf_api->setInvoice( $invoice_data );




	// $castka je castka, ktere bylo nakonec po zakaznikovi pozadovano ...
	$castka = get_post_meta( $prihlaska_id, 'prihlaska-castka', true );

	if( $castka == 0 ) { // na "bezplatnou" prihlasku fakturu nevystavuji
		return false;
	}


	$jednotkova_castka_kurzu = $castka;

	$radek_se_slevou = null;

	// pokud je nejaka "puvodni castka", tak to znamena, ze "castka" je snizena cena (akcni, nebo slevovym poukazem atd.)
	$puvodni_castka = get_post_meta( $prihlaska_id, 'prihlaska-puvodni-castka', true );

	if( $puvodni_castka && $puvodni_castka != $castka ) {

		$jednotkova_castka_kurzu = $puvodni_castka;

		// v $castka je nyni cena po nejake sleve

		$sleva_castka = $jednotkova_castka_kurzu - $castka; // rozdil normalni ceny kurzu a castky, co se platila


		// Takze ... logika je, ze na fakture nesmi byt uvedeno neco jako Darkovy poukaz a spol.
		// Bude tam maximalne 2. polozka oznacena jako Sleva (a zaporna castka), coz je rozdil puvodni ceny kurzu a uctovane ceny
		// Coz je fajn, tim padem nemusim nic komplikovane vyhodnocovat, staci mi tam uvest rozdil puvodni a soucasne castky

		$sleva_castka_bez_dph =  round( ($sleva_castka - ( $sleva_castka * SF_TAX_KOEFICIENT ) ), 4 );

		$radek_se_slevou = array(
			'name' => __('Sleva','jz'),
			'description' => '',
			'unit_price' => ( $credentials['vat']? $sleva_castka_bez_dph * -1 : $sleva_castka * -1 ),
			'quantity' => 1,
			'tax' => ( $credentials['vat']? SF_TAX : 0 )
		);

	}


	$jednotkova_castka_kurzu_bez_dph = round( ($jednotkova_castka_kurzu - ( $jednotkova_castka_kurzu * SF_TAX_KOEFICIENT ) ), 4 );

	$sf_api->addItem(array(
		'name' => __('Kurz','jz').' - '. get_the_title( $kurz_id ) ,
		'description' => '',
		'unit_price' => ( $credentials['vat']? $jednotkova_castka_kurzu_bez_dph : $jednotkova_castka_kurzu ) ,
		'quantity' => 1,
		'tax' => ( $credentials['vat']? SF_TAX : 0 )
	));


	if( $radek_se_slevou ) {
		$sf_api->addItem( $radek_se_slevou );
	}




	// save invoice in SuperFaktura
	$response = $sf_api->save();

	if( $response->error === 0 ) { // uspesne ulozeno?

		$data = $response->data;

		$invoice_id = $data->Invoice->id;
		$invoice_number = $data->Invoice->invoice_no_formatted;
		$invoice_token = $data->Invoice->token;

		if( $typ_faktury == 'regular' ) {

			update_post_meta( $prihlaska_id, 'prihlaska-superfaktura-regular-id', $invoice_id );
			update_post_meta( $prihlaska_id, 'prihlaska-superfaktura-regular-number', $invoice_number );
			update_post_meta( $prihlaska_id, 'prihlaska-superfaktura-regular-token', $invoice_token );

			$file_prefix = 'fa_';

		} else {

			update_post_meta( $prihlaska_id, 'prihlaska-superfaktura-proforma-id', $invoice_id );
			update_post_meta( $prihlaska_id, 'prihlaska-superfaktura-proforma-number', $invoice_number );
			update_post_meta( $prihlaska_id, 'prihlaska-superfaktura-proforma-token', $invoice_token );

			$file_prefix = 'zal_';

		}


		// Rovnou stahnu PDF a ulozim jej do tempu

		require_once( ABSPATH . 'wp-admin/includes/file.php' );

		$download_url = 'https://moje.superfaktura.cz/invoices/pdf/'.$invoice_id.'/token:'.$invoice_token;

		$temp_filename = download_url( $download_url );

		if( is_wp_error( $temp_filename ) ) {

			prihlaska_faktura_notifikace_o_chybe( $prihlaska_id, 'sf_no_pdf' );

			return new WP_Error( 'sf_no_pdf', 'Nelze stáhnout PDF fakturu.' );

		} else {

			// potrebuji docasny soubor nejak vhodne prejmenovat ... aby byl jako priloha spravne pojmenovan
			$file = $file_prefix . 'JemneZrozeni_'.$invoice_number.'.pdf';
			$new_filename = dirname( $temp_filename ).'/'.$file;

			if( rename( $temp_filename, $new_filename ) ) { // proste to bud vyjde nebo ne ...

				$temp_filename = $new_filename;

			}

		}


		return $temp_filename; // uspech, vracime cestu k pdf fakture (nesmim zapomenout ji unlinknout!!!)

	} else {

		prihlaska_faktura_notifikace_o_chybe( $prihlaska_id, 'sf_response_error' );

		return new WP_Error( 'sf_response_error', $response->error_message );

	}

}












/*
	Odeslani finalni faktury pri prepnuti prihlasky na uhrazeno
*/
function prihlaska_odeslat_email_s_fakturou( $prihlaska_id, $faktura_file ) {

	$kurz_id = get_post_meta( $prihlaska_id, 'prihlaska-kurz', true );

	$text_emailu = get_field( 'superfaktura_sablona_emailu', $kurz_id );
	$predmet_emailu = get_field( 'superfaktura_predmet_emailu', $kurz_id );

	if( ! $text_emailu )
		$text_emailu = '<p>'. __('Dobrý den,<br>v příloze tohoto e-mailu Vám posíláme fakturu za úhradu Vaší přihlášky.<br><br>Tým Jemného Zrození','jz').'</p>';

	if( ! $predmet_emailu )
		$predmet_emailu = __('Vaše faktura','jz');

	$html_email_zakaznik =  jz_zacatek_html_emailu() . $text_emailu . jz_konec_html_emailu();

	$prihlaska_jmeno = get_post_meta($prihlaska_id,'prihlaska-jmeno', true).' '.get_post_meta($prihlaska_id,'prihlaska-prijmeni', true);
	$prihlaska_email = get_post_meta($prihlaska_id,'prihlaska-email', true);

	$to = $prihlaska_email;

	$registracni_email = get_field('email_pro_prijem_prihlasek', 'options');

	if( !$registracni_email )
		$registracni_email = JZ_KONTAKTNI_EMAIL;

	$headers = array();
	$headers[] = 'Content-Type: text/html; charset=UTF-8';
	$headers[] = 'From: '.get_bloginfo('name').' <'.$registracni_email.'>';

	$attachments = array( $faktura_file );

	if( ! wp_mail( $to, $predmet_emailu, $html_email_zakaznik, $headers, $attachments ) ) {

		// ?

	}

	unlink( $faktura_file );
}







function prihlaska_faktura_notifikace_o_chybe( $prihlaska_id, $kod_chyby ) {

	$to = JZ_KONTAKTNI_EMAIL;
	$lektorka_email = get_field('email', get_field('lektorka', get_field('prihlaska-termin', $prihlaska_id)));

	$headers = array();
	$headers[] = 'Content-Type: text/html; charset=UTF-8';
	$headers[] = 'From: '.get_bloginfo('name').' <'.$to.'>';

	$predmet_emailu = '['.get_bloginfo('name').'] Problém při vystavení faktury';
	$text_emailu = 'Dobrý den,

při automatickém vystavování faktury k přihlášce '.$prihlaska_id.' došlo k chybě: ';

	if ( $kod_chyby == 'sf_response_error' ) {

		$text_emailu .= ' Nepodařilo se spojit se SuperFaktura API, faktura nebyla vystavena.';

	} else if( $kod_chyby == 'sf_no_pdf' ) {

		$text_emailu .= ' Faktura byla vystavena, ale nepodařilo se vygenerovat její PDF a přiložit ji k e-mailu. Zákaznice o ní tedy neví.';

	}

	$headers[] = 'Bcc: info@storypress.cz';
	$headers[] = 'Cc: '. $lektorka_email;

	if( ! wp_mail( $to, $predmet_emailu, $text_emailu , $headers ) ) {

		// ?

	}


}



















































































/*
88888888ba   88888888ba   88  88        88  88                  db          ad88888ba   88      a8P   8b        d8
88      "8b  88      "8b  88  88        88  88                 d88b        d8"     "8b  88    ,88'     Y8,    ,8P
88      ,8P  88      ,8P  88  88        88  88                d8'`8b       Y8,          88  ,88"        Y8,  ,8P
88aaaaaa8P'  88aaaaaa8P'  88  88aaaaaaaa88  88               d8'  `8b      `Y8aaaaa,    88,d88'          "8aa8"
88""""""'    88""""88'    88  88""""""""88  88              d8YaaaaY8b       `"""""8b,  8888"88,          `88'
88           88    `8b    88  88        88  88             d8""""""""8b            `8b  88P   Y8b          88
88           88     `8b   88  88        88  88            d8'        `8b   Y8a     a8P  88     "88,        88
88           88      `8b  88  88        88  88888888888  d8'          `8b   "Y88888P"   88       Y8b       88



8b           d8             db         88888888ba,    88b           d88  88  888b      88  88        88
`8b         d8'            d88b        88      `"8b   888b         d888  88  8888b     88  88        88
 `8b       d8'            d8'`8b       88        `8b  88`8b       d8'88  88  88 `8b    88  88        88
  `8b     d8'            d8'  `8b      88         88  88 `8b     d8' 88  88  88  `8b   88  88        88
   `8b   d8'            d8YaaaaY8b     88         88  88  `8b   d8'  88  88  88   `8b  88  88        88
    `8b d8'            d8""""""""8b    88         8P  88   `8b d8'   88  88  88    `8b 88  88        88
     `888'            d8'        `8b   88      .a8P   88    `888'    88  88  88     `8888  Y8a.    .a8P
      `8'            d8'          `8b  88888888Y"'    88     `8'     88  88  88      `888   `"Y8888Y"'
*/








function jz_informace_o_prihlasce( $prihlaska_id, $email = false ) {

	ob_start();

	$termin_id = get_post_meta( $prihlaska_id, 'prihlaska-termin', true );

	tabulka_s_podrobnostmi_o_terminu( $termin_id );
	$kurz_id = get_field('kurz', $termin_id);
	$kurz = get_post( $kurz_id );

	$typ_kurzu = typ_kurzu_kod( $kurz );
	?>


	<table class="table-objednavka">
	<?php 
		if($typ_kurzu == 'online'): ?>
			<tr>
				<td colspan="2" class="tdnadpis">
					<h4><?php _e('Cena přihlášky','jz') ?></h4>
				</td>
			</tr>
			<tr>
				<td class="td1" style="width: 150px"><?php _e('Cena','jz') ?>:</td>
				<td class="td2">
				<?php
					$castka = get_post_meta( $prihlaska_id, 'prihlaska-castka', true);
					$mena = get_post_meta( $prihlaska_id, 'prihlaska-mena', true);
					$mena = jz_mena_znacka( $mena );
					echo '<strong>';
					echo $castka.' '.$mena;
					echo '</strong>';
				?>
				</td>
			</tr>
		<?php else: ?>
			<tr>
				<td colspan="2" class="tdnadpis">
					<h4><?php _e('Typ přihlášky','jz') ?></h4>
				</td>
			</tr>
			<tr>
				<td class="td1" style="width: 150px"><?php _e('Typ','jz') ?>:</td>
				<td class="td2">
				<?php

					$soukromy_kurz = get_post_meta( $prihlaska_id, 'prihlaska-soukromy-kurz', true );

					if( $soukromy_kurz == 'yes' ) {

						echo '<strong>'.__('Soukromý kurz','jz').'</strong>';

					} else {

						$typ = get_post_meta( $prihlaska_id, 'prihlaska-typ', true);
						$typ_popis = get_post_meta( $prihlaska_id, 'prihlaska-typ-popis', true);
						$castka = get_post_meta( $prihlaska_id, 'prihlaska-castka', true);
						$mena = get_post_meta( $prihlaska_id, 'prihlaska-mena', true);

						$mena = jz_mena_znacka( $mena );

						echo '<strong>';
						echo $typ_popis;
						echo ' ('.$castka.' '.$mena.')';
						echo '</strong>';

					}

				?>
				</td>
			</tr>
			<?php if( isset($typ) && $typ == 'par' ) { ?>
				<tr>
					<td class="td1"><?php _e('S kým přijdu','jz') ?>:</td>
					<td class="td2"><strong><?php echo esc_html(get_post_meta( $prihlaska_id, 'prihlaska-par-upresneni', true)) ?></strong></td>
				</tr>
			<?php } ?>

		<?php endif;
	?>

	<?php

	// info o slevovem poukazu, ale do emailu ho nedavam ...

	$slevovy_poukaz = get_post_meta( $prihlaska_id, 'prihlaska-slevovy-poukaz', true);

	if( $slevovy_poukaz && ! $email ) { ?>

		<tr>
			<td colspan="2" class="tdnadpis">
				<h4><?php _e('Slevový poukaz','jz') ?></h4>
			</td>
		</tr>
		<tr>
			<td class="td1"><?php _e('Kód','jz') ?>:</td>
			<td class="td2"><strong><?php echo esc_html( $slevovy_poukaz ) ?></strong></td>
		</tr>
		<tr>
			<td class="td1"><?php _e('Typ','jz') ?>:</td>
			<td class="td2">
			<strong>
			<?php
			$typ = get_post_meta( $prihlaska_id, 'prihlaska-slevovy-poukaz-typ', true );

			if( $typ == 'procentualni' ) {

				$odecist_procent = get_post_meta( $prihlaska_id, 'prihlaska-slevovy-poukaz-procentualni-sleva', true );

				echo '-'.$odecist_procent.' %';

			} else if( $typ == 'odecitaci' ) {

				$odecist = get_post_meta( $prihlaska_id, 'prihlaska-slevovy-poukaz-odecet', true );

				echo '-'.$odecist.' '.$mena;

			} else if( $typ == 'zdarma' ) {

				echo 'Kurz zdarma';

			}
			?>
			</strong>
			</td>
		</tr>

	<?php } ?>

	<tr>
		<td colspan="2" class="tdnadpis">
			<h4><?php _e('Osobní údaje','jz') ?></h4>
		</td>
	</tr>
	<tr>
		<td class="td1"><?php _e('Jméno','jz') ?>:</td>
		<td class="td2"><strong><?php echo esc_html(get_post_meta( $prihlaska_id, 'prihlaska-jmeno', true)) ?></strong></td>
	</tr>
	<tr>
		<td class="td1"><?php _e('Příjmení','jz') ?>:</td>
		<td class="td2"><strong><?php echo esc_html(get_post_meta( $prihlaska_id, 'prihlaska-prijmeni', true)) ?></strong></td>
	</tr>
	<tr>
		<td class="td1"><?php _e('Očekávaný termín porodu','jz') ?>:</td>
		<td class="td2"><strong><?php echo esc_html(get_post_meta( $prihlaska_id, 'prihlaska-ocekavany-termin-porodu', true)) ?></strong></td>
	</tr>
	<tr>
		<td class="td1"><?php _e('Čekám vícerčata','jz') ?>:</td>
		<td class="td2"><strong><?php echo esc_html(get_post_meta( $prihlaska_id, 'prihlaska-vicercata', true)) ?></strong></td>
	</tr>

	<tr>
		<td class="td1"><?php _e('Ulice, čp.','jz') ?>:</td>
		<td class="td2"><strong><?php echo esc_html(get_post_meta( $prihlaska_id, 'prihlaska-ulice-cp', true)) ?></strong></td>
	</tr>
	<tr>
		<td class="td1"><?php _e('Město','jz') ?>:</td>
		<td class="td2"><strong><?php echo esc_html(get_post_meta( $prihlaska_id, 'prihlaska-mesto', true)) ?></strong></td>
	</tr>
	<tr>
		<td class="td1"><?php _e('PSČ','jz') ?>:</td>
		<td class="td2"><strong><?php echo esc_html(get_post_meta( $prihlaska_id, 'prihlaska-psc', true)) ?></strong></td>
	</tr>

	<tr>
		<td class="td1"><?php _e('Stát','jz') ?>:</td>
		<td class="td2"><strong><?php

		$stat_meta = get_post_meta( $prihlaska_id, 'prihlaska-stat', true);

		$staty = include( WC()->plugin_path() . '/i18n/countries.php' );

		echo $staty[ $stat_meta ];

		?></strong></td>
	</tr>

	<tr>
		<td colspan="2" class="tdnadpis">
			<h4><?= __('Kontaktní informace', 'jz'); ?></h4>
		</td>
	</tr>
	<tr>
		<td class="td1"><?php _e('E-mail','jz') ?>:</td>
		<td class="td2"><strong><?php echo esc_html(get_post_meta( $prihlaska_id, 'prihlaska-email', true)) ?></strong></td>
	</tr>
	<tr>
		<td class="td1"><?php _e('Telefon','jz') ?>:</td>
		<td class="td2"><strong><?php echo esc_html(get_post_meta( $prihlaska_id, 'prihlaska-telefon', true)) ?></strong></td>
	</tr>





	<?php if( get_post_meta( $prihlaska_id , 'prihlaska-poznamka', true) ) { ?>
		<tr>
			<td colspan="2" class="tdnadpis">
				<h4><?php _e('Poznámka','jz') ?>:</h4>
			</td>
		</tr>
		<tr>
			<td class="td1"></td>
			<td class="td2"><strong><?php echo nl2br(esc_html(get_post_meta( $prihlaska_id, 'prihlaska-poznamka', true))) ?></strong></td>
		</tr>
	<?php } ?>




	</table>
	<?php

	$kod = ob_get_clean();

	return $kod;
}























function prihlaska_admin_sloupce( $columns ) {

	unset($columns['title']);
	unset($columns['date']);


	$columns['prihlaska'] = 'Přihláška';
	$columns['vs'] = 'VS';
	$columns['datum'] = 'Datum registrace';
	$columns['jmeno'] = 'Jméno / e-mail';
	$columns['kurz'] = 'Kurz';
	$columns['stav'] = 'Stav';


	return $columns;
}
add_action( 'manage_prihlaska_posts_columns', 'prihlaska_admin_sloupce' );



function prihlaska_admin_sloupce_content( $column_name, $post_id ) {


	if( $column_name == 'prihlaska' ) {

		echo '<a href="'.get_edit_post_link( $post_id ).'"><strong>ZOBRAZIT</strong></a>';

	} else if( $column_name == 'vs' ) {

		echo prihlaska_vs( $post_id );

	} else if( $column_name == 'datum' ) {

		$datum = get_post_meta( $post_id, 'prihlaska-datum', true );
		echo $datum = date('j. n. Y, H:i', strtotime($datum));

	} else if( $column_name == 'jmeno' ) {

		$jmeno = get_post_meta( $post_id, 'prihlaska-jmeno', true );
		$prijmeni = get_post_meta( $post_id, 'prihlaska-prijmeni', true );
		$email = get_post_meta( $post_id, 'prihlaska-email', true );

		echo $jmeno.' '.$prijmeni;

		$uzivatel = get_post_meta( $post_id, 'prihlaska-uzivatel', true );
		echo ' <small><a href="'.get_edit_user_link($uzivatel).'" title="ID uživatelského profilu (+ odkaz na něj)">(# '.$uzivatel.')</a></small>';

		echo '<br><small>'.esc_html($email).'</small><br>';

		$vicercata = get_post_meta( $post_id, 'prihlaska-vicercata', true );
		$poznamka = get_post_meta( $post_id, 'prihlaska-poznamka', true );
		$interni_poznamka = get_post_meta( $post_id, 'prihlaska-interni-poznamka', true );

		if( $vicercata == 'ano' )
			echo '<small style="color:red">Čeká vícerčata</small> &nbsp; ';

		if( $poznamka ) {
			$poznamka = esc_attr( mb_substr( trim( $poznamka) , 0, 100, 'utf-8' ) );
			echo '<small style="color:blue" title="'.$poznamka.'">poznámka</small> &nbsp;';
		}

		if( $interni_poznamka ) {
			$interni_poznamka = esc_attr( mb_substr( trim( $interni_poznamka) , 0, 100, 'utf-8' ) );
			echo '<small style="color:orange" title="Interní poznámka: '.$interni_poznamka.'">int. poznámka</small>';
		}



	} else if( $column_name == 'kurz' ) {

		$kurz_id = get_post_meta( $post_id, 'prihlaska-kurz', true );

		$kurz = get_post( $kurz_id );

		echo '<strong>'.$kurz->post_title.'</strong><br>';

		$termin_id = get_post_meta( $post_id, 'prihlaska-termin', true );
		$termin_datum = termin_datum_konani( $termin_id );

		echo $termin_datum;

		echo ' &nbsp; <small><a href="'.admin_url('post.php?post='.$termin_id.'&action=edit').'">detail termínu</a></small>';

		echo '<br>';

		$soukromy_kurz = get_post_meta( $post_id, 'prihlaska-soukromy-kurz', true );

		if( $soukromy_kurz == 'yes' ) {

			// ... ?

		} else {

			$typ = get_post_meta( $post_id, 'prihlaska-typ', true );
			$castka = get_post_meta( $post_id, 'prihlaska-castka', true );
			$mena = mb_strtoupper(get_post_meta( $post_id, 'prihlaska-mena', true ), 'utf-8');


			if( $typ == 'par' ) {
				echo 'Pár';
			} else if( $typ == 'jednotlivec' ) {
				echo 'Jednotlivec';
			}

			echo ' ('.$castka.' '.$mena.')';

			$slevovy_poukaz = get_post_meta( $post_id, 'prihlaska-slevovy-poukaz', true );

			if( $slevovy_poukaz ) {
				echo ' <span style="color:green">Poukaz: <strong>'.$slevovy_poukaz.'</strong></span>';
			}

		}

	} else if( $column_name == 'stav' ) {

		echo $stav = prihlaska_stav_vizualne( $post_id );

	}

}
add_action( 'manage_prihlaska_posts_custom_column', 'prihlaska_admin_sloupce_content', 10, 2);








function prihlaska_admin_head(  ) {
	?><style type="text/css">
		.post-type-prihlaska .wp-list-table { table-layout: auto;}
		.type-prihlaska .column-datum { text-align: left; width:150px !important; overflow:hidden }
		.type-prihlaska .column-jmeno { text-align: left; width:200px !important; overflow:hidden }

		.prihlaska-stav { padding: 3px 10px; color: #fff; font-weight: bold; display: inline-block; background-color: silver; }
		.prihlaska-stav.uhrazeno { background: green; }
		.prihlaska-stav.neuhrazeno { background: gray; }
		.prihlaska-stav.stornovano { background: orange; }
		.prihlaska-stav.odmitnuto { background: red; }
		.prihlaska-stav.manual { background: navy; }

		.post-type-prihlaska #edit-slug-box { display: none; }

	</style>

	<script>
	jQuery(function(){
		jQuery(".post-type-prihlaska .page-title-action").remove();
	});
	</script>
	<?php

	$screen = get_current_screen();

	/*
	if( $screen->id == 'edit-prihlaska' ) {
		?>
		<script>
		jQuery(function(){
			jQuery(".post-type-prihlaska .wp-heading-inline").after('<a href="options.php?kpm-export-prihlasek=all" class="page-title-action">Exportovat vše do XLSX</a>');
		});
		</script>
		<?php
	}
	*/

}
add_action( 'admin_head', 'prihlaska_admin_head' );







function prihlasky_post_row_actions( $actions, $post ) {
    if ( 'prihlaska' == $post->post_type ) {
        return array();
    }
    return $actions;
}
add_filter( 'post_row_actions', 'prihlasky_post_row_actions', 10, 2 );





function prihlaska_je_uhrazena( $prihlaska_id ) {

	return get_post_meta( $prihlaska_id, 'prihlaska-stav', true ) == 'uhrazeno' ;
}






/*
	Pozor na stavy! Zmeny je treba sladit s funkci spocitat_pocty_prihlasek() !
*/
function prihlaska_stav( $prihlaska_id ){

	return get_post_meta( $prihlaska_id, 'prihlaska-stav', true );

}

function prihlaska_stav_vizualne( $prihlaska_id ) {

	$stav = prihlaska_stav( $prihlaska_id );

	return prihlaska_stav_html( $stav );
}


function prihlaska_stav_html( $stav ) {

	if( $stav == 'uhrazeno' )  {
		$stav = '<span class="prihlaska-stav uhrazeno">UHRAZENO</span>';
	} else if( $stav == 'neuhrazeno' ) {
		$stav = '<span class="prihlaska-stav neuhrazeno">NEUHRAZENO</span>';
	} else if( $stav == 'stornovano' ) {
		$stav = '<span class="prihlaska-stav stornovano">STORNOVÁNO</span>';
	} else if( $stav == 'odmitnuto' ) {
		$stav = '<span class="prihlaska-stav odmitnuto">ODMÍTNUTO (Fio)</span>';
	} else if( $stav == 'manual' ) {
		$stav = '<span class="prihlaska-stav manual">ZKONTROLOVAT!</span>';
	} else {
		$stav = '<span class="prihlaska-stav nevim">NEZNÁMÝ</span>';
	}

	return $stav;
}








function zmena_stavu_prihlasky( $prihlaska_id, $novy_stav ) {

	$aktualni_stav = get_post_meta( $prihlaska_id, 'prihlaska-stav', true );
	$zmeny = get_post_meta( $prihlaska_id, 'prihlaska-zmeny-stavu', true ); // array
	$termin_id = get_post_meta( $prihlaska_id, 'prihlaska-termin', true );

	if( empty( $zmeny ) )
		$zmeny = array();

	// pokracuji jen pokud se stavy lisi ...

	if( $aktualni_stav != $novy_stav ) {

		if( update_post_meta( $prihlaska_id, 'prihlaska-stav', $novy_stav ) ) {

			$zmeny[] = array(
				'stary' => $aktualni_stav,
				'novy' => $novy_stav,
				'datum' => current_time('mysql'),
				'uzivatel' => get_current_user_id(), // teoreticky to muze byt robot / notifikace -> 0
			);

			update_post_meta( $prihlaska_id, 'prihlaska-zmeny-stavu', $zmeny );


			// akce po uhrazeni ...

			if( $novy_stav == 'uhrazeno' ) {

				zmena_stavu_prihlasky_uhrazeno( $prihlaska_id );

			}


			// puvodni stav byl "manual" (ke kontrole), a novy je jiny ...

			if( $aktualni_stav == 'manual' && function_exists('fio_kontrola_prepocitat_prihlasky_k_manualni_kontrole') ) {

				fio_kontrola_prepocitat_prihlasky_k_manualni_kontrole();

			}


		}

		// vymazu transient s poctem prihlasek
		delete_transient( 'prihlasek_na_termin_'.$termin_id );
	}

}







// prihlaska byla prepnuta do stavu "uhrazeno"

function zmena_stavu_prihlasky_uhrazeno( $prihlaska_id ) {

	$termin_id = get_post_meta( $prihlaska_id,'prihlaska-termin', true );
	$kurz_id = get_post_meta( $prihlaska_id,'prihlaska-kurz', true );
	$castka = get_post_meta( $prihlaska_id,'prihlaska-castka', true );



	// Vytvoreni dokladu o platbe? ma-li to termin povolen a bylo-li vubec neco placeno

	if (lektorka_fapi($termin_id) && $castka > 0) {
			
		fapi_uhrazena_faktura( $prihlaska_id );
	}

	if (lektorka_iDoklad($termin_id) && $castka > 0) {
			
		iDoklad_uhrazena_faktura( $prihlaska_id );
	}

	if( lektorka_sf( $termin_id ) && $castka > 0 ) {

		$response = prihlaska_faktura( $prihlaska_id ); // regulerni ...

		if( is_wp_error( $response )) {

			// a co ted?

			if( $response->get_error_code() == 'sf_response_error' ) {

				// faktura vubec nezalozena ...

			} else if( $response->get_error_code() == 'sf_no_pdf' ) {

				// nemam pdf ...

			}

		} else if( is_string( $response ) ) {

			prihlaska_odeslat_email_s_fakturou( $prihlaska_id, $response ); // $response obsahuje soubor faktury

		}



	}






	// musim zjistit, jestli jsou nejake "informace" nastavene na zverejneni hned po uhrazeni prihlasky,
	// a jestli maji nastaveny AUTOMATICKY EMAIL
	// Pokud ano, tak jej nyni odeslu
	if(get_post_meta( $prihlaska_id, 'prihlaska-vicercata', true) != 'ne'){
		$info_type = array(
			'key' => 'urceno_pro_dvojcata',
			'value' => '1'
		);
	} else {
		$info_type = array(
			'relation' => 'OR',
			array(
				'key'     => 'urceno_pro_dvojcata',
				'value'   => '0',
				'compare' => '=',
			),
			array(
				'key'     => 'urceno_pro_dvojcata',
				'compare' => 'NOT EXISTS',
			)
		);
	}

	$informace_pro_dany_kurz_s_emailem_ihned_zverejnene = get_posts( array(
		'post_type' => 'informace',
		'posts_per_page' => -1,
		'order' => 'ASC',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'souvisejici_kurz',
				'value' => $kurz_id
			),
			array(
				'key' => 'automaticky_email',
				'value' => 1
			),
			array(
				'key' => 'zpristupnit_po_uhrazeni_prihlasky',
				'value' => 1
			),
			$info_type
		)
	) );

	if( $informace_pro_dany_kurz_s_emailem_ihned_zverejnene ) {

		// fajn, bude se posilat email ... (nebo vice)

		$email = get_post_meta( $prihlaska_id,'prihlaska-email', true );

		foreach( $informace_pro_dany_kurz_s_emailem_ihned_zverejnene as $informace ) {

			if( informace_odeslani_automatickeho_emailu( $informace->ID , $termin_id, array( $email ) ) ) {

				prihlaska_zaznamenat_manualni_rozesilku( $prihlaska_id, array(
					'datum' => current_time('mysql'),
					'informace' => get_post_field( 'post_title', $informace->ID ) .' (automaticky po přepnutí na UHRAZENO)',
					'informace_id' => $informace->ID )
				);

			}

		}

	}



}






































































/*
88b           d88  88888888888  888888888888         db         88888888ba     ,ad8888ba,    8b        d8  8b        d8
888b         d888  88                88             d88b        88      "8b   d8"'    `"8b    Y8,    ,8P    Y8,    ,8P
88`8b       d8'88  88                88            d8'`8b       88      ,8P  d8'        `8b    `8b  d8'      Y8,  ,8P
88 `8b     d8' 88  88aaaaa           88           d8'  `8b      88aaaaaa8P'  88          88      Y88P         "8aa8"
88  `8b   d8'  88  88"""""           88          d8YaaaaY8b     88""""""8b,  88          88      d88b          `88'
88   `8b d8'   88  88                88         d8""""""""8b    88      `8b  Y8,        ,8P    ,8P  Y8,         88
88    `888'    88  88                88        d8'        `8b   88      a8P   Y8a.    .a8P    d8'    `8b        88
88     `8'     88  88888888888       88       d8'          `8b  88888888P"     `"Y8888Y"'    8P        Y8       88



88888888ba   88888888ba   88  88        88  88                  db          ad88888ba   88      a8P   8b        d8
88      "8b  88      "8b  88  88        88  88                 d88b        d8"     "8b  88    ,88'     Y8,    ,8P
88      ,8P  88      ,8P  88  88        88  88                d8'`8b       Y8,          88  ,88"        Y8,  ,8P
88aaaaaa8P'  88aaaaaa8P'  88  88aaaaaaaa88  88               d8'  `8b      `Y8aaaaa,    88,d88'          "8aa8"
88""""""'    88""""88'    88  88""""""""88  88              d8YaaaaY8b       `"""""8b,  8888"88,          `88'
88           88    `8b    88  88        88  88             d8""""""""8b            `8b  88P   Y8b          88
88           88     `8b   88  88        88  88            d8'        `8b   Y8a     a8P  88     "88,        88
88           88      `8b  88  88        88  88888888888  d8'          `8b   "Y88888P"   88       Y8b       88
*/



function prihlaska_registrace_metaboxu() {
	add_meta_box(
		'metabox-prihlaska',
		'Údaje přihlášky',
		'prihlaska_metabox',
		'prihlaska',
		'normal',
		'high'
		);

	add_meta_box(
		'metabox-prihlaska-zmena-terminu',
		'Změna termínu',
		'prihlaska_metabox_zmena_terminu',
		'prihlaska',
		'side',
		'default'
	);

	add_meta_box(
		'metabox-prihlaska-odeslat-email',
		'Odeslání informačního emailu',
		'prihlaska_metabox_informacni_email',
		'prihlaska',
		'side',
		'default'
	);

	add_meta_box(
		'metabox-prihlaska-poznamka',
		'Interní poznámka k přihlášce',
		'prihlaska_metabox_interni_poznamka',
		'prihlaska',
		'normal',
		'default'
	);


}
add_action('add_meta_boxes', 'prihlaska_registrace_metaboxu');










function prihlaska_metabox( $prihlaska ) {


	echo '<div style="width:49%; float:left">';

	echo prihlaska_metabox_informace( $prihlaska->ID );

	echo '</div>';

	echo '<div style="width:49%; float:right">';

	echo '<div id="stav-prihlasky">';

	echo prihlaska_metabox_stav( $prihlaska->ID );

	echo '</div>';

	echo '</div>';

	echo '<div style="clear:both"></div>';
}






function prihlaska_metabox_informace( $prihlaska_id ) {

	return jz_informace_o_prihlasce( $prihlaska_id );

}






function prihlaska_metabox_stav( $prihlaska_id ) {
	?>

	<p><strong>Stav přihlášky:</strong></p>

	<p><?php echo prihlaska_stav_vizualne( $prihlaska_id ) ?></p>

	<p>&nbsp;</p>

	<p>
		<strong>Uživatelský účet: </strong>
		<?php
			$uzivatel = get_post_meta( $prihlaska_id, 'prihlaska-uzivatel', true );
			echo ' <a href="'.get_edit_user_link($uzivatel).'" title="ID uživatelského profilu (+ odkaz na něj)"># '.$uzivatel.'</a>';
		?>
	</p>

	<p>&nbsp;</p>

	<?php echo prihlaska_informace_o_platbe( $prihlaska_id ) ?>

	<p>&nbsp;</p>

	<p><strong>Změnit stav:</strong></p>

	<p>
	<?php
		$stav = prihlaska_stav( $prihlaska_id );
	?>
		<label id="prihlaska-zmenit-stav">Změnit stav na:</label>
		<select name="prihlaska-zmenit-stav" id="prihlaska-zmenit-stav">
		<?php
			foreach( array(
				'' => 'neměnit',
				'uhrazeno' => 'UHRAZENO',
				'neuhrazeno' => 'NEUHRAZENO',
				'stornovano' => 'STORNOVÁNO',
				'odmitnuto' => 'ODMÍTNUTO (Fio)',
			) as $stav_kod => $stav_nazev ) {
				?>
				<option value="<?php echo $stav_kod ?>" <?php echo  $stav == $stav_kod ? 'disabled' : '' ?>><?php echo $stav_nazev ?></option>
				<?php
			}
		?>
		</select>
		<input type="hidden" name="prihlaska-zmenit-stav-field" value="yes">
	</p>

	<p>&nbsp;</p>



	<p><strong>Historie změn stavu:</strong></p>

	<p>
		<?php echo date('j. n. Y, H:i:s', strtotime( get_post_field( 'post_date', $prihlaska_id ) ) ) ?>: <span class="prihlaska-stav">VYTVOŘENA</span>
	</p>

	<?php

	$zmeny = get_post_meta( $prihlaska_id, 'prihlaska-zmeny-stavu', true );

	if( $zmeny ) {

		foreach( (array) $zmeny as $zmena ) {
			?>

			<p>
				<?php echo date('j. n. Y, H:i:s',strtotime( $zmena['datum'] ) ) ?>

				<?php if( !empty( $zmena['stary'] ) ) { ?>
					<?php echo prihlaska_stav_html( $zmena['stary'] ) ?>
					&rarr;
				<?php } ?>

				<?php echo prihlaska_stav_html( $zmena['novy'] ) ?>
			</p>

			<?php
		}

	}

}







function prihlaska_metabox_interni_poznamka( $prihlaska ) {

	$poznamka = get_post_meta( $prihlaska->ID, 'prihlaska-interni-poznamka', true );

	?>
	<textarea name="prihlaska-interni-poznamka" id="prihlaska-interni-poznamka" cols="30" rows="10" style="width:100%;height: 125px"><?php echo esc_html( $poznamka ) ?></textarea>
	<?php

}











function prihlaska_informace_o_platbe( $prihlaska_id ) {

	$html = '';

	$typ_platby = get_post_meta( $prihlaska_id, 'prihlaska-platba', true );

	if( 'on-line' == $typ_platby ) {

		$html .= '<p><strong>Informace o platbě - on-line platba kartou</strong></p>';

		$vysledek = get_post_meta( $prihlaska_id, 'prihlaska-fio-result', true );
		$trans_id = get_post_meta( $prihlaska_id, 'prihlaska-fio-trans-id', true );
		$response = get_post_meta( $prihlaska_id, 'prihlaska-fio-response', true );

		if( $vysledek == 'ERROR' ) {
			$response = get_post_meta( $prihlaska_id, 'prihlaska-fio-error', true );
		}

		$html .= '<p>';
		$html .= 'Výsledek transakce: <strong>'.$vysledek.'</strong>';
		$html .= '</p>';

		$html .= '<p>';
		$html .= 'FIO ID transakce: <strong>'.$trans_id.'</strong>';
		$html .= '</p>';

		$html .= '<p>';
		$html .= 'Podrobné informace:<br><strong>'. nl2br( $response ) .'</strong>';
		$html .= '</p>';


	} else if( 'bankovni-prevod' == $typ_platby ) {

		$ucet = get_post_meta( $prihlaska_id, 'prihlaska-platba-cislo-uctu', true );

		$html .= '<p><strong>Informace o platbě - bankovní převod</strong></p>';

		$html .= '<p>
			<strong>Cílový účet:</strong> '.$ucet.' <br>
			<strong>Var. symbol:</strong> '.prihlaska_vs( $prihlaska_id ).'
		</p>';

		if( $fio_transakce = get_post_meta( $prihlaska_id, 'prihlaska-fio-bankovni-transakce', true ) ) {

			$html .= '<p><br></p>';

			$html .= '<p><strong style="color:blue">Spárovaná FIO transakce:</strong></p>';

			$html .= '<p style="color:blue">			
			Účet: <strong>'.htmlspecialchars($fio_transakce['ucet']).'</strong><br>
			VS: <strong>'.htmlspecialchars($fio_transakce['vs']).'</strong><br>
			Částka: <strong>'.htmlspecialchars($fio_transakce['castka']).'</strong><br>
			Majitel: <strong>'.htmlspecialchars($fio_transakce['uzivatel']).'</strong><br>
			Zpráva: <strong>'.htmlspecialchars($fio_transakce['zprava']).'</strong>
			</p>';


		}


	}

	return $html;
}































function prihlaska_metabox_zmena_terminu( $prihlaska ) {

	?>
	<p>Přesunout přihlášku na jiný termín tohoto kurzu:</p>

	<select name="prihlaska-zmenit-termin" id="prihlaska-zmenit-termin">
		<option value="">vyberte jiný termín</option>
		<?php

		$soucasny_termin = get_post_meta( $prihlaska->ID, 'prihlaska-termin', true );
		$kurz_id = get_post_meta( $soucasny_termin, 'kurz', true );
	
		if(get_the_title($kurz_id) == 'Online kurz') {
			$terminy = get_posts(array(
				'post_type' => 'termin',
				'posts_per_page' => -1,
			));
		} else {
			$terminy = get_posts(array(
				'post_type' => 'termin',
				'posts_per_page' => -1,
				'meta_query' => array(
					array(
						'key' => 'datum_konani',
						'value' => date('Ymd', strtotime("-5 days")),
						'compare' => '>=',
					),
				)
			));
		}
		

		if( $terminy ) {
			foreach( $terminy as $termin )	 {

				if( $termin->ID == $soucasny_termin ) continue;

				$lektorka_id = get_post_meta($termin->ID, 'lektorka', true);
				
				$nazev = termin_datum_konani( $termin->ID );
				$nazev .= ' ('. termin_lokalita( $termin->ID ).')';
				$nazev .= ' - ' . get_the_title(get_post_meta( $termin->ID, 'kurz', true ));
				$nazev .= ' (' . get_the_title($lektorka_id) . ')'; ?>
				
				<option value="<?php echo $termin->ID ?>"><?php echo $nazev ?></option>
				
			<?php }
		}


		?>
	</select>


	<?php

}


















function prihlaska_metabox_informacni_email( $prihlaska ) {

	$soucasny_termin = get_post_meta( $prihlaska->ID, 'prihlaska-termin', true );
	$kurz_id = get_post_meta( $prihlaska->ID, 'prihlaska-kurz', true );

	if(get_post_meta( $prihlaska->ID, 'prihlaska-vicercata', true) != 'ne'){
		$info_type = array(
			'key' => 'urceno_pro_dvojcata',
			'value' => '1'
		);
	} else {
		$info_type = array(
			'relation' => 'OR',
			array(
				'key'     => 'urceno_pro_dvojcata',
				'value'   => '0',
				'compare' => '=',
			),
			array(
				'key'     => 'urceno_pro_dvojcata',
				'compare' => 'NOT EXISTS',
			)
		);
	}

	$informace_pro_dany_kurz_s_emailem = get_posts( array(
		'post_type' => 'informace',
		'posts_per_page' => -1,
		'order' => 'ASC',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'souvisejici_kurz',
				'value' => $kurz_id
			),
			array(
				'key' => 'automaticky_email',
				'value' => 1
			),
			$info_type
		)
	) );

	if( ! $informace_pro_dany_kurz_s_emailem ) {
		?>
		<p><em>Pro tento typ kurzu nejsou zveřejněny žádné informace s automatickým emailem.</em></p>
		<?php
	}

	?>

	<p>
	<select name="prihlaska-odeslat-informacni-email" id="prihlaska-odeslat-informacni-email">
		<option value="">vyberte informační e-mail k odeslání</option>

		<?php
		foreach( $informace_pro_dany_kurz_s_emailem as $informace )	 {
			?>
			<option value="<?php echo $informace->ID ?>"><?php echo $informace->post_title ?></option>
			<?php
		}
		?>

	</select>
	</p>

	<p>
		Máte na výběr z informací pro tento typ kurzu (nezávisle na jejich případném datu zpřístupnění), které obsahují	automatické rozeslání e-mailu.
	</p>

	<?php


	$rozesilky = get_post_meta( $prihlaska->ID, 'manualni_rozesilky_informaci', true );

	if(  $rozesilky ) {

		?>
		<p>Již proběhlo:</p>
		<?php

		foreach( $rozesilky as $informace_id => $data ) {

			$datum = date('j. n. Y H:i', strtotime($data['datum']));
			$informace = $data['informace'];
			$informace_id = $data['informace_id'];
			?>
			<p style="margin-bottom: 25px">
				<strong><?php echo $datum ?></strong> - <em><?php echo $informace ?></em> <small><a href="<?php echo admin_url('post.php?post='.$informace_id.'&action=edit') ?>">detail</a></small>
			</p>
			<?php

		}
	}
}














// zaznamenam manualni odeslani emailu/informaci k dane prihlasce
// pouziva se to i pri zmene stavu na uhrazeno
// $data = array( 'datum' => , 'informace' => , 'informace_id' => )

function prihlaska_zaznamenat_manualni_rozesilku( $prihlaska_id, $data ) {

	$manualni_rozesilky = get_post_meta( $prihlaska_id, 'manualni_rozesilky_informaci', true );

	if( ! $manualni_rozesilky )
		$manualni_rozesilky = array();

	$manualni_rozesilky[] = $data;

	update_post_meta( $prihlaska_id, 'manualni_rozesilky_informaci', $manualni_rozesilky );

}
































function save_post_prihlaska( $post_id, $post, $update ) {

	if( ! isset( $_POST['prihlaska-zmenit-stav-field'] ) ) return;



	if( ! empty( $_POST['prihlaska-zmenit-stav'] ) ) {

		$novy_stav = trim($_POST['prihlaska-zmenit-stav']);

		zmena_stavu_prihlasky( $post_id, $novy_stav );

	}


	if( ! empty( $_POST['prihlaska-zmenit-termin'] ) && intval( $_POST['prihlaska-zmenit-termin'] ) ) {

		$novy_termin = intval($_POST['prihlaska-zmenit-termin']);
		$novy_kurz =  get_post_meta( $novy_termin, 'kurz', true );
		$stary_termin = get_post_meta( $post_id,'prihlaska-termin', true );

		update_post_meta( $post_id, 'prihlaska-termin', $novy_termin );
		update_post_meta( $post_id, 'prihlaska-kurz', $novy_kurz );

		delete_transient( 'prihlasek_na_termin_'.$stary_termin );
		delete_transient( 'prihlasek_na_termin_'.$novy_termin );

	}


	if( isset($_POST['prihlaska-interni-poznamka']) ) {

		update_post_meta( $post_id, 'prihlaska-interni-poznamka', trim( $_POST['prihlaska-interni-poznamka'] ) );

	}


	if( ! empty( $_POST['prihlaska-odeslat-informacni-email'] ) && intval( $_POST['prihlaska-odeslat-informacni-email'] ) ) {

		$informace_id = intval( $_POST['prihlaska-odeslat-informacni-email'] );

		$prihlaska_id = $post->ID;

		$termin_id = get_post_meta( $prihlaska_id,'prihlaska-termin', true );

		$email = get_post_meta( $prihlaska_id,'prihlaska-email', true );



		if( ! informace_odeslani_automatickeho_emailu( $informace_id, $termin_id, array( $email ) ) ) {

			// co nadelam?

			wp_die('Bohužel, informační email nebylo možné odeslat, došlo k chybě.');

		} else {

			prihlaska_zaznamenat_manualni_rozesilku( $prihlaska_id, array(
				'datum' => current_time('mysql'),
				'informace' => get_post_field( 'post_title', $informace_id ),
				'informace_id' => $informace_id )
			);

		}

	}


}
add_action( 'save_post_prihlaska', 'save_post_prihlaska', 10, 3 );










































/*
88888888888  8b        d8  88888888ba     ,ad8888ba,    88888888ba   888888888888      88888888ba   88888888ba   88  88        88  88                  db          ad88888ba   88888888888  88      a8P
88            Y8,    ,8P   88      "8b   d8"'    `"8b   88      "8b       88           88      "8b  88      "8b  88  88        88  88                 d88b        d8"     "8b  88           88    ,88'
88             `8b  d8'    88      ,8P  d8'        `8b  88      ,8P       88           88      ,8P  88      ,8P  88  88        88  88                d8'`8b       Y8,          88           88  ,88"
88aaaaa          Y88P      88aaaaaa8P'  88          88  88aaaaaa8P'       88           88aaaaaa8P'  88aaaaaa8P'  88  88aaaaaaaa88  88               d8'  `8b      `Y8aaaaa,    88aaaaa      88,d88'
88"""""          d88b      88""""""'    88          88  88""""88'         88           88""""""'    88""""88'    88  88""""""""88  88              d8YaaaaY8b       `"""""8b,  88"""""      8888"88,
88             ,8P  Y8,    88           Y8,        ,8P  88    `8b         88           88           88    `8b    88  88        88  88             d8""""""""8b            `8b  88           88P   Y8b
88            d8'    `8b   88            Y8a.    .a8P   88     `8b        88           88           88     `8b   88  88        88  88            d8'        `8b   Y8a     a8P  88           88     "88,
88888888888  8P        Y8  88             `"Y8888Y"'    88      `8b       88           88           88      `8b  88  88        88  88888888888  d8'          `8b   "Y88888P"   88888888888  88       Y8b
*/






function jz_prihlasky_admin_footer() {

	$screen = get_current_screen();

	if( $screen->post_type == 'prihlaska' && !empty( $_GET['pouze-termin'] ) ) {

		?>
		<script>
			jQuery( function(){
				jQuery('h1').after('<a href="<?php echo admin_url('?jz-export-prihlasek=' . intval( $_GET['pouze-termin'] ) ) ?>" target="_blank" id="exportdoexcelu" class="page-title-action">Export přihlášek do Excelu</a>');
			});
		</script>
		<?php
	}
}
add_action( 'admin_footer', 'jz_prihlasky_admin_footer' );









function jz_export_prihlasek() {

	$pouze_termin = intval( $_GET['jz-export-prihlasek'] );

	$prihlasky = get_posts( array(
		'post_type' => 'prihlaska',
		'posts_per_page' => -1,
		'order' => 'ASC',
		'meta_query' => array(
			array(
				'key' => 'prihlaska-termin',
				'value' => $pouze_termin
			)
		)
	) );


	if( ! $prihlasky ) {
		wp_die('Žádné přihlášky nejsou uloženy');
	}


	$sloupce = array();


	// POZOR! Pri pridani dalsiho sloupce je treba zmenit index pismeno u poznamky na konci funkce!

	$fields = array(
		'prihlaska-datum' => 'Datum přihlášení',
		'prihlaska-stav' => 'Stav',
		'prihlaska-kurz' => 'Kurz',
		'prihlaska-termin' => 'Termín',
		'prihlaska-typ' => 'Pár / jednotlivec',
		'prihlaska-par-upresneni' => 'S kým přijdu',
		'prihlaska-castka' => 'Částka',
		'prihlaska-mena' => 'Měna',
		'prihlaska-jmeno' => 'Jméno',
		'prihlaska-prijmeni' => 'Příjmení',
		'prihlaska-ocekavany-termin-porodu' => 'Termín porodu',
		'prihlaska-vicercata' => 'Vícerčata?',
		'prihlaska-ulice-cp' => 'Ulice a čp.',
		'prihlaska-mesto' => 'Město',
		'prihlaska-psc' => 'PSČ',
		'prihlaska-stat' => 'Stát',
		'prihlaska-email' => 'E-mail',
		'prihlaska-telefon' => 'Telefon',
		'prihlaska-poznamka' => 'Poznámka',
		'prihlaska-platba' => 'Způsob úhrady',
		'prihlaska-platba-cislo-uctu' => 'Na BÚ',
		'prihlaska-vs' => 'Var. symbol',
	);


	$data_rows = array();

	$a = 1;
	foreach( $prihlasky as $prihlaska ) {

		$prihl_id = $prihlaska->ID;

		$zaznamenat_sloupec = $a == 1 ? true : false;

		$data_row = array();

		foreach( $fields as $field => $nazev_pole ) {

			$value = get_post_meta( $prihl_id, $field, true );

			$hodnota = $value;



			if( false ) {

			} else {

				if( $zaznamenat_sloupec ) {
					$sloupce[ $field ] = $nazev_pole;
				}


				// jen jemne nuance

				if( $field == 'prihlaska-stav' ) {

					$hodnota = mb_strtoupper( $hodnota, 'utf-8' );

				} else if( $field == 'prihlaska-termin' ) {

					$hodnota = termin_datum_konani( $hodnota );

				} else if( $field == 'prihlaska-kurz' ) {

					$hodnota = get_the_title( $hodnota ); // hodnota je post id kurzu

				}

				$data_row[ $field ] = $hodnota;

			}

		}

		$data_rows[] = $data_row;


		$a++;
	}

	// mam sloupce, mam data ...

	$data = array_merge( array( 0 => $sloupce) , $data_rows );

	require_once dirname(__FILE__).'/phpexcel/PHPExcel/IOFactory.php';

	$objPHPExcel = new PHPExcel();
	$objPHPExcel->setActiveSheetIndex(0);

	$objPHPExcel->getActiveSheet()->fromArray( $data, NULL, 'A1');

	// obarvim prvni radek - hlavicka
	$first_letter = PHPExcel_Cell::stringFromColumnIndex( 0 );
	$last_letter = PHPExcel_Cell::stringFromColumnIndex( count($sloupce) - 1 );
	$header_range = "{$first_letter}1:{$last_letter}1";
	$objPHPExcel->getActiveSheet()->getStyle($header_range)->getFont()->setBold(true);


	foreach (range(0, count( $sloupce ) ) as $col) {
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
	}

	// kratky popisek
	$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(50);

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');


	$filename = 'export-prihlasek-'.sanitize_title( termin_nazev_kurzu( $pouze_termin ) ).'.xlsx';

	header('Content-type: application/vnd.ms-excel');

	header('Content-Disposition: attachment; filename="'.$filename.'"');


	$objWriter->save('php://output');

	exit;

}

if( isset( $_GET['jz-export-prihlasek'] ) )
	add_action( 'admin_init', 'jz_export_prihlasek' );




















































/*
88  888b      88  88888888888    ,ad8888ba,    88888888ba   88b           d88         db           ,ad8888ba,   88888888888
88  8888b     88  88            d8"'    `"8b   88      "8b  888b         d888        d88b         d8"'    `"8b  88
88  88 `8b    88  88           d8'        `8b  88      ,8P  88`8b       d8'88       d8'`8b       d8'            88
88  88  `8b   88  88aaaaa      88          88  88aaaaaa8P'  88 `8b     d8' 88      d8'  `8b      88             88aaaaa
88  88   `8b  88  88"""""      88          88  88""""88'    88  `8b   d8'  88     d8YaaaaY8b     88             88"""""
88  88    `8b 88  88           Y8,        ,8P  88    `8b    88   `8b d8'   88    d8""""""""8b    Y8,            88
88  88     `8888  88            Y8a.    .a8P   88     `8b   88    `888'    88   d8'        `8b    Y8a.    .a8P  88
88  88      `888  88             `"Y8888Y"'    88      `8b  88     `8'     88  d8'          `8b    `"Y8888Y"'   88888888888



8b           d8             db         88888888ba,    88b           d88  88  888b      88  88        88
`8b         d8'            d88b        88      `"8b   888b         d888  88  8888b     88  88        88
 `8b       d8'            d8'`8b       88        `8b  88`8b       d8'88  88  88 `8b    88  88        88
  `8b     d8'            d8'  `8b      88         88  88 `8b     d8' 88  88  88  `8b   88  88        88
   `8b   d8'            d8YaaaaY8b     88         88  88  `8b   d8'  88  88  88   `8b  88  88        88
    `8b d8'            d8""""""""8b    88         8P  88   `8b d8'   88  88  88    `8b 88  88        88
     `888'            d8'        `8b   88      .a8P   88    `888'    88  88  88     `8888  Y8a.    .a8P
      `8'            d8'          `8b  88888888Y"'    88     `8'     88  88  88      `888   `"Y8888Y"'
*/






add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) { ?>
    <h3><?php _e("Číslo účtu lektorky", "blank"); ?></h3>

    <table class="form-table">
    <tr>
        <th><label for="bank_account"><?php _e("Číslo účtu"); ?></label></th>
        <td>
            <input type="text" name="bank_account" id="bank_account" value="<?php echo esc_attr( get_the_author_meta( 'bank_account', $user->ID ) ); ?>" class="regular-text" /><br />
        </td>
    </tr>
    <tr>
        <th><label for="fio_apiKey"><?php _e("FIO API klíč"); ?></label></th>
        <td>
            <input type="text" name="fio_apiKey" id="fio_apiKey" value="<?php echo esc_attr( get_the_author_meta( 'fio_apiKey', $user->ID ) ); ?>" class="regular-text" /><br />
        </td>
    </tr>
    </table>


    <?php 

   	if(get_user_meta( $user->id, 'fapi_login', 1 ) && get_user_meta( $user->id, 'fapi_apiKey', 1 )){
   		if( ! class_exists('FAPIClient') ) {
			require_once __DIR__.'/FAPIClient/FAPIClient.php';
		}

		$fapi = new FAPIClient(get_user_meta( $user->id, 'fapi_login', 1 ), get_user_meta( $user->id, 'fapi_apiKey', 1 ));

		try {
			$fapi->checkConnection();
		} catch (Exception $e) { ?>
			<div class="error notice">
		        <p>FAPI údaje jsou neplatné</p>
		    </div>
		<?php }
   	}

    ?>

    <h3><?php _e("FAPI údaje", "blank"); ?></h3>

    <table class="form-table">
	    <tr>
	        <th><label for="fapi_login"><?php _e("FAPI Login"); ?></label></th>
	        <td>
	            <input type="text" name="fapi_login" id="fapi_login" value="<?php echo esc_attr( get_the_author_meta( 'fapi_login', $user->ID ) ); ?>" class="regular-text" /><br />
	        </td>
	    </tr>
	    <tr>
	        <th><label for="fapi_apiKey"><?php _e("FAPI klíč"); ?></label></th>
	        <td>
	            <input type="text" name="fapi_apiKey" id="fapi_apiKey" value="<?php echo esc_attr( get_the_author_meta( 'fapi_apiKey', $user->ID ) ); ?>" class="regular-text" /><br />
	        </td>
	    </tr>
	</table>

    <h3><?php _e("iDoklad údaje", "blank"); ?></h3>

    <table class="form-table">
	    <tr>
	        <th><label for="iDoklad_login"><?php _e("iDoklad Login"); ?></label></th>
	        <td>
	            <input type="text" name="iDoklad_login" id="iDoklad_login" value="<?php echo esc_attr( get_the_author_meta( 'iDoklad_login', $user->ID ) ); ?>" class="regular-text" /><br />
	        </td>
	    </tr>
	    <tr>
	        <th><label for="iDoklad_apiKey"><?php _e("iDoklad klíč"); ?></label></th>
	        <td>
	            <input type="text" name="iDoklad_apiKey" id="iDoklad_apiKey" value="<?php echo esc_attr( get_the_author_meta( 'iDoklad_apiKey', $user->ID ) ); ?>" class="regular-text" /><br />
	        </td>
	    </tr>
	</table>
	
	<h3><?php _e("Superfaktura údaje", "blank"); ?></h3>

    <table class="form-table">
	    <tr>
	        <th><label for="sf_login"><?php _e("Superfaktura Login"); ?></label></th>
	        <td>
	            <input type="text" name="sf_login" id="sf_login" value="<?php echo esc_attr( get_the_author_meta( 'sf_login', $user->ID ) ); ?>" class="regular-text" /><br />
	        </td>
	    </tr>
	    <tr>
	        <th><label for="sf_apiKey"><?php _e("Superfaktura klíč"); ?></label></th>
	        <td>
	            <input type="text" name="sf_apiKey" id="sf_apiKey" value="<?php echo esc_attr( get_the_author_meta( 'sf_apiKey', $user->ID ) ); ?>" class="regular-text" /><br />
	        </td>
		</tr>
		<tr>
	        <th><label for="sf_company"><?php _e("Superfaktura company_id"); ?></label></th>
	        <td>
	            <input type="text" name="sf_company" id="sf_company" value="<?php echo esc_attr( get_the_author_meta( 'sf_company', $user->ID ) ); ?>" class="regular-text" /><br />
	        </td>
	    </tr>
    </table>

    <h3><?php _e("Povolené systémy", "blank"); ?></h3>

    <table class="form-table">
	    <tr>
	        <td>
	        	<label for="fio_allow">
					<input name="fio_allow" type="checkbox" id="fio_allow" value="1" <?= get_the_author_meta( 'fio_allow', $user->ID ) ?'checked="checked"':'' ?>>
						FIO banka
				</label>

	          	<label for="fapi_allow">
					<input name="fapi_allow" type="checkbox" id="fapi_allow" value="1" <?= get_the_author_meta( 'fapi_allow', $user->ID ) ?'checked="checked"':'' ?>>
						FAPI
				</label>

				<label for="sf_allow">
					<input name="sf_allow" type="checkbox" id="sf_allow" value="1" <?= get_the_author_meta( 'sf_allow', $user->ID ) ?'checked="checked"':'' ?>>
						Superfaktura
				</label>

				<label for="iDoklad_allow">
					<input name="iDoklad_allow" type="checkbox" id="iDoklad_allow" value="1" <?= get_the_author_meta( 'iDoklad_allow', $user->ID ) ?'checked="checked"':'' ?>>
						iDoklad
				</label>
	        </td>
	    </tr>
	</table>
<?php }


function tm_save_profile_fields( $user_id ) {

    if ( ! current_user_can( 'edit_user', $user_id ) ) {
   	 return false;
    }

    if ( $_POST['bank_account'] ) {
   	 	update_usermeta( $user_id, 'bank_account', $_POST['bank_account'] );
    } 

    if ( $_POST['fio_allow'] ) {
   	 	update_usermeta( $user_id, 'fio_allow', $_POST['fio_allow'] );
    } else {
    	update_usermeta( $user_id, 'fio_allow', false );
    }

    if ( $_POST['fapi_allow'] ) {
   	 	update_usermeta( $user_id, 'fapi_allow', $_POST['fapi_allow'] );
    } else {
    	update_usermeta( $user_id, 'fapi_allow', false );
	}
	
	if ( $_POST['sf_allow'] ) {
		update_usermeta( $user_id, 'sf_allow', $_POST['sf_allow'] );
	} else {
		update_usermeta( $user_id, 'sf_allow', false );
	}

	if ( $_POST['iDoklad_allow'] ) {
		update_usermeta( $user_id, 'iDoklad_allow', $_POST['iDoklad_allow'] );
	} else {
		update_usermeta( $user_id, 'iDoklad_allow', false );
	}

    if ( $_POST['fio_apiKey'] ) {
   	 	update_usermeta( $user_id, 'fio_apiKey', $_POST['fio_apiKey'] );
    } else {
    	update_usermeta( $user_id, 'fio_apiKey', '' );
	}

    if ( $_POST['fapi_apiKey'] ) {
   	 	update_usermeta( $user_id, 'fapi_apiKey', $_POST['fapi_apiKey'] );
    } else {
    	update_usermeta( $user_id, 'fapi_apiKey', '' );
	}

    if ( $_POST['iDoklad_login'] ) {
   	 	update_usermeta( $user_id, 'iDoklad_login', $_POST['iDoklad_login'] );
    } else {
    	update_usermeta( $user_id, 'iDoklad_login', '' );
	}

    if ( $_POST['iDoklad_apiKey'] ) {
   	 	update_usermeta( $user_id, 'iDoklad_apiKey', $_POST['iDoklad_apiKey'] );
    } else {
    	update_usermeta( $user_id, 'iDoklad_apiKey', '' );
	}

    if ( $_POST['fapi_login'] ) {
   	 	update_usermeta( $user_id, 'fapi_login', $_POST['fapi_login'] );
	} else {
    	update_usermeta( $user_id, 'fapi_login', '' );
	}
	
	if ( $_POST['sf_apiKey'] ) {
		update_usermeta( $user_id, 'sf_apiKey', $_POST['sf_apiKey'] );
	} else {
    	update_usermeta( $user_id, 'sf_apiKey', '' );
	}

	if ( $_POST['sf_login'] ) {
			update_usermeta( $user_id, 'sf_login', $_POST['sf_login'] );
	} else {
    	update_usermeta( $user_id, 'sf_login', '' );
	}

	if ( $_POST['sf_company'] ) {
		update_usermeta( $user_id, 'sf_company', $_POST['sf_company'] );
	} else {
    	update_usermeta( $user_id, 'sf_company', '' );
	}
    
}

add_action( 'personal_options_update', 'tm_save_profile_fields' );
add_action( 'edit_user_profile_update', 'tm_save_profile_fields' );



function informace_admin_sloupce( $columns ) {

	$new_columns = array();

	foreach( $columns as $name => $label ) {

		if( $name == 'date' ) continue;

		$new_columns[ $name ] = $label;

		if( $name == 'title' ) {

			$new_columns[ 'kurz' ] = 'Kurz';
			$new_columns[ 'zverejneni' ] = 'Kdy zveřejnit';
			$new_columns[ 'email' ] = 'Upozornění e-mailem?';
			$new_columns[ 'informace' ] = 'Materiály';
		}

	}

	return $new_columns;
}
add_action( 'manage_informace_posts_columns', 'informace_admin_sloupce' );



function informace_admin_sloupce_content( $column_name, $post_id ) {


	if( $column_name == 'kurz' ) {

		$kurz_id = get_post_meta( $post_id, 'souvisejici_kurz', true );

		echo get_post_field( 'post_title', $kurz_id );


	} else if( $column_name == 'zverejneni' ) {


		$zpristupnit_po_uhrazeni_prihlasky = get_post_meta( $post_id, 'zpristupnit_po_uhrazeni_prihlasky', true );
		$kdy_informace_zpristupnit = get_post_meta( $post_id, 'kdy_informace_zpristupnit', true );

		if( $zpristupnit_po_uhrazeni_prihlasky ) {
			echo 'Po uhrazení přihlášky';
		} else if( $kdy_informace_zpristupnit < 0 ) {
			echo sklonovani( abs($kdy_informace_zpristupnit), 'den', 'dny', 'dní') .' před datem konání';
		} else if( $kdy_informace_zpristupnit > 0 ) {
			echo sklonovani( $kdy_informace_zpristupnit, 'den', 'dny', 'dní') .' po datu konání';
		} else {
			echo 'V den konání';
		}

	} else if( $column_name == 'email' ) {

		$automaticky_email =  get_post_meta( $post_id, 'automaticky_email', true );

		echo $automaticky_email ? 'Ano' : 'Ne';

	} else if( $column_name == 'informace' ) {

		$informace = get_field( 'materialy', $post_id );

		echo sklonovani( count( $informace ), 'soubor', 'soubory', 'souborů' );

	}

}
add_action( 'manage_informace_posts_custom_column', 'informace_admin_sloupce_content', 10, 2);
























/*
88           88888888888  88      a8P   888888888888    ,ad8888ba,    88888888ba   88      a8P   8b        d8
88           88           88    ,88'         88        d8"'    `"8b   88      "8b  88    ,88'     Y8,    ,8P
88           88           88  ,88"           88       d8'        `8b  88      ,8P  88  ,88"        Y8,  ,8P
88           88aaaaa      88,d88'            88       88          88  88aaaaaa8P'  88,d88'          "8aa8"
88           88"""""      8888"88,           88       88          88  88""""88'    8888"88,          `88'
88           88           88P   Y8b          88       Y8,        ,8P  88    `8b    88P   Y8b          88
88           88           88     "88,        88        Y8a.    .a8P   88     `8b   88     "88,        88
88888888888  88888888888  88       Y8b       88         `"Y8888Y"'    88      `8b  88       Y8b       88
*/







add_action( 'manage_lektorka_posts_columns', 'lektorkaAdminSloupce' );
add_action( 'manage_lektorka_posts_custom_column', 'lektorkaAdminSloupceContent', 10, 2);
add_action( 'admin_head', 'lektorkaAdminHead' );

function lektorkaAdminSloupce( $columns ) {
	$new_columns = array();
	foreach( $columns as $index=>$title ) {

		if( $index == 'date' ) continue; // datum nas tam nezajima

		$new_columns[ $index ] = $title;
		if( $index == 'title' ) {
			$new_columns[ 'email' ] = 'E-mail';
			$new_columns[ 'mesto' ] = 'Město';
			$new_columns[ 'wp-uzivatel' ] = 'WordPress lektorský účet';
		}
	}
	return $new_columns;
}

function lektorkaAdminSloupceContent( $column_name, $post_id ) {

	if( $column_name == 'email' ) {

		echo get_field( 'email', $post_id );

	} else if( $column_name == 'mesto' ) {

		echo get_field( 'mesto', $post_id );

	} else if( $column_name == 'wp-uzivatel' ) {

		$user = get_field('uzivatelsky_ucet_lektorky', $post_id); // vraci user object

		if( ! $user ) {

			echo '<strong style="color:red">neexistuje!</strong>';

		} else {

			echo '<a href="'.get_edit_user_link( $user['ID'] ).'">#'.$user['ID'].' '.$user['user_firstname'].' '.$user['user_lastname'].'</a>';

		}

	}
}

function lektorkaAdminHead(  ) {
	?>
	<style type="text/css">
		.post-type-lektorka .wp-list-table { table-layout: auto;}
		/*.type-lektorka .column-poradi { text-align: left; width:60px !important; overflow:hidden }*/
	</style>
	<?php
}





































/*
88b           d88  88        88          88      88        88    ,ad8888ba,            88           88888888888  88      a8P   888888888888    ,ad8888ba,    88888888ba   88      a8P          db
888b         d888  88        88          88      88        88   d8"'    `"8b           88           88           88    ,88'         88        d8"'    `"8b   88      "8b  88    ,88'          d88b
88`8b       d8'88  88        88          88      88        88  d8'                     88           88           88  ,88"           88       d8'        `8b  88      ,8P  88  ,88"           d8'`8b
88 `8b     d8' 88  88        88          88      88        88  88                      88           88aaaaa      88,d88'            88       88          88  88aaaaaa8P'  88,d88'           d8'  `8b
88  `8b   d8'  88  88        88          88      88        88  88                      88           88"""""      8888"88,           88       88          88  88""""88'    8888"88,         d8YaaaaY8b
88   `8b d8'   88  88        88          88      88        88  Y8,                     88           88           88P   Y8b          88       Y8,        ,8P  88    `8b    88P   Y8b       d8""""""""8b
88    `888'    88  Y8a.    .a8P  88,   ,d88      Y8a.    .a8P   Y8a.    .a8P  888      88           88           88     "88,        88        Y8a.    .a8P   88     `8b   88     "88,    d8'        `8b
88     `8'     88   `"Y8888Y"'    "Y8888P"        `"Y8888Y"'     `"Y8888Y"'   888      88888888888  88888888888  88       Y8b       88         `"Y8888Y"'    88      `8b  88       Y8b  d8'          `8b
*/

function prihlasena_lektorka() {

	$user = wp_get_current_user();

	if( $user &&  array_intersect( (array) $user->roles , array('lektorka') ) ) {
		return true;
	}

	return false;
}



// dohledam patricn ID postu lektorky (cpt "lektorka"), ktery ma prirazeny tohoto uzivatele
function prihlasena_lektorka_post_id() {
	global $wpdb;

	if( ! prihlasena_lektorka () )
		return 0;

	$ids = $wpdb->get_results( "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key='uzivatelsky_ucet_lektorky' AND meta_value='".get_current_user_id()."'", ARRAY_A );

	if(count($ids)){
		foreach($ids as $pid){
			$icl = apply_filters( 'wpml_post_language_details', NULL, $pid['post_id'] ) ;
			if($icl['language_code'] == ICL_LANGUAGE_CODE){
				return $pid['post_id'];
			}
		}
	}
}



function my_account_terminy_lektorky_tabulka($lektorka_post_id) {

	$the_query = new WP_Query(array(
		'post_type' => 'termin',
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key' => 'lektorka',
				'value' => $lektorka_post_id,
			),
			
			array(
				'key' => 'datum_konani',
				'value' => !$_POST['showAll']? date('Ymd', strtotime("-14 days")):'',
				'compare' => '>=',
			),
			
		)
	));

	if ($_GET['deleted']) { ?>
		<div id="message" class="updated"><p>Termín byl zrušen</p></div>
	<?php }
	if( $the_query->have_posts() ) {
		?>
		<table class="shop_table">
		<tr>
			<th>Datum</th>
			<th>Kurz</th>
			<th>Místo</th>
			<th>Informace</th>
			<th>Upravit</th>
		</tr>
		<?php
		while( $the_query->have_posts() ) {
			$the_query->the_post();

			$termin_id = get_the_id();
			?>

			<tr>
				<td><a href="<?= get_permalink( $termin_id ); ?>"><?php echo termin_datum_konani( $termin_id ) ?></a></td>
				<td><?php the_title() ?></td>
				<td><?php echo termin_lokalita( $termin_id ) ?></td>
				<td>
					<a href="<?php echo trailingslashit( wc_get_endpoint_url( 'terminy-lektorky', $termin_id, wc_get_page_permalink( 'myaccount' ) ) ) ?>">Informace</a>
				</td>
				<td>
					<a href="<?php echo trailingslashit( wc_get_endpoint_url( 'terminy-lektorky', $termin_id, wc_get_page_permalink( 'myaccount' )) ). '?edit' ?>">Upravit</a>
				</td>
			</tr>

			<?php
		}
		wp_reset_postdata();
		?>
		</table>
		<?php if(!$_POST['showAll']): ?>
			<form method="post">
				<input type="hidden" name="showAll" value="true">
				<input type="submit" value="Zobrazit všechny termíny">
			</form>
		<?php
		endif;
	} else {
		?>
		<p><em>Nemáte vypsány žádné termíny.</em></p>
		<?php
	}

}



















function my_account_terminy_lektorky_detail( $termin_id ) {

	?>
	<p><a href="<?php echo wc_get_endpoint_url( 'terminy-lektorky', '', wc_get_page_permalink( 'myaccount' ) ) ?>">&larr; Návrat na přehled mých termínů</a></p>
	<?php

	// je to jeji termin?

	$termin = get_post( $termin_id );
	$t_id = $termin_id;
	$termin_lektorka_post_id = get_post_meta( $termin_id, 'lektorka', true );

	$lektorka_post_id = prihlasena_lektorka_post_id();

	if( $termin_lektorka_post_id != $lektorka_post_id ) {
		?>
		<p><strong style="color:red">Toto není váš termín!</strong></p>
		<?php
	}

	// podrobnosti o terminu

	tabulka_s_podrobnostmi_o_terminu( $termin_id );

	$prihlasky = get_posts(array(
		'post_type' => 'prihlaska',
		'posts_per_page' => -1,
		'order' => 'ASC',
		'meta_query' => array(
			array(
				'key' => 'prihlaska-termin',
				'value' => $t_id
			),
			array(
				'key' => 'prihlaska-stav',
				'value' => array('uhrazeno','neuhrazeno', 'stornovano'),
				'compare' => 'IN'
			),
		)
	));

	if($_POST['send_mail']){

		if (! isset( $_POST['send_mail_all_nonce'] ) || ! wp_verify_nonce( $_POST['send_mail_all_nonce'], 'send_mail_all' ) ) {

		   print 'Došlo k chybě, zkuste to prosím znovu.';
		   exit;

		} else {
			$email_text = jz_zacatek_html_emailu();
			$email_text .= $_POST['email_content'];
			$email_text .= jz_konec_html_emailu();

			$prijemce_emailu = get_field('email',prihlasena_lektorka_post_id());

			$headers = array();
			$headers[] = 'Content-Type: text/html; charset=UTF-8';
			$headers[] = 'From: '.get_bloginfo('name').' <'.$prijemce_emailu.'>';

			foreach( $prihlasky as $prihlaska ) {
				if(prihlaska_stav($prihlaska->ID) != 'stornovano'){
					$headers[] = 'Bcc: '.get_post_meta( $prihlaska->ID, 'prihlaska-email', true );
				}
			}
			
			$resp_mail = wp_mail( $prijemce_emailu , 'Zpráva od lektorky', $email_text, $headers ) ;

			if ($resp_mail) { ?>
				<div class="woocommerce-message" role="alert">Email byl odeslán.</div> <?php 
			} else { ?>
				<div class="woocommerce-message" role="alert">Email nebyl odeslán, došlo k chybě.</div> <?php 
			}
			
		}
	}





	// letak?

	?>
	<h2>Leták ke stažení</h2>
	<?php

	$letak = get_field('letak_ke_stazeni', $termin_id );

	if( $letak ) {

		?>
		<p>Dokument: <strong><a href="<?php echo $letak ?>" target="_blank" class="nofancybox"><?php echo basename( $letak ) ?></a></strong></p>
		<?php

	} else {
		?><p><em>Žádný dokument není přiložen.</em></p><?php
	}

	?>

	<h2>Stornovat termín</h2>
	<p>Termín je možné stornovat, pokud jsou všechny přihlášky stornovány, nebo termín neobsahuje žádné přihlášky.</p>
	<?php 
		$exists = false;
		if($prihlasky){
			foreach ($prihlasky as $prihlaska) {
				if(prihlaska_stav($prihlaska->ID) == 'uhrazeno' || prihlaska_stav($prihlaska->ID) == 'neuhrazeno'){
					$exist = true;
				}
			}
		}

		if(!$prihlasky || $exist == false){
			if ($_POST['tid'] && $_POST['act'] == 'storno') {
				if (! isset( $_POST['storno_terminu_nonce'] ) || ! wp_verify_nonce( $_POST['storno_terminu_nonce'], 'storno_terminu' ) ) {
				   print 'Došlo k chybě, zkuste to prosím znovu.';
				   exit;
				} else {
					wp_trash_post($_POST['tid']);
					wp_redirect(wc_get_endpoint_url( 'terminy-lektorky', '', wc_get_page_permalink( 'myaccount' )).'?deleted=1');
				}
			}
		?>
			<form method="post" onsubmit="return confirm('Opravdu stornovat termín?');">
			  	<input type="hidden" name="tid" value="<?= $termin_id ?>">
			  	<input type="hidden" name="act" value="storno">
			  	<?php 
			  		wp_nonce_field( 'storno_terminu', 'storno_terminu_nonce' ); 
			  	?>
			  	<input type="submit" name="submit" value="Stornovat termín" class="send_mail_btn">
			</form>
		<?php }
	?>
	
	<h2>Přihlášky</h2>
	<?php

	// prehled prihlasek se lektorce zpristupni az 7 dni pred terminem

	$datum_konani = get_post_meta( $termin_id, 'datum_konani', true );

	$pocet_dni_do_kurzu = ( strtotime( $datum_konani ) - strtotime(current_time('Ymd') ) ) / DAY_IN_SECONDS ;

	/* if( $pocet_dni_do_kurzu > 7 ) {

		?>
		<p><em>Seznam přihlášených se zveřejní až 7 dní před datem konání.</em></p>
		<?php

	} else {*/

		if( $prihlasky ) {
			
			if ( $_POST['o']){
				if (! isset( $_POST['storno_order_nonce'] ) || ! wp_verify_nonce( $_POST['storno_order_nonce'], 'storno_order' ) ) {

				   print 'Došlo k chybě, zkuste to prosím znovu.';
				   exit;

				} else {
					if($_POST['t'] == 'so'){
						zmena_stavu_prihlasky( $_POST['o'], 'stornovano' );
					} elseif($_POST['t'] == 'po'){
						zmena_stavu_prihlasky( $_POST['o'], 'uhrazeno' );
					}
				   
				}
			}
			?>
			<div id="info_popup" class="white-popup mfp-hide">
			  <h2>Poznámky u přihlášky</h2>
			  <p class="client_note">Klient/ka neuvedl/a žádnou poznámku</p>
			  <h2>Interní poznámka lektorky</h2>
			  <form method="post" class="save_note">
			  	<input type="hidden" name="tid" class="tid">
			  	<textarea class="internal_post" rows="5" name="internal_post" style="width: 100%;"></textarea>
			  	<input type="submit" name="submit" value="Uložit poznámku">
			  </form>
			  
			</div>
			<div id="termin_popup" class="white-popup mfp-hide">
			  <h2>Změna termínu objednávky</h2>
			  <p><strong>Aktuálně zvolený termín: </strong><span class="termname"></span></p>
			  <form method="post" class="save_term_change" onsubmit="return confirm('Opravdu změnit termín objednávky?');">
			  	<input type="hidden" name="tid_change" class="tid_change">
			  	<select name="prihlaska-zmenit-termin" id="prihlaska-zmenit-termin" style="width: 50%;">

					<option value="">vyberte jiný termín</option>
					<?php
					$kurz_id = get_post_meta( $t_id, 'kurz', true );
					$terminy = get_posts(array(
						'post_type' => 'termin',
						'posts_per_page' => -1,
						'meta_query' => array(
							array(
								'key' => 'datum_konani',
								'value' => date('Ymd'),
								'compare' => '>=',
							),
							array(
								'key' => 'lektorka',
								'value' => prihlasena_lektorka_post_id(),
							),
						)
					));

					if( $terminy ) {
						foreach( $terminy as $termin )	 {
							if($termin->ID != $t_id){
								$nazev = termin_datum_konani( $termin->ID);
								$nazev .= ' ('. termin_lokalita( $termin->ID ).')';
								$nazev .= ' - ' . get_the_title(get_post_meta( $termin->ID, 'kurz', true ));

								?>
								<option value="<?php echo $termin->ID ?>"><?php echo $nazev ?></option>
								<?php
							}

						}
					}


					?>
				</select>
			  	<input type="submit" name="submit" value="Změnit termín">
			  </form>
			  
			</div>

			<div class="table_overflow" style="overflow-x: auto;">
				<table class="shop_table">
				<tr>
					<th>VS</th>
					<th title="Datum přihlášení na kurz">Dat. p.</th>
					<th>Jméno</th>
					<th>E-mail</th>
					<th>Stav přihlášky</th>
					<th>Informace</th>
					<th>Stornovat</th>
					<th>Uhradit</th>
					<th>Změnit <br> termín</th>
					<th>Info</th>

				</tr>

				<?php

				$nazev_terminu = termin_datum_konani( $t_id );
				$nazev_terminu .= ' ('. termin_lokalita( $t_id ).')';

				foreach( $prihlasky as $prihlaska ) {

					$jmeno = get_post_meta( $prihlaska->ID, 'prihlaska-jmeno', true ).' '.get_post_meta( $prihlaska->ID, 'prihlaska-prijmeni', true );
					$email = get_post_meta( $prihlaska->ID, 'prihlaska-email', true );
					$stav = prihlaska_stav_vizualne( $prihlaska->ID );
					$client_note = get_post_meta( $prihlaska->ID, 'prihlaska-poznamka', true );
					$internal_note = get_post_meta( $prihlaska->ID, 'internal_post', true );

					$par_jednotlivec = get_post_meta( $prihlaska->ID, 'prihlaska-typ', true );

					if( $par_jednotlivec == 'par' ) {

						$s_kym = get_post_meta( $prihlaska->ID, 'prihlaska-par-upresneni', true );

						$par_jednotlivec = 'v páru ('.htmlspecialchars($s_kym).')';
					} else {
						$par_jednotlivec = 'sama';
					}

					$informace = $par_jednotlivec;

					$vicercata = get_post_meta( $prihlaska->ID,'prihlaska-vicercata', true );

					if( $vicercata == 'ano' ) {
						$informace .= ', <strong>čeká vícerčata</strong>';
					}

					?>
					
					<tr class="<?= $vicercata == 'ano'?'vice_bg':''?>">
						<td><small><?= $prihlaska->ID ?></small></td>
						<td><small><?php echo date('j. n.', strtotime( $prihlaska->post_date ) )  ?></small></td>
						<td><strong><?php echo $jmeno ?></strong></td>
						<td><small><?php echo $email ?></small></td>
						<?php 
							$normal_state = prihlaska_stav($prihlaska->ID);
							if ($normal_state== 'uhrazeno') {
								$cls = 'bg-gren-status';
							} elseif($normal_state=='stornovano'){
								$cls = 'bg-red-status';
							} else {
								$cls = '';
							}
						?>
						<td class="stat <?= $cls ?>"><small><?php echo $stav ?></small></td>
						<td><small><?php echo $informace ?></small></td>
						<td>

							<?php 
							if (prihlaska_stav( $prihlaska->ID ) != 'stornovano'): ?>
								<small>
									<?php if($pocet_dni_do_kurzu < 0): ?>
										Nelze stornovat
									<?php else: ?>
										<form method="post" onsubmit="return confirm('Opravdu stornovat?');">
											<input type="hidden" name="o" value="<?= $prihlaska->ID ?>">
											<input type="hidden" name="t" value="so">
											<?php wp_nonce_field( 'storno_order', 'storno_order_nonce' ); ?>
											<input type="submit" value="Stornovat">
										</form>
									<?php endif; ?>
								</small>
							<?php endif ?>
						</td>
						<td>
							<?php if (prihlaska_stav( $prihlaska->ID ) != 'uhrazeno'): ?>
								<small>
									<form method="post" onsubmit="return confirm('Opravdu nastavit jako uhrazeno?');">
										<input type="hidden" name="o" value="<?= $prihlaska->ID ?>">
										<input type="hidden" name="t" value="po">
										<?php wp_nonce_field( 'storno_order', 'storno_order_nonce' ); ?>
										<input type="submit" value="Uhrazeno">
									</form>
								</small>
							<?php endif ?>
						</td>
						
						<td><small><a href="#" class="open-popup-termin" data-termname="<?= $nazev_terminu ?>" data-term="<?= $prihlaska->ID  ?>">Změnit</a></small></td>
						<td>
							<small>
								<div class="info_ico <?= $client_note?'info_ico_pink':''?>"><a href="#info_popup" class="open-popup-link" data-internal="<?= $internal_note ?>" data-client="<?= $client_note ?>" data-tid="<?= $prihlaska->ID ?>">i</a></div>
							</small>
						</td>
						
					</tr>
					<?php

				}
				?>
				</table>
			</div>
			<h2>Zaslání hromadného emailu všem přihlášeným</h2>
			  <form method="post" class="send_mail_all">
			  	<input type="hidden" name="send_mail" value="1">
			  	<?php 
			  		wp_nonce_field( 'send_mail_all', 'send_mail_all_nonce' ); 
			  		wp_editor( '', 'email_content', array( 'media_buttons' => false ) );
			  	?>
			  	<input type="submit" name="submit" value="Odeslat email" class="send_mail_btn">
			  </form>
			<?php

		} else {
			?>
			<p><em>Zatím žádné přihlášky.</em></p>
			<?php
		}

/*	} // 7 dni*/

}



















/*
88b           d88  88        88          88      88        88    ,ad8888ba,            888888888888         db         88      a8P          db         888888888888  888b      88  88  88      a8P
888b         d888  88        88          88      88        88   d8"'    `"8b                    ,88        d88b        88    ,88'          d88b                 ,88  8888b     88  88  88    ,88'
88`8b       d8'88  88        88          88      88        88  d8'                            ,88"        d8'`8b       88  ,88"           d8'`8b              ,88"   88 `8b    88  88  88  ,88"
88 `8b     d8' 88  88        88          88      88        88  88                           ,88"         d8'  `8b      88,d88'           d8'  `8b           ,88"     88  `8b   88  88  88,d88'
88  `8b   d8'  88  88        88          88      88        88  88                         ,88"          d8YaaaaY8b     8888"88,         d8YaaaaY8b        ,88"       88   `8b  88  88  8888"88,
88   `8b d8'   88  88        88          88      88        88  Y8,                      ,88"           d8""""""""8b    88P   Y8b       d8""""""""8b     ,88"         88    `8b 88  88  88P   Y8b
88    `888'    88  Y8a.    .a8P  88,   ,d88      Y8a.    .a8P   Y8a.    .a8P  888      88"            d8'        `8b   88     "88,    d8'        `8b   88"           88     `8888  88  88     "88,
88     `8'     88   `"Y8888Y"'    "Y8888P"        `"Y8888Y"'     `"Y8888Y"'   888      888888888888  d8'          `8b  88       Y8b  d8'          `8b  888888888888  88      `888  88  88       Y8b
*/




// vypisuje se pred tabulkou zakoupenych stahovatelnych produktu z eshopu

function my_account_informace_ke_kurzum() {



	// jeji prihlasky?
	// zjistim, na ktere terminy je prihlasena ... a ma zaplaceno
	// a jestli jsou k danemu terminu nejake materialy, a jiz jsou verejne

	$prihlasky = get_posts( array(
		'post_type' => 'prihlaska',
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key' => 'prihlaska-uzivatel',
				'value' => get_current_user_id(),
			)
		),
		'orderby' => 'post_date',
		'order' => 'DESC',
	) );

	if( $prihlasky ) {

		$terminy_ids = array();
		$terminy_a_prihlasky = array();

		foreach( $prihlasky as $prihlaska ) {

			$prihlaska_id = $prihlaska->ID;

			$termin_id = get_post_meta( $prihlaska_id, 'prihlaska-termin', true );

			if( ! $termin_id ) continue;

			$terminy_ids[] = $termin_id;

			if( isset( $terminy_a_prihlasky[ $termin_id ] ) ) {

				// Ha! Prave nastala divna situace a to ta, ze jiz mam ulozenou informaci o tom, ze mam prihlasku na tento termin...
				// Co to znamena? Ze zakaznice ma na dany termin vice prihlasek... treba se ji nepodarilo zaplatit on-line, pak udelala dalsi objednavku a uz to bylo OK.
				// Takze ma na termin dve prihlasky, starsi je stornovana a novejsi je uhrazena ... respektive, mel bych brat ohled jen na tu nejnovejsi.
				// Coz vzhledem k tomu, ze prihlasky nacitam dle data sestupne, tak je vzdycky ta prvni pro dany termin.
				// Takze pokud uz mam pro dany termin nejakou prihlasku poznamenanou, tak vsechny dalsi uz neresim, protoze ta prvni je ta nejaktualnejsi ...
				// ... a snad i ta uhrazena.

				// Nebo radeji ...

				// Takze ... je zaznamenana prihlaska pro dany termin, a pritom tu mam dalsi.
				// Teto dalsi bych dal prednost jedine v pripade, ze predchozi zaznamenana prihlaska neni uhrazena, a tato ano.
				// Jinak to necham lezet ...

				if( ! prihlaska_je_uhrazena( $terminy_a_prihlasky[ $termin_id ] ) && prihlaska_je_uhrazena( $prihlaska_id ) ) {

					$terminy_a_prihlasky[ $termin_id ] = $prihlaska_id;

				}

			} else {

				$terminy_a_prihlasky[ $termin_id ] = $prihlaska_id;

			}

		}

		$terminy = get_posts( array(
			'post_type' => 'termin',
			'posts_per_page' => -1,
			'post__in' => $terminy_ids,
		) );


		if( ! $terminy ) {

			// nevim ... nejaka necekana situace

		} else {

			foreach( $terminy as $termin ) {

				$termin_id = $termin->ID;
				$prihlaska_id = $terminy_a_prihlasky[ $termin_id ];

				?>
				<div class="muj-ucet-informace-ke-kurzu">

					<h2><?php echo get_the_title( $termin_id ) ?></h2>

					<?php tabulka_s_podrobnostmi_o_terminu( $termin_id ); ?>

					<!--
					<p>
						<strong><a href="?termin-do-ics=<?php echo $termin_id ?>" target="_blank"><?php _e('Stáhnout ICS soubor pro vložení do kalendáře','jz') ?></a></strong>
					</p>
					-->

					<?php

						// zaplacena prihlaska?
						// materialy?
						// datum terminu?

						// get_post_meta( $prihlaska_id, 'prihlaska-vicercata', true)

						if( ! prihlaska_je_uhrazena( $prihlaska_id ) ) {

							?><p><em><?php _e('Vaše přihláška není uhrazená. Případné informace ke kurzu budou přístupné po jejím uhrazení.','jz') ?></em></p><?php

						} else {

							// nejake informace k terminu??
							$kurz_id = termin_id_kurzu( $termin_id );

							// musim udelat 2 requesty ... nejdrive informace zverejnene hned po uhrazeni prihlasky
							// a pak teprve informace "nacasovane" dle nejakeho poctu dni pred nebo po
/* 							print_r(get_post_meta( $prihlaska_id, 'prihlaska-vicercata', true));
							die(); */
							if(get_post_meta( $prihlaska_id, 'prihlaska-vicercata', true) != 'ne'){
								$info_type = array(
									'key' => 'urceno_pro_dvojcata',
									'value' => '1'
								);
							} else {
								$info_type = array(
									'relation' => 'OR',
									array(
										'key'     => 'urceno_pro_dvojcata',
										'value'   => '0',
										'compare' => '=',
									),
									array(
										'key'     => 'urceno_pro_dvojcata',
										'compare' => 'NOT EXISTS',
									)
								);
							}

							$informace_hned_po_uhrazeni = get_posts( array(
								'post_type' => 'informace',
								'posts_per_page' => -1,
								'order' => 'ASC', // razeni dle data pridani do adminu, kdyz uz
								'meta_query' => array(
									'relation' => 'AND',
									array(
										'key' => 'souvisejici_kurz',
										'value' => $kurz_id
									),
									array(
										'key' => 'zpristupnit_po_uhrazeni_prihlasky',
										'value' => 1
									),
									$info_type
								)
							) );

							$informace_nacasovane = get_posts( array(
								'post_type' => 'informace',
								'posts_per_page' => -1,
								'meta_key' => 'kdy_informace_zpristupnit',
								'orderby' => 'meta_value_num',
								'order' => 'ASC',
								'meta_query' => array(
									'relation' => 'AND',
									array(
										'key' => 'souvisejici_kurz',
										'value' => $kurz_id
									),
									array(
										'key' => 'zpristupnit_po_uhrazeni_prihlasky',
										'value' => 0
									),
									$info_type
								)
							) );


							$informace = array_merge( $informace_hned_po_uhrazeni, $informace_nacasovane  );

							//print_r($info_type); exit;

							if( empty( $informace ) ) {

								?><p><em><?php _e('Ke kurzu nejsou zveřejněny žádné informace.','jz') ?></em></p><?php

							} else {

								// informace jsou ... ale maji byt jiz zverejnene?

								// pozor, podobne casovani se resi i v cronu

								$datum_konani = get_post_meta( $termin_id, 'datum_konani', true ); // v surovem stavu

								foreach( $informace as $info ) {

									$informace_id = $info->ID;

									$tyto_informace_maji_byt_zverejnene = false;

									// hne dpo uhrazeni prihlasky?

									$zpristupnit_po_uhrazeni_prihlasky = get_post_meta( $informace_id, 'zpristupnit_po_uhrazeni_prihlasky', true );

									if( $zpristupnit_po_uhrazeni_prihlasky ) {

										$tyto_informace_maji_byt_zverejnene = true;

									} else {

										// az od nejakeho data?

										$kdy_informace_zpristupnit = (int) get_post_meta( $informace_id , 'kdy_informace_zpristupnit', true );

										$zverejnit_od_data = 0;

										if( $kdy_informace_zpristupnit == 0 ) { // v den konani kurzu (a dale)

											$zverejnit_od_data = $datum_konani;

										} else { // zverejnit nekdy pred terminem / po terminu

											// je-li to kladne cislo, musim doplnit + pro strtotime (zaporne cislo ma - automaticky)
											if( $kdy_informace_zpristupnit > 0 )
												$kdy_informace_zpristupnit = '+'.$kdy_informace_zpristupnit;

											$zverejnit_od_data = date( 'Ymd', strtotime( $kdy_informace_zpristupnit.'days', strtotime( $datum_konani ) ) );

										}


										if( $zverejnit_od_data && $zverejnit_od_data <= date('Ymd') ) {

											$tyto_informace_maji_byt_zverejnene = true;

										}

									}

									


									if( $tyto_informace_maji_byt_zverejnene ) {

										// tak ... nejake informace jsou "zverejnitelne" ...
										// ale musi to byt bud textove informace, a nebo materialy ... pokud je to jen emailova rozesilka, tak nemam v uctu co nabidnout

										$zverejnit_informace_v_mem_uctu = get_field( 'zverejnit_informace_v_mem_uctu', $informace_id );
										$informace_v_mem_uctu = get_field( 'informace_v_mem_uctu', $informace_id );

										$zverejnit_materialy_ke_stazeni = get_field( 'zverejnit_materialy_ke_stazeni', $informace_id );
										$zverejnene_materialy = get_field( 'materialy', $informace_id );

										$afterYear =  date( 'Ymd', strtotime( '+1 year' , strtotime( $datum_konani )));

										if ($afterYear < date( 'Ymd')){
											$zverejnit_materialy_ke_stazeni = false;
										}


										if( ! $zverejnit_informace_v_mem_uctu && ! $zverejnit_materialy_ke_stazeni  ){

											continue; // neni co zverejnit!

										}

										?>
										<div class="muj-ucet-informace">
											<h3 class="muj-ucet-informace-nadpis"><?php echo $info->post_title ?></h3>

											<div class="muj-ucet-informace-obsah">
											<?php



											if( $zverejnit_informace_v_mem_uctu && $informace_v_mem_uctu ) {

												if( strpos($informace_v_mem_uctu, 'LOKALITA') ) {

													$informace_o_lokalite = termin_lokalita_informace( $termin_id );

													$informace_v_mem_uctu = str_replace( 'LOKALITA', $informace_o_lokalite, $informace_v_mem_uctu );
												}

												?>
												<div class="muj-ucet-informace-textove">
													<?php echo $informace_v_mem_uctu ?>
												</div>
												<?php
											}



											if( $zverejnit_materialy_ke_stazeni && $zverejnene_materialy ) {

												?>
												<table class="shop_table">
												<tr>
													<th><?php _e('Materiál','jz') ?></th>
													<th><?php _e('Stáhnout','jz') ?></th>
												</tr>
												<?php
												foreach( $zverejnene_materialy as $soubor ) {

													$soubor_id = $soubor['soubor']['id'];

													//-------------
													// Tak. Zde, v tento uzasny okamzik vim, ze je prihlasen uzivatel, ktery ma mit pristup k tomuto jednomu souboru (tedy ke vsem, samozrejme).
													// Udelam tedy to, ze mu id sady materialu a id souboru ulozim jako (docasnou?) meta informaci.
													// A do teto mety budu pocitat stazeni onoho materialu.
													// Ale! Budu vedet, ze abych stazeni souboru vubec umoznil, tak uzivatel tuto metu MUSI mit vytvorenou!
													// Tim totiz vim, ze uzivatel alespon jednou videl tuto obrazovku s pristupnymi soubory ... a vim, ze ma tedy na stazeni narok.
													//-------------

													$material_meta_key = 'material-'.$informace_id.'-'.$soubor_id;

													if( ! ( $stazeno = get_user_meta( get_current_user_id(), $material_meta_key , true ) ) ) {

														update_user_meta( get_current_user_id(), $material_meta_key, 'povoleno' ); // ulozim string "povoleno", jako prvotni hodnotu (pak uz to bude 1, 2, 3, ...)

													}

													$stazeno = intval( $stazeno ); // intval('povoleno') == 0 a


													//-------------


													$nazev = $soubor['nazev'];
													$soubor_filename = basename( $soubor['soubor']['url'] );

													if( ! $nazev ) {
														$nazev = $soubor_filename;
													}

													/* if( $stazeno >= MAX_POCET_STAZENI_MATERIALU ) {

														$odkaz = '<em>'.__('Dosažen limit stažení', 'jz').'</em>';

													} else {

														$soubor_url = material_chranena_url( $informace_id, $soubor_id );

														$odkaz = '<a href="'. $soubor_url .'" target="_blank">'.__('Stáhnout','jz') .'</a>';

														$odkaz .= ' <small title="'.__('Počet využitých povolených stažení tohoto souboru','jz').'">('.$stazeno.'/'.MAX_POCET_STAZENI_MATERIALU.'</small>)';

													} */

													$soubor_url = material_chranena_url( $informace_id, $soubor_id );

													$odkaz = '<a href="'. $soubor_url .'" target="_blank">'.__('Stáhnout','jz') .'</a>';

													//$odkaz .= ' <small title="'.__('Počet využitých povolených stažení tohoto souboru','jz').'">('.$stazeno.'/'.MAX_POCET_STAZENI_MATERIALU.'</small>)';


													?>
													<tr>
														<td><?php echo $nazev ?></td>
														<td><?php echo $odkaz ?></td>
													</tr>
													<?php
												}
												?>
												</table>
												<?php

											}


											?>
											</div>

										</div>
										<?php
									}


								}


							}


						} // if zaplacena




					?>

				</div>
				<?php

			}

		}



	} else {
		?>
		<p><em><?php _e('Nejste přihlášená na žádný kurz.','jz') ?></em></p>
		<?php
	}


}







/*
  ,ad8888ba,   88888888ba     ,ad8888ba,    888b      88      88b           d88         db         888888888888  88888888888  88888888ba   88         db         88           8b        d8
 d8"'    `"8b  88      "8b   d8"'    `"8b   8888b     88      888b         d888        d88b             88       88           88      "8b  88        d88b        88            Y8,    ,8P
d8'            88      ,8P  d8'        `8b  88 `8b    88      88`8b       d8'88       d8'`8b            88       88           88      ,8P  88       d8'`8b       88             Y8,  ,8P
88             88aaaaaa8P'  88          88  88  `8b   88      88 `8b     d8' 88      d8'  `8b           88       88aaaaa      88aaaaaa8P'  88      d8'  `8b      88              "8aa8"
88             88""""88'    88          88  88   `8b  88      88  `8b   d8'  88     d8YaaaaY8b          88       88"""""      88""""88'    88     d8YaaaaY8b     88               `88'
Y8,            88    `8b    Y8,        ,8P  88    `8b 88      88   `8b d8'   88    d8""""""""8b         88       88           88    `8b    88    d8""""""""8b    88                88
 Y8a.    .a8P  88     `8b    Y8a.    .a8P   88     `8888      88    `888'    88   d8'        `8b        88       88           88     `8b   88   d8'        `8b   88                88
  `"Y8888Y"'   88      `8b    `"Y8888Y"'    88      `888      88     `8'     88  d8'          `8b       88       88888888888  88      `8b  88  d8'          `8b  88888888888       88
*/


function jz_materialy_shedule_cron() {

	if (! wp_next_scheduled ( 'jz_rozeslani_informaci' )) {
		wp_schedule_event(time(), 'twicedaily', 'jz_rozeslani_informaci');
	}

}

add_action( 'init', 'jz_materialy_shedule_cron' );




function jz_rozeslani_informaci_job() {

	// tak ... spustil se cron, co ma obejit vsechny terminy a obeslat zaplacene prihlasky informacemi o pripadne zpristupnenych materialech

	// nactu vsechny materialy k terminum a zjistim, kolik dni pred a po terminu maji byt odeslany ...
	// vezmu nejnizsi datum odeslani (treba "20 dni pred terminem"), stejne tak i nejvyssi ("poslat 10 dni po konani"), a prictu/odectu to k dnesku, coz mi da nejzassi datum konani nejakeho terminu,
	// pro ktery mam nactene materialy k odeslani / a opacne.
	// A nactu pouze terminy v tomto rozmezi, pujdu po nich a jejich prihlaskach, budu kontrolovat drivejsi odeslani dane sady materialu,
	// a pripadne onu sadu rozesilat

	$dnesek = current_time('Ymd');

	$informace = get_posts( array(
		'post_type' => 'informace',
		'posts_per_page' => -1,
		'meta_key' => 'kdy_informace_zpristupnit',
		'orderby' => 'meta_value_num',
		'order' => 'ASC',
	) );

	if( ! $informace ) return;

	$informace_ke_kurzum = array();

	$nejnizsi_kdy_informace_zpristupnit = $nejvyssi_kdy_informace_zpristupnit = 0;

	foreach( $informace as $info ) {

		$informace_id = $info->ID;

		$kurz_id = get_post_meta( $informace_id, 'souvisejici_kurz', true );

		$informace_ke_kurzum[ $kurz_id ][] = $info; // post obj.

		$kdy_informace_zpristupnit = (int) get_post_meta( $informace_id , 'kdy_informace_zpristupnit', true );

		$nejnizsi_kdy_informace_zpristupnit = min( $nejnizsi_kdy_informace_zpristupnit, $kdy_informace_zpristupnit );

		$nejvyssi_kdy_informace_zpristupnit = max( $nejvyssi_kdy_informace_zpristupnit, $kdy_informace_zpristupnit );

	}


	$nejvzdalenejsi_pocet_dni_do_budoucnosti = abs( $nejnizsi_kdy_informace_zpristupnit ); // zaporne cislo

	$nejvzdalenejsi_datum_konani_v_budoucnosti = date('Ymd', strtotime( '+'.$nejvzdalenejsi_pocet_dni_do_budoucnosti.'days' ) );

	$nejvzdalenejsi_pocet_dni_do_minulosti = abs( $nejvyssi_kdy_informace_zpristupnit ); // kladne cislo tak jako tak

	$nejvzdalenejsi_datum_konani_v_minulosti = date('Ymd', strtotime( '-'.$nejvzdalenejsi_pocet_dni_do_minulosti.'days' ) );

	$terminy = get_posts( array(
		'post_type' => 'termin',
		'posts_per_page' => -1,
		'meta_key' => 'datum_konani',
		'orderby' => 'meta_value_num',
		'order' => 'ASC',
		'meta_query' => array(
			array(
				'key' => 'datum_konani',
				'value' => $nejvzdalenejsi_datum_konani_v_budoucnosti,
				'compare' => '<=',
			),
			array(
				'key' => 'datum_konani',
				'value' => $nejvzdalenejsi_datum_konani_v_minulosti,
				'compare' => '>=',
			)
		),
	) );


	if( ! $terminy ) return; // nejsou zadne terminy, pro ktere by bylo mozne neco odesilat


	foreach( $terminy as $termin ) {

		$termin_id = $termin->ID;

		$kurz_id = get_post_meta( $termin_id, 'kurz', true );

		$datum_konani = get_post_meta( $termin_id, 'datum_konani', true );

		// informace o probehlych rozesilkach materialu tohoto terminu

		$probehle_rozesilky_k_tomuto_terminu = get_post_meta( $termin_id, 'probehle_rozesilky', true ); // array

		if( ! $probehle_rozesilky_k_tomuto_terminu )
			$probehle_rozesilky_k_tomuto_terminu = array();


		if( isset( $informace_ke_kurzum[ $kurz_id ] ) ) {

			$informace_pro_tento_termin = $informace_ke_kurzum[ $kurz_id ];

			foreach( $informace_pro_tento_termin as $info ) {

				$informace_id = $info->ID;


				// POKUD SE NEPOSILA AUTOMATICKY EMAIL, TAK TO DAL NERESIM !

				$posilat_email = get_post_meta( $informace_id, 'automaticky_email', true );

				if( ! $posilat_email ) continue; // o techto informacich se info neposila ...


				if( isset( $probehle_rozesilky_k_tomuto_terminu[ $informace_id ] ) ) continue; // pecu na to, tato sada materialu se jiz nekdy rozesilala (nebo to mel web v planu)


				$kdy_informace_zpristupnit = (int) get_post_meta( $informace_id , 'kdy_informace_zpristupnit', true );

				$zverejnit_k_datu = false;

				if( $kdy_informace_zpristupnit == 0 ) { // v den konani kurzu (a dale)

					$zverejnit_k_datu = $datum_konani;

				} else { // zverejnit nekdy pred terminem / po terminu

					// je-li to kladne cislo, musim doplnit + pro strtotime (zaporne cislo ma - automaticky)
					if( $kdy_informace_zpristupnit > 0 )
						$kdy_informace_zpristupnit = '+'.$kdy_informace_zpristupnit;

					$zverejnit_k_datu = date( 'Ymd', strtotime( $kdy_informace_zpristupnit.'days', strtotime( $datum_konani ) ) );

				}

				if( $zverejnit_k_datu == $dnesek ) {

					// tak ... tyto informace se dnes maji zverejnit! tralala!

					// ale to, jestli jsou vlozene nejake soubory, uz nemusim resit! informace mohou byt zverejneny samy o sobe (textove), nebo se jen posila email


					// ale ma tento termin vubec nejake prihlasky? zaplacene? jinak ani zadne rozesilani resit nebudu ...
					// (ale potreboval bych si zalogovat, ze jsem to mel vubec v umyslu)

					$zaplacene_prihlasky_ids = termin_id_zaplacenych_prihlasek( $termin_id );

					if( count( $zaplacene_prihlasky_ids ) ) {

						// tak ... bude se rozesilat.

						$zaplacene_prihlasky = get_posts( array(
							'post_type' => 'prihlaska',
							'posts_per_page' => -1,
							'post__in' => $zaplacene_prihlasky_ids,
						) );

						if( $zaplacene_prihlasky ) {

							$emaily = $jmena = array();

							foreach( $zaplacene_prihlasky as $prihlaska ) {

								$prihlaska_id = $prihlaska->ID;

								if(get_field('urceno_pro_dvojcata', $informace_id)
									&& get_post_meta( $prihlaska_id, 'prihlaska-vicercata', true) != 'ne') {

									$jmena[] = get_post_meta( $prihlaska_id, 'prihlaska-jmeno', true ).' '.get_post_meta( $prihlaska_id, 'prihlaska-prijmeni', true );
									$emaily[] = get_post_meta( $prihlaska_id, 'prihlaska-email', true );

								} elseif (!get_field('urceno_pro_dvojcata', $informace_id)
									&& get_post_meta( $prihlaska_id, 'prihlaska-vicercata', true) == 'ne') {

									$jmena[] = get_post_meta( $prihlaska_id, 'prihlaska-jmeno', true ).' '.get_post_meta( $prihlaska_id, 'prihlaska-prijmeni', true );
									$emaily[] = get_post_meta( $prihlaska_id, 'prihlaska-email', true );
								}

							}

							if( informace_odeslani_automatickeho_emailu( $informace_id, $termin_id, $emaily ) ) {

								$probehle_rozesilky_k_tomuto_terminu[ $informace_id ] = array(
									'datum' => current_time('Y-m-d H:i:s'),
									'informace' => 'Rozesílka o informacích pro: '.implode(', ',$jmena),
								);

							} else {

								$probehle_rozesilky_k_tomuto_terminu[ $informace_id ] = array(
									'datum' => current_time('Y-m-d H:i:s'),
									'informace' => 'CHYBA ROZESÍLKY! Pokud o odeslání informací nebyl úspěšný.',
								);

							}


						}


					} else {

						// ma se rozesilat, ale nikdo neni prihlaseny (tedy nema zaplaceno)

						$probehle_rozesilky_k_tomuto_terminu[ $informace_id ] = array(
							'datum' => current_time('Y-m-d H:i:s'),
							'informace' => 'Rozesílka neprovedena, není evidována žádná uhrazená přihláška.',
						);

					}




					// mela probihat rozesilka ... ulozim si log

					update_post_meta( $termin_id, 'probehle_rozesilky', $probehle_rozesilky_k_tomuto_terminu );

				}


			}

		}

	}

	// if( isset($_GET['falsecron']) ) exit;

}
add_action('jz_rozeslani_informaci', 'jz_rozeslani_informaci_job');



/*
if( isset($_GET['falsecron']) )
	add_action('init', 'jz_rozeslani_informaci_job');
*/










/*
	Tato funkce se vola i z administrace -> u prihlasky je mozne manualne odeslat vybrany email (= informace) znovu

	Je treba pocitat s tim, ze $emaily_prijemcu muze byt pole emailu, a nebo jen jeden email (string?)

*/
function informace_odeslani_automatickeho_emailu( $informace_id, $termin_id, $emaily_prijemcu ) {

	$email_predmet = get_field( 'automaticky_email_predmet', $informace_id );
	$email_text = get_field( 'automaticky_email_text', $informace_id );

	if( strpos( $email_text, 'LOKALITA' ) ) {

		$info_o_lokalite = termin_lokalita_informace( $termin_id );

		$email_text = str_replace( 'LOKALITA', $info_o_lokalite, $email_text );

	}

	$email_text = jz_zacatek_html_emailu() . $email_text . jz_konec_html_emailu();


	$prijemce_emailu = 'info@jemnezrozeni.cz';

	$headers = array();
	$headers[] = 'Content-Type: text/html; charset=UTF-8';
	$headers[] = 'From: '.get_bloginfo('name').' <'.JZ_KONTAKTNI_EMAIL.'>';


	// radeji trochu kontrola, kdyby byl predan jen jeden email jako string
	if( is_string( $emaily_prijemcu ) ) {

		$emaily_prijemcu = array( $emaily_prijemcu );

	}


	if( is_array($emaily_prijemcu) && count( $emaily_prijemcu ) ) {

		foreach( $emaily_prijemcu as $email_prijemce ) {
			$headers[] = 'Bcc: '.$email_prijemce;
		}

	}

	// testing ...
	// $headers[] = 'Bcc: marek.klusak@gmail.com';

	return wp_mail( $prijemce_emailu , $email_predmet, $email_text, $headers ) ;
}










































function material_chranena_url( $informace_id, $attach_id ) {

	return home_url( '?mat='.$informace_id.'&a='.$attach_id );

}




function material_output() {

	$informace_id = intval( $_GET['mat'] );
	$soubor_id = intval( $_GET['a'] );

	// uzivatel musi byt prihlaseny a musi mit adekvatni metu

	if( ! is_user_logged_in() ) {

		wp_die( __('Materiály ke kurzům mohou stahovat pouze přihlášení účastníci!','jz') );

	}

	$meta_key = 'material-'.$informace_id.'-'.$soubor_id;

	$meta_value = get_user_meta(  get_current_user_id() , $meta_key, true );

	if( ! $meta_value ) {

		// ha! neni vytvorena meta! takze uzivatel nikdy nevidel seznam techto pristupnych materialu ve svem uctu, a nema k nim tedy pristup

		wp_die( __('K tomuto materiálu nemáte přístup!','jz') );

	}


	/* if( intval( $meta_value ) >= MAX_POCET_STAZENI_MATERIALU ) { // pokud je metavalue 'povoleno', tak intval('povoleno') = 0 ... tudiz OK

		wp_die( __('Bohužel, byl dosažen limit počtu stažení tohoto materiálu.','jz') );

	} */



	// tak ... muzeme stahovat.
	// a zapocitat stazeni
	// ale existuje material?

	$path = get_attached_file( $soubor_id );

	if( ! $path ) {

		wp_die( __('Bohužel, materiál ke stažení byl odstraněn.','jz') );

	}


	// vse ok ...

	if( ob_get_status() ) { // co kdyby ...

		ob_end_clean();

	}


	header('Content-Type: application/octet-stream');
	header("Content-Transfer-Encoding: Binary");
	header("Content-disposition: attachment; filename=\"" . basename($path) . "\"");

	if( @readfile($path) ) {

		// zapocitame ...

		$novy_pocet_stazeni = intval( $meta_value ) + 1; // intval('povoleno') = 0 ... tudiz OK

		update_user_meta( get_current_user_id() , $meta_key, $novy_pocet_stazeni );

	}

	exit;
}

if( ! empty($_GET['mat']) && !empty( $_GET['a'] ) )
	add_action( 'init', 'material_output' );

























































/*
88b           d88         db         88  88             ,ad8888ba,   88        88  88  88b           d88  88888888ba
888b         d888        d88b        88  88            d8"'    `"8b  88        88  88  888b         d888  88      "8b
88`8b       d8'88       d8'`8b       88  88           d8'            88        88  88  88`8b       d8'88  88      ,8P
88 `8b     d8' 88      d8'  `8b      88  88           88             88aaaaaaaa88  88  88 `8b     d8' 88  88aaaaaa8P'
88  `8b   d8'  88     d8YaaaaY8b     88  88           88             88""""""""88  88  88  `8b   d8'  88  88""""""'
88   `8b d8'   88    d8""""""""8b    88  88           Y8,            88        88  88  88   `8b d8'   88  88
88    `888'    88   d8'        `8b   88  88            Y8a.    .a8P  88        88  88  88    `888'    88  88
88     `8'     88  d8'          `8b  88  88888888888    `"Y8888Y"'   88        88  88  88     `8'     88  88
*/

// https://rudrastyh.com/api/mailchimp-subscription.html


function jz_mailchimp( $email , $merge_fields = array('FNAME' => '','LNAME' => '') ) {

	$status = 'subscribed';

	$list_id = '62e5d5c2c4';

	$api_key = '50f6f749cf23f0ffaf0a5e466ef6f492-us16';

	$result = json_decode( rudr_mailchimp_subscriber_status( $email, $status, $list_id, $api_key, $merge_fields ) );

	if( $result->status == 400 ){

		return false;

	} elseif( $result->status == 'subscribed' ){
		return true;
	}

}


function rudr_mailchimp_subscriber_status( $email, $status, $list_id, $api_key, $merge_fields = array('FNAME' => '','LNAME' => '') ){
	$data = array(
		'apikey'        => $api_key,
		'email_address' => $email,
		'status'        => $status,
		'merge_fields'  => $merge_fields
	);
	$mch_api = curl_init(); // initialize cURL connection

	curl_setopt($mch_api, CURLOPT_URL, 'https://' . substr($api_key,strpos($api_key,'-')+1) . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/' . md5(strtolower($data['email_address'])));
	curl_setopt($mch_api, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.base64_encode( 'user:'.$api_key )));
	curl_setopt($mch_api, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
	curl_setopt($mch_api, CURLOPT_RETURNTRANSFER, true); // return the API response
	curl_setopt($mch_api, CURLOPT_CUSTOMREQUEST, 'PUT'); // method PUT
	curl_setopt($mch_api, CURLOPT_TIMEOUT, 10);
	curl_setopt($mch_api, CURLOPT_POST, true);
	curl_setopt($mch_api, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($mch_api, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json

	$result = curl_exec($mch_api);
	return $result;
}

















function jz_footer_mailchimp() {

	$response = array()	;

	if( !empty( $_POST['mc-email'] )  ) {

		if( jz_mailchimp( $_POST['mc-email'] ) ) {

			$response['success'] = __('E-mail uložen.','jz');

		} else {

			$response['error'] = __('Došlo k chybě. Zkuste to prosím později.','jz');

		}

	} else {
		$response['error'] = __('E-mailová adresa je prázdná!','jz');
	}

	wp_send_json( $response );
	exit;

}
add_action( 'wp_ajax_jz_footer_mailchimp', 'jz_footer_mailchimp'  );
add_action( 'wp_ajax_nopriv_jz_footer_mailchimp', 'jz_footer_mailchimp'  );





function jz_save_note() {

	$response = array()	;

	if( !empty( $_POST['internal_post'] )  ) {
		update_post_meta( $_POST['order_id'], 'internal_post', $_POST['internal_post'], '' );

		$response['success'] = __('Poznámka byla uložena.','jz');

	} else {

		$response['error'] = __('Poznámka je prázdná!','jz');

	}

	wp_send_json( $response );
	exit;

}
add_action( 'wp_ajax_jz_save_note', 'jz_save_note');
add_action( 'wp_ajax_nopriv_jz_save_note', 'jz_save_note');















/*
function preceneni() {

	$terminy = get_posts( array('post_type' => 'termin', 'posts_per_page' => -1) );

	if( ! $terminy ) return;

	foreach( $terminy as $termin  ){

		$kurz_id = get_post_meta( $termin->ID, 'kurz', true );

		if( $kurz_id == 476 ) { // kompletni

			update_field( 'field_5a26fdff09c28', 3300 , $termin->ID ); // akcni cena czk par
			update_field( 'field_5a26fe5c09c2a', 2500 , $termin->ID ); // akcni cena czk jednotlivec

			update_field( 'field_5a26fe4109c29', 'Akční cena při zakoupení do 31. 12. 2017' , $termin->ID ); // akcni cena par poznamka
			update_field( 'field_5a26fe8b09c2c', 'Akční cena při zakoupení do 31. 12. 2017' , $termin->ID ); // akcni cena jednotlivec poznamka

		} else if( $kurz_id == 526 ) { // hypnoporod

			update_field( 'field_5a26fe5c09c2a', 1590 , $termin->ID ); // akcni cena czk jednotlivec
			update_field( 'field_5a26fe8b09c2c', 'Akční cena při zakoupení do 31. 12. 2017' , $termin->ID ); // akcni cena jednotlivec poznamka
		}

	}

	wp_die( 'Asi preceneno.');

}

if( isset($_GET['preceneni']) )
	add_action( 'init', 'preceneni' );
*/









/*
function doplnenicislauctu() {

	$terminy = get_posts( array('post_type' => 'termin', 'posts_per_page' => -1) );

	if( ! $terminy ) return;

	foreach( $terminy as $termin  ){

		$kurz_id = get_post_meta( $termin->ID, 'kurz', true );

		update_field( 'field_5a2627fe2ff9f', '2800835793/2010' , $termin->ID ); // akcni cena czk par


	}

	wp_die( 'Asi preceneno.');

}

if( isset($_GET['doplnenicislauctu']) )
	add_action( 'init', 'doplnenicislauctu' );
*/



















/*
88888888888            88888888ba     ,ad8888ba,      ,ad8888ba,    88      a8P   8b        d8
88                     88      "8b   d8"'    `"8b    d8"'    `"8b   88    ,88'     Y8,    ,8P
88                     88      ,8P  d8'        `8b  d8'        `8b  88  ,88"        Y8,  ,8P
88aaaaa                88aaaaaa8P'  88          88  88          88  88,d88'          "8aa8"
88"""""      aaaaaaaa  88""""""8b,  88          88  88          88  8888"88,          `88'
88           """"""""  88      `8b  Y8,        ,8P  Y8,        ,8P  88P   Y8b          88
88                     88      a8P   Y8a.    .a8P    Y8a.    .a8P   88     "88,        88
88888888888            88888888P"     `"Y8888Y"'      `"Y8888Y"'    88       Y8b       88
*/



function ebooky_ke_stazeni( $atts = array() ) {

	$code = '';

	$code .= '<div class="ebook-shortcode">';

	$zobrazit_ebooky = false;

	if( is_user_logged_in() ) {

		$zobrazit_ebooky = true;

	} else if( ! empty($_POST['ebook-email']) && is_email( $_POST['ebook-email'] ) ) {

		$zobrazit_ebooky = true;

		jz_mailchimp( $_POST['ebook-email'] );

	}


	if( $zobrazit_ebooky ) {

		$code .= '<p><strong>'.__('Nyní si můžete stáhnout následující e-booky:','jz').'</strong></p>';

		foreach( array( 1239, 1246 ) as $attach_id ) {

			$url = wp_get_attachment_url($attach_id);
			$nazev = get_the_title( $attach_id );

			$code .= '<h3><a href="'.$url.'" target="_blank">'.$nazev.'</a></h3>';

		}


	} else {

		global $post;

		$code .= '<form method="post" action="'.get_permalink( $post->ID ).'">';

		$code .= '<p><label for="ebook-email">'.__('Pro stažení e-booku zadejte prosím Váš e-mail','jz').'</label></p>';

		$code .= '<p><input type="email" name="ebook-email" id="ebook-email"></p>';

		$code .= '<p><input type="submit" value="'.__('Stáhnout e-book','jz').'"></p>';
		$code .= '<p>Odesláním formuláře souhlasíte se <a href="/wp-content/uploads/2018/06/souhlas.pdf" target="_blank">zpracováním osobních údajů</a></p>';

		$code .= '</form>';
	}


	$code .= '</div>';

	return $code;
}
add_shortcode( 'ebooky-ke-stazeni', 'ebooky_ke_stazeni' );














































/*
88      a8P   88        88  88888888ba     ,ad8888ba,    888b      88  8b        d8
88    ,88'    88        88  88      "8b   d8"'    `"8b   8888b     88   Y8,    ,8P
88  ,88"      88        88  88      ,8P  d8'        `8b  88 `8b    88    Y8,  ,8P
88,d88'       88        88  88aaaaaa8P'  88          88  88  `8b   88     "8aa8"
8888"88,      88        88  88""""""'    88          88  88   `8b  88      `88'
88P   Y8b     88        88  88           Y8,        ,8P  88    `8b 88       88
88     "88,   Y8a.    .a8P  88            Y8a.    .a8P   88     `8888       88
88       Y8b   `"Y8888Y"'   88             `"Y8888Y"'    88      `888       88
*/





add_action( 'manage_slevovy-poukaz_posts_columns', 'slevovyPoukazAdminSloupce' );
add_action( 'manage_slevovy-poukaz_posts_custom_column', 'slevovyPoukazAdminSloupceContent', 10, 2);
add_action( 'admin_head', 'slevovyPoukazAdminHead' );

function slevovyPoukazAdminSloupce( $columns ) {

	unset($columns['date']);

	$columns['kod'] = 'Kód';
	$columns['typ'] = 'Typ slevy';
	$columns['hodnota'] = 'Hodnota';
	$columns['kurz'] = 'Kurz';
	$columns['platnost'] = 'Platnost';
	$columns['pouziti'] = 'Použití';

	return $columns;
}

function slevovyPoukazAdminSloupceContent( $column_name, $post_id ) {

	if( $column_name == 'kod' ) {

		echo get_field('kod_poukazu', $post_id);

	} else if( $column_name == 'typ' ) {

		$typ = get_field('typ_slevy', $post_id);

		$typy = array(
			'procentualni' => 'Procentuální sleva',
			'odecitaci' => 'Odečtení pevné částky',
			'zdarma' => 'Kurz bude zdarma',
		);

		echo $typy[ $typ ];


	} else if( $column_name == 'hodnota' ) {

		$typ = get_field('typ_slevy', $post_id);

		if( $typ == 'procentualni' ) {

			echo get_field('procentualni_sleva', $post_id).' %';

		} else if( $typ == 'odecitaci') {

			$czk = get_field('odecist_castku_czk', $post_id);
			$eur = get_field('odecist_castku_eur', $post_id);

			echo "$czk Kč / $eur €";

		} else {

			echo '-';

		}


	} else if( $column_name == 'typ' ) {

		$typ = get_field('typ_slevy', $post_id);

		$typy = array(
			'procentualni' => 'Procentuální sleva',
			'odecitaci' => 'Odečtení pevné částky',
			'zdarma' => 'Kurz bude zdarma',
		);

		echo $typy[ $typ ];

	} else if( $column_name == 'kurz' ) {

		$omezit = get_field('omezit_na_kurz', $post_id);

		if( ! $omezit ) {
			echo 'Všechny';
		} else {

			$kurz_id = get_field( 'kurz', $post_id );

			echo get_the_title( $kurz_id );

		}

	}  else if( $column_name == 'platnost' ) {

		$platnost = get_field('casove_omezeni_platnosti', $post_id);

		if( ! $platnost ) {
			echo '<strong style="color:blue">Neomezeno</strong>';
		} else {

			$od = get_field( 'casove_omezeni_platnosti_od', $post_id );
			$do = get_field( 'casove_omezeni_platnosti_do', $post_id );
			$dnes = current_time('Ymd');

			$plati = $od <= $dnes && $do >= $dnes;

			echo '<span style="color:'.( $plati ? 'green' : 'red' ).'">';

			echo 'Od '.date('j. n. Y', strtotime($od) ).'<br>';
			echo 'do '.date('j. n. Y', strtotime($do) );

			echo '</span>';
		}

	} else if( $column_name == 'pouziti' ) {

		$omezit_pocet_pouziti = get_field('omezit_pocet_pouziti', $post_id);

		if( ! $omezit_pocet_pouziti ) {
			echo '<strong style="color:blue">Neomezeno</strong>';
		} else {

			$zbyvajici_pocet_pouziti = get_field( 'zbyvajici_pocet_pouziti', $post_id );

			echo '<strong style="color:'.( $zbyvajici_pocet_pouziti ? 'green' : 'red' ).'">Zbývá '.$zbyvajici_pocet_pouziti.'</strong>';

		}

	}

}

function slevovyPoukazAdminHead(  ) {
	?>
	<style type="text/css">
		.post-type-slevovy-poukaz .wp-list-table { table-layout: auto;}
	</style>
	<?php
}






















/*
	Aby byl poukaz validni, tak:
	- musi existovat
	- musi byt urcen pro dany kurz, je-li omezeno
	- musi platit datum, je-li omezeno
	- musi mit nenulovy pocet pouziti, je-li omezeno
*/

function slevovy_poukaz_je_validni( $slevovy_poukaz, $kurz_id ) {

	$post_id = slevovy_poukaz_post_id( $slevovy_poukaz );

	if( ! $post_id )
		return false;

	$omezit_na_kurz = get_field('omezit_na_kurz', $post_id);

	if( $omezit_na_kurz && $kurz_id != $omezit_na_kurz ) {
		return false;
	}

	$omezeni_platnosti = get_field('casove_omezeni_platnosti', $post_id);

	if( $omezeni_platnosti ) {

		$od = get_field( 'casove_omezeni_platnosti_od', $post_id );
		$do = get_field( 'casove_omezeni_platnosti_do', $post_id );
		$dnes = current_time('Ymd');

		if( $od > $dnes || $do < $dnes ) {
			return false;
		}

	}

	$omezit_pocet_pouziti = get_field('omezit_pocet_pouziti', $post_id);

	if( $omezit_pocet_pouziti ) {

		$zbyvajici_pocet_pouziti = get_field( 'zbyvajici_pocet_pouziti', $post_id );

		if( $zbyvajici_pocet_pouziti < 1 ) {
			return false;
		}

	}

	// no, asi je validni ...

	return true;
}





function slevovy_poukaz_post_id( $slevovy_poukaz ) {

	global $wpdb;

	$post_id = (int) $wpdb->get_var($wpdb->prepare(
		"SELECT p.ID 
		FROM {$wpdb->posts} p
		LEFT JOIN {$wpdb->postmeta} pm ON pm.post_id=p.ID
		WHERE pm.meta_key='kod_poukazu' AND pm.meta_value= %s
				AND p.post_type='slevovy-poukaz' AND p.post_status='publish'
		"
	, $slevovy_poukaz ));

	return $post_id;
}



function slevovy_poukaz_nazev( $slevovy_poukaz ) {

	$post_id = slevovy_poukaz_post_id( $slevovy_poukaz );

	if( $post_id ) {
		return get_the_title( $post_id );
	}
}












function slevovy_poukaz_zapocitat_pouziti( $slevovy_poukaz ) {

	$post_id = slevovy_poukaz_post_id( $slevovy_poukaz );

	if( get_field( 'omezit_pocet_pouziti', $post_id ) ) {

		$zbyvajici_pocet_pouziti = get_field( 'zbyvajici_pocet_pouziti', $post_id );

		$zbyvajici_pocet_pouziti--;

		update_post_meta( $post_id, 'zbyvajici_pocet_pouziti', $zbyvajici_pocet_pouziti );

	}

}



// reset počtu stažení u uživatele

add_action('show_user_profile', 'material_meta');
add_action('edit_user_profile', 'material_meta');

function material_meta($user)
{

    global $wpdb;

    $data = $wpdb->get_results(
        "SELECT * from " . $wpdb->prefix . "usermeta where user_id ='" . $user->ID . "' AND meta_key LIKE 'material%'"
    );

    ?>
    <h3>Materiály ke stažení</h3>
    <table class="form-table">

        <?php

        foreach ($data as $material) {
            if ($material->meta_value != 'povoleno'):
                $name = explode('-', $material->meta_key);

                $post = get_the_title($name[2]);


                ?>
                <tr>
                    <th><label for="<?= $material->meta_key ?>"><?= $post ?></label></th>

                    <td>
                        <input type="number" max="<?= MAX_POCET_STAZENI_MATERIALU ?>" min="0" name="<?= $material->meta_key ?>" id="<?= $material->meta_key ?>"
                               value="<?= $material->meta_value ?>" class="regular-text"/><br/>
                        <span class="description">Počet stažení materiálu z <?= MAX_POCET_STAZENI_MATERIALU ?>.</span>
                    </td>
                </tr>
            <?php
            endif;
        }
        ?>
    </table>
<?php }

add_action('personal_options_update', 'material_meta_save');
add_action('edit_user_profile_update', 'material_meta_save');

function material_meta_save($user_id)
{

    if (!current_user_can('edit_user', $user_id))
        return false;

    echo 'done';
    global $wpdb;

    $data = $wpdb->get_results("SELECT * from " . $wpdb->prefix . "usermeta where user_id ='" . $user_id . "' AND meta_key LIKE 'material%'");


    foreach ($data as $material) {
        if ($material->meta_value != 'povoleno'):
            update_user_meta($user_id, $material->meta_key, $_POST[$material->meta_key]);
        endif;
    }


}

function remove_napsali_category( $query ) {
    if ( $query->is_home() && $query->is_main_query() ) {
        $query->set( 'cat', '-67' );
    }
}
add_action( 'pre_get_posts', 'remove_napsali_category' );