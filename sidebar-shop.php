

<div class="sidebar-rubriky">
	
	<h3 class="podnadpis"><span><?php _e('Kategorie','jz') ?></span></h3>
	
	<ul>
	<?php 
		$cat_list = wp_list_categories( array( 
						'taxonomy' => 'product_cat',
						'title_li' => '',
						'hide_empty' => false,
						'show_count' => true,
						'show_option_all' => __('Všechny kategorie','jz'),
						'echo' => 0, ) 
					); 
		$cat_list = preg_replace('~<\/a> \(([0-9]+)\)~', ' <span class="count">($1)</span></a>', $cat_list);
		
		if( ! is_tax() ) { // nevypisuje se zadna kategorie, takze moznost "vse" se ma zvyraznit jako aktivni
			$cat_list = str_replace('cat-item-all', 'cat-item-all current-cat', $cat_list);
		}
		
		echo $cat_list;
	?>
	</ul>
</div>




<?php get_template_part( 'template-nejblizsi-terminy-sidebar' ); ?>