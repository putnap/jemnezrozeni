<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<p><?php
	_e('Vítáme Vás ve Vašem uživatelském účtu.','jz')
?></p>

<p><?php
	printf(
		__( 'Zde si můžete prohlédnout své <a href="%1$s">nedávné objednávky</a>, upravit <a href="%2$s">fakturační a doručovací adresy</a> a <a href="%3$s">změnit své heslo a osobní informace</a>.', 'jz' ),
		esc_url( wc_get_endpoint_url( 'orders' ) ),
		esc_url( wc_get_endpoint_url( 'edit-address' ) ),
		esc_url( wc_get_endpoint_url( 'edit-account' ) )
	);
?></p>

<?php if(!prihlasena_lektorka()): ?>
<p>
	<?php _e('Pokud jste se přihlásila na jeden z našich kurzů, najdete zde také informace o kurzu a materiály ke stažení před kurzem. Po kurzu se Vám zpřístupní také nahrávky a další bonusové materiály ke stažení. Prosím zkontrolujte si také ve své emailové schránce složku spam. Občas se stává, že pošta spadne do spamu.','jz') ?>
</p>

<p>
	<?php _e('To je asi vše, co jsme Vám v tuto chvíli chtěly sdělit, a doufáme, že se Vám naše stránky líbí. Neváhejte nás samozřejmě kontaktovat, pokud potřebujete s čímkoli pomoci.','jz') ?>
</p>
<?php endif; ?>

<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
