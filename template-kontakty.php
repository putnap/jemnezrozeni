<div class="kontakty">
	
	<?php if( get_field('hlavni_email', 'options') ) { ?>
	<p class="kontakt-item kontakt-hlavni-email">
		<a class="item" href="mailto:<?php echo get_field('hlavni_email', 'options') ?>"><?php echo get_field('hlavni_email', 'options') ?></a>
	</p>
	<?php } ?>
	
	<?php if( get_field('telefon_1', 'options') ) { ?>
	<p class="kontakt-item kontakt-telefon-1">
		<span class="item"><?php echo get_field('telefon_1', 'options') ?></span>
	</p>
	<?php } ?>
	
	<?php if( get_field('telefon_2', 'options') ) { ?>
	<p class="kontakt-item kontakt-telefon-2">
		<span class="item"><?php echo get_field('telefon_2', 'options') ?></span>
	</p>
	<?php } ?>
	
</div>