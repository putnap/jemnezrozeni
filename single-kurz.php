<?php get_header(); ?>




<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	
	
<div class="content content-kurz p20p50" id="content">
	
	
	<h1 class="main-title nad-zpetnym-odkazem"><?php the_title() ?></h1>
	<p class="zpet-pod-nadpisem"><a href="<?php echo get_post_type_archive_link( 'kurz' ) ?>"><?php _e('< Zpět na přehled kurzů','jz') ?></a></p>
	
	
	<div class="kurz-foto-a-popis cf">
		
		<div class="kurz-popis cf">
			
			
			
			<div class="kurz-foto-a-ceny">
				
				<div class="kurz-foto">
					<?php the_post_thumbnail( 'large' ) ?>
				</div>
				
				
				<?php if( $popisek = get_field('popisek_v_detailu') ) { ?>
				<div class="kurz-popisek">
					<?php echo nl2br( trim( $popisek ) ) ?>
				</div>
				<?php } ?>
				
				
				<div class="kurz-ceny">
					
				<?php 
				while( have_rows('ceny') ) { 
					the_row();
					
					$poznamka = get_sub_field('poznamka');
					
					if( $poznamka ) {
						$poznamka = '<span class="poznamka hint--bottom" data-hint="'.esc_attr( $poznamka ).'"><span class="otaznik">?</span><span class="t">('.$poznamka.')</span></span>';
					}
					
					$castka = get_sub_field('castka_' . jz_aktualni_mena_kod() );
					
					?>
					<div class="kurz-cena cf <?php echo $poznamka ? 'mapoznamku' : '' ?>">
						<div class="popisek">
							<?php the_sub_field('popisek') ?>
						</div>
						<div class="castka">
							<strong><?php echo $castka ?></strong>
							<?php echo $poznamka ?>
						</div>
					</div>
					<?php
				}
				?>
					
				</div>
				
				<div class="kurz-odkazy cf">
					
					<?php
					if( ($podminky = get_field('podminky_ucasti') ) ) {
						?>
						<a class="podminky" href="<?php echo get_permalink( $podminky ) ?>"><?php _e('Podmínky účasti na kurzu','jz') ?></a>
						<?php
					}
					?>
					
					<?php
					if( ($prispevky = get_field('prispevky_od_pojistoven') ) ) {
						?>
						<a class="prispevky" href="<?php echo get_permalink( $prispevky ) ?>"><?php _e('Příspěvky od pojišťoven','jz') ?></a>
						<?php
					}
					?>
					
				</div>
				
				<div class="kurz-sdileni">
					<?php share_buttons() ?>
				</div>
				
			</div>
			
			
			<?php the_content() ?>
			
		</div>
		
	</div>
	
	
	
	
		
		

	<div class="kurz-reference">
	<?php 

	get_template_part( 'template-reference' );

	?>
	</div>
		
		
		
		
	<?php
	
	$den1 = trim( get_field('den_1') );
	$den2 = trim( get_field('den_2') );
	
	$class = 'jedenden';
	
	if( $den1 && $den2 )
		$class = 'dvadny';
	
	if( $den1 ) {
		?>
		<div class="kurz-rozvrh cf <?php echo $class ?>">
			
			<h2><?php _e('Rozvrh kurzu', 'jz') ?></h2>
			
			<?php for( $i = 1; $i <= 2; $i++ ) { 
				
				$den = $i == 1 ? $den1 : $den2;
				
				if( empty($den) ) continue;
				
				?>
				<div class="kurz-rozvrh-den-<?php echo $i ?>">
					
					<?php 
						if( $den1 && $den2 ) {
							echo '<h3>'. __('Den','jz').' '.$i.'</h3>';
						}
					?>
					
					<div class="kurz-rozvrh-den-sloupce cf">
						
						<?php
							echo kurz_rozvrh_sloupce( $den );
						?>
						
					</div>
					
				</div>
			<?php } ?>
			
		</div>
	<?php } ?>
	
	
	
	
	
	
	
	
	<?php 
	
	if( 'soukromy' == get_field('typ') ) {
		?>
		
		<div class="kurz-kontaktni-formular">
			
			<h2 class="center"><?php _e('Mám zájem o soukromý kurz','jz') ?></h2>
			
			<?php get_template_part('template-kontaktni-formular') ?>
		</div>
		
		<?php	
	}
	?>
	
	
	
	
</div> <?php // .content ?>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
<?php get_template_part( 'template-nejblizsi-terminy' ) ?>		
	
	
	
<?php endwhile;?>
<?php endif; ?>




<?php get_footer(); ?>
