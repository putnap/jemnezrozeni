<?php 

get_header(); 

$detail = is_single();

?>

<div class="content blog-content content-padding cf" id="content">

<?php if( $detail ) { ?>
	
	<h2 class="main-title nad-zpetnym-odkazem"><?php _e('Detail článku','jz') ?></h2>
	<p class="zpet-pod-nadpisem"><a href="<?php echo get_permalink( icl_object_id( get_option('page_for_posts') ) ) ?>"><?php _e('< Zpět na přehled článků','jz') ?></a></p>

<?php } else if( is_search() ) { ?>
	
	<h2 class="main-title"><?php _e('Těhotenství a porod','jz') ?></h2>
	
	<h2><?php _e('Vyhledávání') ?>: <?php echo get_search_query() ?></h2>

<?php } else { ?>
	<h1 class="main-title"><?php _e('Těhotenství a porod','jz') ?></h1>

<?php } ?>


<div class="blog-main column-main border same-height-2-1200">

<?php if (have_posts()) : ?>
	
<div class="blog-items">
<?php while (have_posts()) : the_post(); ?>
	
	<div class="blog-item cf <?php echo $detail ? 'detail' : '' ?>">
	
		<div class="foto">
			<?php if( ! $detail ) { ?><a href="<?php the_permalink() ?>"><?php } ?>
				<?php the_post_thumbnail( 'thumbnail' ); ?>
			<?php if( ! $detail ) { ?></a><?php } ?>
		</div>
		<div class="info">
			
			<?php if( $detail ) { ?>
				<h1 class="nadpis"><?php the_title() ?></h1>
			<?php } else { ?>
				<h2 class="nadpis"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h2>
			<?php } ?>
			
			<?php if( ! $detail ) { ?>
			<p class="excerpt">
				<?php echo wp_trim_words( strip_shortcodes( get_the_content() ), 45 ); ?>
			</p>
			<?php } ?>
			
			<div class="meta">
				
				<?php if( ! $detail ) { ?>
				<p class="odkaz">
					<a href="<?php the_permalink() ?>" class="tlacitko nizsi"><?php _e('Celý článek','jz') ?></a>
				</p>
				<?php } ?>
				
				<p class="datum">
					<time datetime="<?php echo current_time('c') ?>"><?php echo current_time('j. n. Y') ?></time>
				</p>
				
				<p class="autor">
					<span><?php echo autor_clanku() ?></span>
				</p>
				
			</div>
			
		</div>	
	
		<?php if( $detail ) { ?>
			
			<div class="cf"></div>
			
			<div class="blog-text">
		
				<?php the_content() ?>
		
			</div>
			
			<?php share_buttons() ?>
			
			<?php if ( comments_open() ) { ?>
		
				<?php comments_template(); ?>
		
			<?php } ?>
		
		<?php } ?>
	
	</div>
		
	
<?php endwhile;?>
</div>

<?php get_template_part( 'strankovani' ); ?>

<?php endif; ?>

</div>



<div class="blog-sidebar column-sidebar border same-height-2-1200">
	
	<div class="sidebar-vyhledavani">
		
		<h3 class="podnadpis"><span><?php _e('Vyhledávání','jz') ?></span></h3>
		
		<div class="search-form-wrapper">
		<form role="search" method="get" class="search-form cf" action="<?php echo site_url(); ?>">
			<input type="search" class="search-field"
				placeholder="<?php _e('Vyhledávání','jz') ?>"
				value="<?php echo get_search_query() ?>" name="s" />
			<input type="submit" class="search-submit" value="<?php _e('OK', 'jz') ?>" />
			<input type="hidden" name="post_type" value="post">
		</form>
		</div>
		
	</div>
	
	
	<div class="sidebar-rubriky">
		
		<h3 class="podnadpis"><span><?php _e('Kategorie','jz') ?></span></h3>
		
		<ul>
		<?php 
			$cat_list = wp_list_categories( array( 
							'title_li' => '',
							'hide_empty' => false,
							'show_count' => true,
							'show_option_all' => 'Všechny články a videa',
							'echo' => 0 ) 
						); 
			$cat_list = preg_replace('~<\/a> \(([0-9]+)\)~', ' <span class="count">($1)</span></a>', $cat_list);
			
			if( ! is_category() ) { // nevypisuje se zadna kategorie, takze moznost "vse" se ma zvyraznit jako aktivni
				$cat_list = str_replace('cat-item-all', 'cat-item-all current-cat', $cat_list);
			}
			
			echo $cat_list;
		?>
		</ul>
	</div>
	
	
	
	
	<?php get_template_part( 'template-nejblizsi-terminy-sidebar' ); ?>
	
	
	
	
</div>


</div>

<?php get_footer(); ?>
