<?php

get_header(); 

$detail = is_single();

?>

<div class="content p20p50 content-pribehy" id="content">

<?php if( $detail ) { ?>
	
	<h2 class="main-title nad-zpetnym-odkazem"><?php _e('Příběhy klientů','jz') ?></h2>
	<p class="zpet-pod-nadpisem"><a href="<?php echo get_post_type_archive_link( 'pribeh' ) ?>"><?php _e('< Zpět na přehled příběhů','jz') ?></a></p>

<?php } else { ?>
	<h1 class="main-title"><?php _e('Příběhy klientů','jz') ?></h1>
<?php } ?>




<div class="pribehy-main column-main border same-height-2-1200">
<?php if (have_posts()) { ?>
<?php while (have_posts()) { the_post(); ?>
	
	<div class="pribeh cf <?php echo $detail ? 'detail' : '' ?>"">
		
		<div class="foto">
			<?php if( ! $detail ) { ?><a href="<?php the_permalink() ?>"><?php } ?>
				<?php the_post_thumbnail( 'thumbnail' ); ?>
			<?php if( ! $detail ) { ?></a><?php } ?>
		</div>
		
		<div class="info">
			
			<?php if( $detail ) { ?>
				<h1 class="nadpis"><?php the_title() ?></h1>
			<?php } else { ?>
				<h2 class="nadpis"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h2>
			<?php } ?>
			
			<?php if( ! $detail ) { ?>
				<p class="excerpt">
					<?php echo wp_trim_words( strip_shortcodes( get_the_content() ), 45 ); ?>
				</p>
				<p class="odkaz">
					<a href="<?php the_permalink() ?>" class="tlacitko nizsi"><?php _e('Celý příběh','jz') ?></a>
				</p>
			<?php } ?>
		</div>
	
	
	
			
		<?php if( $detail ) { ?>
			
			<div class="cf"></div>
			
			<div class="pribeh-text cf">
		
				<?php the_content() ?>
		
			</div>
		
			<?php share_buttons() ?>
			
		<?php } ?>
	
		
		
	</div>
	
<?php } ?>

<?php get_template_part( 'strankovani' ); ?>

<?php } ?>
</div>


<div class="pribeh-sidebar column-sidebar border same-height-2-1200">
	
	<?php get_template_part( 'template-nejblizsi-terminy-sidebar' ); ?>
	
</div>


</div>

<?php get_footer(); ?>
