<?php 

global $post;

if( $post && $post->ID ) {
?>
<div class="share-buttons">
	
	<div class="fb-like" data-href="<?php echo get_permalink( $post->ID ) ?>" data-layout="standard" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
	
</div>
<?php } ?>