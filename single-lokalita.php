<?php get_header(); ?>

<div class="content p20p50 content-lokalita" id="content">


<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); $lokalita_id = get_the_id() ?>
	
	
	<h1 class="main-title"><?php the_title() ?></h1>
	
	<p class="detail-lokality-adresa">
		<strong><?php _e('Adresa','js') ?>:</strong>
		<?php echo lokalita_adresa( $lokalita_id ) ?>
	</p>
	
	<div class="detail-lokality-text cf">
		<?php the_content() ?>
	</div>
	
	
	<?php 
	
	$poloha_na_mape = get_field( 'poloha_na_mape' );
	
	if( $poloha_na_mape ) {	
		?>
		
		<div class="detail-lokality-mapa google-mapa">
			<div class="marker" data-lat="<?php echo $poloha_na_mape['lat'] ?>" data-lng="<?php echo $poloha_na_mape['lng'] ?>" 
				data-icon="<?php echo get_template_directory_uri(); ?>/img/mapa-ikona.png"
				data-link="https://www.google.com/maps/dir/?api=1&destination=<?php echo urlencode($poloha_na_mape['address']) ?>"></div>
		</div>
		
	<?php } ?>
	
	
	<?php  if( ! is_woocommerce() && ! is_cart() && ! is_checkout() && ! is_account_page() ) {  ?>
	
		<?php share_buttons() ?>
	
	<?php } ?>
	
<?php endwhile;?>
<?php endif; ?>


</div>

<?php get_footer(); ?>
