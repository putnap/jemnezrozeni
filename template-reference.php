<?php


$args = array(
	'post_type' => 'reference',
	'posts_per_page' => -1,
);


if( is_singular( array('kurz') ) ) {

	$kurz_id = get_the_id();
	
	$args['meta_query'] = array(
		array(
			'key' => 'souvisejici_kurz',
			'value' => $kurz_id,
		)
	);
	
} 


$the_query = new WP_Query( $args );	

if( $the_query->have_posts() ) {
	?>
	<div class="reference-sekce">
	<div class="reference-wrapper">
		
		<a href="#" id="reference-prev"></a> 	
		<a href="#" id="reference-next"></a> 	
		
	<div class="reference-slider">
	<?php
	while( $the_query->have_posts() ) {
		$the_query->the_post();
		?>
		
		<div class="reference">
			
			<div class="reference-text">
				<?php the_content() ?>
			</div>
			
			<p class="reference-jmeno">
				<strong>
					<?php the_title() ?>
				</strong>
			</p>
			
		</div>
		
		<?php
	}
	wp_reset_postdata(); 
	?>
	</div>
	
	<div class="reference-tecky"></div>
	
	</div>
	</div>
	<?php
} 	
?>