<?php get_header(); ?>

<div class="content p20p50" id="content">


<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	
	
	<h1 class="main-title"><?php the_title() ?></h1>
	
	<?php the_content() ?>
	
	
	<?php  if( is_singular() &&  ! is_woocommerce() && ! is_cart() && ! is_checkout() && ! is_account_page() ) {  ?>
	
		<?php share_buttons() ?>
	
	<?php } ?>
	
<?php endwhile;?>
<?php endif; ?>


</div>

<?php get_footer(); ?>
