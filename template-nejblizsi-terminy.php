<?php

$lektorka_id = $kurz_id = 0;

if( is_singular( array('lektorka') ) ) {
	global $post;
	$lektorka_id = $post->ID;
} else if( is_singular( array('kurz') ) ) {
	global $post;
	$kurz_id = $post->ID;
}



$args = array(
	'post_type' => 'termin',
	'posts_per_page' => 4,
	'meta_query' => array(
		array(
			'key' => 'datum_konani',
			'value' => current_time('Ymd'),
			'compare' => '>=',
		),
	)
);

if( $lektorka_id ) {
	$args['meta_query'][] =
	array(
		'key' => 'lektorka',
		'value' => $lektorka_id,
	);
	
} else if( $kurz_id ) {
	$args['meta_query'][] =
	array(
		'key' => 'kurz',
		'value' => $kurz_id,
	);
	
}



$the_query = new WP_Query( $args );
if( $the_query->have_posts() ) {
?>
	
<div class="nejblizsi-terminy">
	
	<?php if( $lektorka_id ) { ?>
		
		<h2><span><?php _e('Nejbližší termíny kurzů lektorky','jz') ?></span></h2>
	
	<?php } else { ?>
	
		<h2><span><?php _e('Nejbližší termíny kurzů','jz') ?></span></h2>
		
	<?php } ?>
	
	
	<div class="nejblizsi-terminy-prehled cf">
			
		<?php
		while( $the_query->have_posts() ) {
			$the_query->the_post();
			?>
			
			<?php
				get_template_part( 'template-termin-box' );
			?>
			
			<?php
		}
		wp_reset_postdata(); 
		?>
	
	</div>
	
	<div class="zobrazit-vsechny-terminy">
		<a href="<?php echo get_post_type_archive_link( 'termin' ) ?>"><?php _e('zobrazit všechny termíny','jz') ?></a>
	</div>
	
</div>

<?php
} 
?>