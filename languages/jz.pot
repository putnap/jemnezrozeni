#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-01-03 15:04+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#: archive-lektorka.php:5
msgid "Lektorky"
msgstr "Lecturers"

#: header.php:48
msgid "relaxační hudba"
msgstr "relaxation music"

#: header.php:102
msgid "Můj účet"
msgstr "My account"

#: header.php:107
msgid "Přihlášení"
msgstr "Login"

#: header.php:119
#, php-format
msgid "V košíku: %s / %s"
msgstr "In cart: %s / %s"

#: header.php:121
msgid "Košík"
msgstr "Cart"

#: template-nejblizsi-terminy.php:53
msgid "Nejbližší termíny kurzů lektorky"
msgstr "Nearest course dates of the lecturer"

#: template-nejblizsi-terminy.php:57 template-nejblizsi-terminy-sidebar.php:3
msgid "Nejbližší termíny kurzů"
msgstr "Nearest course dates"

#: template-nejblizsi-terminy.php:81
msgid "zobrazit všechny termíny"
msgstr "Show all course dates"

#. Name of the template
#: functions-woocommerce.php:1326 single-lektorka.php:89
msgid "Kontakt"
msgstr "Contact"

#: page-kontakt.php:33
msgid "Bankovní spojení"
msgstr "Bank details"

#: functions-woocommerce.php:36
msgid "Detail produktu"
msgstr "Product detail"

#: functions-woocommerce.php:38
msgid "&lt; Zpět na přehled produktů"
msgstr "Back to all products"

#: functions-woocommerce.php:343
msgid "Přehrát ukázku"
msgstr "Play demo"

#: functions-woocommerce.php:639
msgid "Dokončit objednávku"
msgstr "Finish order"

#: functions-woocommerce.php:1117
msgid "Mé termíny"
msgstr "My terms"

#: functions-woocommerce.php:1131
msgid "Objednávky v e-shopu"
msgstr "Orders in eshop"

#: functions-woocommerce.php:1134
msgid "Produkty ke stažení"
msgstr "Products to download"

#: functions-woocommerce.php:1140
msgid "Materiály ke kurzům"
msgstr "Course materials"

#: functions-woocommerce.php:1252
msgid "Zakoupené produkty ke stažení"
msgstr "Purchased products to download"

#: functions-woocommerce.php:1325
msgid "Obchodní podmínky"
msgstr "Terms & conditions"

#: sidebar-shop.php:5 template-blog.php:131
msgid "Kategorie"
msgstr "Category"

#: sidebar-shop.php:14
msgid "Všechny kategorie"
msgstr "All categories"

#: template-lektorky-mapa.php:67
msgid "Zrušit filtraci dle kraje"
msgstr "Cancel filter by region"

#: archive-termin.php:6
msgid "Termíny kurzů"
msgstr "Course dates"

#: archive-termin.php:16
msgid "Název kurzu"
msgstr "Course name"

#: archive-termin.php:18
msgid "Vyberte název kurzu"
msgstr "Choose course name"

#: archive-termin.php:43 functions.php:3167 functions.php:4515
msgid "Město"
msgstr "Town/City"

#: archive-termin.php:45
msgid "Vyberte město"
msgstr "Choose town/city"

#: archive-termin.php:100 template-kontaktni-formular.php:20
msgid "Lektorka"
msgstr "Lecturer"

#: archive-termin.php:102
msgid "Vyberte lektorku"
msgstr "Choose lecturer"

#: archive-termin.php:193
msgid "REZERVOVAT"
msgstr "BOOK"

#: functions.php:1293
msgid "Vaše miminko by mělo přijít na svět okolo"
msgstr "Your baby is likely to be born around"

#: functions.php:1379
msgid "Vaše zpráva byla úspěšně odeslána!"
msgstr "Your message was successfuly sent"

#: functions.php:1381
msgid "Vaši zprávu nyní nelze odeslat. Zkuste to prosím později."
msgstr "Your message cannot be sent at the moment. Please try again later."

#: functions.php:1745 functions.php:1762
msgid "Termín"
msgstr "Course date"

#: functions.php:1816
msgid "1 denní"
msgstr "1 day"

#: functions.php:1817
msgid "2 denní"
msgstr "2 day"

#: functions.php:1818
msgid "Be Balanced"
msgstr "Be Balanced"

#: functions.php:1819
msgid "Online"
msgstr "Online"

#: functions.php:1820
msgid "Soukromý"
msgstr "Private"

#: functions.php:2002
msgid "Volných míst"
msgstr "Free spaces"

#: functions.php:2833 functions.php:3091
msgid "Povinné pole!"
msgstr "Required field!"

#: functions.php:2972
msgid ""
"Byla jste úspěšně přihlášena do svého uživatelského účtu. Nyní můžete "
"přihlášku dokončit."
msgstr "You have successfully logged in into your user account. You can now complete your booking."

#: functions.php:2975
msgid "Zkontrolujte prosím zadané informace"
msgstr "Please check all your details."

#: functions.php:2977
msgid "Poté přihlášku potvrďte tlačítkem na konci."
msgstr "Then confirm your booking with the button at the end."

#: functions.php:2993
msgid "Vyberte druh přihlášky"
msgstr "Please select type of registration."

#: functions.php:3006 functions.php:3013
msgid "Zúčastním se sama"
msgstr "I will attend alone."

#: functions.php:3086
msgid "S kým přijdete?"
msgstr "Who will come with you?"

#: functions.php:3088
msgid "Např. s kamarádkou / dulou"
msgstr "E.g. with a friend/doula"

#: functions.php:3124 functions.php:4490
msgid "Osobní údaje"
msgstr "Personal details"

#: functions.php:3128 functions.php:4494
msgid "Jméno"
msgstr "Name"

#: functions.php:3132 functions.php:4498
msgid "Příjmení"
msgstr "Surname"

#: functions.php:3137 functions.php:4502
msgid "Očekávaný termín porodu"
msgstr "Estimated due date"

#: functions.php:3142 functions.php:4506
msgid "Čekám vícerčata"
msgstr "I am pregnant with multiples"

#: functions.php:3155
msgid "Ano"
msgstr "Yes"

#: functions.php:3163 functions.php:4511
msgid "Ulice, čp."
msgstr "Street, house number"

#: functions.php:3171 functions.php:4519
msgid "PSČ"
msgstr "Postcode"

#: functions.php:3176 functions.php:4524
msgid "Stát"
msgstr "Country"

#: functions.php:3217 functions.php:4542 template-kontaktni-formular.php:31
msgid "E-mail"
msgstr "E-mail"

#: functions.php:3221 functions.php:4546 template-kontaktni-formular.php:36
msgid "Telefon"
msgstr "Phone number"

#: functions.php:3232 functions.php:4557
msgid "Poznámka"
msgstr "Note/comment"

#: functions.php:3249
msgid "Výběr způsobu platby"
msgstr "Choose payment type"

#: functions.php:3260
msgid "Bankovní převod"
msgstr "Bank transfer"

#: functions.php:3264
msgid "Platba kartou on-line"
msgstr "Card payment"

#: functions.php:3302
msgid "Souhlas se zpracováním osobních údajů a s obchodními podmínkami"
msgstr "Agree to personal data processing and Terms and Conditions"

#: functions.php:3309
#, php-format
msgid ""
"Odesláním tohoto formuláře souhlasím se zpracováním svých osobních údajů a "
"<a href=\"%s\">obchodními podmínkami</a>."
msgstr "By submitting this bookin I agree to personal data processing and <a href=\"%s\">terms and conditions</a>."

#: functions.php:3321
msgid "Souhlasím se zpracováním svých osobních údajů a s obchodními podmínkami"
msgstr "I agree to personal data processing and Terms and Conditions"

#: functions.php:3324
msgid "Ano, souhlasím se zpracováním osobních údajů a s obchodními podmínkami"
msgstr "Yes, I agree to personal data processing and Terms and Conditions"

#: functions.php:3343
msgid "Chci se vrátit o krok zpět a opravit informace"
msgstr "I want go back and correct my information"

#: functions.php:3347
msgid "Potvrzuji přihlášku"
msgstr "I confirm my booking"

#: functions.php:3366
msgid "Pokračovat na kontrolu informací"
msgstr "Continue to check all the details"

#: functions.php:3369
msgid ""
"Po odeslání formuláře si budete moci ještě zkontrolovat zadané údaje, a "
"teprve poté přihlášku finálně potvrdit."
msgstr "After submitting the form, you will be able to check the entered details before confirming your booking"

#: functions.php:3442
msgid "Nebyla vyplněna všechna povinná pole!"
msgstr "Not all required fields have been filled!"

#: functions.php:3460
#, php-format
msgid "Účet s e-mailovou adresou %s již existuje, nelze vytvořit nový!"
msgstr "An account with this email address %s already exists, you can't create a new one!"

#: functions.php:3464
#, php-format
msgid ""
"Ve Vašem uživatelském účtu Vám zpřístupníme všechny materiály a relaxační "
"MP3, \n"
"\t\t\t\t\t\t\t\t\t\t\t\t\t\tje tedy nutné se před dokončením přihlášky do "
"svého účtu <a href=\"%s\">přihlásit</a>."
msgstr "All the materials and relaxation MP3s will be available to you in your user account; it is, therefore, necessary to log in before you complete your application."

#: functions.php:3467
#, php-format
msgid ""
"Pokud jste zapomněla heslo, snadno si můžete nechat <a href=\"%s\">poslat "
"nové</a>!"
msgstr "If you have forgotten your password, you can easily  <a href="%s">send a new one</a>!"

#: functions.php:3570
msgid ""
"Bohužel, došlo k problému při vytváření uživatelského účtu. Zkuste to prosím "
"později znovu. Za komplikace se omlouváme."
msgstr "Unfortunately there was a problem creating user account. Please try again later. We apologize for the inconvenience."

#: functions.php:3669
msgid ""
"Přihlášku se nepodařilo uložit! Zkuste to prosím později znovu. Za "
"komplikace se omlouváme."
msgstr "Sorry your booking could not be saved! Please try again later. We apologize for the incovenience. "

#: functions.php:3772
msgid "Přihláška byla úspěšně uložena."
msgstr "Your booking was successfully saved."

#: functions.php:3788
#, php-format
msgid ""
"Bohužel došlo k chybě při odesílání e-mailu s podrobnými informacemi a "
"platebními pokyny.\n"
"\t\t\t\t\t\t\t\t\t\t\t\t\tKontaktujte nás prosím na e-mailu %s. Za "
"komplikace se omlouváme."
msgstr "Unfortunately there was an error sending an email with detailed information and payment instructions. Please contact us at %s. We apologize for the inconvenience. "

#: functions.php:3794
msgid ""
"Na Vaši e-mailovou adresu byl odeslán e-mail se souhrnnými informacemi a "
"případnými platebními pokyny."
msgstr "An email with all information and payment instructions has been sent to your email address."

#: functions.php:3824
msgid "Přihláška na kurz"
msgstr "Course booking"

#: functions.php:3848
#, php-format
msgid ""
"Bohužel došlo k chybě při při komunikaci s platební bránou.\n"
"\t\t\t\t\t\t\t\t\t\t\t\t\t\tKontaktujte nás prosím na e-mailu %s. Za "
"komplikace se omlouváme."
msgstr "Unfortunately there was an error communicating with the payment gateway. Please contact us at %s. We apologize for the inconvenience."

#: functions.php:4014
msgid "Přihláška bude uhrazena on-line na platební bráně."
msgstr "Your course booking will be paid online at the payment gateway"

#: functions.php:4028
msgid "Číslo účtu"
msgstr "Account numbe"

#: functions.php:4029
msgid "Variabilní symbol"
msgstr "Variable symbo"

#: functions.php:4030
msgid "Částka"
msgstr "Amoun"

#: functions.php:4042
msgid "Přihlašovací e-mail"
msgstr "Login emai"

#: functions.php:4045 functions.php:4047
msgid "Přihlašovací heslo"
msgstr "Login passwor"

#: functions.php:4047
msgid "nastaveno někdy dříve (můžete si nechat zaslat nové na odkazu níže)"
msgstr "set up earlier (you can now get a new one through the link below"

#: functions.php:4050
msgid "Adresa pro přihlášení"
msgstr "Login addres"

#: functions.php:4211
#, php-format
msgid ""
"Bohužel došlo k chybě při při komunikaci s platební bránou.\n"
"\t\t\t\t\t\t\t\t\t\t\t\t\tKontaktujte nás prosím na e-mailu %s. Za "
"komplikace se omlouváme."
msgstr "Unfortunately therew as an error communicating with the payment gateway. Please contact us at %s. We apologize for the inconvenience."

#: functions.php:4243
msgid "Přihláška byla úspěšně uhrazena!"
msgstr "Your course booking was successfuly paid"

#: functions.php:4247
#, php-format
msgid ""
"Bohužel, přihláška nebyla na platební bráně uhrazena.\n"
"\t\t\t\t\t\t\t\t\t\t\t\t\t\tKontaktujte nás prosím na e-mailu %s. Za "
"komplikace se omlouváme."
msgstr "Unfortunately your course booking was not paid at the payment gateway. Please contact us at %s. We apologize for the inconvenience."

#: functions.php:4458
msgid "Typ přihlášky"
msgstr "Type of bookin"

#: functions.php:4462
msgid "Typ"
msgstr "Type"

#: functions.php:4483
msgid "S kým přijdu"
msgstr "I will come wit"

#: functions.php:5606
msgid "Materiály ke kurzům ke stažení"
msgstr "Course materials to download"

#: functions.php:5753
msgid "Materiál"
msgstr "Material"

#: functions.php:5754 functions.php:5799
msgid "Stáhnout"
msgstr "Download"

#: functions.php:5793
msgid "Dosažen limit stažení"
msgstr "Reached download limit"

#: functions.php:5850
msgid "Nejste přihlášená na žádný kurz."
msgstr "You are not registered in any course."

#: functions.php:6179
msgid "Materiály ke kurzům mohou stahovat pouze přihlášení účastníci!"
msgstr "Course materials can only be downloaded by registered clients!"

#: functions.php:6191
msgid "K tomuto materiálu nemáte přístup!"
msgstr "You do not have access to this material!"

#: functions.php:6198
msgid "Bohužel, byl dosažen limit počtu stažení tohoto materiálu."
msgstr "Unfortunately the download limit for this material has been reached."

#: functions.php:6212
msgid "Bohužel, materiál ke stažení byl odstraněn."
msgstr "Unfortunately, the download has been removed. "

#: functions.php:6388
msgid "E-mail uložen."
msgstr "Email saved."

#: functions.php:6392
msgid "Došlo k chybě. Zkuste to prosím později."
msgstr "Error occurred. Please try again later."

#: functions.php:6397
msgid "E-mailová adresa je prázdná!"
msgstr "Email address is empty!"

#: functions.php:6548
msgid "Nyní si můžete stáhnout následující e-booky:"
msgstr "Now you can download following e-books:"

#: functions.php:6566
msgid "Pro stažení e-booku zadejte prosím Váš e-mail"
msgstr "To download the e-book, please, enter your email"

#: functions.php:6570
msgid "Stáhnout e-book"
msgstr "Download e-book"

#: template-kontaktni-formular.php:5
msgid "Vyplňte prosím pole označená hvězdičkou!"
msgstr "Please fill in the fields marked with an asterisk!"

#: template-kontaktni-formular.php:26
msgid "Vaše jméno"
msgstr "Your name"

#: template-kontaktni-formular.php:41
msgid "Zpráva"
msgstr "Message"

#: template-kontaktni-formular.php:47
#, php-format
msgid "Pole označená %s jsou povinná"
msgstr "Field marked %s are required"

#: template-kontaktni-formular.php:53
msgid "Odeslat"
msgstr "Send"

#. Name of the template
msgid "Těhotenská kalkulačka"
msgstr "Pregnancy calculator"

#: page-tehotenska-kalkulacka.php:28
msgid "První den poslední menstruace"
msgstr "First day of the last period"

#: page-tehotenska-kalkulacka.php:30
msgid "Vyberte datum"
msgstr "Select date"

#: page-tehotenska-kalkulacka.php:36
msgid "Průměrná délka cyklu"
msgstr "Average cycle length"

#: page-tehotenska-kalkulacka.php:44
msgid "Průměrná délka luteální fáze"
msgstr "Average luteal phase length"

#: page-tehotenska-kalkulacka.php:52
msgid "Vypočítat"
msgstr "Calculate"

#: page-tehotenska-kalkulacka.php:66
msgid ""
"Porod není jen o rození miminek. Porod je o matce, která se stává silnější, "
"zdatnější a schopnější. O matce, která věří svému tělu a zná svou vnitřní "
"sílu"
msgstr "Birth is not only about making babies. Birth is about making mothers--strong, competent, capable mothers who trust themselves and know their inner strength."

#: single-lektorka.php:21
msgid "< Zpět na přehled lektorek"
msgstr "< Back to all lecturers"

#. Name of the template
msgid "Úvodní stránka"
msgstr "Homepage"

#: page-uvodni-stranka.php:20
msgid "Hypnoporod"
msgstr "Hypnobirthing"

#: page-uvodni-stranka.php:28
msgid "Kurzy"
msgstr "Courses"

#: page-uvodni-stranka.php:36 template-pribeh.php:13 template-pribeh.php:17
msgid "Příběhy klientů"
msgstr "Client stories"

#: page-uvodni-stranka.php:44 page-uvodni-stranka.php:277
msgid "E-shop"
msgstr "E-shop"

#: page-uvodni-stranka.php:59
msgid "Prožít těhotenství a porod v lásce a klidu"
msgstr "Experience pregnancy and birth in love and peace"

#: page-uvodni-stranka.php:78
msgid "1 denní kurz"
msgstr "1 day course"

#: page-uvodni-stranka.php:81
msgid "Kurz Hypnoporodu"
msgstr "Hypnobirthing course"

#: page-uvodni-stranka.php:85 page-uvodni-stranka.php:135
msgid "Co mne v kurzu čeká?"
msgstr "What can I expect?"

#: page-uvodni-stranka.php:128
msgid "2 denní kurz"
msgstr "2 day course"

#: page-uvodni-stranka.php:131
msgid "Kompletní předporodní příprava s Hypnoporodem"
msgstr "Complete antenatal birth preparation with Hypnobirthing"

#: page-uvodni-stranka.php:202 template-blog.php:18 template-blog.php:23
msgid "Těhotenství a porod"
msgstr "Pregnancy and birth"

#: page-uvodni-stranka.php:240
msgid "zobrazit všechny příspěvky"
msgstr "view all posts"

#: page-uvodni-stranka.php:284
msgid "zobrazit všechny produkty"
msgstr "show all products"

#: archive-kurz.php:6
msgid "Nabídka kurzů"
msgstr "All courses"

#: archive-kurz.php:29
msgid "Detail kurzu"
msgstr "Course detail"

#: single-kurz.php:14
msgid "< Zpět na přehled kurzů"
msgstr "< Back to all courses"

#: single-kurz.php:72
msgid "Podmínky účasti na kurzu"
msgstr "Course terms & conditions"

#: single-kurz.php:80
msgid "Příspěvky od pojišťoven"
msgstr "Health insurance coverage"

#: single-kurz.php:131
msgid "Rozvrh kurzu"
msgstr "Course schedule"

#: single-kurz.php:144
msgid "Den"
msgstr "Day"

#: single-kurz.php:176
msgid "Mám zájem o soukromý kurz"
msgstr "I am interested in private course"

#: template-blog.php:13
msgid "Detail článku"
msgstr "Article detail"

#: template-blog.php:14
msgid "< Zpět na přehled článků"
msgstr "< Back to all articles"

#: template-blog.php:60
msgid "Celý článek"
msgstr "Entire article"

#: template-blog.php:114 template-blog.php:119
msgid "Vyhledávání"
msgstr "Search"

#: template-blog.php:121
msgid "OK"
msgstr "OK"

#: template-pribeh.php:14
msgid "< Zpět na přehled příběhů"
msgstr "< Back to all stories"

#: template-pribeh.php:48
msgid "Celý příběh"
msgstr "Entire birth story"

#: footer.php:23
msgid "Váš e-mail"
msgstr "Your email"

#: footer.php:24
msgid "Odebírat novinky"
msgstr "Subscribe to news"

#: footer.php:31
msgid "Vyrobilo studio bARTvisions"
msgstr "Created by studio bARTvisions"

#: template-nejblizsi-terminy-sidebar.php:39
msgid "Další termíny"
msgstr "Other course dates"

#. Name of the theme
msgid "JEMNÉ ZROZENÍ"
msgstr ""

#. Description of the theme
msgid "WordPress theme"
msgstr ""

#. Author of the theme
msgid "bARTvisions"
msgstr ""
