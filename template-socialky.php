<div class="socialky">
	
	<?php if( get_field('facebook_profil_url', 'options') ) { ?>
	
		<a class="socialka fb" href="<?php echo get_field('facebook_profil_url', 'options') ?>"><span></span></a>
	
	<?php } ?>
	
	<?php if( get_field('google_profil_url', 'options') ) { ?>
	
		<a class="socialka gplus" href="<?php echo get_field('google_profil_url', 'options') ?>"><span></span></a>
	
	<?php } ?>
	
	<?php if( get_field('instagram_profil_url', 'options') ) { ?>
	
		<a class="socialka insta" href="<?php echo get_field('instagram_profil_url', 'options') ?>"><span></span></a>
	
	<?php } ?>
	
</div>