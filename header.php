<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo style_css() ?>">

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NGLC339');</script>
	
	<?php wp_head() ?>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110411099-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	  gtag('config', 'UA-69235161-1');
	</script>
	<meta name="google-site-verification" content="xEC7ZiBOLREADgK0VeAHqoBJSjq7JPI765USRoAIuIU" />
	<link rel="stylesheet" href="https://use.typekit.net/fue7llu.css">
</head>

<body <?php body_class() ?> id="top">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NGLC339"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>







<div class="web-wrapper">




<header class="header" id="header">
<div class="header-main cf">

	<div class="relaxacni-hudba">
		<a href="<?php echo get_permalink( icl_object_id( PAGE_ID_RELAXACNI_HUDBA ) ) ?>"><?php _e('relaxační hudba', 'jz') ?></a>
	</div>

	<div class="prepinac-jazyku">
		<?php prepinac_jazyku() ?>
	</div>

	<a href="/" class="hlavni-logo">
		<img src="<?php echo get_template_directory_uri() ?>/img/zrozeni.svg" alt="<?php bloginfo('name') ?>">
	</a>

	<a href="#" class="menu-toggle"><span></span></a>

	<nav class="navigace cf">
		<?php wp_nav_menu( array('theme_location' => 'primary', 'link_before' => '<span>', 'link_after' => '</span>', 'menu_class' => 'menu cf'  ) ) ?>
        <br>
        <a href="<?= get_permalink(PAGE_ID_OBCHODNI_PODMINKY) ?>"><?= __('Obchodní podmínky', 'jz') ?></a>
	</nav>


	<?php get_template_part( 'template-kontakty' ) ?>

	<?php get_template_part( 'template-socialky' ) ?>

</div>
</header>




<div class="main cf" id="main">



	<div class="top-login">

		<?php

		$account_url = get_permalink( wc_get_page_id( 'myaccount' ) );

		?>

		<?php

		if( is_user_logged_in() ) {

			$user = wp_get_current_user();


			?>

			<strong class="jmeno"><?php echo $user->display_name ?></strong>

			&nbsp;-&nbsp;

			<a href="<?php echo $account_url ?>" class="muj-ucet"><?php _e('Můj účet','jz') ; ?></a>
			<?php

		} else {
			?>
			<a href="<?php echo $account_url ?>" class="prihlaseni"><?php _e('Přihlášení','jz') ; ?></a>
			<?php
		}
		?></a>

		&nbsp;-&nbsp;

		<?php
		$kosik_polozek = WC()->cart->get_cart_contents_count();
		$kosik_celkem = WC()->cart->get_cart_total();

		if( $kosik_polozek > 0 ) { ?>
			<a href="<?php echo wc_get_cart_url() ?>" class="kosik"><?php echo sprintf( __('V košíku: %s / %s','jz'), $kosik_polozek, $kosik_celkem )  ?></a>
		<?php } else { ?>
			<a href="<?php echo wc_get_cart_url() ?>" class="kosik"><?php _e('Košík','jz') ?></a>
		<?php } ?>

	</div>









