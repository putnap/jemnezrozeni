<div class="sidebar-nejblizsi-terminy">

    <h3 class="podnadpis"><span><?php _e('Nejbližší termíny kurzů', 'jz') ?></span></h3>

    <div class="sidebar-nejblizsi-terminy-seznam">

        <?php
        $the_query = new WP_Query(array(
            'post_type' => 'termin',
            'posts_per_page' => 3,
            'meta_query' => array(
                array(
                    'key' => 'datum_konani',
                    'value' => current_time('Ymd'),
                    'compare' => '>=',
                ),
            )
        ));
        if ($the_query->have_posts()) {
            while ($the_query->have_posts()) {
                $the_query->the_post();

                $termin_id = get_the_id();
                ?>
                <div class="termin">
                    <?php if (!get_field('typ_terminu', $termin_id) || get_field('typ_terminu', $termin_id) == "jednoden"): ?>
                        <p class="datum"><span><?php echo termin_datum_konani($termin_id) ?></span></p>
                    <?php else: ?>
                        <p class="datum"><span>od <?= date('j. n. Y', strtotime(get_field('datum_konani',$termin_id))). ' do '. date('j. n. Y', strtotime(get_field('datum_konani_druhy_den',$termin_id))) ?></span></p>
                    <?php endif; ?>
                    <p class="lokalita"><span><?php echo termin_lokalita($termin_id) ?></span></p>
                    <p class="odkaz"><span><a
                                    href="<?php echo get_permalink($termin_id) ?>"><?php echo termin_nazev_kurzu($termin_id) ?></a></span>
                    </p>

                </div>
                <?php
            }
            wp_reset_postdata();
        }
        ?>

        <p class="vice">
            <a href="<?php echo get_post_type_archive_link('termin') ?>"
               class="tlacitko"><?php _e('Další termíny', 'jz') ?></a>
        </p>

    </div>

</div>