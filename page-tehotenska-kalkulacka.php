<?php

/*
	Template Name: Těhotenská kalkulačka
*/

get_header(); ?>

<div class="content p20p50 content-kalkulacka" id="content">


<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	
	
	<h1 class="main-title"><?php the_title() ?></h1>
	
	
	
	<div class="kalkulacka cf">
		
		<div class="left kalkulacka-vypocet">
			
			<form action="" id="tk">
				
				<div class="item kalkulacka-prvni-den">
					
					<label for="prvni-den"><?php _e('První den poslední menstruace','jz') ?></label>
					
					<input type="text" name="prvni-den" id="prvni-den" placeholder="<?php _e('Vyberte datum','jz') ?>">
					
				</div>
				
				<div class="item kalkulacka-prumerna-delka-cyklu">
					
					<label for="prumerna-delka-cyklu"><?php _e('Průměrná délka cyklu','jz') ?></label>
					
					<input type="number" name="prumerna-delka-cyklu" id="prumerna-delka-cyklu" value="28" min="20" max="40">
					
				</div>
				
				<div class="clear"></div>
				
				<p><strong><?php _e('nebo','jz') ?></strong></p>
				
				<div class="item kalkulacka-datum-souloze">
					
					<label for="datum-souloze"><?php _e('Datum soulože','jz') ?></label>
					
					<input type="text" name="datum-souloze" id="datum-souloze" placeholder="<?php _e('Vyberte datum','jz') ?>">
					
				</div>
				
				<div class="kalkulacka-submit">
					
					<input type="submit" value="<?php _e('Vypočítat','jz') ?>">
					
				</div>
				
				<div class="kalkulacka-vysledek">
					
				</div>
				
			</form>
				
		</div>
		
		<div class="right kalkulacka-citat">
			
			<p class="citat">"<?php _e('Porod není jen o rození miminek. Porod je o matce, která se stává silnější, zdatnější a schopnější. O matce, která věří svému tělu a zná svou vnitřní sílu','jz') ?>"</p>
			<p class="citat-autor">Barbara Katz Rothman</p>
			
		</div>
		
	</div>
	
	
	<script type="text/javascript">
	    jQuery(document).ready(function(){

	        
	        
	        jQuery('#prvni-den').datepicker('hide');
	        jQuery('#prvni-den').click(function() {

	            jQuery('#prvni-den').datepicker({
	                dateFormat: 'dd. mm. yy', 
	                changeMonth: true,
	                changeYear: true
	            });
	            jQuery('#prvni-den').datepicker('show');		 

	        });
	     
	     	
	     	
	     	jQuery('#datum-souloze').datepicker('hide');
	     	jQuery('#datum-souloze').click(function() {

	     	    jQuery('#datum-souloze').datepicker({
	     	        dateFormat: 'dd. mm. yy', 
	     	        changeMonth: true,
	     	        changeYear: true
	     	    });
	     	    jQuery('#datum-souloze').datepicker('show');		 

	     	});
	     
	    });
	    
	    jQuery(function(){
	    	
	    	jQuery('#tk').on('submit', function(){
	    		
	    		jQuery('.kalkulacka-vysledek').html('').hide();
	    		
	    		if( jQuery("#prvni-den").val() == '' && jQuery("#prumerna-delka-cyklu").val() == '' && jQuery("#datum-souloze") == '' ) {
	    			alert('<?php _e('Zadejte prosím do kalkulačky potřebné hodnoty!') ?>');
	    			return false;
	    		}
	    		
	    		var datasend = jQuery(this).serialize();
	    		datasend += '&action=tehotenska_kalkulacka';
	    		
	    		jQuery.post( ajaxurl, datasend, function( response ){
	    			
	    			if( response.success ) {
	    				
	    				jQuery('.kalkulacka-vysledek').html( response.success ).slideDown('fast');
	    				
	    			}
	    			
	    		});
	    		
	    		return false;
	    		
	    	});
	    	
	    });
	    
	</script> 
	
	
	
	
	
	<?php if( get_the_content() ) { ?>
		
		<div class="kalkulacka-content cf">
			<?php the_content() ?>
		</div>
	
	<?php } ?>
	
	
	
	
	
	
	
	<?php get_template_part('template-nejblizsi-terminy') ?>
	
	
	
	
	
<?php endwhile;?>
<?php endif; ?>


</div>

<?php get_footer(); ?>
