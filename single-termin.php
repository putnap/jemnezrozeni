<?php get_header(); ?>

<div class="content p20p50 content-termin" id="content">

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	
	
	<h1 class="main-title"><?php the_title() ?></h1>
	
	<?php 
	$termin_id = get_the_id();
	
	formular_prihlasky( $termin_id );
	
	?>
	
<?php endwhile;?>
<?php endif; ?>


</div>

<?php get_footer(); ?>
