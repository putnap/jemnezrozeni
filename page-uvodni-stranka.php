<?php

/*
	Template Name: Úvodní stránka
*/

get_header(); ?>

<div class="content hp" id="content">

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>


<div class="promo cf">
	
	<div class="promo-hypnoporod">
		<a href="<?php echo get_permalink(  PAGE_ID_HYPNOPOROD  ) ?>">
			<div class="promo-bg"></div>
			<div class="promo-title"><?php _e('Hypnoporod', 'jz') ?></div>
		</a>
	</div>
	
	
	<div class="promo-kurzy">
		<a href="<?php echo get_post_type_archive_link( 'kurz' ) ?>">
			<div class="promo-bg"></div>
			<div class="promo-title"><?php _e('Kurzy', 'jz') ?></div>
		</a>
	</div>
	
	
	<div class="promo-pribehy-klientu">
		<a href="<?php echo get_post_type_archive_link( 'pribeh' ) ?>">
			<div class="promo-bg"></div>
			<div class="promo-title"><?php _e('Příběhy klientů', 'jz') ?></div>
		</a>
	</div>
	
	
	<div class="promo-eshop">
		<a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ) ?>">
			<div class="promo-bg"></div>
			<div class="promo-title"><?php _e('E-shop', 'jz') ?></div>
		</a>
	</div>
	
	
</div>






<div class="hp-title">
	
	<h1 class="center">
		<?php _e('Prožít těhotenství a porod v lásce a klidu', 'jz') ?>
	</h1>
	
</div>









<div class="hp-kurzy cf">
	
	<div class="hp-kurz hp-kurz-1">
		
		<div class="hp-kurz-top cf">
			
			<h2><?php _e('1 denní kurz','jz') ?></h2>
			
			<div class="hp-kurz-top-nazev">
				<?php _e('Kurz Hypnoporodu','jz') ?>
			</div>
			
			<div class="hp-kurz-top-odkaz">
				<a href="<?php echo get_permalink( KURZ_ID_1DENNI ) ?>"><?php _e('Co mne v kurzu čeká?','jz') ?></a>
			</div>
			
		</div>
		
		<div class="hp-kurz-terminy cf">
			
			<?php
			$args = array(
				'post_type' => 'termin',
				'posts_per_page' => 2,
				'meta_query' => array(
					array(
						'key' => 'kurz',
						'value' => KURZ_ID_1DENNI,
					),
					array(
						'key' => 'datum_konani',
						'value' => current_time('Ymd'),
						'compare' => '>=',
					),
				)
			);

			$the_query = new WP_Query( $args );
			if( $the_query->have_posts() ) {
				while( $the_query->have_posts() ) {
					$the_query->the_post();
					get_template_part( 'template-termin-box' );
				}
				wp_reset_postdata();
			}			
			?>
			
		</div>
		
	</div>
	
	
	<div class="hp-kurz hp-kurz-2">
		
		<div class="hp-kurz-top cf">
			
			<h2><?php _e('2 denní kurz','jz') ?></h2>
			
			<div class="hp-kurz-top-nazev">
				<?php _e('Kompletní předporodní příprava s Hypnoporodem','jz') ?>
			</div>
			
			<div class="hp-kurz-top-odkaz">
				<a href="<?php echo get_permalink( KURZ_ID_2DENNI ) ?>"><?php _e('Co mne v kurzu čeká?','jz') ?></a>
			</div>
			
		</div>
		
		<div class="hp-kurz-terminy cf">
			
			<?php
			$args = array(
				'post_type' => 'termin',
				'posts_per_page' => 2,
				'meta_query' => array(
					array(
						'key' => 'kurz',
						'value' => KURZ_ID_2DENNI,
					),
					array(
						'key' => 'datum_konani',
						'value' => current_time('Ymd'),
						'compare' => '>=',
					),
				)
			);

			$the_query = new WP_Query( $args );
			if( $the_query->have_posts() ) {
				while( $the_query->have_posts() ) {
					$the_query->the_post();
					get_template_part( 'template-termin-box' );
				}
				wp_reset_postdata();
			}			
			?>
			
		</div>
		
	</div>
	
	
</div>










<div class="hp-reference">
	
	<?php get_template_part('template-reference') ?>
	
</div>










<div class="hp-blog">
	
	<h2 class="center"><?php _e('Těhotenství a porod','jz') ?></h2>
	
	<div class="hp-blog-prispevky cf">
	<?php
	$the_query = new WP_Query(array(
		'post_type' => 'post',
		'posts_per_page' => 2
	));
	if( $the_query->have_posts() ) {
		while( $the_query->have_posts() ) {
			$the_query->the_post();
			?>
			<div class="hp-blog-prispevek cf">
				<div class="foto">
					<a href="<?php the_permalink() ?>">
						<?php the_post_thumbnail( 'thumbnail' ); ?>
					</a>
				</div>
				<div class="info">
					
					<h3><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h3>
					
					<p class="datum">
						<time datetime="<?php echo current_time('c') ?>"><?php echo current_time('j. n. Y') ?></time>
					</p>
					
					<p class="excerpt">
						<?php echo wp_trim_words( get_the_content(), 30 ); ?>
					</p>
					
				</div>				
			</div>
			<?php
		}
		wp_reset_postdata(); 
		
		?>
		<div class="hp-blog-vse">
			<a href="<?php echo get_permalink( icl_object_id( get_option('page_for_posts') ) ) ?>"><?php _e('zobrazit všechny příspěvky','jz') ?></a>
		</div>
		<?php
		
	} 
	?>
	</div>
	
</div>


























<div class="hp-eshop cf">
	
	<h2 class="center"><?php _e('E-shop','jz') ?></h2>
	
	<?php
	echo do_shortcode( '[products limit="4" columns="4" visibility="featured"]' );
	?>
	
	<div class="hp-eshop-vse">
		<a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ) ?>"><?php _e('zobrazit všechny produkty','jz') ?></a>
	</div>
	
</div>









<?php endwhile;?>
<?php endif; ?>
</div>

<?php get_footer(); ?>
