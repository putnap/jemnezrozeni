<?php


// kontrolu spousti bud cron, nebo rucne tlacitko v adminu
function fio_kontrola_transakci() {


	// TODO doplnit ostatni ucty
	$ucty_a_tokeny = array(
		'2800835793/2010' => 'Teh8rbQaeVEMp3a2OlIMLvJIVrr05oflz1HyN65MmPg8oV1e0eBnDOE2im8fhGsE',
		'2301242723/2010' => 'jONubhJo8QSQg2NnF54dFnjHSFEauEt44tkvY1xn9gpPGy3PbCgUgAkVhTsPUxtn',
		'2800572794/2010' => 'vbXCWIbCeR47Li3IPC2YjJNfKDcK7FdJ0ZUvkD0L4mxLMzhmpjT1lVZxWJV4Bx3E',
		'2801489319/2010' => 'xX24lrpCbyvajkF0gGZMmaX3g8LXXwZbsii3is3ymDhGrAz8wYw943hJTAF85tGm',
	);

	$args = array(
	    'role'    => 'lektorka',
	    'orderby' => 'user_nicename',
	    'order'   => 'ASC'
	);
	$users = get_users( $args );
	//print_r($users);
	foreach ($users as $user) {
		if($user->bank_account && strpos($user->bank_account, '/2010') && $user->fio_allow){
			if ($user->fio_apiKey) {
				$ucty_a_tokeny[$user->bank_account] = $user->fio_apiKey;
			}
		}
	}

	foreach( $ucty_a_tokeny as $ucet => $token ) {

		fio_kontrola_transakci_na_uctu( $ucet, $token );

	}

	fio_kontrola_prepocitat_prihlasky_k_manualni_kontrole();

}
add_action( 'fio_kontrola_cron_job', 'fio_kontrola_transakci' );



function fio_kontrola_registrace_cronu() {
	if ( ! wp_next_scheduled( 'fio_kontrola_cron_job' ) ) {
		wp_schedule_event( time(), 'hourly', 'fio_kontrola_cron_job');
	}
}
add_action( 'wp', 'fio_kontrola_registrace_cronu' );









function fio_kontrola_transakci_na_uctu( $ucet, $token ) {

	if( file_exists(dirname(__FILE__).'/transactions.json') ) {
		$transactions = file_get_contents(dirname(__FILE__).'/transactions.json');
	} else {
		$from = date('Y-m-d', strtotime('-30 days', (int) current_time('timestamp') ) );
		$to = date('Y-m-d', (int) current_time('timestamp') );

		$url = 'https://www.fio.cz/ib_api/rest/periods/'.$token.'/'.$from.'/'.$to.'/transactions.json';
		$transactions = @file_get_contents( $url );
	}

	if( ! $transactions ) {
		fio_log_chyby( $ucet, array( array('error' => 'Nepodařilo se stáhnout seznam transakcí. Kontrolu lze provést nejdříve po 30 sekundách.') ) );
		return;
	}

	$json = json_decode( $transactions );

	if( is_null( $json ) ) {
		fio_log_chyby( $ucet, array( array('error' => 'Transakční data z API nelze přečíst.') ) );
		return;
	}

	$data = array();
	$vs = array();

	foreach( $json->accountStatement->transactionList->transaction as $trans ) {

		if( (int) $trans->column1->value <= 0 ) continue;

		$data[ $trans->column5->value ][] = array(
			'vs' => $trans->column5->value,
			'castka' => $trans->column1->value,
			'ucet' => $trans->column2->value.'/'.$trans->column3->value,
			'uzivatel' => $trans->column7 ? $trans->column7->value : '',
			'datum' => $trans->column0->value,
			'zprava' => $trans->column16->value,
		);
		$vs[] = $trans->column5->value;
	}

	if( ! $data ) {
		fio_log_chyby( $ucet, array( array('error' => 'Nebyly staženy žádné transakce.') ) );
		return;
	}


	$prihlasky = get_posts( array(
		'post_type' => 'prihlaska',
		'posts_per_page' => -1,
		'fields' => 'ids',
		'meta_query' => array(
			array(
				'key' => 'prihlaska-vs',
				'value' => $vs,
				'compare' => 'IN'
			)
		)
	) );

	if( ! $prihlasky  ) {
		fio_log_chyby( $ucet, array( array('error' => 'Ke staženým transakcím nebyly nalezeny žádné přihlášky.') ) );
		return;
	}


	$results = array();
	$uhrazeno = $preskoceno = $ke_kontrole = 0;

	foreach( $prihlasky as $prihlaska_id ) {

		$var_symbol = get_post_meta( $prihlaska_id, 'prihlaska-vs', true );

		$cena_kurzu = get_post_meta( $prihlaska_id, 'prihlaska-castka', true );

		$jmeno = get_post_meta( $prihlaska_id, 'prihlaska-jmeno', true ).' '.get_post_meta( $prihlaska_id, 'prihlaska-prijmeni', true );

		$stav = get_post_meta( $prihlaska_id, 'prihlaska-stav', true );

		// resim pouze prihlasky, ktere jsou ve stavu 'neuhrazeno'

		if( $stav != 'neuhrazeno' ) {
			$preskoceno++;
			continue;
		}

		if( ! isset( $data[ $var_symbol ] ) )
			continue;

		$transakce = $data[ $var_symbol ];

		if( count($transakce) > 1 ) { // vice transakci s jednim VS -> manualni kontrola

			zmena_stavu_prihlasky( $prihlaska_id, 'manual' );

			$results[] = array( 'manual' => "K přihlášce <a href='post.php?post=$prihlaska_id&action=edit'><strong>$var_symbol</strong></a> bylo nalezeno více transakcí (".count($transakce)."). Je nutná manuální kontrola." );
			$ke_kontrole++;
			continue;

		}

		$transakce = $transakce[0];

		if( $transakce['castka'] != $cena_kurzu ) {

			zmena_stavu_prihlasky( $prihlaska_id, 'manual' );

			update_post_meta( $prihlaska_id, 'prihlaska-fio-bankovni-transakce', $transakce );

			$results[] = array( 'manual' => "U přihlášky <a href='post.php?post=$prihlaska_id&action=edit'><strong>$var_symbol</strong></a> nesouhlasí výše platby (cena $cena_kurzu, přišlo ".$transakce['castka']."). Je nutná manuální kontrola." );
			$ke_kontrole++;
			continue;

		}

		// Vse OK

		zmena_stavu_prihlasky( $prihlaska_id, 'uhrazeno' );

		update_post_meta( $prihlaska_id, 'prihlaska-fio-bankovni-transakce', $transakce );

		$results[] = array( 'success' => "Přihláška <a href='post.php?post=$prihlaska_id&action=edit'><strong>$var_symbol</strong></a> uhrazena." );

		$uhrazeno++;

	}


	$results[] = array( 'success' => "Uhrazeno <strong>$uhrazeno</strong>, přeskočeno <strong>$preskoceno</strong>, nutno manuálně zkontrolovat <strong>$ke_kontrole</strong> plateb." );

	fio_log_chyby( $ucet, $results, 'Staženo '.count($data).' transakcí' );
}









// pouziva to i plat. brana (v $ucet je pak "on-line" nebo tak neco)

function fio_log_chyby( $ucet, $results, $nazev = '' ) {
	$logs = get_option('fio_transakcni_log');

	if( ! $logs)
		$logs = array();

	// logy jsou ulozeny od nejnovejsiho ...
	$logs = array_reverse($logs); // obratim je

	// soucasny pridam na konec
	$logs[] = array(
		'datum' => current_time('mysql'),
		'nazev' => $nazev,
		'ucet' => $ucet,
		'vysledek' => $results,
	);

	// a otocim to zpet (od nejnovejsiho k nejstarsim)
	$logs = array_reverse($logs);

	if(count($logs) > 100 )
		$logs = array_slice($logs,0,100);

	update_option('fio_transakcni_log', $logs);
	unset($logs);
}













// vola se i pri zmene stavu prihlasky

function fio_kontrola_prepocitat_prihlasky_k_manualni_kontrole() {

	$prihlasky_ke_kontrole = get_posts( array(
		'post_type' => 'prihlaska',
		'posts_per_page' => -1,
		'fields' => 'ids',
		'meta_query' => array(
			array(
				'key' => 'prihlaska-stav',
				'value' => 'manual',
				'compare' => '='
			)
		)
	) );

	$pocet_prihlasek_ke_kontrole = $prihlasky_ke_kontrole ? count( $prihlasky_ke_kontrole ) : 0;

	set_transient( 'fio_kontrola_pocet_prihlasek_k_manualni_kontrole', $pocet_prihlasek_ke_kontrole, 3600 );

	unset($prihlasky_ke_kontrole);

	return $pocet_prihlasek_ke_kontrole;
}

















/*
       db         88888888ba,    88b           d88  88  888b      88  88   ad88888ba   888888888888  88888888ba          db           ,ad8888ba,   88888888888
      d88b        88      `"8b   888b         d888  88  8888b     88  88  d8"     "8b       88       88      "8b        d88b         d8"'    `"8b  88
     d8'`8b       88        `8b  88`8b       d8'88  88  88 `8b    88  88  Y8,               88       88      ,8P       d8'`8b       d8'            88
    d8'  `8b      88         88  88 `8b     d8' 88  88  88  `8b   88  88  `Y8aaaaa,         88       88aaaaaa8P'      d8'  `8b      88             88aaaaa
   d8YaaaaY8b     88         88  88  `8b   d8'  88  88  88   `8b  88  88    `"""""8b,       88       88""""88'       d8YaaaaY8b     88             88"""""
  d8""""""""8b    88         8P  88   `8b d8'   88  88  88    `8b 88  88          `8b       88       88    `8b      d8""""""""8b    Y8,            88
 d8'        `8b   88      .a8P   88    `888'    88  88  88     `8888  88  Y8a     a8P       88       88     `8b    d8'        `8b    Y8a.    .a8P  88
d8'          `8b  88888888Y"'    88     `8'     88  88  88      `888  88   "Y88888P"        88       88      `8b  d8'          `8b    `"Y8888Y"'   88888888888
*/









function fio_kontrola_podstranka_do_admin_menu() {
	add_submenu_page( 'edit.php?post_type=termin', 'FIO transakce', 'FIO transakce', 'manage_woocommerce', 'fio_kontrola_plateb', 'fio_kontrola_plateb_admin_stranka' );
}
add_action( 'admin_menu', 'fio_kontrola_podstranka_do_admin_menu' );




function fio_kontrola_plateb_admin_stranka() {
	?>
	<div class="wrap">

	<h2>Kontrola transakcí na platební bráně a FIO účtech</h2>



	<form method="post" action="<?php echo admin_url('edit.php?post_type=termin&page=fio_kontrola_plateb') ?>"  >
		<?php
			wp_nonce_field('fio-manual-check');
			submit_button('Spustit manuální kontrolu', 'primary', 'fio-manual-check');
		?>
	</form>


	<?php

		fio_kontrola_vypsani_logu();

	?>

	</div>
	<?php
}





function fio_kontrola_admin_init_listener() {

	if( isset($_POST['fio-manual-check']) && wp_verify_nonce( $_POST['_wpnonce'], 'fio-manual-check') ) {

		fio_kontrola_transakci();

		wp_redirect( admin_url('edit.php?post_type=termin&page=fio_kontrola_plateb') );
		exit;

	}

}
add_action( 'admin_init', 'fio_kontrola_admin_init_listener' );









function fio_kontrola_vypsani_logu() {
	$logs = get_option('fio_transakcni_log');

	if(!$logs) return;

	echo '<div id="fio-log">';

	foreach($logs as $log) {

		$datum = date('j. n. Y H:i:s', strtotime($log['datum']));

		$info = $datum;

		if( $log['ucet'] )
			$info .= ' - '.$log['ucet'];

		if( $log['nazev'] )
			$info .= ' - '.$log['nazev'];

		echo '<div class="fio-log-item" style="margin-bottom: 25px">';

		echo '<h3>'.$info.'</h3>';

		echo fio_kontrola_htmlize_results( $log['vysledek'] );

		echo '</div>';

	}

	echo '</div>';
}



function fio_kontrola_htmlize_results($result ){
	$result_html = '';
	foreach( $result as $r) {
		if(!empty($r['success']))
			$result_html .= '<div class="notice inline notice-success"><p>'.$r['success'].'</p></div>';
		else if(!empty($r['manual'])) {
			$result_html .= '<div class="notice inline notice-warning"><p>'.$r['manual'].'</p></div>';
		}	else {
			$result_html .= '<div class="notice inline notice-error"><p>'.$r['error'].'</p></div>';
		}
	}
	return $result_html;
}












function fio_kontrola_admin_head() {
	?>
	<style type="text/css">
		#adminmenu .update-plugins.manual-check { background-color: #00A2DC; }
	</style>
	<?php

	$warning_count = get_transient( 'jsme_inline_manual_check' );

	if( $warning_count === FALSE ) {

		$warning_count = fio_kontrola_prepocitat_prihlasky_k_manualni_kontrole();

	}

	if( $warning_count ) {
		$warning_title = esc_attr( sprintf( '%d přihlášek ke kontrole', $warning_count ) );
		$menu_label = "Přihlášky <span class='update-plugins manual-check count-$warning_count' title='$warning_title'><span class='update-count'>" . number_format_i18n($warning_count) . "</span></span>";
		?>
		<script type="text/javascript">
		jQuery(function(){
			jQuery("#menu-posts-termin a[href='edit.php?post_type=prihlaska']").html( "<?php echo $menu_label ?>" );
		});
		</script>
		<?php
	}

}
add_action( 'admin_head', 'fio_kontrola_admin_head' );










