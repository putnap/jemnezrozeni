<?php get_header(); ?>

<div class="content content-prehled-kurzu p20p50" id="content">


<h1 class="main-title"><?php _e('Nabídka kurzů','jz') ?></h1>

<div class="prehled-kurzu cf">
<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); 
		if(!get_field('hide')): ?>
			<div class="kurz-item cf same-height-2-1200 same-height-3-1800">
				
				<div class="kurz-item-foto">
					<a href="<?php the_permalink( ) ?>">
						<?php the_post_thumbnail( 'thumbnail' ); ?>
					</a>
				</div>
				
				<div class="kurz-item-info">
					
					<h2><a href="<?php the_permalink( ) ?>"><?php the_title() ?></a></h2>
					
					<p class="desc">
						<?php the_field('popisek_na_prehledu') ?>
					</p>
					
					<p class="link">
						<a href="<?php the_permalink() ?>" class="tlacitko hover-do-ruzove"><?php _e('Detail kurzu','jz') ?></a>
					</p>
					
				</div>
				
			</div>
		<?php endif; ?>
		
	<?php endwhile;?>
<?php endif; ?>
</div>



</div>

<?php get_footer(); ?>
