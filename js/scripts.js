(function(d){
	
  var c = " ", f = "flex", fw = "-webkit-"+f, e = d.createElement('b');
  try { 
    e.style.display = fw; 
    e.style.display = f; 
    c += (e.style.display == f || e.style.display == fw) ? f : "no-"+f; 
  } catch(e) { 
    c += "no-"+f; 
  }
  d.documentElement.className += c; 
})(document);





(function(){
	"use strict";
	
	
	/* 	-----------------------------------------------
		MEDIA QUERY BREAKPOINT
	*/
	if ( ! jQuery.fn.getMediaQueryBreakpoint ) {
		jQuery.fn.getMediaQueryBreakpoint = function() {

			if ( jQuery( '#media-query-breakpoint' ).length < 1 ) {
				jQuery( 'body' ).append( '<span id="media-query-breakpoint" style="display: none;"></span>' );
			}
			var value = jQuery( '#media-query-breakpoint' ).css( 'font-family' );
			if ( typeof value !== 'undefined' ) {
				value = value.replace( "\"", "" ).replace( "\"", "" ).replace( "\'", "" ).replace( "\'", "" );
			}
			if ( isNaN( value ) ) {
				return jQuery(window).width();
			}
			else {
				return parseInt( value );
			}

		};
	}
	
	var mediaQueryBreakpoint
	if ( jQuery.fn.getMediaQueryBreakpoint ) {
		mediaQueryBreakpoint = jQuery.fn.getMediaQueryBreakpoint();
		jQuery(window).resize(function(){
			if ( jQuery.fn.getMediaQueryBreakpoint() !== mediaQueryBreakpoint ) {
				mediaQueryBreakpoint = jQuery.fn.getMediaQueryBreakpoint();
				jQuery.event.trigger({
					type: 'screenTransition',
					message: 'Screen transition completed.',
					time: new Date()
				});
			}
		});
	}
	else {
		mediaQueryBreakpoint = jQuery(document).width();
	}
	
	
	
	
	
	/* 	-----------------------------------------------
		ON DELAYED
	*/	
	if( ! jQuery.fn.onDelayed ) {
		jQuery.fn.onDelayed = function(eventName,delayInMs,callback){
		    var _timeout;
		    this.on(eventName,function(e){
		      if(!!_timeout){
		          clearTimeout(_timeout);
		          //console.log('timer being re-set: ' + eventName);
		      } else {
		          //console.log('timer being set for the first time: ' + eventName);
		      }
		      _timeout = setTimeout(function(){
		          callback(e);
		      },delayInMs);
		    });
		}
	}
	
	
	
	
	/* 	-----------------------------------------------
		MIN HEIGHT
	*/
	if( ! jQuery.fn.setMinHeight ) {
		jQuery.fn.setMinHeight = function(setCount) {
		  for(var i = 0; i < this.length; i+=setCount) {
		    var curSet = this.slice(i, i+setCount), 
		        height = 0;
		    curSet.each(function() { height = Math.max(height, jQuery(this).outerHeight()); })
		          .css('min-height', height);
		  }
		  return this;
		}
	}
	
	
	
	
	/* 	-----------------------------------------------
		FANCYBOX
	*/
	if( ! jQuery.fn.loadFancybox ) {
		jQuery.fn.loadFancybox = function() {
			
			var $this = jQuery(this);
			
			$this.find(".gallery").each(function(){
				jQuery(this).find("a").attr("rel","lightbox["+ jQuery(this).attr("id") +"]");
			});
			
			$this.find("a[href$='.jpg'],a[href$='.png'],a[href$='.gif'],.fancybox").not('.nofancybox').fancybox({ loop : false });
			
			$this.find(".fancyvideo").fancybox({ 
				type: 'iframe'
			});
		}
	}
	
	
	/* 	-----------------------------------------------
		POSUNOVNAI OKNA
	*/
	if( ! jQuery.fn.loadPosunovac ) {
		 jQuery.fn.loadPosunovac = function() {
		 	
		 	var $this = jQuery(this);
		 	
		 	$this.find("a[href^=#]").on('click', function(){
		 		
		 		var target = jQuery(this).attr("href");
		 		var target_elem = jQuery( target );
		 			
		 		if( target_elem.length == 1 ) {
		 		
		 			jQuery('html, body').animate({ scrollTop: target_elem.offset().top - 80 }, 300);  
		 			
		 			return false;
		 		}
		 	});
		 }
		
	}
	
	
	/* 	-----------------------------------------------
		Vyska sidebaru a hlavniho obsahu
	*/
	if( ! jQuery.fn.loadMainColumnsHeight ) {
		
		jQuery.fn.loadMainColumnsHeight = function () {
			
			if( jQuery('html').hasClass('flex') ) return;
			
			var $this = jQuery(this);  // body
			var header = $this.find('#header');
			var main = $this.find('#main');
			
			if( mediaQueryBreakpoint >= 960 ) {
			
				var w_height = jQuery(window).height();
				
				if( main.outerHeight() < w_height  ) {
					// hlavni sloupec je nizsi nez okno
					
					main.height( w_height );
					header.height( w_height );
					
					
				} else if( header.outerHeight() < main.outerHeight() ) { // hm ... s timhle se nejak nemohu srovnat.
					
					header.height( main.outerHeight() );
					
				} else if( header.outerHeight() > main.outerHeight() ) {
					
					header.height( main.outerHeight() );
				}
				
			} else {
				
				main.css('height', 'auto');
				header.css('height', 'auto');
				
			}
		
		}
		
	}
	
	
	/* 	-----------------------------------------------
		Mobilni menu
	*/
	if( ! jQuery.fn.loadMobileMenu ) {
		
		
		
		jQuery.fn.loadMobileMenu = function () {
		
			var $this = jQuery(this);
			
			$this.find(".menu-toggle").on('click',function(){
				
				$this.find(".navigace").slideToggle('fast');
				
				jQuery(this).toggleClass('on');
				
				return false;
			});
			
			
		}
	
	}
		
	
	
	
	
	
	
	
	/* 	-----------------------------------------------
		Same height
	*/
	if( ! jQuery.fn.loadSameHeights ) {
		
		
		
		jQuery.fn.loadSameHeights = function () {
		
			var $this = jQuery(this);
			
			
			// hp kurzy + terminy
			
			var hp_kurzy_zahlavi = jQuery('.hp-kurz-top');
			
			if( hp_kurzy_zahlavi.length > 0 ) {
				
				hp_kurzy_zahlavi.css('min-height', 'auto');
				
				if( mediaQueryBreakpoint >= 1400 ) {
					
					hp_kurzy_zahlavi.setMinHeight( 2 );
					
				}
				
				
				var hp_terminy_adresy = jQuery('.hp-kurz .termin-box-adresa');
				
				hp_terminy_adresy.css('min-height', 'auto');
				
				if( mediaQueryBreakpoint >= 1400 ) {
					
					hp_terminy_adresy.setMinHeight( 4 );
					
				} else if( mediaQueryBreakpoint >= 768 ) {
					
					hp_terminy_adresy.setMinHeight( 2 );
					
				}
								
				
				var hp_terminy_lektorky = jQuery('.hp-kurz .termin-box-lektorka');
				
				hp_terminy_lektorky.css('min-height', 'auto');
				
				if( mediaQueryBreakpoint >= 1400 ) {
					
					hp_terminy_lektorky.setMinHeight( 4 );
					
				} else if( mediaQueryBreakpoint >= 768 ) {
					
					hp_terminy_lektorky.setMinHeight( 2 );
					
				}
				
			}
			
			
			
			var hp_prispevky = $this.find('.hp-blog-prispevek,.hp-blog-vse');
			
			if( hp_prispevky.length > 0 ) {
				
				hp_prispevky.css('min-height', 'auto');
				
				if( mediaQueryBreakpoint >= 1600 ) {
					
					hp_prispevky.setMinHeight( 3 );
					
				} 
				
			}
			
			
			
			
			var hp_produkty = $this.find('.hp-eshop .product h2');
			
			if( hp_produkty.length > 0 ) {
				
				hp_produkty.css('min-height', 'auto');
				
				if( mediaQueryBreakpoint >= 768 ) {
					
					hp_produkty.setMinHeight( 4 );
					
				} 
				
			}
			
			
			var hp_eshop_a_odkaz = $this.find('.hp-eshop .woocommerce, .hp-eshop-vse');
			
			if( hp_eshop_a_odkaz.length > 0 ) {
				
				hp_eshop_a_odkaz.css('min-height', 'auto');
				
				if( mediaQueryBreakpoint >= 1600 ) {
					
					hp_eshop_a_odkaz.setMinHeight( 2 );
					
				} 
				
			}
			
			
			
			
			// lektorky
			
			var lektorky = $this.find('.lektorka h2');
			
			if( lektorky.length > 0 ) {
				
				lektorky.css('min-height', 'auto');
				
				if( mediaQueryBreakpoint >= 768 ) {
					
					lektorky.setMinHeight( 3 );
					
				}
				
			}
			
			
			// produkty
			
			var produkty = $this.find('.shop-archive li.product');
			
			if( produkty.length > 0 ) {
				
				produkty.css('min-height', 'auto');
				
				if( mediaQueryBreakpoint >= 768 ) {
					
					produkty.setMinHeight( 3 );
					
				} else if( mediaQueryBreakpoint >= 500 ) {
					
					produkty.setMinHeight( 2 );
					
				} 
				
			}
			
			
			// "related" produkty
			
			var produkty = $this.find('.related li.product');
			
			if( produkty.length > 0 ) {
				
				produkty.css('min-height', 'auto');
				
				if( mediaQueryBreakpoint >= 768 ) {
					
					produkty.setMinHeight( 4 );
					
				} else if( mediaQueryBreakpoint >= 500 ) {
					
					produkty.setMinHeight( 2 );
					
				} 
				
			}
			
			
			
			// kontakt
			
			var kontakt_informace_sloupce = $this.find('.kontakt-informace .first-col, .kontakt-informace .second-col');
			
			if( kontakt_informace_sloupce.length > 0 ) {
				
				kontakt_informace_sloupce.css('min-height', 'auto');
				
				if( mediaQueryBreakpoint >= 768 ) {
					
					kontakt_informace_sloupce.setMinHeight( 2 );
					
				} 

			}
			
			
			// nejblizsi terminy
			var hp_terminy_adresy = jQuery('.nejblizsi-terminy .termin-box-adresa');
			
			if( hp_terminy_adresy.length > 0 ) {
				
				hp_terminy_adresy.css('min-height', 'auto');
				
				if( mediaQueryBreakpoint >= 1600 ) {
					
					hp_terminy_adresy.setMinHeight( 4 );
					
				} else if( mediaQueryBreakpoint >= 768 ) {
					
					hp_terminy_adresy.setMinHeight( 2 );
					
				} 
			
			}
			
			
			
			
			
			// same-height-2-1200
			
			var sh1800 = jQuery('.same-height-3-1800');
			var sh1200 = jQuery('.same-height-2-1200');
			
			if( sh1800.length > 0 ) {
				
				sh1800.css('min-height', 'auto');
				
				if( mediaQueryBreakpoint >= 1800 ) {
					
					sh1800.setMinHeight( 3 );
					
				}
				
			} else if( sh1200.length > 0 ) {
				
				sh1200.css('min-height', 'auto');
				
				if( mediaQueryBreakpoint >= 1200 ) {
					
					sh1200.setMinHeight( 2 );
					
				}
				
			}
			
		}
	
	}
	
	
	
	
	
	
	
	
	/* 	-----------------------------------------------
		Audios
	*/
	if( ! jQuery.fn.loadAudios ) {
		
		
		
		jQuery.fn.loadAudios = function () {
		
			var $this = jQuery(this);
			
			$this.find('.audio-start').on('click', function(){
				
				var target = jQuery(this).data('target');
				var audio = document.getElementById( target );
				
				if( audio ) {
					
					if( audio.paused || audio.currentTime == 0 ) { // zastavene, nebo jeste nezacalo
						
						//audio.volume = 0;
						audio.play();
						//jQuery(audio).animate( { volume : 1 }, 2000 );
						
					} else { // spoustime
						
						audio.pause();
						audio.currentTime = 0;
					}
					
				}
				
				return false;
			});
			
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/* 	-----------------------------------------------
		Fluid media playery
	*/
	
	if ( ! jQuery.fn.fluidMedia ) {
		jQuery.fn.fluidMedia = function( ){

			var $this = jQuery(this),
				allMedia;

			// CHECK FOR ANY LOOSE IFRAMES
			$this.find( 'iframe, embed' ).each(function(){

				if ( jQuery(this).parents( '.embed-media' ).length < 1 ) {
					if ( jQuery(this).parent().is( 'p' ) ) {
						jQuery(this).unwrap();
					}
					jQuery(this).wrap( '<div class="embed-media"></div>' );
				}

			});

			var reloadFluidMedia = function(){
				// Resize all media according to their own aspect ratio
				allMedia.each(function() {
					var el = jQuery(this),
						elContainer = el.parents( '.embed-media' ),
						newWidth = elContainer.width();
					el.width( newWidth ).height( newWidth * el.attr( 'data-aspectratio' ) );
				});
				jQuery.event.trigger({
					type: 'fluidMediaReloaded',
					message: 'Fluid media reloaded.',
					time: new Date()
				});
			};

			var generateFluidMedia = function(){
				// Find all media
				allMedia = $this.find( '.embed-media iframe, .embed-media embed' );
				// The element that is fluid width
				//$fluidEl = jQuery('.embed-media').first();
				// Figure out and save aspect ratio for each media
				allMedia.each(function() {
					jQuery(this).attr( 'data-aspectratio', jQuery(this).height() / jQuery(this).width() )
						.removeAttr( 'height' )
						.removeAttr( 'width' );
				});
				reloadFluidMedia();
			};

			if ( $this.find( '.embed-media' ).length > 0 ) {
				generateFluidMedia();
				jQuery(window).resize(function(){
					reloadFluidMedia();
				});
			}

		};
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
	/* 	-----------------------------------------------
		Slideshows
	*/
	if( ! jQuery.fn.loadSlideshows ) {
		
		
		
		jQuery.fn.loadSlideshows = function () {
			
			if( typeof jQuery.fn.carouFredSel != 'function' ) {
				return false;
			}
			
			var $this = jQuery(this);
			
			var reference = $this.find('.reference-slider');
			
			if( reference.length > 0 ) {
				
				buildReferenceSlideshow( reference );
				
				jQuery(window).onDelayed('resize', 400, function(){
					buildReferenceSlideshow( reference );
				});
				
				function buildReferenceSlideshow( slider ) {
					
					slider.trigger('destroy');
					
					slider.find('.reference').css({ 'display' : 'block', 'float' : 'left' });
					
					slider.carouFredSel({
						responsive: true,
						auto : {
							play : false,
							pauseOnHover : true,
							timeoutDuration: 5000
						},
						prev : '#reference-prev',
						next : '#reference-next',
						/*
						pagination : {
							container: slider.parent().find('.reference-tecky')
						},
						*/
						scroll: {
							items : 1,
							pauseOnHover : true,
							onAfter : function() { jQuery('body').loadMainColumnsHeight() }
						},
						items: {
							visible: 1
						}
					});
					
				}
				
			}
			
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/* 	-----------------------------------------------
		Filtr Terminu
	*/
	if( ! jQuery.fn.loadFiltrTerminuListeners ) {
	
		jQuery.fn.loadFiltrTerminuListeners = function () {
			
			var $this = jQuery(this);
			
			var form = $this.find('#terminy-filtr-form');
			var select = form.find('select');
			
			select.on('change', function(){
				
				form.trigger('submit');
				
				return false;
			});
			
		}
	}
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/* 	-----------------------------------------------
		Kontaktni formular
	*/
	if( ! jQuery.fn.loadKontaktniFormular ) {
	
		jQuery.fn.loadKontaktniFormular = function () {
			
			var $this = jQuery(this);
			
			var kf = jQuery("#kf");

			var alert_message = kf.find('.alert');
			var alert_message_zaloha = alert_message.text();
			
			kf.on('submit', function(){
				
				alert_message.slideUp('fast');
				
				if( jQuery("#kf-jmeno").val() == '' || jQuery("#kf-email").val() == '' || jQuery("#kf-telefon").val() == ''  || jQuery("#kf-zprava").val() == '' 
						|| ( jQuery("#kf-souhlas").length == 1 && ! jQuery("#kf-souhlas").is(':checked')  )
					) {
					alert_message.html( alert_message_zaloha );
					alert_message.slideDown('fast');
					return false;
				}
				
				var datasend = {
					'action' : 'jz_kontakt_send',
					'kf-jmeno' : jQuery("#kf-jmeno").val(),
					'kf-email' : jQuery("#kf-email").val(),
					'kf-telefon' : jQuery("#kf-telefon").val(),
					'kf-zprava' : jQuery("#kf-zprava").val()
				}
				
				var lektorka = jQuery("#kf-lektorka");
					
				if( lektorka.length == 1 ) {
					datasend['kf-lektorka'] = lektorka.val();
				}
				
				jQuery.post(ajaxurl, datasend, function(response) {
					if( response.error ) {
					
						alert_message.html( response.error );
						alert_message.slideDown('fast');
					
					} else {
					
						var message = '<div class="success">'+response.success+'</div>';
						alert_message.html( message );
						alert_message.slideDown('fast');
						kf.trigger("reset");
						
					}
				});
				
				return false;
			});
			
			
			kf.find('#kf-lektorka').on('change', function(){
				
				var select = jQuery(this);
				
				if( select.val() == '' ) {
					select.removeClass('hasvalue');
				} else {
					select.addClass('hasvalue');
				}
				
			});
			
			
		}
	}
		

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/* 	-----------------------------------------------
		Přihláška
	*/
	if( ! jQuery.fn.loadPrihlaska ) {
	
		jQuery.fn.loadPrihlaska = function () {
			
			var $this = jQuery(this);
			
			var par_upresneni = $this.find('.par-upresneni'); // div
			
			if( par_upresneni.length == 0 ) return;
			
			var par_upresneni_input = jQuery("#prihlaska-par-upresneni");
			
			$this.find('input[name="prihlaska-typ-prihlasky"]').on('change', function(){
				
				if( jQuery(this).val() == 'par' ) {
					
					par_upresneni.slideDown('fast');
					par_upresneni_input.prop('required', true );
					
				} else {
					
					par_upresneni.slideUp('fast');
					par_upresneni_input.prop('required', false );
					
				}
				
			});
			
		}
	}
		
	
	
	
	
	
	
	
	
	
	
	
	/* 	-----------------------------------------------
		Footer mailchimp
	*/
	if( ! jQuery.fn.loadFooterMailchimp ) {
	
		jQuery.fn.loadFooterMailchimp = function () {
			
			var $this = jQuery(this);
			
			jQuery('.footer-mc-form').on('submit', function(){
				
				var form = jQuery(this);
				
				form.find( '.success,.error' ).remove();
				
				form.css({ opacity: .3 });
				
				var datasend = {
					'action' : 'jz_footer_mailchimp',
					'mc-email' : jQuery("#mc-email").val()
				}
				
				jQuery.post( ajaxurl, datasend, function(response){
					
					var html;
					
					if( response.success ) {
						
						html = '<p class="success">' + response.success + '</p>';
						
					} else {
						
						html = '<p class="error">' + response.error + '</p>';
						
					}
					
					form.prepend( html );
					
					form.css({ opacity: 1 });
					
				});
				
				return false;
			});
			
		}
	}
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


	/* 	-----------------------------------------------
		Maps
	*/
	if( ! jQuery.fn.loadGoogleMaps ) {
		
		jQuery.fn.loadGoogleMaps = function () {
			
			var $this = jQuery(this);
			
			$this.find('.google-mapa').each(function(){
				renderGoogleMap( jQuery(this) );
			});

		}
	}
	

	function renderGoogleMap( $el ) {
		var $markers = $el.find('.marker');
		
		var args = {
			center		: new google.maps.LatLng( 49.6141228, 15.6380019 ),
			mapTypeId	: google.maps.MapTypeId.HYBRID,
			mapTypeControlOptions : {
				position: google.maps.ControlPosition.TOP_RIGHT
			},
			scrollwheel : false,
			zoom		: 8,
			zoomControlOptions: {
				position: google.maps.ControlPosition.TOP_RIGHT
			},
			streetViewControlOptions: {
				position: google.maps.ControlPosition.TOP_RIGHT
			}
		};

		var map = new google.maps.Map( $el[0], args);
		var single_zoom = 17;
		var no_marker_center = null;

		map.markers = [];
		var temp_marker;

		$markers.each(function(){
			
			var $marker = jQuery(this);
			
			var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );


			var marker_config = {
				position	: latlng,
				map			: map,
			}

			if( $marker.attr("data-icon") ) {
				marker_config.icon = $marker.attr("data-icon");
			}

			if( $marker.attr("data-zoom") ) {
				single_zoom = $marker.attr("data-zoom");
			}
			
			
			
			var marker = new google.maps.Marker( marker_config );
			map.markers.push( marker );
			
			
			/*
			if( $marker.attr("data-link") ) {
				var link = $marker.attr("data-link");
				
				google.maps.event.addListener(marker, 'click', function() {
					window.location.href = link;
				});
				
			}
			*/
			
			
			
			/*
			if( $marker.html() ) 	{
				var infowindow = new google.maps.InfoWindow({
					content		: $marker.html()
				});

				google.maps.event.addListener(marker, 'click', function() {
					infowindow.open( map, marker );
				});
			}
			*/
		});


		// center map
		var bounds = new google.maps.LatLngBounds();

		jQuery.each( map.markers, function( i, marker ){
			var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
			bounds.extend( latlng );
		});
		
		
		if( map.markers.length == 1 )
		{
		    map.setCenter( bounds.getCenter() );
		    map.setZoom( single_zoom *1 );
		}
		else if( map.markers.length == 0 )
		{
			//map.setCenter( no_marker_center );
			//map.setZoom( single_zoom * 1 );
		}
		else
		{

			map.fitBounds( bounds );
		}
		

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/* 	-----------------------------------------------
		LET'S ROLL...
	*/
	
	jQuery(function(){
		
		jQuery( 'body' ).one( 'touchstart', function(){
			jQuery(this).addClass( 'has-touch' );
		});
		
		
		
		
		
		
		
		
		if ( ! jQuery.fn.initPage ) {
			jQuery.fn.initPage = function( element ){

				var $element = jQuery( element );

				
				if( jQuery.fn.loadMobileMenu ) {
					$element.loadMobileMenu();
				}
				
				if ( jQuery.fn.loadFancybox ){
					$element.loadFancybox();
				}
				
				
				if ( jQuery.fn.loadMainColumnsHeight ){
					
					$element.loadMainColumnsHeight(); // nastavim vysky hned pri loadu
					
					setTimeout( function(){ $element.loadMainColumnsHeight(); }, 500 ); // po pul sekunde je jeste prepocitam
					
					jQuery(window).onDelayed("resize", 150, function(){ $element.loadMainColumnsHeight(); }); // ... a pak pri kazde zmene okna
					
				}
				
				
				if ( jQuery.fn.loadSameHeights ){
					
					$element.loadSameHeights();
					
					jQuery(window).onDelayed("resize", 150, function(){ $element.loadSameHeights(); });
				}
				
				
				if ( jQuery.fn.loadAudios ){
					$element.loadAudios();
				}
				
				if ( jQuery.fn.fluidMedia ){
					$element.fluidMedia();
				}
				
				if ( jQuery.fn.loadSlideshows ){
					$element.loadSlideshows();
				}
				
				
				if ( jQuery.fn.loadFiltrTerminuListeners ){
					$element.loadFiltrTerminuListeners();
				}
				
				if ( jQuery.fn.loadKontaktniFormular ){
					$element.loadKontaktniFormular();
				}
				
				
				if ( jQuery.fn.loadPrihlaska ){
					$element.loadPrihlaska();
				}
				
				if ( jQuery.fn.loadFooterMailchimp ){
					$element.loadFooterMailchimp();
				}
				
				if( jQuery.fn.loadGoogleMaps ) {
					$element.loadGoogleMaps();
				}
				
				
				
			};
		}
		jQuery.fn.initPage( 'body' );
		
		
		
		
		
		/* 	-----------------------------------------------
			MEDIA QUERY
		*/
		var mediaQueryBreakpoint;
		if ( jQuery.fn.getMediaQueryBreakpoint ) {
			mediaQueryBreakpoint = jQuery.fn.getMediaQueryBreakpoint();
			jQuery( document ).on( 'screenTransition', function(){
				mediaQueryBreakpoint = jQuery.fn.getMediaQueryBreakpoint();
			});
		}
		else {
			mediaQueryBreakpoint = jQuery(window).width();
		}
		
		if(jQuery('.open-popup-loc').length){
			jQuery('.open-popup-loc').click(function(){
				let elm = jQuery(this);
				jQuery.magnificPopup.open({
					items: {
					    src: '#info_popup',
					    type:'inline',
					},
				  	midClick: true, // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
					callbacks: {
					    beforeOpen: function() {
					        jQuery('.loc_info').html(elm.closest('.info_ico').find('.info_holder').html());
					    },
					    close: function(){
					    	jQuery('.loc_info').html('Není uvedena žádná poznámka');
					    }
					},
				});
			});
		}
		
		if(jQuery('.open-popup-link').length){
			jQuery('.open-popup-link').click(function(){
				let elm = jQuery(this);
				jQuery.magnificPopup.open({
					items: {
					    src: '#info_popup',
					    type:'inline',
					},
				  	midClick: true, // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
					callbacks: {
					    beforeOpen: function() {
					        jQuery('.internal_post').val(elm.attr('data-internal'));
					        if (elm.attr('data-client')) {
					        	jQuery('.client_note').text(elm.attr('data-client'));
					        }
					      	jQuery('.tid').val(elm.attr('data-tid'));
					    },
					    close: function(){
					    	jQuery('.internal_post', '.tid').val('');
					    	jQuery('.client_note').text('Klient/ka neuvedl/a žádnou poznámku');
					    }
					},
				});
			});
		}
		
		if(jQuery('.open-popup-termin').length){
			jQuery('.open-popup-termin').click(function(){
				let elm = jQuery(this);
				jQuery.magnificPopup.open({
					items: {
					    src: '#termin_popup',
					    type:'inline',
					},
				  	midClick: true, // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
					callbacks: {
					    beforeOpen: function() {
					        jQuery('.termname').text(elm.attr('data-termname'));
					      	jQuery('.tid_change').val(elm.attr('data-term'));
					    },
					    close: function(){
					    	jQuery('.termname').text('');
					      	jQuery('.tid_change').val('');
					      	jQuery('#prihlaska-zmenit-termin').val('');
					    }
					},
				});
			});
		}

		jQuery('.save_note').submit(function(e){
			e.preventDefault();

			var form = jQuery(this);
				
			form.find( '.success,.error' ).remove();
			
			form.css({ opacity: .3 });

			var datasend = {
				'action' : 'jz_save_note',
				'order_id' : jQuery('.tid').val(),
				'internal_post' : jQuery('.internal_post').val(),
			}
				
			jQuery.post( ajaxurl, datasend, function(response){
				
				var html;
				
				if( response.success ) {
					html = '<p class="success">' + response.success + '</p>';
					jQuery('a[data-tid="'+jQuery('.tid').val()+'"]').attr('data-internal',jQuery('.internal_post').val() );
				} else {
					
					html = '<p class="error">' + response.error + '</p>';
					
				}
				
				form.prepend( html );
				
				form.css({ opacity: 1 });
				
			});
		});


		if (typeof acf !== 'undefined'){
			acf.add_filter('validation_complete', function( json, $form ){
		
	            // if errors?
	            if( jQuery("input#acf-_post_title").length && !jQuery("input#acf-_post_title").val() ) {
	                
	                var temp = new Object();
	                temp["input"] = "acf[_post_title]";
	                temp["message"] = "Název je povinný";
	                json.errors.push(temp);
	                
	            }
	            
	            
	            // return
	            return json;
	                    
	        });
		}
		
		
	});

})();









