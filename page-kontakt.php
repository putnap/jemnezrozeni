<?php

/*
	Template Name: Kontakt
*/

get_header(); ?>

<div class="content p20p50 content-kontakt" id="content">


<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	
	
	<h1 class="main-title"><?php the_title() ?></h1>
	
	
	<div class="kontakt cf">
		
		<div class="kontakt-informace cf">
			
			<div class="first-col">
				
				<h2 class="nazev-firmy"><?php the_field('nazev_firmy') ?></h2>
				
				<p class="fakturacni-informace">
				<?php 
					echo nl2br( trim( get_field('fakturacni_informace') ) );
				?>
				</p>
				
				<h4><?php _e('Bankovní spojení','jz') ?></h4>
				
				<p class="bankovni-spojeni">
				<?php 
					echo nl2br( trim( get_field('bankovni_spojeni') ) );
				?>
				</p>
				
			</div>
			
			
			<div class="second-col">
				
				<?php get_template_part( 'template-kontakty' ) ?>				
				
				<?php get_template_part( 'template-socialky' ) ?>
				
			</div>
			
		</div>
		
		<?php get_template_part('template-kontaktni-formular') ?>
		
	</div>
	

	
	
	<div class="kontakt-lektorky">
		

		<?php get_template_part('template-lektorky-mapa') ?>


		<div class="lektorky-prehled cf">
		<?php 
		
		$args = array(
			'post_type' => 'lektorka',
			'posts_per_page' => -1
		);
		
		if( !empty($_GET['kraj']) ) {
			
			$args['meta_query'] = array(
				array(
					'key' => 'kraj',
					'value' => $_GET['kraj'],
					'compare' => 'LIKE'
				)
			);

		}
		
		$the_query = new WP_Query( $args );
		if( $the_query->have_posts() ) {
			while( $the_query->have_posts() ) {
				$the_query->the_post();
				?>
				
				<?php get_template_part('template-lektorky-loop') ?>
				
				<?php
			}
			wp_reset_postdata(); 
		} 		
		?>
		</div>
		
		
		
		
	</div>
	
	
	
	
	
	

	
<?php endwhile;?>
<?php endif; ?>


</div>

<?php get_footer(); ?>
