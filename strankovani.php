
<?php
global $wp_query;

$big = 999999999; 

$strankovani = paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '?paged=%#%',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $wp_query->max_num_pages,
	'prev_text' => '&laquo;',
	'next_text' => '&raquo;',
) );

if( $strankovani ) {
	?>
	<div class="strankovani cisla">
		<?php echo $strankovani ?>
	</div>
	<?php
}

?>

