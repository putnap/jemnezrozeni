















<footer class="footer">
<div class="in">

		<div class="footer-mailchimp">

			<form class="footer-mc-form cf">
				<input type="email" name="mc-email" id="mc-email" placeholder="<?php _e('Váš e-mail','jz') ?>">
				<input type="submit" id="mc-submit" value="<?php _e('Odebírat novinky','jz') ?>">
                <br>
                <div class="newslette_acc">
                <?= __('Odesláním formuláře souhlasíte se <a href="/wp-content/uploads/2018/06/souhlas.pdf" target="_blank">zpracováním osobních údajů', 'jz') ?></a>
                </div>
			</form>

		</div>

		<div class="footer-copy">
			&copy; Jemné Zrození s.r.o. | <?php echo date('Y') ?>
            <a href="<?= get_permalink(PAGE_ID_OBCHODNI_PODMINKY) ?>"><?= __('Obchodní podmínky', 'jz') ?></a> |
            <a href="/wp-content/uploads/2018/06/souhlas.pdf" target="_blank"><?= __('Zásady zpracování osobních údajů', 'jz') ?></a> <br>
            <a href="https://www.bartvisions.cz"><?php _e('Vyrobilo studio bARTvisions','jz') ?></a>
		</div>

</div>
</footer>


</div> <!-- /main -->






</div> <!-- web-wrapper -->

<?php wp_footer(); ?>

</body>
</html>
