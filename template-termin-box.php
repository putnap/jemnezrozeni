<?php
$termin_id = get_the_id();
?>
<div class="termin-box">
    <a href="<?php echo get_permalink( $termin_id ) ?>">


        <?php if (!get_field('typ_terminu', $termin_id) || get_field('typ_terminu', $termin_id) == "jednoden"): ?>
            <p class="termin-box-datum">
                <?php echo termin_datum_konani( $termin_id ) ?>
            </p>
        <?php else: ?>
            <p class="termin-box-datum">
                od <?= date('j. n. Y', strtotime(get_field('datum_konani',$termin_id))) . ' (vícedenní)' ?>
            </p>
        <?php endif; ?>

        <p class="termin-box-adresa">
            <?php echo termin_lokalita_adresa( $termin_id, true ) ?>
        </p>

        <p class="termin-box-lektorka">
            <?php _e('Lektorka', 'jz') ?>: <?php echo termin_lektorka( $termin_id ) ?>
        </p>

        <p class="termin-box-kapacita">
            <?php echo termin_volna_mista_html( $termin_id ); ?>
        </p>

    </a>
</div>