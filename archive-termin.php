<?php get_header(); ?>

<div class="content p20p50 content-terminy" id="content">


    <h1 class="main-title"><?php _e('Termíny kurzů', 'jz') ?></h1>


    <div class="terminy-filtr cf">

        <form action="<?php echo get_post_type_archive_link('termin') ?>" method="get" id="terminy-filtr-form">

            <div class="filtr-item filtr-item-course">

                <label for="filter-course"><?php _e('Název kurzu', 'jz') ?></label>
                <select name="filter-course" id="filter-course">
                    <option value=""><?php _e('Vyberte název kurzu', 'jz') ?></option>
                    <option value="online" <?php selected(@$_GET['filter-course'], 'online') ?>>Online</option>
                    <?php
                    $the_query = new WP_Query(array(
                        'post_type' => 'kurz',
                        'post__not_in' => array(KURZ_ID_SOUKROMY, 6771, 12956, 537, 13035, 6706), // NIKOLI SOUKROMY KURZ!!!
                        'posts_per_page' => -1,
                        'orderby' => 'title',
                        'order' => 'ASC',
                    ));
                    if ($the_query->have_posts()) {
                        while ($the_query->have_posts()) {
                            $the_query->the_post();
                            ?>
                            <option value="<?php the_id() ?>" <?php selected(@$_GET['filter-course'], get_the_id()) ?>><?php the_title() ?></option>
                            <?php
                        }
                        wp_reset_postdata();
                    }
                    ?>
                </select>

            </div>


            <div class="filtr-item filtr-item-locality">

                <label for="filter-locality"><?php _e('Město', 'jz') ?></label>
                <select name="filter-locality" id="filter-locality">
                    <option value=""><?php _e('Vyberte město', 'jz') ?></option>
                    <?php
                    $lokality_all = get_posts(array(
                        'post_type' => 'lokalita',
                        'posts_per_page' => -1,
                        'orderby' => 'meta_value',
                        'order' => 'ASC',
                        'meta_query' => array(
                            array(
                                'key' => 'adresa_mesto_filtr',
                                //'value' => '',
                                'operator' => 'EXISTS'
                            )
                        )
                    ));

                    $mesta_a_id = array(); // nazev mesta bude klic pole

                    if ($lokality_all) {
                        foreach ($lokality_all as $lokalita) {

                            $lokalita_id = $lokalita->ID;
                            $mesto = get_post_meta($lokalita_id, 'adresa_mesto_filtr', true);

                            if (!$mesto) continue;

                            if (!isset($mesta_a_id[$mesto]))
                                $mesta_a_id[$mesto] = array();

                            $mesta_a_id[$mesto][] = $lokalita_id;
                        }
                    }

                    if ($mesta_a_id) {

                        ksort($mesta_a_id);

                        foreach ($mesta_a_id as $mesto => $ids) {

                            $ids = implode(',', $ids);
                            ?>
                            <option value="<?php echo $ids ?>" <?php selected(@$_GET['filter-locality'], $ids) ?>><?php echo $mesto ?></option>
                            <?php
                        }
                    }

                    ?>
                </select>

            </div>


            <div class="filtr-item filtr-item-lecturer">

                <label for="filter-lecturer"><?php _e('Lektorka', 'jz') ?></label>
                <select name="filter-lecturer" id="filter-lecturer">
                    <option value=""><?php _e('Vyberte lektorku', 'jz') ?></option>
                    <?php
                    $the_query = new WP_Query(array(
                        'post_type' => 'lektorka',
                        'posts_per_page' => -1,
                        'orderby' => 'title',
                        'order' => 'ASC',
                    ));
                    if ($the_query->have_posts()) {
                        while ($the_query->have_posts()) {
                            $the_query->the_post();
                            ?>
                            <option value="<?php the_id() ?>" <?php selected(@$_GET['filter-lecturer'], get_the_id()) ?>><?php the_title() ?></option>
                            <?php
                        }
                        wp_reset_postdata();
                    }
                    ?>
                </select>

            </div>

        </form>

    </div>


    <?php if (have_posts()) { ?>

        <table class="terminy">
            <tbody>
            <?php while (have_posts()) {
                the_post();

                $termin = get_post();
                $termin_id = $termin->ID;

                // kurz
                $kurz_id = get_field('kurz', $termin_id);
                $kurz = get_post($kurz_id);


                if (!get_field('typ_terminu') || get_field('typ_terminu', get_the_ID()) == "jednoden"):
                    // typ terminu
                    $typ_kurzu_kod = typ_kurzu_kod($kurz);

                    $typ_kurzu_nazev = typ_kurzu_nazev($kurz);

                    $typ_kurzu = '<span class="typ typ-' . $typ_kurzu_kod . '">' . $typ_kurzu_nazev . '</span>';
                    $datum = '<span class="datum">' . termin_datum_konani($termin_id) . '</span>';


                else:
                    $datum = '<span class="datum">od ' .  date('j. n. Y', strtotime(get_field('datum_konani'))).'</span>';

                    $typ_kurzu_nazev = get_field('typ_terminu') == 'jednoden'?'1 denní':'Vícedenní';

                    $typ_kurzu = '<span class="typ typ-2denni">' . $typ_kurzu_nazev . '</span>';

                endif;


                $nazev_kurzu = termin_nazev_kurzu($termin_id);
                $kurz_url = get_permalink($kurz->ID);

                $nazev_kurzu = '<a href="' . $kurz_url . '">' . $nazev_kurzu . '</a>';


                // datum


                // mesto

                $lokalita = termin_lokalita($termin_id);
                $lokalita = '<span><span class="hint--bottom" data-hint="' . esc_attr(termin_lokalita_adresa($termin_id)) . '">' . $lokalita . '</span></span>';

                // lektorka

                $lektorka_id = termin_id_lektorky($termin_id);
                $lektorka_jmeno = termin_lektorka($termin_id);
                $lektorka = '<span><a href="' . get_permalink($lektorka_id) . '">' . $lektorka_jmeno . '</a></span>';


                // rezervace

                $rezervace = termin_volna_mista_html($termin_id);

                if (termin_pocet_volnych_mist($termin_id) > 0) {
                    $rezervace .= '<a href="' . get_permalink() . '" class="tlacitko ruzove nizsi">' . __('REZERVOVAT', 'jz') . '</a>';
                }


                ?>

                <tr class="row">


                    <td class="termin-typ">
                        <?php 
                            echo $typ_kurzu;
                            if (get_field('akcni_cena_par_czk', get_the_ID()) || get_field('akcni_cena_par_eur', get_the_ID() ) || get_field('akcni_cena_jednotlivec_czk', get_the_ID()) || get_field('akcni_cena_jednotlivec_eur', get_the_ID())) { ?>
                                <span class="typ typ-sleva"><?= __('Sleva', 'jz') ?></span>
                            <?php }
                        ?>
                    </td>
                    <td class="termin-nazev-kurzu">
                        <?php echo $nazev_kurzu ?>
                        <br>
                        <?php echo $datum ?>
                    </td>
                    <td class="termin-lokalita">
                        <?php echo $lokalita ?>
                    </td>
                    <td class="termin-lektorka">
                        <?php echo $lektorka ?>
                    </td>

                    <td class="termin-rezervace">
                        <?php echo $rezervace ?>
                    </td>

                </tr>

            <?php } ?>
            </tbody>
        </table>

    <?php } ?>


</div>
<?php get_footer(); ?>
