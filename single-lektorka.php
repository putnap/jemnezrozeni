<?php get_header(); ?>

<div class="content lektorka-content lektorka-detail content-padding cf" id="content">

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	
	
<h1 class="main-title nad-zpetnym-odkazem">
	
	<?php the_title() ?>
	
	<?php if( ( $mesto = get_field('mesto') ) ){ ?>
	<span class="main-title-mesto">
		<?php echo $mesto; ?>
	</span>
	<?php } ?>
	
</h1>

<p class="zpet-pod-nadpisem"><a href="<?php echo get_post_type_archive_link( 'lektorka' ); ?>"><?php _e('< Zpět na přehled lektorek','jz') ?></a></p>



<?php 

$motto = get_field('motto');

?>


<div class="lektorka-main cf">

	
	<div class="lektorka-info cf">
		
		
		<div class="lektorka-foto-a-prouzek">
		
			<div class="lektorka-foto">
				<a href="<?php the_post_thumbnail_url('large') ?>">
				<?php the_post_thumbnail( 'thumbnail' ); ?>
				</a>
			</div>
			
			<?php if( $prouzek = get_field('prouzek') ) { ?>
			<div class="lektorka-prouzek">
				<?php echo $prouzek ?>
			</div>
			<?php } ?>
				
		</div>
		
		<div class="lektorka-predstaveni <?php echo ( $motto ? 'ma-motto' : '' ) ?>">
			
			<div class="lektorka-predstaveni-text cf">
			
				<?php if( $motto ) { ?>
				<div class="lektorka-motto-wrapper">
				<div class="lektorka-motto">
					<?php echo $motto ?>
				</div>
				</div>
				<?php } ?>
			
				<div class="lektorka-text-wrapper">
				<?php the_content() ?>
				
				
				<?php if( ( $fb = get_field('facebook_profil') ) ) { ?>
				<p class="fb-link">
					<a href="<?php echo $fb ?>"></a>
				</p>
				<?php } ?>
				
				</div>
				
			</div>
			
		</div>
		
	</div>

	
	<?php if ( ( $email = get_field('email') ) ) { ?>
	<div class="lektorka-kontakt cf">
		
		<span class="kontakt-label">
			<?php _e('Kontakt','jz') ?>
		</span>
		
		<span class="kontakt-email">
			<a href="mailto: <?php echo $email ?>"><?php echo $email ?></a>
		</span>
		<span class="kontakt-phone">
			<a href="tel: <?php echo get_field('telefonni_cislo') ?>"><?php echo get_field('telefonni_cislo') ?></a>
		</span>
	</div>
	<?php } ?>


</div>





<?php get_template_part('template-nejblizsi-terminy') ?>
	




	
<?php endwhile;?>
<?php endif; ?>




</div>

<?php get_footer(); ?>
