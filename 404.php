<?php get_header(); ?>



<div class="content p20p50">

<h1 class="center"><?= __('Chyba 404 - Stránka nenalezena', 'jz') ?></h1>
		
<p class="center"><?= __('Vámi hledaná adresa na tomto webu neexistuje.', 'jz') ?></p>

<p class="center"><a href="/"><?= __('Přejděte raději na úvodní stránku', 'jz') ?></a></p>
	

</div>







<?php get_footer(); ?>
