<div class="kontakt-formular cf">

	<form action="" id="kf" class="form">

		<div class="alert"><?php _e('Vyplňte prosím pole označená hvězdičkou!','jz') ?></div>

		<?php if( is_singular( array('kurz') ) ) { ?>
		<div class="row row-lektorka row-fields">
			<select name="kf-lektorka" id="kf-lektorka" class="form-item" required>
			<option value=""></option>
			<?php
			$lektorky = get_posts( array( 'post_type' => 'lektorka', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC' ) );
			foreach( (array) $lektorky as $lektorka ) {
				?>
				<option value="<?php echo $lektorka->ID ?>"><?php echo $lektorka->post_title ?></option>
				<?php
			}
			?>
			</select>
			<label for="kf-lektorka"><?php _e('Lektorka','jz') ?> <span class="star">*</span></label>
		</div>
		<?php } ?>

		<div class="row row-jmeno row-fields">
			<input type="text" name="kf-jmeno" id="kf-jmeno" required class="form-item">
			<label for="kf-jmeno"><?php _e('Vaše jméno','jz') ?> <span class="star">*</span></label>
		</div>

		<div class="row row-email row-fields">
			<input type="text" name="kf-email" id="kf-email" required class="form-item">
			<label for="kf-email"><?php _e('E-mail','jz') ?> <span class="star">*</span></label>
		</div>

		<div class="row row-telefon row-fields">
			<input type="text" name="kf-telefon" id="kf-telefon" required class="form-item">
			<label for="kf-telefon"><?php _e('Telefon','jz') ?> <span class="star">*</span></label>
		</div>

		<div class="row row-zprava row-fields row-textarea">
			<textarea name="kf-zprava" id="kf-zprava" required class="form-item" cols="30" rows="5"></textarea>
			<label for="kf-zprava"><?php _e('Zpráva','jz') ?> <span class="star">*</span></label>
		</div>

		<?php if( is_singular( array('kurz') ) ) { ?>
		<div class="row row-souhlas">

			<input type="checkbox" name="kf-souhlas" id="kf-souhlas" required>
			<label for="kf-souhlas">
				<?php echo sprintf(
							__('Odesláním tohoto formuláře souhlasím se zpracováním svých osobních údajů a <a href="%s">obchodními podmínkami</a>.','jz'),
							get_permalink(  PAGE_ID_OBCHODNI_PODMINKY  )
						) ?>
				<span class="star">*</span>
			</label>

		</div>
		<?php } ?>

		<div class="row row-info">
			<p class="kf-item-info">
				<?php echo sprintf(__('Pole označená %s jsou povinná','jz'), '<span class="star">*</span>' ) ?>
			</p>
		</div>
        <div class="row">Odesláním formuláře souhlasíte se <a href="/wp-content/uploads/2018/06/souhlas.pdf" target="_blank">zpracováním osobních údajů</a></div>

        <div class="row row-submit">
			<p class="kf-item-submit">
				<input type="submit" value="<?php _e('Odeslat','jz') ?>" class="submit">
			</p>
		</div>

	</form>

</div>