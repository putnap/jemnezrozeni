<?php

function jz_woocommerce_template_actions() {

	remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
	remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

	remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

	function div_main_wrapper_start() {

		$class = is_archive() ? 'shop-archive' : '';

		echo '<div class="content shop-content content-padding ' . $class . '" id="content">';
	}
	add_action('woocommerce_before_main_content', 'div_main_wrapper_start', 1);

	//premistim hlavni nadppis (archiv)
	add_filter('woocommerce_show_page_title', '__return_false');

	//premistim hlavni nadppis (archiv)
	add_filter('woocommerce_before_main_content', 'new_page_title_position', 3);

	function new_page_title_position() {

		if (is_singular('product')) {
			?>
			<h2 class="woocommerce-products-header__title main-title page-title nad-zpetnym-odkazem"><?php _e('Detail produktu', 'jz')?></h2>
			<p class="zpet-pod-nadpisem">
				<a href="<?php echo get_permalink(wc_get_page_id('shop')) ?>"><?php _e('&lt; Zpět na přehled produktů', 'jz')?></a>
			</p>
		<?php
} else {
			?>
			<h1 class="woocommerce-products-header__title main-title page-title"><?php woocommerce_page_title();?></h1>
		<?php
}
	}

	// zrusim drobeckovou navigaci
	remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
	//add_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 2 );

	// presunu razeni a odstraneni pocet vysledku
	remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
	remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

	// razeni a pocet produktu jen na prehledech
	//if( is_archive() )
	//	add_action( 'woocommerce_before_main_content', 'jz_pocet_produktu_a_razeni', 3 );

	// prouzek nad prehledem produktu

	function woocommerce_content_before_products() {
		?>
		<div class="woocommerce-content-before-products cf">
			<?php woocommerce_catalog_ordering()?>
		</div>
		<?php
}
	add_action('woocommerce_archive_description', 'woocommerce_content_before_products', 5);

	function div_main_wrapper_products_start() {echo '<div class="products-wrapper border cf ' . (is_archive() ? 'column-main same-height-2-1200' : '') . '">';}
	add_action('woocommerce_before_main_content', 'div_main_wrapper_products_start', 5);

	function div_main_wrapper_products_end() {echo '</div>';}
	add_action('woocommerce_after_main_content', 'div_main_wrapper_products_end', 5);

	if (!is_singular('product')) {
		// v detailu produktu sidebar nechci

		function woocommerce_sidebar_wrapper_start() {echo '<div class="sidebar-wrapper border ' . (is_archive() ? 'column-sidebar same-height-2-1200' : '') . '">';}
		add_action('woocommerce_after_main_content', 'woocommerce_sidebar_wrapper_start', 9);

		add_action('woocommerce_after_main_content', 'woocommerce_get_sidebar', 10);

		function woocommerce_sidebar_wrapper_end() {echo '</div>';}
		add_action('woocommerce_after_main_content', 'woocommerce_sidebar_wrapper_end', 11);

	}

	function div_main_wrapper_end() {echo '</div>';}
	add_action('woocommerce_after_main_content', 'div_main_wrapper_end', 999);

}
add_action('template_redirect', 'jz_woocommerce_template_actions');

/*
Prehled kategorii - zrusim obrazek
 */
remove_action('woocommerce_before_subcategory_title', 'woocommerce_subcategory_thumbnail', 10);

function pocet_produktu_v_kosiku_v_menu() {
	$pocet = intval(WC()->cart->cart_contents_count);
	echo '<span title="Váš nákupní košík" class="kosik-v-menu pocet' . $pocet . '">';
	echo $pocet;
	echo '</span>';
}

function woocommerce_header_add_to_cart_fragment($fragments) {
	ob_start();
	pocet_produktu_v_kosiku_v_menu();
	$fragments['.kosik-v-menu'] = ob_get_clean();
	return $fragments;
}
add_filter('woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');

/*
88888888ba   88888888ba   88888888888  88        88  88           88888888888  88888888ba,
88      "8b  88      "8b  88           88        88  88           88           88      `"8b
88      ,8P  88      ,8P  88           88        88  88           88           88        `8b
88aaaaaa8P'  88aaaaaa8P'  88aaaaa      88aaaaaaaa88  88           88aaaaa      88         88
88""""""'    88""""88'    88"""""      88""""""""88  88           88"""""      88         88
88           88    `8b    88           88        88  88           88           88         8P
88           88     `8b   88           88        88  88           88           88      .a8P
88           88      `8b  88888888888  88        88  88888888888  88888888888  88888888Y"'
 */

// pocet produktu na radku
function jz_loop_columns() {
	return 3;
}
add_filter('loop_shop_columns', 'jz_loop_columns');

function jz_body_class($classes) {

	if (is_woocommerce()) {
		$classes[] = 'columns-3';
	}

	return $classes;
}
add_filter('body_class', 'jz_body_class');

// unhooknu tlacitko "Add to cart"
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

// KATEGORIE nad fotkou produktu
function woocommerce_before_shop_loop_item_kategorie() {
	global $post;

	$terms = get_the_terms($post->ID, 'product_cat');

	if ($terms && !is_wp_error($terms)) {

		$cats = array();

		foreach ($terms as $term) {
			$cats[] = $term->name;
		}

		$cats = join(', ', $cats);

		echo '<p class="product-cats">' . $cats . '</p>';
	}
}
add_action('woocommerce_before_shop_loop_item', 'woocommerce_before_shop_loop_item_kategorie', 15);

// strankovani
function woocommerce_pagination_args_filter($args) {

	$args['type'] = 'plain';
	$args['prev_text'] = '&laquo;';
	$args['next_text'] = '&raquo;';

	return $args;
}
add_filter('woocommerce_pagination_args', 'woocommerce_pagination_args_filter');

/*888888ba,    88888888888  888888888888         db         88  88
88      `"8b   88                88             d88b        88  88
88        `8b  88                88            d8'`8b       88  88
88         88  88aaaaa           88           d8'  `8b      88  88
88         88  88"""""           88          d8YaaaaY8b     88  88
88         8P  88                88         d8""""""""8b    88  88
88      .a8P   88                88        d8'        `8b   88  88
88888888Y"'    88888888888       88       d8'          `8b  88  888888888*/

// umistim skladovost pod nadpis
function woocommerce_single_product_summary_skladovost() {
	?>
	<div class="skladovost">
		<strong><?= __('Skladem', 'jz') ?></strong>
	</div>
	<?php
}

add_action('woocommerce_single_product_summary', 'woocommerce_single_product_summary_skladovost', 7);

// premistim popis produktu pod nadpis (a skladovost) / a taky zrusim kratky popis, kdyby nahodou ...
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);

function woocommerce_single_product_summary_cely_popis() {
	?>
	<div class="product-description cf">
		<?php echo apply_filters('the_content', get_the_content()); ?>
		<?php share_buttons()?>
	</div>
	<?php
}
add_action('woocommerce_single_product_summary', 'woocommerce_single_product_summary_cely_popis', 20);

// prehrat ukazku (pod popisem)
function woocommerce_single_product_summary_prehrat_ukazku() {
	global $product;

	$mp3_ukazka_url = get_post_meta($product->id, 'mp3_ukazka_url', true);

	if ($product->is_downloadable() && $mp3_ukazka_url) {
		?>
		<div class="prehrat-ukazku cf">
			<audio id="ukazka">
				<source src="<?php echo $mp3_ukazka_url ?>" type="audio/mpeg">
				<?= __('Váš prohlížeč neumí přehrávat audio.', 'jz') ?>
			</audio>

			<a href="#" id="prehrat" class="audio-start" data-target="ukazka"><?php _e('Přehrát ukázku', 'jz')?></a>

		</div>
		<?php
}

}
add_action('woocommerce_single_product_summary', 'woocommerce_single_product_summary_prehrat_ukazku', 22);

// premistim cenu k tlacitku pridat do kosiku (ale u variabilniho produktu to musim resit jinak)

function move_single_price_for_nonvariable_products() {
	global $product;
	if (!$product->is_type('variable')) {

		remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);

		add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 35);

	}
}
add_action('woocommerce_before_single_product', 'move_single_price_for_nonvariable_products', 1);

// premisitim cenu u produktu s variantami
// pokud maji varianty stejne ceny, tak se nemeni cena dle varianty v roletce ... takze neni na strance zadna

function move_single_price_for_variable_products() {
	global $product;

	if ($product->is_type('variable')) {

		// odstranim cenu zpod nadpisu
		remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);

		// a k tlacitku ji premistim POUZE TEHDY, pokud jsou ceny variant stejne!

		if ($product->get_variation_price('min', true) == $product->get_variation_price('max', true)) {

			add_action('woocommerce_before_single_variation', 'woocommerce_template_single_price', 10);

		}

	}
}
add_action('woocommerce_before_single_product', 'move_single_price_for_variable_products', 1);

// zrusim uplne produktove taby
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);

// clearnu DIV
function woocommerce_after_single_product_summary_clearing() {
	?><div class="cf"></div><?php
}
add_action('woocommerce_after_single_product_summary', 'woocommerce_after_single_product_summary_clearing', 99);

// zrusim/presunu "related" za DIV produktu

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
add_action('woocommerce_after_single_product', 'woocommerce_output_related_products', 20);

function jz_related_products_args($args) {
	$args['posts_per_page'] = 4; // 4 related products
	$args['columns'] = 4; // arranged in 4 columns
	return $args;
}
add_filter('woocommerce_output_related_products_args', 'jz_related_products_args');

/*
,ad8888ba,    88888888ba           88  88888888888  88888888ba,    888b      88         db         8b           d8  88      a8P          db
d8"'    `"8b   88      "8b          88  88           88      `"8b   8888b     88        d88b        `8b         d8'  88    ,88'          d88b
d8'        `8b  88      ,8P          88  88           88        `8b  88 `8b    88       d8'`8b        `8b       d8'   88  ,88"           d8'`8b
88          88  88aaaaaa8P'          88  88aaaaa      88         88  88  `8b   88      d8'  `8b        `8b     d8'    88,d88'           d8'  `8b
88          88  88""""""8b,          88  88"""""      88         88  88   `8b  88     d8YaaaaY8b        `8b   d8'     8888"88,         d8YaaaaY8b
Y8,        ,8P  88      `8b          88  88           88         8P  88    `8b 88    d8""""""""8b        `8b d8'      88P   Y8b       d8""""""""8b
Y8a.    .a8P   88      a8P  88,   ,d88  88           88      .a8P   88     `8888   d8'        `8b        `888'       88     "88,    d8'        `8b
`"Y8888Y"'    88888888P"    "Y8888P"   88888888888  88888888Y"'    88      `888  d8'          `8b        `8'        88       Y8b  d8'          `8b
 */

// Nektere staty predradim na zacatek

function jz_woocommerce_countries($countries) {

	if (is_admin()) {
		return $countries;
	}

	$new_countries = array();

	$predradit = array('CZ', 'SK', 'GB', 'US', 'ES', 'IT', 'DE', 'CA', 'PL', 'CH', 'AT');

	foreach ($predradit as $stat) {

		$new_countries[$stat] = $countries[$stat];

		unset($countries[$stat]);
	}

	// staty, co zbyly v puvodnim seznamu, seradim
	asort($countries);

	// a oba seznamy spojim

	$countries = array_merge($new_countries, $countries);

	return $countries;
}
add_filter('woocommerce_countries', 'jz_woocommerce_countries');

// Staty si radim sam v predchozi funkci
function jz_woocommerce_sort_countries() {

	if (is_admin()) {
		return true;
	}

	return false;
}

add_filter('woocommerce_sort_countries', 'jz_woocommerce_sort_countries');

// Zruseni druheho adresniho radku

function jz_custom_override_checkout_fields($fields) {
	unset($fields['billing']['billing_company']);
	unset($fields['billing']['billing_address_2']);
	unset($fields['shipping']['shipping_address_2']);
	return $fields;
}
add_filter('woocommerce_checkout_fields', 'jz_custom_override_checkout_fields');

function popis_u_dopravy($method, $index) {

	$chosen_methods = WC()->session->get('chosen_shipping_methods');
	$chosen_shipping = $chosen_methods[0];

	if ($chosen_shipping != $method->method_id) // popis jen u VYBRANE dopravy
	{
		return false;
	}

	$methods = WC()->shipping->load_shipping_methods();

	if (isset($methods[$method->method_id]) && !empty($methods[$method->method_id]->settings['description'])) {
		echo '<p class="shipping-desc">';
		echo $methods[$method->method_id]->settings['description'];
		echo '</p>';
	}

}
add_action('woocommerce_after_shipping_rate', 'popis_u_dopravy', 10, 2);

function wooc_extra_register_fields() {
	echo 'Registrací souhlasíte se <a href="/wp-content/uploads/2018/06/souhlas.pdf" target="_blank">zpracováním osobních údajů</a>';
}

add_action('woocommerce_register_form_end', 'wooc_extra_register_fields');
add_action('woocommerce_checkout_before_terms_and_conditions', 'wooc_extra_register_fields_cart');

function wooc_extra_register_fields_cart() {
	echo 'Nákupem souhlasíte se <a href="/wp-content/uploads/2018/06/souhlas.pdf" target="_blank">zpracováním osobních údajů</a>';
}

// to ani nevim, co dela ... zrusi to platebni sekci POD objednavkou (protoze ji chci premistit primo do tabulky)
remove_action('woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20);

function platebni_metody_hned_za_dopravu() {

	?>
	<tr>
		<th><?= __('Platba', 'jz') ?></th>
		<td><?php

	woocommerce_checkout_payment();

	?></td>
	</tr>
	<?php

}
//add_action('woocommerce_review_order_after_shipping', 'platebni_metody_hned_za_dopravu');
add_action('woocommerce_review_order_before_order_total', 'platebni_metody_hned_za_dopravu');

function po_shrnuti_objednavky() {

	?>
	<div class="form-row place-order">
		<noscript>
			<?php _e('Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'woocommerce');?>
			<br/><input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="<?php esc_attr_e('Update totals', 'woocommerce');?>" />
		</noscript>

		<?php wc_get_template('checkout/terms.php');?>

		<?php do_action('woocommerce_review_order_before_submit');?>

		<?php $order_button_text = __('Dokončit objednávku', 'jz');?>

		<?php echo apply_filters('woocommerce_order_button_html', '<input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr($order_button_text) . '" data-value="' . esc_attr($order_button_text) . '" />'); ?>

		<?php do_action('woocommerce_review_order_after_submit');?>

		<?php wp_nonce_field('woocommerce-process_checkout');?>
	</div>
	<?php

}
add_action('woocommerce_checkout_after_order_review', 'po_shrnuti_objednavky');

/*
888888888888  88        88         db         888b      88  88      a8P       8b        d8    ,ad8888ba,    88        88
88       88        88        d88b        8888b     88  88    ,88'         Y8,    ,8P    d8"'    `"8b   88        88
88       88        88       d8'`8b       88 `8b    88  88  ,88"            Y8,  ,8P    d8'        `8b  88        88
88       88aaaaaaaa88      d8'  `8b      88  `8b   88  88,d88'              "8aa8"     88          88  88        88
88       88""""""""88     d8YaaaaY8b     88   `8b  88  8888"88,              `88'      88          88  88        88
88       88        88    d8""""""""8b    88    `8b 88  88P   Y8b              88       Y8,        ,8P  88        88
88       88        88   d8'        `8b   88     `8888  88     "88,            88        Y8a.    .a8P   Y8a.    .a8P
88       88        88  d8'          `8b  88      `888  88       Y8b           88         `"Y8888Y"'     `"Y8888Y"'
 */

function jz_thankyou_page_codes($order_id) {

	$order_total = '';

	$order = wc_get_order($order_id);

	if ($order) {
		$order_total = $order->get_total();
	}

	// <!-- Merici kod Sklik.cz -->

	// mailchimp

	$email = $order->get_billing_email();
	$jmeno = $order->get_billing_first_name();
	$prijmeni = $order->get_billing_last_name();

	jz_mailchimp($email, array('FNAME' => $jmeno, 'LNAME' => $prijmeni));

}
add_action('woocommerce_thankyou', 'jz_thankyou_page_codes', 10, 1);

/*
88        88  88      a8P          db         888888888888  88      a8P   8b        d8      88b           d88  88888888ba    ad888888b,
88        88  88    ,88'          d88b                 ,88  88    ,88'     Y8,    ,8P       888b         d888  88      "8b  d8"     "88
88        88  88  ,88"           d8'`8b              ,88"   88  ,88"        Y8,  ,8P        88`8b       d8'88  88      ,8P          a8P
88        88  88,d88'           d8'  `8b           ,88"     88,d88'          "8aa8"         88 `8b     d8' 88  88aaaaaa8P'       aad8"
88        88  8888"88,         d8YaaaaY8b        ,88"       8888"88,          `88'          88  `8b   d8'  88  88""""""'         ""Y8,
88        88  88P   Y8b       d8""""""""8b     ,88"         88P   Y8b          88           88   `8b d8'   88  88                   "8b
Y8a.    .a8P  88     "88,    d8'        `8b   88"           88     "88,        88           88    `888'    88  88           Y8,     a88
`"Y8888Y"'   88       Y8b  d8'          `8b  888888888888  88       Y8b       88           88     `8'     88  88            "Y888888P'
 */

function woocommerce_product_options_general_custom_fields() {
	global $post;

	$mp3_ukazka_url = get_post_meta($post->ID, 'mp3_ukazka_url', true);
	$mp3_ukazka_id = get_post_meta($post->ID, 'mp3_ukazka_id', true);

	// custom pole
	?>
 	<p class="form-field show_if_downloadable custom_field_type">
 		<label for="custom_field_type"><?= __('Krátká ukázka (MP3)', 'jz') ?></label>
 		<span class="wrap">

 			<input type="text" name="mp3_ukazka_url" id="mp3_ukazka_url" value="<?php echo esc_attr($mp3_ukazka_url); ?>">
 			<input type="hidden" name="mp3_ukazka_id" id="mp3_ukazka_id" value="<?php echo $mp3_ukazka_id ?>">
 			<input type="button" id="media-lib-button" value="Zvolit soubor" class="button" />

 		</span>
 		<!--<span class="description"><?php _e('Place your own description here!', 'woocommerce');?></span>-->
 	</p>
 	<script>
 		jQuery(function($){

 			// Set all variables to be used in scope
 			var frame;

 			// ADD IMAGE LINK
 			jQuery("#media-lib-button").on( 'click', function( event ){

 				event.preventDefault();

 				// If the media frame already exists, reopen it.
 				if ( frame ) {
 					frame.open();
 					return;
 				}

 				// Create a new media frame
 				frame = wp.media({
 					title: 'Vyberte / nahrajte soubor',
 					button: { text: 'Vložit do produktu' },
 					multiple: false
 				});


 				// When an image is selected in the media frame...
 				frame.on( 'close', function() {


 					var gallery_ids = [];
 					var my_index = 0;

 					var attachment = frame.state().get('selection').first().toJSON();

 					jQuery('#mp3_ukazka_url').val( attachment.url );
 					jQuery('#mp3_ukazka_id').val( attachment.id );

 					//jQuery("#media-lib-button").prop('disabled', true);

 				});


 				frame.on('open',function() {
 					// On open, get the id from the hidden input
 					// and select the appropiate images in the media manager

 					var selection =  frame.state().get('selection');

 					var id = jQuery('#mp3_ukazka_id').val();

 					attachment = wp.media.attachment(id);
 					attachment.fetch();
 					selection.add( attachment ? [ attachment ] : [] );

 				});


 				// Finally, open the modal on click
 				frame.open();
 			});


 		});
 	</script>
 	<?php

}
add_action('woocommerce_product_options_general_product_data', 'woocommerce_product_options_general_custom_fields');

function woocommerce_product_options_general_custom_fields_saving($post_id) {

	if (isset($_POST['mp3_ukazka_url'])) {
		update_post_meta($post_id, 'mp3_ukazka_url', $_POST['mp3_ukazka_url']);
		update_post_meta($post_id, 'mp3_ukazka_id', $_POST['mp3_ukazka_id']);
	}

}
add_action('woocommerce_process_product_meta', 'woocommerce_product_options_general_custom_fields_saving');

/*
88888888888  88        88  88888888ba         ,ad8888ba,   88888888888  888b      88  8b        d8
88           88        88  88      "8b       d8"'    `"8b  88           8888b     88   Y8,    ,8P
88           88        88  88      ,8P      d8'            88           88 `8b    88    Y8,  ,8P
88aaaaa      88        88  88aaaaaa8P'      88             88aaaaa      88  `8b   88     "8aa8"
88"""""      88        88  88""""88'        88             88"""""      88   `8b  88      `88'
88           88        88  88    `8b        Y8,            88           88    `8b 88       88
88           Y8a.    .a8P  88     `8b        Y8a.    .a8P  88           88     `8888       88
88888888888   `"Y8888Y"'   88      `8b        `"Y8888Y"'   88888888888  88      `888       88
 */

function jz_eshop_doplneni_eur_ceny_simple() {
	woocommerce_wp_text_input(array(
		'id' => '_regular_price_eur',
		'type' => 'number',
		'class' => 'wc_input_price_extra_info short',
		'label' => __('Běžná cena (EUR)', 'woocommerce'),
		'custom_attributes' => array('step' => 'any', 'min' => 0),
	));
}
add_action('woocommerce_product_options_pricing', 'jz_eshop_doplneni_eur_ceny_simple');

function jz_eshop_doplneni_eur_ceny_variable($loop, $variation_data, $variation) {
	woocommerce_wp_text_input(array(
		'id' => '_regular_price_eur[' . $variation->ID . ']',
		'type' => 'number',
		'class' => 'wc_input_price_extra_info short',
		'wrapper_class' => 'form-row form-row-first',
		'label' => __('Běžná cena (EUR)', 'woocommerce'),
		'custom_attributes' => array('step' => 'any', 'min' => 0),
		'value' => get_post_meta($variation->ID, '_regular_price_eur', true),
	));
}

add_action('woocommerce_variation_options_pricing', 'jz_eshop_doplneni_eur_ceny_variable', 10, 3);

function jz_eshop_doplneni_eur_ceny_ulozeni($post_id) {

	if (is_array($_POST['_regular_price_eur'])) {

		foreach ($_POST['_regular_price_eur'] as $variable_id => $price) {

			update_post_meta($variable_id, '_regular_price_eur', $price);

		}

	} else if (isset($_POST['_regular_price_eur'])) {

		update_post_meta($post_id, '_regular_price_eur', floatval($_POST['_regular_price_eur']));

	}

}
add_action('woocommerce_process_product_meta', 'jz_eshop_doplneni_eur_ceny_ulozeni', 2);

/*
FRONTEND
 */

function jz_eshop_correct_price($price, $product_or_variation) {

	// $price je bezna cena

	if (!is_admin()) {

		$mena = jz_aktualni_mena_kod();

		if ($mena == 'eur') {
			$price = get_post_meta($product_or_variation->get_id(), '_regular_price_eur', true);
		}

		//echo 'price: '. $product_or_variation->get_id().'<br>';
		//wc_delete_product_transients( $product_or_variation->get_id() );
	}

	return $price;
}
// Simple, grouped and external products
add_filter('woocommerce_product_get_price', 'jz_eshop_correct_price', 99, 2);
add_filter('woocommerce_product_get_regular_price', 'jz_eshop_correct_price', 99, 2);

// Products variations
add_filter('woocommerce_variation_prices_price', 'jz_eshop_correct_price', 99, 2);
add_filter('woocommerce_variation_prices_regular_price', 'jz_eshop_correct_price', 99, 2);

add_filter('woocommerce_product_variation_get_regular_price', 'jz_eshop_correct_price', 99, 2);
add_filter('woocommerce_product_variation_get_price', 'jz_eshop_correct_price', 99, 2);

/*
Prebiju defaultni WOO menu, je-li ulozena jina (v COOKIE).
Pak se i spravne ulozi k objednavce ...
 */
function jz_woocommerce_currency($currency) {

	if (!is_admin()) {
		return strtoupper(jz_aktualni_mena_kod()); // CZK, EUR ... velka pismena!
	}

	return $currency;
}
add_filter('woocommerce_currency', 'jz_woocommerce_currency', 99, 1);

/*
Eshop ma nastaveno zaokrouhlovani na zadne desetinne misto.
Na Frontendu to pro EUR menu zmenim na 2.
 */

function jz_eshop_rounding_num_decimals($value) {

	if (!is_admin() && jz_aktualni_mena_kod() == 'eur') {
		$value = 2;
	}

	return $value;
}
add_filter('pre_option_woocommerce_price_num_decimals', 'jz_eshop_rounding_num_decimals');

// spravny pocet des. mist v adminu (jenom?)
function jz_wc_price_args($args) {
	if ($args['currency'] == 'EUR') {
		$args['decimals'] = 2;
	}
	return $args;
}
add_filter('wc_price_args', 'jz_wc_price_args');

// nulova cena? co kdyz nema produkt nastavenou EUR cenu nebo ma proste cenu nula? potrebuji skryt vlozeni do kosiku
function jz_eshop_remove_add_to_cart_on_0($purchasable, $product) {

	if ($product->get_price() == 0) {
		$purchasable = false;
	}

	return $purchasable;
}
add_filter('woocommerce_is_purchasable', 'jz_eshop_remove_add_to_cart_on_0', 10, 2);

/*
88b           d88  88        88          88      88        88    ,ad8888ba,            88           88888888888  88      a8P   888888888888    ,ad8888ba,    88888888ba   88      a8P          db
888b         d888  88        88          88      88        88   d8"'    `"8b           88           88           88    ,88'         88        d8"'    `"8b   88      "8b  88    ,88'          d88b
88`8b       d8'88  88        88          88      88        88  d8'                     88           88           88  ,88"           88       d8'        `8b  88      ,8P  88  ,88"           d8'`8b
88 `8b     d8' 88  88        88          88      88        88  88                      88           88aaaaa      88,d88'            88       88          88  88aaaaaa8P'  88,d88'           d8'  `8b
88  `8b   d8'  88  88        88          88      88        88  88                      88           88"""""      8888"88,           88       88          88  88""""88'    8888"88,         d8YaaaaY8b
88   `8b d8'   88  88        88          88      88        88  Y8,                     88           88           88P   Y8b          88       Y8,        ,8P  88    `8b    88P   Y8b       d8""""""""8b
88    `888'    88  Y8a.    .a8P  88,   ,d88      Y8a.    .a8P   Y8a.    .a8P  888      88           88           88     "88,        88        Y8a.    .a8P   88     `8b   88     "88,    d8'        `8b
88     `8'     88   `"Y8888Y"'    "Y8888P"        `"Y8888Y"'     `"Y8888Y"'   888      88888888888  88888888888  88       Y8b       88         `"Y8888Y"'    88      `8b  88       Y8b  d8'          `8b
 */

function jz_woocommerce_account_menu_items($items) {

	if (prihlasena_lektorka()) {

		$items = array(
			'dashboard' => __('Úvodní informace', 'woocommerce'),
			'terminy-lektorky' => __('Mé termíny', 'jz'),
			'prehled-lokalit' => __('Přehled lokalit', 'jz'),
			'pridat-termin' => __('Přidat termín', 'jz'),
			'pridat-lokalitu' => __('Přidat lokalitu', 'jz'),
			'edit-account' => __('Account details', 'woocommerce'),
			'customer-logout' => __('Logout', 'woocommerce'),
		);

	} else {

		// takze zakaznik

		$new_items = array();

		foreach ($items as $item => $label) {

			if ($item == 'dashboard') {
				$label = __('Úvodní informace', 'jz');
			}

			if ($item == 'orders') {
				$label = __('Objednávky v e-shopu', 'jz');
			}

			if ($item == 'downloads') {
				$label = __('Produkty ke stažení', 'jz');
			}

			$new_items[$item] = $label;

			if ($item == 'dashboard') {
				$new_items['informace-ke-kurzum'] = __('Informace a nahrávky ke kurzům', 'jz');
			}

		}

		$items = $new_items;

	}

	return $items;
}
add_filter('woocommerce_account_menu_items', 'jz_woocommerce_account_menu_items');

function jz_add_my_account_endpoint() {
	add_rewrite_endpoint('terminy-lektorky', EP_PAGES);
	add_rewrite_endpoint('prehled-lokalit', EP_PAGES);
	add_rewrite_endpoint('pridat-termin', EP_PAGES);
	add_rewrite_endpoint('pridat-lokalitu', EP_PAGES);
	add_rewrite_endpoint('informace-ke-kurzum', EP_PAGES);
}
add_action('init', 'jz_add_my_account_endpoint');

function jz_terminy_lektorky_endpoint_content() {

	if (!prihlasena_lektorka()) {
		return;
	}

	$lektorka_post_id = prihlasena_lektorka_post_id();

	if (!$lektorka_post_id) {
		?>
		<p><strong style="color:red"><?= __('Váš uživatelský účet není propojen s profilem žádné lektorky, nelze dohledat termíny kurzů. Kontaktujte administrátorku.', 'jz') ?></strong></p>
		<?php
		return;
	}

	$detail_terminu = get_query_var('terminy-lektorky');

	if ($detail_terminu) {

		
		if(isset($_GET['edit'])){ 

			// je to jeji termin?

			$termin_lektorka_post_id = get_post_meta( get_query_var('terminy-lektorky'), 'lektorka', true );

			if( $termin_lektorka_post_id != $lektorka_post_id ) {
				?>
				<p><strong style="color:red">Toto není váš termín!</strong></p>
				<a class="acf-button button button-primary button-large" href="<?php echo trailingslashit( wc_get_endpoint_url( 'terminy-lektorky', '', wc_get_page_permalink( 'myaccount' ) ) ) ?>">Zpět</a>
				<?php
			} else { ?>

				<h2>Upravit termín</h2>

				<?php acf_form_head();

				setlocale( LC_ALL, 'cs_CZ' );

				acf_form(array(
					'post_id'		=> get_query_var('terminy-lektorky'),
					'fields' => array(
						'kurz', 'typ_terminu', 'datum_konani', 'cas_konani','cas_konani_do', 'dalsi_dny', 'specifikace_terminu', 'specifikace_temrinu', 'kapacita', 'lokalita', 'cena_par_czk', 'cena_par_eur', 'cena_jednotlivec_czk', 'cena_jednotlivec_eur', 'akcni_cena_par_czk', 'akcni_cena_par_eur', 'akcni_cena_par_poznamka', 'akcni_cena_jednotlivec_czk', 'akcni_cena_jednotlivec_eur', 'akcni_cena_jednotlivec_poznamka', 'letak_ke_stazeni'
					),
					'new_post'		=> array(
						'post_type'		=> 'termin',
						'post_status'		=> 'publish'
					),
					'uploader' => 'basic',
					'updated_message' => "Termín byl upraven",
					'submit_value'		=> 'Uprav termín'
				)); ?>
				<a class="acf-button button button-primary button-large" href="<?php echo trailingslashit( wc_get_endpoint_url( 'terminy-lektorky', '', wc_get_page_permalink( 'myaccount' ) ) ) ?>">Zpět</a>
				<?php
			}
		 } else {
			my_account_terminy_lektorky_detail($detail_terminu);
		}

	} else {

		my_account_terminy_lektorky_tabulka($lektorka_post_id);

	}

}

add_action('woocommerce_account_pridat-lokalitu_endpoint', 'jz_pridat_lokalitu_endpoint_content');

function jz_prehled_lokalit_endpoint_content(){
	if (!prihlasena_lektorka()) {
		return;
	} 

	$lektorka_post_id = prihlasena_lektorka_post_id();

	if (!$lektorka_post_id) {
		?>
		<p><strong style="color:red">Váš uživatelský účet není propojen s profilem žádné lektorky, nelze dohledat termíny kurzů. Kontaktujte administrátorku.</strong></p>
		<?php
		return;
	} ?>

	<h2>Přehled lokalit</h2>

	<?php 

	$the_query = new WP_Query(array(
		'post_type' => 'lokalita',
		'posts_per_page' => -1,
		'post_stastus' => 'publish',
		'orderby' => 'title',
		'order' => 'asc'
	));
	if( $the_query->have_posts() ) {
		?>
		<div id="info_popup" class="white-popup mfp-hide">
		  <h2>Poznámka u lokality</h2>
		  <p class="loc_info"></p>  
		</div>
		<table class="shop_table">
		<tr>
			<th>Název</th>
			<th>Město</th>
			<th>Adresa</th>
			<th>Zadáno lektorkou</th>
			<th>Informace</th>
			<th>Upravit</th>
		</tr>
		<?php
		while( $the_query->have_posts() ) {
			$the_query->the_post();

			$lokalita_id = get_the_id();
			?>

			<tr>
				<td><?= get_the_title(); ?></td>
				<td><?= get_field('adresa_mesto') ?></td>
				<td><?= get_field('adresa_ulice_a_cp') . ', '.  get_field('adresa_psc') ?></td>
				<td>
					<?php print_r(get_the_author()); ?>
				</td>
				<td>
					<?php if(get_field('informace')): ?>
						<small>
							
							<div class="info_ico">
								<a href="#info_popup" class="open-popup-loc">i</a>
								<div style="display: none;" class="info_holder">
									<?= get_field('informace') ?>
								</div>
							</div>
						</small>
					<?php endif; ?>
				</td>
				<td>
						<?php
							if(get_the_author_meta('id') == get_current_user_id()){ ?>
								<a href="<?php echo trailingslashit( wc_get_endpoint_url( 'pridat-lokalitu', $lokalita_id, wc_get_page_permalink( 'myaccount' )) ). '?edit' ?>">Upravit</a>
							<?php }
						?>
				</td>
			</tr>

			<?php
		}
		wp_reset_postdata();
		?>
		</table>

		<?php
	}
}

add_action('woocommerce_account_prehled-lokalit_endpoint', 'jz_prehled_lokalit_endpoint_content');

function jz_pridat_lokalitu_endpoint_content() {
	if (!prihlasena_lektorka()) {
		return;
	} 

	$lektorka_post_id = prihlasena_lektorka_post_id();

	if (!$lektorka_post_id) {
		?>
		<p><strong style="color:red">Váš uživatelský účet není propojen s profilem žádné lektorky, nelze dohledat termíny kurzů. Kontaktujte administrátorku.</strong></p>
		<?php
		return;
	}

	if(isset($_GET['edit'])){ 
		
		// je to jeji termin?
	
		$pid = get_query_var('pridat-lokalitu');
		if(get_post_field ('post_author',  $pid) != get_current_user_id()) {
			?>
			<p><strong style="color:red">Toto není váše lokalita!</strong></p>
			<a class="acf-button button button-primary button-large" href="<?php echo trailingslashit( wc_get_endpoint_url( 'prehled-lokalit', '', wc_get_page_permalink( 'myaccount' ) ) ) ?>">Zpět</a>
			<?php
		} else { ?>
		
	
			<h2>Upravit lokalitu</h2>
			
			<?php acf_form_head();
			
			setlocale( LC_ALL, 'cs_CZ' );
			
			acf_form(array(
				'post_id'		=> $pid,
				'post_title' => true,
				'post_content' => true,
				'updated_message' => "Lokalita byla upravena",
				'submit_value'		=> 'Uprav lokalitu',
				'html_after_fields' => '<div><input type="hidden" name="cpt" value="lokalita"><input type="hidden" name="post_id" value="'.$pid.'"></div>'
			)); ?>

			<a class="acf-button button button-primary button-large" href="<?php echo trailingslashit( wc_get_endpoint_url( 'prehled-lokalit', '', wc_get_page_permalink( 'myaccount' ) ) ) ?>">Zpět</a>
			<?php
		}
	} else { ?>
		<h2>Přidat lokalitu</h2>

		<?php acf_form_head();

		acf_form(array(
			'post_id'		=> 'new_post',
			'post_title' => true,
			'post_content' => true,
			'new_post'		=> array(
				'post_type'		=> 'lokalita',
				'post_status'		=> 'publish'
			),
			'submit_value'		=> 'Vytvoř lokalitu',
			'updated_message' => "Lokalita byla vytvořena",
			'html_after_fields' => '<div><input type="hidden" name="cpt" value="lokalita"></div>'

		)); 
	}
}

function my_acf_prepare_field( $field ) {

    $field['label'] = "Název";
    return $field;  

}
add_filter('acf/prepare_field/name=_post_title', 'my_acf_prepare_field');

function my_acf_content( $field ) {

    $field['label'] = "Informace na veřejné stránce";
    return $field;  
    
}
add_filter('acf/prepare_field/name=_post_content', 'my_acf_content');

add_action('woocommerce_account_pridat-termin_endpoint', 'jz_pridat_termin_endpoint_content');

function jz_pridat_termin_endpoint_content() {

	if (!prihlasena_lektorka()) {
		return;
	}

	$lektorka_post_id = prihlasena_lektorka_post_id();

	if (!$lektorka_post_id) {
		?>
		<p><strong style="color:red">Váš uživatelský účet není propojen s profilem žádné lektorky, nelze dohledat termíny kurzů. Kontaktujte administrátorku.</strong></p>
		<?php
		return;
	} ?>

	<h2>Přidat termín</h2>



	<?php 

	if (wp_get_current_user()->bank_account) {

		acf_form_head();

		setlocale( LC_ALL, 'cs_CZ' );

		acf_form(array(
			'post_id'		=> 'new_post',
			'fields' => array(
				'kurz', 'typ_terminu', 'datum_konani', 'cas_konani','cas_konani_do', 'dalsi_dny', 'specifikace_terminu', 'specifikace_temrinu', 'kapacita', 'lokalita', 'cena_par_czk', 'cena_par_eur', 'cena_jednotlivec_czk', 'cena_jednotlivec_eur', 'akcni_cena_par_czk', 'akcni_cena_par_eur', 'akcni_cena_par_poznamka', 'akcni_cena_jednotlivec_czk', 'akcni_cena_jednotlivec_eur', 'akcni_cena_jednotlivec_poznamka', 'letak_ke_stazeni'
			),
			'new_post'		=> array(
				'post_type'		=> 'termin',
				'post_status'		=> 'publish'
			),
			'uploader' => 'basic',
			'updated_message' => "Termín byl vytvořen",
			'submit_value'		=> 'Vytvoř termín'
		)); 
	} else { ?>
		<p>Přidání termínu je možné jen tehdy, pokud je nastaveno <a href="<?= site_url('/muj-ucet/edit-account/')?>">číslo účtu v profilu.</a></p>
	<?php }
	


}


add_action( 'woocommerce_save_account_details_errors','wooc_validate_custom_field', 10, 1 );

// with something like:

function wooc_validate_custom_field( $args )
{
    
	if($_POST['fapi_login'] && $_POST['fapi_apiKey']){
   		if( ! class_exists('FAPIClient') ) {
			require_once __DIR__.'/FAPIClient/FAPIClient.php';
		}

		$fapi = new FAPIClient($_POST['fapi_login'], $_POST['fapi_apiKey']);
		try {
			$fapi->checkConnection();
		} catch (Exception $e) { 

			$args->add( 'error', __( 'FAPI údaje jsou neplatné, zadejte je prosím znovu.', 'woocommerce' ),'');
		}
   	}
    
}


//priradit id lektorky do terminu
function my_acf_save_post( $post_id ) {
    
    // bail early if no ACF data
    if( empty($_POST['acf']) ) {
        
        return;
        
    } 
    if(!is_admin()){
    	$_POST['acf']['field_59bfa6f578d6f'] = prihlasena_lektorka_post_id();
    	$_POST['acf']['field_5bd3155acc60c'] = wp_get_current_user()->bank_account;
    }
    
}

add_action('acf/save_post', 'my_acf_save_post', 1);

//přidání FIO api klíče  do profilu lektorky
add_action( 'woocommerce_edit_account_form', 'add_fioApiKey', 10);
function add_fioApiKey() {
	$user = wp_get_current_user();
	if (prihlasena_lektorka() && $user->fio_allow) {
	    
	    ?>
	    	<h3>FIO napojení</h3>
	        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
	        <label for="fio_apiKey">FIO API klíč
	        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="fio_apiKey" id="fio_apiKey" value="<?php echo esc_attr( $user->fio_apiKey ); ?>" />
	    </p>
	    <?php
    }
}

//přidání FAPI api klíče  do profilu lektorky
add_action( 'woocommerce_edit_account_form', 'add_fapiAccess',11 );
function add_fapiAccess() {
	$user = wp_get_current_user();
	if (prihlasena_lektorka() && $user->fapi_allow) {
	    ?>
	    	<h3>FAPI údaje</h3>
	    	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		        <label for="fapi_login">Fapi uživatelské jméno
		        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="fapi_login" id="fapi_login" value="<?php echo esc_attr( $user->fapi_login ); ?>" />
	    	</p>
	        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		        <label for="fapi_apiKey">Fapi API klíč
		        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="fapi_apiKey" id="fapi_apiKey" value="<?php echo esc_attr( $user->fapi_apiKey ); ?>" />
	    	</p>
	    <?php
    }
}

//přidání iDoklad api klíče  do profilu lektorky
add_action( 'woocommerce_edit_account_form', 'add_iDokladAccess',11 );
function add_iDokladAccess() {
	$user = wp_get_current_user();
	if (prihlasena_lektorka() && $user->iDoklad_allow) {
	    ?>
	    	<h3>iDoklad údaje</h3>
	    	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		        <label for="iDoklad_login">iDoklad uživatelské jméno
		        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="iDoklad_login" id="iDoklad_login" value="<?php echo esc_attr( $user->iDoklad_login ); ?>" />
	    	</p>
	        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		        <label for="iDoklad_apiKey">iDoklad API klíč
		        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="iDoklad_apiKey" id="iDoklad_apiKey" value="<?php echo esc_attr( $user->iDoklad_apiKey ); ?>" />
	    	</p>
	    <?php
    }
}

//přidání SF api klíče  do profilu lektorky
add_action( 'woocommerce_edit_account_form', 'add_sfAccess',11 );
function add_sfAccess() {
	$user = wp_get_current_user();
	if (prihlasena_lektorka() && $user->sf_allow) {
	    ?>
	    	<h3>Superfaktura údaje</h3>
	    	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		        <label for="sf_login">Superfaktura uživatelské jméno
		        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="sf_login" id="sf_login" value="<?php echo esc_attr( $user->sf_login ); ?>" />
	    	</p>
	        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		        <label for="sf_apiKey">Superfaktura API klíč
		        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="sf_apiKey" id="sf_apiKey" value="<?php echo esc_attr( $user->sf_apiKey ); ?>" />
			</p>
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		        <label for="sf_company">Superfaktura company_id
		        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="sf_company" id="sf_company" value="<?php echo esc_attr( $user->sf_company ); ?>" />
	    	</p>
	    <?php
    }
}

add_action( 'woocommerce_edit_account_form', 'add_vatField', 10);
function add_vatField() {
	$user = wp_get_current_user();
	if (prihlasena_lektorka() && ($user->fapi_allow || $user->sf_allow ||  $user->iDoklad_allow)) {
	    
	    if ($user->lektorka_vat) {
	    	$checked = 'checked="checked"';
	    }
	    ?>
	    	<h3>Plátce DPH</h3>
	    	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		        <label class="container">Pokud zvolíte tuto možnost, bude se při generování faktur připočítávat DPH.
				  <input type="checkbox" name="lektorka_vat" id="lektorka_vat" <?= $checked ?> >
				  <span class="checkmark"></span>
				</label>
	    	</p>
	    <?php
    }
}


// Save the custom field 'bank_account' 
add_action( 'woocommerce_save_account_details', 'save_fioApiKey', 12, 1 );
function save_fioApiKey( $user_id ) {
    
    if( prihlasena_lektorka() && isset( $_POST['fio_apiKey'] ) )
        update_user_meta( $user_id, 'fio_apiKey', sanitize_text_field( $_POST['fio_apiKey'] ) );
}

add_action( 'woocommerce_save_account_details', 'save_vat', 12, 1 );
function save_vat( $user_id ) {

    if( prihlasena_lektorka() && isset( $_POST['lektorka_vat'] ) ){
        update_user_meta( $user_id, 'lektorka_vat', true );
    } elseif(prihlasena_lektorka() && !isset( $_POST['lektorka_vat'] )) {
    	update_user_meta( $user_id, 'lektorka_vat', false );
    }
}

add_action( 'woocommerce_save_account_details', 'save_fapiAccess', 12, 1 );
function save_fapiAccess( $user_id ) {
    
    if( prihlasena_lektorka() && isset( $_POST['fapi_login']) && isset( $_POST['fapi_apiKey']) ){
    	update_user_meta( $user_id, 'fapi_login', sanitize_text_field( $_POST['fapi_login'] ) );
        update_user_meta( $user_id, 'fapi_apiKey', sanitize_text_field( $_POST['fapi_apiKey']) );
    }
   
}

add_action( 'woocommerce_save_account_details', 'save_iDokladAccess', 12, 1 );
function save_iDokladAccess( $user_id ) {
    
    if( prihlasena_lektorka() && isset( $_POST['iDoklad_login']) && isset( $_POST['iDoklad_login']) ){
    	update_user_meta( $user_id, 'iDoklad_login', sanitize_text_field( $_POST['iDoklad_login'] ) );
        update_user_meta( $user_id, 'iDoklad_apiKey', sanitize_text_field( $_POST['iDoklad_apiKey']) );
    }
   
}

add_action( 'woocommerce_save_account_details', 'save_sfAccess', 12, 1 );
function save_sfAccess( $user_id ) {

    if( prihlasena_lektorka() && isset( $_POST['sf_login']) && isset( $_POST['sf_apiKey']) && isset($_POST['sf_company'])){
    	update_user_meta( $user_id, 'sf_login', sanitize_text_field( $_POST['sf_login'] ) );
        update_user_meta( $user_id, 'sf_apiKey', sanitize_text_field( $_POST['sf_apiKey']) );
        update_user_meta( $user_id, 'sf_company', sanitize_text_field( $_POST['sf_company']) );
    }
   
}


//přidání čisla účtu do profilu lektorky
add_action( 'woocommerce_edit_account_form', 'add_bank_account_to_edit_account_form', 9 );
function add_bank_account_to_edit_account_form() {
	if (prihlasena_lektorka()) {
	    $user = wp_get_current_user();
	    ?>
	        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
	        <label for="bank_account">Číslo účtu pro zasílání plateb
	        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="bank_account" id="bank_account" value="<?php echo esc_attr( $user->bank_account ); ?>" />
	    </p>
	    <?php
    }
}

// Save the custom field 'bank_account' 
add_action( 'woocommerce_save_account_details', 'save_bank_account_account_details', 12, 1 );
function save_bank_account_account_details( $user_id ) {
    // For Favorite color
    if( prihlasena_lektorka() && isset( $_POST['bank_account'] ) )
        update_user_meta( $user_id, 'bank_account', sanitize_text_field( $_POST['bank_account'] ) );
}

//přidání telefonu do profilu lektorky
add_action( 'woocommerce_edit_account_form', 'phone_to_edit_account_form' , 9);
function phone_to_edit_account_form() {
	if (prihlasena_lektorka()) {
	    $user = wp_get_current_user();
	    ?>
	        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
	        <label for="lektorka_phone">Telefonní číslo
	        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="lektorka_phone" id="lektorka_phone" value="<?= get_field('telefonni_cislo', prihlasena_lektorka_post_id()); ?>" />
	    </p>
	    <?php
    }
}

// Save the custom field 'phone' 
add_action( 'woocommerce_save_account_details', 'save_phone_account_details', 12, 1 );
function save_phone_account_details( $user_id ) {
    // For Favorite color
    if( prihlasena_lektorka() && isset( $_POST['lektorka_phone'] ) )
    	update_field('telefonni_cislo', $_POST['lektorka_phone'], prihlasena_lektorka_post_id());
}


// fce pro valdiaci přidání lokality - mapa
add_filter('acf/validate_value/name=poloha_na_mape', 'my_validated_map_filter', 11, 4);

function my_validated_map_filter($valid, $value, $field, $input) {
  // field key of the field you want to validate against
    $query = new WP_Query( array(
        'post_type' => 'lokalita',
        'posts_per_page' => -1,
        'post_status' =>'publish',
        'post__not_in'      => array($_POST['post_id']?$_POST['post_id']:$_POST['ID']),
   	));

   	if(!$value['address'] || !$value['lat'] || !$value['lng']){
		$valid = 'Adresa není vyplněna, nebo není potvrzena klávesou enter. Musí být zobrazen bod na mapě.';
		return $valid;
	}

   	while ($query->have_posts()) {
   	    $query->the_post();

   	    if(get_field('poloha_na_mape')['address'] == $value['address']){
   	    	$valid = 'Lokalita na dané adrese již existuje';
   	    	return $valid;
   	    }
   	}
    wp_reset_query();
    return $valid;
}

// fce pro valdiaci přidání lokality
add_filter('acf/validate_value/name=adresa_nazev', 'my_validated_name_filter', 10, 4);

function my_validated_name_filter($valid, $value, $field, $input) {
  // field key of the field you want to validate against

  	$posts = get_posts(array(
		'numberposts'	=> -1,
		'post_type'		=> 'lokalita',
		'meta_key'		=> 'adresa_nazev',
		'meta_value'	=> $value,
        'post__not_in'      => array($_POST['post_id']?$_POST['post_id']:$_POST['ID']),
	));
	if(!$value){
		$valid = 'Název místa je povinný.';
		return $valid;
	}
	if (count($posts)) {
	 	$valid = 'Lokalita již existuje.';
	 	return $valid;
	}
  return $valid;
}

// fce pro valdiaci přidání termínu - kontrla obsazenosti
add_filter('acf/validate_value/name=cas_konani', 'my_validated_check_filter', 10, 4);
//add_filter('acf/validate_value/name=dalsi_dny', 'my_validated_check_filter', 11, 4);

function my_validated_check_filter($valid, $value, $field, $input) {
  // field key of the field you want to validate against

	if(!$_POST['acf']['field_59bfa71278d70'] || !$_POST['acf']['field_5bc6f60f69268']){
		return $valid;
	}
	$pid = 0;
	$query = new WP_Query( array(
        'post_type' => 'termin',
        'posts_per_page' => -1,
        'post_status' =>'publish',
   	));

	global $post;
   	
   	while ($query->have_posts()) {
   	    $query->the_post();
   	    
   	    if (get_field('lokalita')->ID == $_POST['acf']['field_59bfb10f9be2c'] && (get_the_ID() != $_POST['ID'] && (get_the_ID() != $_POST['pll_post_id'] && get_the_ID() != $_POST['post_id'] && get_the_ID() != $_POST['_acf_post_id'] )) && get_field('lokalita')->ID != 941 && get_field('lokalita')->ID != 12958) { //941 a 12958 - lokalita, kde nechci ověřovat dostupnost, dle požadavku klientky
	   	    $dny = array();
	   	    $dny[get_field('datum_konani')] = [get_field('cas_konani'), get_field('cas_konani_do')];
			foreach (get_field('dalsi_dny') as $dalsi) {
				$dny[$dalsi['datum']] = [$dalsi['cas'],$dalsi['cas_do']];
			}
			//print_r($_POST);

			foreach ($dny as $day => $time) {
				if ($day == $_POST['acf']['field_59bfa6d278d6e']) {
					$from = DateTime::createFromFormat('H:i', $time[0]);
					$to = DateTime::createFromFormat('H:i', $time[1]);
					$termin_from = DateTime::createFromFormat('H:i:s', $_POST['acf']['field_59bfa71278d70']);
					$termin_to = DateTime::createFromFormat('H:i:s', $_POST['acf']['field_5bc6f60f69268']);
/*					if ($termin_from >= $from && $termin_from <= $to) {
						$valid = 'Lokalita je již v tomto termínu obsazena termin_from>=from1';
					}


					if ($termin_to >= $from && $termin_to <= $to) {
						$valid = 'Lokalita je již v tomto termínu obsazena termin_to >= from2' . get_the_ID();
					}*/

					/*print_r([ 
						'post_id'=>get_the_ID(),
						'from' =>$from,
						'to' =>$to,
						'termin_from' => $termin_from,
						'termin_to' => $termin_to,
					]);*/

					

					if(($termin_from >= $from && $termin_from <= $to) || ($termin_to >= $from && $termin_to <= $to)){
						$valid = 'Lokalita je již v tomto termínu nebo ve dnech uvedených pro další dny obsazena.';
						$query->wp_reset_query();
						wp_reset_postdata();
						return $valid;
					}
				}
			}

		}
   	}
    $query->wp_reset_query();
    wp_reset_postdata();
    return $valid;
}



// hide drafts
function relationship_options_filter($options, $field, $the_post) {

	$options['post_status'] = array('publish');
	return $options;
}

add_filter('acf/fields/post_object/query/name=kurz', 'relationship_options_filter', 10, 3);

add_action('woocommerce_account_terminy-lektorky_endpoint', 'jz_terminy_lektorky_endpoint_content');

/*
88b           d88  88        88          88      88        88    ,ad8888ba,            888888888888         db         88      a8P          db         888888888888  888b      88  88  88      a8P
888b         d888  88        88          88      88        88   d8"'    `"8b                    ,88        d88b        88    ,88'          d88b                 ,88  8888b     88  88  88    ,88'
88`8b       d8'88  88        88          88      88        88  d8'                            ,88"        d8'`8b       88  ,88"           d8'`8b              ,88"   88 `8b    88  88  88  ,88"
88 `8b     d8' 88  88        88          88      88        88  88                           ,88"         d8'  `8b      88,d88'           d8'  `8b           ,88"     88  `8b   88  88  88,d88'
88  `8b   d8'  88  88        88          88      88        88  88                         ,88"          d8YaaaaY8b     8888"88,         d8YaaaaY8b        ,88"       88   `8b  88  88  8888"88,
88   `8b d8'   88  88        88          88      88        88  Y8,                      ,88"           d8""""""""8b    88P   Y8b       d8""""""""8b     ,88"         88    `8b 88  88  88P   Y8b
88    `888'    88  Y8a.    .a8P  88,   ,d88      Y8a.    .a8P   Y8a.    .a8P  888      88"            d8'        `8b   88     "88,    d8'        `8b   88"           88     `8888  88  88     "88,
88     `8'     88   `"Y8888Y"'    "Y8888P"        `"Y8888Y"'     `"Y8888Y"'   888      888888888888  d8'          `8b  88       Y8b  d8'          `8b  888888888888  88      `888  88  88       Y8b
 */

function jz_terminy_informace_ke_kurzum_endpoint_content() {

	my_account_informace_ke_kurzum();

}
add_action('woocommerce_account_informace-ke-kurzum_endpoint', 'jz_terminy_informace_ke_kurzum_endpoint_content');

function jz_woocommerce_before_account_downloads($has_downloads) {
	?>
	<h2 class="woocommerce-order-downloads__title"><?php _e('Zakoupené produkty ke stažení', 'jz');?></h2>
	<?php

}
add_action('woocommerce_before_account_downloads', 'jz_woocommerce_before_account_downloads');

function jz_woocommerce_email_styles($css) {

	$css .= '

	#template_container { width: 800px; background-image:url(' . get_template_directory_uri() . '/img/firemni-papir.jpg);background-repeat:no-repeat;background-position:left bottom;background-size:contain }

	#template_header { width: 800px; background-color: transparent !important; }

	#template_body { width: 800px; }

	#template_header h1 { text-align:center; }

	#template_header h1,
	#template_header h1 a {
		color: #C94C6D;
	}

	#body_content { background-color: transparent; }

	#body_content table td {
		padding: 0 48px;
	}

	h1 { text-shadow: none; }
	';

	return $css;
}
add_filter('woocommerce_email_styles', 'jz_woocommerce_email_styles');

/*
function jz_woocommerce_email_header( ){
?>
<style type="text/css">

</style>
<?php
}
add_action( 'woocommerce_email_header', 'jz_woocommerce_email_header');
 */

function jz_woocommerce_email_footer_text($text) {

	// za fitrem je autop()

	$text = '<a href="' . get_home_url() . '">' . get_bloginfo('name') . '</a>';
	$text .= ' - <a href="' . get_permalink(PAGE_ID_OBCHODNI_PODMINKY) . '">' . __('Obchodní podmínky', 'jz') . '</a>';
	$text .= ' - <a href="' . get_permalink(PAGE_ID_KONTAKT) . '">' . __('Kontakt', 'jz') . '</a>';

	return $text;
}
add_filter('woocommerce_email_footer_text', 'jz_woocommerce_email_footer_text');
