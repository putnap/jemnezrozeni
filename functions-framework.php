<?php



function style_css() {
	
	$url = get_template_directory_uri().'/style.css';
	
	if( ! strpos($_SERVER['HTTP_HOST'], '.loc') )
		$url .= '?v='.filemtime(  dirname(__FILE__).'/style.css' );
	
	echo $url; 
}






add_action('init', 'fire');
function fire() {
		
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
	
	// remove admin bar css
	add_action('get_header', 'remove_admin_login_header');
	add_action( 'wp_head',  'my_admin_bar_bump_cb')  ;
}


function fire_after_setup_theme() {
	add_theme_support( 'title-tag' );	
}
add_action( 'after_setup_theme', 'fire_after_setup_theme' );




function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}

function my_admin_bar_bump_cb() {
  if ( is_user_logged_in() ) {
    echo "<style type=\"text/css\" media=\"screen\">";
    echo "html { padding-bottom: 32px !important; /*asd*/ }";
    echo "* html body { padding-bottom: 32px !important; }";
    echo "@media screen and ( max-width: 782px ) {";
    echo "html { padding-bottom: 46px !important; }";
    echo "* html body { padding-bottom: 46px !important; }";
    echo "}";
    echo "#wpadminbar { position:fixed !important;  top:auto !important;bottom:0 !important; }"; 
    echo "#wpadminbar .menupop .ab-sub-wrapper, #wpadminbar .shortlink-input { bottom:32px }"; 
    echo "@media  (max-width: 782px) { ";
    echo "	#wpadminbar .menupop .ab-sub-wrapper,#wpadminbar .shortlink-input { bottom:46px }";  
    echo "}"; 
    echo "</style>";
  }
}


function zmena_formatovani_v_editoru( $init ) {
	$init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Preformatted=pre;';
	return $init;
}
add_filter( 'tiny_mce_before_init', 'zmena_formatovani_v_editoru' );






function nastaveni_vychoziho_cile_nahledu_na_medialni_soubor( $settings ) {
	$settings['galleryDefaults']['link'] = 'file';
	$settings['galleryDefaults']['columns'] = '4';
	return $settings;
}
add_filter( 'media_view_settings', 'nastaveni_vychoziho_cile_nahledu_na_medialni_soubor');





function zmena_grafickeho_editoru_na_GD($pole) {
	return array( 'WP_Image_Editor_GD', 'WP_Image_Editor_Imagick' );
}
add_filter( 'wp_image_editors', 'zmena_grafickeho_editoru_na_GD' );
	
	

function odstraneni_gallery_css_z_html( $gallery_code ) {
	return preg_replace('~<style type=\'text/css\'>.+</style>~msiU','',$gallery_code);
}
add_filter( 'gallery_style' , 'odstraneni_gallery_css_z_html' );






function keep_me_logged_in_for_6_months( $expirein ) {
	return YEAR_IN_SECONDS / 2;
}
add_filter( 'auth_cookie_expiration', 'keep_me_logged_in_for_6_months' );






function zachovani_hierarchie_vybranych_kategorii( $args ) {
	$args['checked_ontop'] = false;
	return $args;
}
add_filter( 'wp_terms_checklist_args', 'zachovani_hierarchie_vybranych_kategorii' );








function sklonovani( $cislo, $jedno, $dve, $pet ) {
	$return = '';
	
	if($cislo == 0 || $cislo >= 5){
		$return = $pet;
	}
	else if($cislo >= 2 && $cislo <= 4){
		$return = $dve;
	}
	else{
		$return = $jedno;
	}
	
	if(strpos(' '.$return,'%i')){
		$return = str_replace('%i',$cislo,$return);
		$return = str_replace(' ','&nbsp;',$return);
	}
	else
		$return = $cislo.'&nbsp;'.$return;
	
	return $return;
}















function odkaz_na_acf_field_groups() {
	global $wp_admin_bar,$post,$wpdb;
	
	if( ! class_exists('acf') )
		return;
	
	$wp_admin_bar->add_menu(array('id' => 'linktoacf', 'title' => 'ACF', 'href' => admin_url( 'edit.php?post_type=acf-field-group' ) ));
			
	$fields = get_posts( array('post_type' => 'acf-field-group', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC') );
	
	if( $fields ) {
		
		foreach( $fields as $field ) {
			
			$wp_admin_bar->add_menu(array('id' => 'linktoacfgroup'. $field->ID , 'title' => $field->post_title, 'parent' => 'linktoacf', 
											'href' => get_edit_post_link( $field->ID ) ) );	
			
		}
		
	}

	
}

add_action('admin_bar_menu', 'odkaz_na_acf_field_groups', 2000);


















function is_dev() {
	
	if( preg_match('~\.loc$~', $_SERVER['HTTP_HOST']) ) {
		return true;
	}
	
	return false;
}









function phpmailer_init_dev_encoding( $phpmailer ) {
	
	if( function_exists('is_dev') && is_dev() ) {
		
		$phpmailer->Encoding = 'quoted-printable';
		
	}
	
	return $phpmailer;
}
add_action( 'phpmailer_init', 'phpmailer_init_dev_encoding' );







